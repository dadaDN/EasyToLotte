package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/15.
 */

public class HistoryBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 27
     * table : [{"lotteryid":"1001","lotteryname":"重庆时时彩","issuenum":"20171015-065","rowid":"1","number":"2,1,3,0,6"},{"lotteryid":"1003","lotteryname":"新疆时时彩 ","issuenum":"20171015-41","rowid":"2","number":"8,3,8,0,4"},{"lotteryid":"1004","lotteryname":"纽约30秒彩","issuenum":"20171015-2030","rowid":"3","number":"0,4,1,9,2"},{"lotteryid":"1005","lotteryname":"腾讯分分彩","issuenum":"20171015-1014","rowid":"4","number":"7,1,7,4,0"},{"lotteryid":"1007","lotteryname":"天津时时彩","issuenum":"20171015-047","rowid":"5","number":"6,1,4,3,5"},{"lotteryid":"1010","lotteryname":"韩国1.5分彩","issuenum":"1986079","rowid":"6","number":"5,7,7,5,3"},{"lotteryid":"1012","lotteryname":"新加坡2分彩","issuenum":"2824155","rowid":"7","number":"1,2,6,6,6"},{"lotteryid":"1013","lotteryname":"台湾5分彩","issuenum":"106058379","rowid":"8","number":"0,6,4,8,7"},{"lotteryid":"1015","lotteryname":"菲律宾1.5分","issuenum":"20171015636","rowid":"9","number":"6,8,4,2,4"},{"lotteryid":"1016","lotteryname":"东京1.5分彩","issuenum":"20171015676","rowid":"10","number":"0,3,4,3,9"},{"lotteryid":"1018","lotteryname":"新加坡30秒","issuenum":"20171015-2030","rowid":"11","number":"1,1,8,3,2"},{"lotteryid":"1019","lotteryname":"台湾45秒彩","issuenum":"20171015-1353","rowid":"12","number":"5,8,7,0,5"},{"lotteryid":"1020","lotteryname":"首尔60秒","issuenum":"20171015-1015","rowid":"13","number":"7,0,2,2,8"},{"lotteryid":"2001","lotteryname":"山东11选5","issuenum":"20171015-50","rowid":"14","number":"05,07,09,06,02"},{"lotteryid":"2002","lotteryname":"广东11选5","issuenum":"20171015-47","rowid":"15","number":"05,04,08,09,11"},{"lotteryid":"2003","lotteryname":"上海11选5","issuenum":"20171015-48","rowid":"16","number":"02,08,01,11,09"},{"lotteryid":"2004","lotteryname":"江西11选5","issuenum":"20171015-47","rowid":"17","number":"11,03,06,09,10"},{"lotteryid":"2005","lotteryname":"纽约30秒11选5","issuenum":"20171015-2030","rowid":"18","number":"04,05,03,09,02"},{"lotteryid":"2006","lotteryname":"韩国1.5分11选5","issuenum":"20171015-676","rowid":"19","number":"09,08,02,06,05"},{"lotteryid":"3001","lotteryname":"时时乐","issuenum":"20171015-13","rowid":"20","number":"4,4,6"},{"lotteryid":"3002","lotteryname":"福彩3D","issuenum":"2017280","rowid":"21","number":"6,5,8"},{"lotteryid":"3003","lotteryname":"体彩P3","issuenum":"2017280","rowid":"22","number":"7,9,7"},{"lotteryid":"3004","lotteryname":"韩国1.5分3D","issuenum":"1986158","rowid":"23","number":"6,0,0"},{"lotteryid":"4001","lotteryname":"北京PK10","issuenum":"645355","rowid":"24","number":"03,09,02,07,05,10,06,04,01,08"},{"lotteryid":"4002","lotteryname":"英国30秒赛车","issuenum":"20171015-2030","rowid":"25","number":"04,05,02,06,01,10,03,08,07,09"},{"lotteryid":"4003","lotteryname":"英国60秒赛车","issuenum":"20171015-1015","rowid":"26","number":"09,02,08,03,01,07,06,04,10,05"},{"lotteryid":"4004","lotteryname":"英国120秒赛车","issuenum":"20171015-448","rowid":"27","number":"01,06,02,10,08,09,07,05,04,03"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * lotteryid : 1001
         * lotteryname : 重庆时时彩
         * issuenum : 20171015-065
         * rowid : 1
         * number : 2,1,3,0,6
         */

        private String lotteryid;
        private String lotteryname;
        private String issuenum;
        private String rowid;
        private String number;

        public String getLotteryid() {
            return lotteryid;
        }

        public void setLotteryid(String lotteryid) {
            this.lotteryid = lotteryid;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }

        public String getIssuenum() {
            return issuenum;
        }

        public void setIssuenum(String issuenum) {
            this.issuenum = issuenum;
        }

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}
