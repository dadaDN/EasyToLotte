package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/29.
 */

public class ZHTBean implements Serializable {
    private String lotteryId;
    private String startIssueNum;
    private String totalNums;
    private String totalSums;
    private String isStop;
    private List<TableBean> table;

    public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getStartIssueNum() {
        return startIssueNum;
    }

    public void setStartIssueNum(String startIssueNum) {
        this.startIssueNum = startIssueNum;
    }

    public String getTotalNums() {
        return totalNums;
    }

    public void setTotalNums(String totalNums) {
        this.totalNums = totalNums;
    }

    public String getTotalSums() {
        return totalSums;
    }

    public void setTotalSums(String totalSums) {
        this.totalSums = totalSums;
    }

    public String getIsStop() {
        return isStop;
    }

    public void setIsStop(String isStop) {
        this.isStop = isStop;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        private String LotteryId;
        private String IssueNum;
        private String PlayId;
        private String Price;
        private String Times;
        private String Num;
        private String SingelBouns;
        private String Point;
        private String Balls;
        private String StrPos;
        private String Alltotal;
        private String IsClear;

        public String getIsClear() {
            return IsClear;
        }

        public void setIsClear(String isClear) {
            IsClear = isClear;
        }

        public String getLotteryId() {
            return LotteryId;
        }

        public void setLotteryId(String lotteryId) {
            LotteryId = lotteryId;
        }

        public String getIssueNum() {
            return IssueNum;
        }

        public void setIssueNum(String issueNum) {
            IssueNum = issueNum;
        }

        public String getPlayId() {
            return PlayId;
        }

        public void setPlayId(String playId) {
            PlayId = playId;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            Price = price;
        }

        public String getTimes() {
            return Times;
        }

        public void setTimes(String times) {
            Times = times;
        }

        public String getNum() {
            return Num;
        }

        public void setNum(String num) {
            Num = num;
        }

        public String getSingelBouns() {
            return SingelBouns;
        }

        public void setSingelBouns(String singelBouns) {
            SingelBouns = singelBouns;
        }

        public String getPoint() {
            return Point;
        }

        public void setPoint(String point) {
            Point = point;
        }

        public String getBalls() {
            return Balls;
        }

        public void setBalls(String balls) {
            Balls = balls;
        }

        public String getStrPos() {
            return StrPos;
        }

        public void setStrPos(String strPos) {
            StrPos = strPos;
        }

        public String getAlltotal() {
            return Alltotal;
        }

        public void setAlltotal(String alltotal) {
            Alltotal = alltotal;
        }
    }
}
