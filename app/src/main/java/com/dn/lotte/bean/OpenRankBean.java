package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 开户级别
 * Created by ASUS on 2017/10/11.
 */

public class OpenRankBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 2
     * table : [{"id":"1","name":"代理"},{"id":"0","name":"会员"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1
         * name : 代理
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
