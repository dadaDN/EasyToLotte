package com.dn.lotte.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/19.
 */

public class PurchaseDetailNumberBean {

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * Title : P_5FS
         * TitleName : 五星直选复式
         * Balls : [{"万":"0,1,2,3,4,5,6,7,8,9"},{"千":"0,1,2,3,4,5,6,7,8,9"},{"百":"0,1,2,3,4,5,6,7,8,9"},{"十":"0,1,2,3,4,5,6,7,8,9"},{"个":"0,1,2,3,4,5,6,7,8,9"}]
         */

        private String Title;
        private String TitleName;
        private List<BallsBean> Balls;

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getTitleName() {
            return TitleName;
        }

        public void setTitleName(String TitleName) {
            this.TitleName = TitleName;
        }

        public List<BallsBean> getBalls() {
            return Balls;
        }

        public void setBalls(List<BallsBean> Balls) {
            this.Balls = Balls;
        }

        public static class BallsBean {
            //            /**
//             * 万 : 0,1,2,3,4,5,6,7,8,9
//             * 千 : 0,1,2,3,4,5,6,7,8,9
//             * 百 : 0,1,2,3,4,5,6,7,8,9
//             * 十 : 0,1,2,3,4,5,6,7,8,9
//             * 个 : 0,1,2,3,4,5,6,7,8,9
//             */
            @SerializedName("万")
            private String wan;
            @SerializedName("千")
            private String qian;
            @SerializedName("百")
            private String bai;
            @SerializedName("十")
            private String shi;
            @SerializedName("个")
            private String ge;
            @SerializedName("组120")
            private String z120;
            @SerializedName("二重号")
            private String erCH;
            @SerializedName("单号")
            private String dH;
            @SerializedName("三重号")
            private String sanCH;
            @SerializedName("四重号")
            private String siCH;
            @SerializedName("组24")
            private String z24;
            @SerializedName("和值")
            private String hz;
            @SerializedName("跨度")
            private String kd;
            @SerializedName("组三")
            private String z3;
            @SerializedName("组六")
            private String z6;
            @SerializedName("包胆")
            private String bd;
            @SerializedName("尾数")
            private String ws;
            @SerializedName("特殊")
            private String ts;
            @SerializedName("不定位")
            private String bdw;
            @SerializedName("第1位")
            private String oneW;
            @SerializedName("第2位")
            private String twoW;
            @SerializedName("第3位")
            private String threeW;
            @SerializedName("四位")
            private String fourW;
            @SerializedName("组二")
            private String z2;
            @SerializedName("组选24")
            private String zh24;
            @SerializedName("龙虎和")
            private String lH;
            @SerializedName("冠军")
            private String gj;
            private String 不定胆;
            private String 选四中;
            private String 万位;
            private String 千位;
            private String 百位;
            private String 十位;
            private String 个位;
            private String 胆码;
            private String 拖码;
            private String 前二组选;
            private String 后二组选;
            private String 前三;
            private String 中三;
            private String 后三;
            private String 前三跨度;
            private String 中三跨度;
            private String 后三跨度;
            private String 前二跨度;
            private String 后二跨度;
            private String 任三组三;
            private String 任三组六;
            private String 任二组选;
            private String 前三直选;
            private String 中三直选;
            private String 后三直选;
            private String 前二直选;
            private String 后二直选;
            private String 前三组选;
            private String 中三组选;
            private String 后三组选;
            private String 前三尾数;
            private String 中三尾数;
            private String 后三尾数;
            private String 一帆风顺;
            private String 好事成双;
            private String 三星报喜;
            private String 四季发财;
            private String 万千;
            private String 万百;
            private String 万十;
            private String 万个;
            private String 千百;
            private String 千十;
            private String 千个;
            private String 百十;
            private String 百个;
            private String 十个;
            private String 亚军;
            private String 季军;
            private String 第四名;
            private String 第五名;
            private String 第六名;
            private String 第七名;
            private String 第八名;
            private String 第九名;
            private String 第十名;
            private String 第一名;
            private String 第二名;
            private String 第三名;
            private String 奇偶;
            private String 拖拉机;
            private String 大小;
            private String 前一;
            private String 后一;
            private String 直选;

            private String 第1名;
            private String 第2名;
            private String 第3名;
            private String 第4名;
            private String 第5名;
            private String 第6名;
            private String 第7名;
            private String 第8名;
            private String 第9名;
            private String 第10名;
            private String 一位;
            private String 二位;
            private String 三位;
            private String 选一中;
            private String 选二中;
            private String 一中一;
            private String 二中二;
            private String 三中三;
            private String 四中四;
            private String 五中五;
            private String 六中五;
            private String 七中五;
            private String 八中五;
//
//            public String getWan() {
//                return wan;
//            }
//
//            public void setWan(String wan) {
//                this.wan = wan;
//            }
//
//            public String getQian() {
//                return qian;
//            }
//
//            public void setQian(String qian) {
//                this.qian = qian;
//            }
//
//            public String getBai() {
//                return bai;
//            }
//
//            public void setBai(String bai) {
//                this.bai = bai;
//            }
//
//            public String getShi() {
//                return shi;
//            }
//
//            public void setShi(String shi) {
//                this.shi = shi;
//            }
//
//            public String getGe() {
//                return ge;
//            }
//
//            public void setGe(String ge) {
//                this.ge = ge;
//            }
//
//            public String getZ120() {
//                return z120;
//            }
//
//            public void setZ120(String z120) {
//                this.z120 = z120;
//            }
//
//            public String getErCH() {
//                return erCH;
//            }
//
//            public void setErCH(String erCH) {
//                this.erCH = erCH;
//            }
//
//            public String getdH() {
//                return dH;
//            }
//
//            public void setdH(String dH) {
//                this.dH = dH;
//            }
//
//            public String getSanCH() {
//                return sanCH;
//            }
//
//            public void setSanCH(String sanCH) {
//                this.sanCH = sanCH;
//            }
//
//            public String getSiCH() {
//                return siCH;
//            }
//
//            public void setSiCH(String siCH) {
//                this.siCH = siCH;
//            }
//
//            public String getZ24() {
//                return z24;
//            }
//
//            public void setZ24(String z24) {
//                this.z24 = z24;
//            }
//
//            public String getHz() {
//                return hz;
//            }
//
//            public void setHz(String hz) {
//                this.hz = hz;
//            }
//
//            public String getKd() {
//                return kd;
//            }
//
//            public void setKd(String kd) {
//                this.kd = kd;
//            }
//
//            public String getZ3() {
//                return z3;
//            }
//
//            public void setZ3(String z3) {
//                this.z3 = z3;
//            }
//
//            public String getZ6() {
//                return z6;
//            }
//
//            public void setZ6(String z6) {
//                this.z6 = z6;
//            }
//
//            public String getBd() {
//                return bd;
//            }
//
//            public void setBd(String bd) {
//                this.bd = bd;
//            }
//
//            public String getWs() {
//                return ws;
//            }
//
//            public void setWs(String ws) {
//                this.ws = ws;
//            }
//
//            public String getTs() {
//                return ts;
//            }
//
//            public void setTs(String ts) {
//                this.ts = ts;
//            }
//
//            public String getBdw() {
//                return bdw;
//            }
//
//            public void setBdw(String bdw) {
//                this.bdw = bdw;
//            }
//
//            public String getOneW() {
//                return oneW;
//            }
//
//            public void setOneW(String oneW) {
//                this.oneW = oneW;
//            }
//
//            public String getTwoW() {
//                return twoW;
//            }
//
//            public void setTwoW(String twoW) {
//                this.twoW = twoW;
//            }
//
//            public String getThreeW() {
//                return threeW;
//            }
//
//            public void setThreeW(String threeW) {
//                this.threeW = threeW;
//            }
//
//            public String getFourW() {
//                return fourW;
//            }
//
//            public void setFourW(String fourW) {
//                this.fourW = fourW;
//            }
//
//            public String getZ2() {
//                return z2;
//            }
//
//            public void setZ2(String z2) {
//                this.z2 = z2;
//            }
//
//            public String getZh24() {
//                return zh24;
//            }
//
//            public void setZh24(String zh24) {
//                this.zh24 = zh24;
//            }
//
//            public String getlH() {
//                return lH;
//            }
//
//            public void setlH(String lH) {
//                this.lH = lH;
//            }
        }
    }
}
