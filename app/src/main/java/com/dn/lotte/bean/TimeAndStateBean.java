package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/10/13.
 */

public class TimeAndStateBean implements Serializable {
    private String stime;
    private String otime;
    private String state;

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getOtime() {
        return otime;
    }

    public void setOtime(String otime) {
        this.otime = otime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
