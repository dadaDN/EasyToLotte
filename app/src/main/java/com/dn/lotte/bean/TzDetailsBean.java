package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/11/13.
 */

public class TzDetailsBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"3634","ssid":"B_4969584750267584947","userid":"1001","usermoney":"0.0000","lotteryid":"1001","playid":"10096","playcode":"P_DD","issuenum":"20171112-110","number":"","singlemoney":"1.0000","times":"1.00","num":"1","detail":"","dx":"0","ds":"","total":"1.0000","point":"0.0000","pointmoney":"0.0000","bonus":"19.2000","winnum":"1","winbonus":"9.6000","realget":"8.6000","pos":"","stime":"2017-11-12 23:10:00","stime2":"2017-11-12 23:07:08","isopen":"0","state":"3","isdelay":"0","iswin":"-1","stime9":"","ischeat":"False","zhid":"0","source":"Web端","remark":"","curuserid":"1001","strdetail":",,,8,","total2":"1.0000","point2":"19.2000/0.00%","moshi":"元","username":"lihuan001","playname":"直选定位胆","lotteryname":"重庆时时彩","betmoney":"1.0000","statename":"已中奖","kjnumber":"2,0,6,8,3"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 3634
         * ssid : B_4969584750267584947
         * userid : 1001
         * usermoney : 0.0000
         * lotteryid : 1001
         * playid : 10096
         * playcode : P_DD
         * issuenum : 20171112-110
         * number :
         * singlemoney : 1.0000
         * times : 1.00
         * num : 1
         * detail :
         * dx : 0
         * ds :
         * total : 1.0000
         * point : 0.0000
         * pointmoney : 0.0000
         * bonus : 19.2000
         * winnum : 1
         * winbonus : 9.6000
         * realget : 8.6000
         * pos :
         * stime : 2017-11-12 23:10:00
         * stime2 : 2017-11-12 23:07:08
         * isopen : 0
         * state : 3
         * isdelay : 0
         * iswin : -1
         * stime9 :
         * ischeat : False
         * zhid : 0
         * source : Web端
         * remark :
         * curuserid : 1001
         * strdetail : ,,,8,
         * total2 : 1.0000
         * point2 : 19.2000/0.00%
         * moshi : 元
         * username : lihuan001
         * playname : 直选定位胆
         * lotteryname : 重庆时时彩
         * betmoney : 1.0000
         * statename : 已中奖
         * kjnumber : 2,0,6,8,3
         */

        private String id;
        private String ssid;
        private String userid;
        private String usermoney;
        private String lotteryid;
        private String playid;
        private String playcode;
        private String issuenum;
        private String number;
        private String singlemoney;
        private String times;
        private String num;
        private String detail;
        private String dx;
        private String ds;
        private String total;
        private String point;
        private String pointmoney;
        private String bonus;
        private String winnum;
        private String winbonus;
        private String realget;
        private String pos;
        private String stime;
        private String stime2;
        private String isopen;
        private String state;
        private String isdelay;
        private String iswin;
        private String stime9;
        private String ischeat;
        private String zhid;
        private String source;
        private String remark;
        private String curuserid;
        private String strdetail;
        private String total2;
        private String point2;
        private String moshi;
        private String username;
        private String playname;
        private String lotteryname;
        private String betmoney;
        private String statename;
        private String kjnumber;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsermoney() {
            return usermoney;
        }

        public void setUsermoney(String usermoney) {
            this.usermoney = usermoney;
        }

        public String getLotteryid() {
            return lotteryid;
        }

        public void setLotteryid(String lotteryid) {
            this.lotteryid = lotteryid;
        }

        public String getPlayid() {
            return playid;
        }

        public void setPlayid(String playid) {
            this.playid = playid;
        }

        public String getPlaycode() {
            return playcode;
        }

        public void setPlaycode(String playcode) {
            this.playcode = playcode;
        }

        public String getIssuenum() {
            return issuenum;
        }

        public void setIssuenum(String issuenum) {
            this.issuenum = issuenum;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getSinglemoney() {
            return singlemoney;
        }

        public void setSinglemoney(String singlemoney) {
            this.singlemoney = singlemoney;
        }

        public String getTimes() {
            return times;
        }

        public void setTimes(String times) {
            this.times = times;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getDx() {
            return dx;
        }

        public void setDx(String dx) {
            this.dx = dx;
        }

        public String getDs() {
            return ds;
        }

        public void setDs(String ds) {
            this.ds = ds;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getPointmoney() {
            return pointmoney;
        }

        public void setPointmoney(String pointmoney) {
            this.pointmoney = pointmoney;
        }

        public String getBonus() {
            return bonus;
        }

        public void setBonus(String bonus) {
            this.bonus = bonus;
        }

        public String getWinnum() {
            return winnum;
        }

        public void setWinnum(String winnum) {
            this.winnum = winnum;
        }

        public String getWinbonus() {
            return winbonus;
        }

        public void setWinbonus(String winbonus) {
            this.winbonus = winbonus;
        }

        public String getRealget() {
            return realget;
        }

        public void setRealget(String realget) {
            this.realget = realget;
        }

        public String getPos() {
            return pos;
        }

        public void setPos(String pos) {
            this.pos = pos;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getStime2() {
            return stime2;
        }

        public void setStime2(String stime2) {
            this.stime2 = stime2;
        }

        public String getIsopen() {
            return isopen;
        }

        public void setIsopen(String isopen) {
            this.isopen = isopen;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getIsdelay() {
            return isdelay;
        }

        public void setIsdelay(String isdelay) {
            this.isdelay = isdelay;
        }

        public String getIswin() {
            return iswin;
        }

        public void setIswin(String iswin) {
            this.iswin = iswin;
        }

        public String getStime9() {
            return stime9;
        }

        public void setStime9(String stime9) {
            this.stime9 = stime9;
        }

        public String getIscheat() {
            return ischeat;
        }

        public void setIscheat(String ischeat) {
            this.ischeat = ischeat;
        }

        public String getZhid() {
            return zhid;
        }

        public void setZhid(String zhid) {
            this.zhid = zhid;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCuruserid() {
            return curuserid;
        }

        public void setCuruserid(String curuserid) {
            this.curuserid = curuserid;
        }

        public String getStrdetail() {
            return strdetail;
        }

        public void setStrdetail(String strdetail) {
            this.strdetail = strdetail;
        }

        public String getTotal2() {
            return total2;
        }

        public void setTotal2(String total2) {
            this.total2 = total2;
        }

        public String getPoint2() {
            return point2;
        }

        public void setPoint2(String point2) {
            this.point2 = point2;
        }

        public String getMoshi() {
            return moshi;
        }

        public void setMoshi(String moshi) {
            this.moshi = moshi;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPlayname() {
            return playname;
        }

        public void setPlayname(String playname) {
            this.playname = playname;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }

        public String getBetmoney() {
            return betmoney;
        }

        public void setBetmoney(String betmoney) {
            this.betmoney = betmoney;
        }

        public String getStatename() {
            return statename;
        }

        public void setStatename(String statename) {
            this.statename = statename;
        }

        public String getKjnumber() {
            return kjnumber;
        }

        public void setKjnumber(String kjnumber) {
            this.kjnumber = kjnumber;
        }
    }
}
