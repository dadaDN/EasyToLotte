package com.dn.lotte.bean;

/**
 * Created by DN on 2017/10/19.
 */

public class SelectedBean {
    String name  ;
    boolean isCheched = false;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    int position ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheched() {
        return isCheched;
    }

    public void setCheched(boolean cheched) {
        isCheched = cheched;
    }
}
