package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/13.
 */

public class NoticeBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 5
     * table : [{"no":6,"id":"122","pic":"","color":"","title":"5","stime":"2017-09-29 11:03:00","isindex":"0"},{"no":7,"id":"121","pic":"","color":"","title":"4","stime":"2017-09-29 11:03:00","isindex":"0"},{"no":8,"id":"120","pic":"","color":"","title":"3","stime":"2017-09-29 11:03:00","isindex":"0"},{"no":9,"id":"119","pic":"","color":"","title":"2","stime":"2017-09-29 11:03:00","isindex":"0"},{"no":10,"id":"118","pic":"","color":"","title":"1","stime":"2017-09-29 11:03:00","isindex":"0"}]
     */

    private String result;
    private String returnval;
    private int recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public int getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(int recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean implements Serializable {
        /**
         * no : 6
         * id : 122
         * pic :
         * color :
         * title : 5
         * stime : 2017-09-29 11:03:00
         * isindex : 0
         */

        private int no;
        private String id;
        private String pic;
        private String color;
        private String title;
        private String stime;
        private String isindex;

        public int getNo() {
            return no;
        }

        public void setNo(int no) {
            this.no = no;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getIsindex() {
            return isindex;
        }

        public void setIsindex(String isindex) {
            this.isindex = isindex;
        }
    }
}
