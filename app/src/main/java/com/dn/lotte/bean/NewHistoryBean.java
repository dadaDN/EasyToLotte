package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/18.
 */

public class NewHistoryBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 27
     * table : [{"lotteryid":"1001","lotteryname":"重庆时时彩","issuenum":"20171018-023","rowid":"1","number":"9,1,0,7,4"},{"lotteryid":"1003","lotteryname":"新疆时时彩 ","issuenum":"20171017-96","rowid":"2","number":"4,0,8,6,2"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * lotteryid : 1001
         * lotteryname : 重庆时时彩
         * issuenum : 20171018-023
         * rowid : 1
         * number : 9,1,0,7,4
         */

        private String lotteryid;
        private String lotteryname;
        private String issuenum;
        private String rowid;
        private String number;

        public String getLotteryid() {
            return lotteryid;
        }

        public void setLotteryid(String lotteryid) {
            this.lotteryid = lotteryid;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }

        public String getIssuenum() {
            return issuenum;
        }

        public void setIssuenum(String issuenum) {
            this.issuenum = issuenum;
        }

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }
}
