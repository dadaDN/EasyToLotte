package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class MemberManageBean implements Serializable {


    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"112445","parentid":"104787","parentname":"试玩组","usergroupname":"会员","userlevel":"1900","point":"10.00%","username":"guest_yg8iasan","money":"10000.0000","score":"0.0000","isonline":"0","ip":"36.48.101.197","lasttime":"2017-12-28 04:01:07","istranacc":"0","isgzcontract":"0","isfhcontract":"0"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 112445
         * parentid : 104787
         * parentname : 试玩组
         * usergroupname : 会员
         * userlevel : 1900
         * point : 10.00%
         * username : guest_yg8iasan
         * money : 10000.0000
         * score : 0.0000
         * isonline : 0
         * ip : 36.48.101.197
         * lasttime : 2017-12-28 04:01:07
         * istranacc : 0
         * isgzcontract : 0
         * isfhcontract : 0
         */

        private String id;
        private String parentid;
        private String parentname;
        private String usergroupname;
        private String userlevel;
        private String point;
        private String username;
        private String money;
        private String score;
        private String isonline;
        private String ip;
        private String lasttime;
        private String istranacc;
        private String isgzcontract;
        private String isfhcontract;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParentid() {
            return parentid;
        }

        public void setParentid(String parentid) {
            this.parentid = parentid;
        }

        public String getParentname() {
            return parentname;
        }

        public void setParentname(String parentname) {
            this.parentname = parentname;
        }

        public String getUsergroupname() {
            return usergroupname;
        }

        public void setUsergroupname(String usergroupname) {
            this.usergroupname = usergroupname;
        }

        public String getUserlevel() {
            return userlevel;
        }

        public void setUserlevel(String userlevel) {
            this.userlevel = userlevel;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getIsonline() {
            return isonline;
        }

        public void setIsonline(String isonline) {
            this.isonline = isonline;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getLasttime() {
            return lasttime;
        }

        public void setLasttime(String lasttime) {
            this.lasttime = lasttime;
        }

        public String getIstranacc() {
            return istranacc;
        }

        public void setIstranacc(String istranacc) {
            this.istranacc = istranacc;
        }

        public String getIsgzcontract() {
            return isgzcontract;
        }

        public void setIsgzcontract(String isgzcontract) {
            this.isgzcontract = isgzcontract;
        }

        public String getIsfhcontract() {
            return isfhcontract;
        }

        public void setIsfhcontract(String isfhcontract) {
            this.isfhcontract = isfhcontract;
        }
    }
}
