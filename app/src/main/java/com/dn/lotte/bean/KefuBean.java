package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/11/5.
 */

public class KefuBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 1
     * table : [{"webname":"1980娱乐","webisopen":"0","zhisopen":"0","betisopen":"0","regisopen":"0","webcloseseason":"22222222222222222","lotterymaxbet":"1000000.00","lotterymaxwin":"200000.00","lotterymaxwinfk":"18000.00","lotterymaxlevel":"13.10","clientversion":"v1.0.0.6","serviceurl":"http://www.hao123.com","points":"500"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * webname : 1980娱乐
         * webisopen : 0
         * zhisopen : 0
         * betisopen : 0
         * regisopen : 0
         * webcloseseason : 22222222222222222
         * lotterymaxbet : 1000000.00
         * lotterymaxwin : 200000.00
         * lotterymaxwinfk : 18000.00
         * lotterymaxlevel : 13.10
         * clientversion : v1.0.0.6
         * serviceurl : http://www.hao123.com
         * points : 500
         */

        private String webname;
        private String webisopen;
        private String zhisopen;
        private String betisopen;
        private String regisopen;
        private String webcloseseason;
        private String lotterymaxbet;
        private String lotterymaxwin;
        private String lotterymaxwinfk;
        private String lotterymaxlevel;
        private String clientversion;
        private String serviceurl;
        private String points;

        public String getWebname() {
            return webname;
        }

        public void setWebname(String webname) {
            this.webname = webname;
        }

        public String getWebisopen() {
            return webisopen;
        }

        public void setWebisopen(String webisopen) {
            this.webisopen = webisopen;
        }

        public String getZhisopen() {
            return zhisopen;
        }

        public void setZhisopen(String zhisopen) {
            this.zhisopen = zhisopen;
        }

        public String getBetisopen() {
            return betisopen;
        }

        public void setBetisopen(String betisopen) {
            this.betisopen = betisopen;
        }

        public String getRegisopen() {
            return regisopen;
        }

        public void setRegisopen(String regisopen) {
            this.regisopen = regisopen;
        }

        public String getWebcloseseason() {
            return webcloseseason;
        }

        public void setWebcloseseason(String webcloseseason) {
            this.webcloseseason = webcloseseason;
        }

        public String getLotterymaxbet() {
            return lotterymaxbet;
        }

        public void setLotterymaxbet(String lotterymaxbet) {
            this.lotterymaxbet = lotterymaxbet;
        }

        public String getLotterymaxwin() {
            return lotterymaxwin;
        }

        public void setLotterymaxwin(String lotterymaxwin) {
            this.lotterymaxwin = lotterymaxwin;
        }

        public String getLotterymaxwinfk() {
            return lotterymaxwinfk;
        }

        public void setLotterymaxwinfk(String lotterymaxwinfk) {
            this.lotterymaxwinfk = lotterymaxwinfk;
        }

        public String getLotterymaxlevel() {
            return lotterymaxlevel;
        }

        public void setLotterymaxlevel(String lotterymaxlevel) {
            this.lotterymaxlevel = lotterymaxlevel;
        }

        public String getClientversion() {
            return clientversion;
        }

        public void setClientversion(String clientversion) {
            this.clientversion = clientversion;
        }

        public String getServiceurl() {
            return serviceurl;
        }

        public void setServiceurl(String serviceurl) {
            this.serviceurl = serviceurl;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }
    }
}
