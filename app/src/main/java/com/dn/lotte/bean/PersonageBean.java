package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class PersonageBean implements Serializable {


    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"stime":" 全部合计","charge":"0.0000","getcash":"0.0000","bet":"0.0000","win":"0.0000","point":"0.0000","cancellation":"0.0000","tranaccin":"0.0000","tranaccout":"0.0000","ddmoney":"0.0000","give":"0.0000","other":"0.0000","agentfh":"0.0000","total":"0.0000","admintotal":"0.0000","moneytotal":"0.0000"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * stime :  全部合计
         * charge : 0.0000
         * getcash : 0.0000
         * bet : 0.0000
         * win : 0.0000
         * point : 0.0000
         * cancellation : 0.0000
         * tranaccin : 0.0000
         * tranaccout : 0.0000
         * ddmoney : 0.0000
         * give : 0.0000
         * other : 0.0000
         * agentfh : 0.0000
         * total : 0.0000
         * admintotal : 0.0000
         * moneytotal : 0.0000
         */

        private String stime;
        private String charge;
        private String getcash;
        private String bet;
        private String win;
        private String point;
        private String cancellation;
        private String tranaccin;
        private String tranaccout;
        private String ddmoney;
        private String give;
        private String other;
        private String agentfh;
        private String total;
        private String admintotal;
        private String moneytotal;

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getCharge() {
            return charge;
        }

        public void setCharge(String charge) {
            this.charge = charge;
        }

        public String getGetcash() {
            return getcash;
        }

        public void setGetcash(String getcash) {
            this.getcash = getcash;
        }

        public String getBet() {
            return bet;
        }

        public void setBet(String bet) {
            this.bet = bet;
        }

        public String getWin() {
            return win;
        }

        public void setWin(String win) {
            this.win = win;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getCancellation() {
            return cancellation;
        }

        public void setCancellation(String cancellation) {
            this.cancellation = cancellation;
        }

        public String getTranaccin() {
            return tranaccin;
        }

        public void setTranaccin(String tranaccin) {
            this.tranaccin = tranaccin;
        }

        public String getTranaccout() {
            return tranaccout;
        }

        public void setTranaccout(String tranaccout) {
            this.tranaccout = tranaccout;
        }

        public String getDdmoney() {
            return ddmoney;
        }

        public void setDdmoney(String ddmoney) {
            this.ddmoney = ddmoney;
        }

        public String getGive() {
            return give;
        }

        public void setGive(String give) {
            this.give = give;
        }

        public String getOther() {
            return other;
        }

        public void setOther(String other) {
            this.other = other;
        }

        public String getAgentfh() {
            return agentfh;
        }

        public void setAgentfh(String agentfh) {
            this.agentfh = agentfh;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getAdmintotal() {
            return admintotal;
        }

        public void setAdmintotal(String admintotal) {
            this.admintotal = admintotal;
        }

        public String getMoneytotal() {
            return moneytotal;
        }

        public void setMoneytotal(String moneytotal) {
            this.moneytotal = moneytotal;
        }
    }
}
