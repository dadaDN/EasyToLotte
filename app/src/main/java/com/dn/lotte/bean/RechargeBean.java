package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/10.
 */

public class RechargeBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 3
     * table : [{"id":"3001","type":"1","mername":"A网银","mercode":"171717171","merkey":"1","mincharge":"50.0000","maxcharge":"50000.0000","starttime":"00:00:00","endtime":"23:59:59","total":"1000000.0000","sort":"3","posturl":"http://pay.abc.com/sign.aspx"},{"id":"3002","type":"6","mername":"C微信","mercode":"181818181","merkey":"1","mincharge":"50.0000","maxcharge":"3000.0000","starttime":"00:00:00","endtime":"23:59:59","total":"1000000.0000","sort":"3","posturl":"http://pay.abc.com/sign.aspx"},{"id":"3003","type":"7","mername":"D支付宝","mercode":"191919191","merkey":"1","mincharge":"50.0000","maxcharge":"30000.0000","starttime":"00:00:00","endtime":"23:59:59","total":"1000000.0000","sort":"3","posturl":"http://pay.abc.com/sign.aspx"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 3001
         * type : 1
         * mername : A网银
         * mercode : 171717171
         * merkey : 1
         * mincharge : 50.0000
         * maxcharge : 50000.0000
         * starttime : 00:00:00
         * endtime : 23:59:59
         * total : 1000000.0000
         * sort : 3
         * posturl : http://pay.abc.com/sign.aspx
         */

        private String id;
        private String type;
        private String mername;
        private String mercode;
        private String merkey;
        private String mincharge;
        private String maxcharge;
        private String starttime;
        private String endtime;
        private String total;
        private String sort;
        private String posturl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMername() {
            return mername;
        }

        public void setMername(String mername) {
            this.mername = mername;
        }

        public String getMercode() {
            return mercode;
        }

        public void setMercode(String mercode) {
            this.mercode = mercode;
        }

        public String getMerkey() {
            return merkey;
        }

        public void setMerkey(String merkey) {
            this.merkey = merkey;
        }

        public String getMincharge() {
            return mincharge;
        }

        public void setMincharge(String mincharge) {
            this.mincharge = mincharge;
        }

        public String getMaxcharge() {
            return maxcharge;
        }

        public void setMaxcharge(String maxcharge) {
            this.maxcharge = maxcharge;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getPosturl() {
            return posturl;
        }

        public void setPosturl(String posturl) {
            this.posturl = posturl;
        }
    }
}
