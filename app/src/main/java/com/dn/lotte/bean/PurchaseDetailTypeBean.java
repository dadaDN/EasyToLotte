package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by DN on 2017/10/12.
 */

public class PurchaseDetailTypeBean {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 15
     * table : [{"id":"1001","title":"五星"},{"id":"1002","title":"四星"},{"id":"1003","title":"前三"},{"id":"1004","title":"中三"},{"id":"1005","title":"后三"},{"id":"1006","title":"前二"},{"id":"1014","title":"后二"},{"id":"1007","title":"定位胆"},{"id":"1008","title":"不定胆"},{"id":"1009","title":"任四"},{"id":"1010","title":"任三"},{"id":"1011","title":"任二"},{"id":"1012","title":"大小单双"},{"id":"1013","title":"趣味"},{"id":"1015","title":"龙虎"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1001
         * title : 五星
         */

        private String id;
        private String title;
        boolean isChecked = false;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
