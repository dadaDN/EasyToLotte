package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by DN on 2017/10/7.
 */

public class UserInfoBean {


    /**
     * result : 1
     * returnval : 操作成功
     * userpaypass : 0
     * userpass : 1
     * recordcount : 1
     * table : [{"id":"1001","parentid":"0","usergroup":"2","point":"130.00","username":"lihuan001","truename":"张三","usertruename":"1","useranswer":"1","userbank":"3","ip":"118.193.190.163","lasttime":"2017-10-09 03:09:12","isenable":"0","newip":"124.235.215.217","emailcount":"0"}]
     */

    private String result;
    private String returnval;
    private String userpaypass;
    private String userpass;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getUserpaypass() {
        return userpaypass;
    }

    public void setUserpaypass(String userpaypass) {
        this.userpaypass = userpaypass;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1001
         * parentid : 0
         * usergroup : 2
         * point : 130.00
         * username : lihuan001
         * truename : 张三
         * usertruename : 1
         * useranswer : 1
         * userbank : 3
         * ip : 118.193.190.163
         * lasttime : 2017-10-09 03:09:12
         * isenable : 0
         * newip : 124.235.215.217
         * emailcount : 0
         */

        private String id;
        private String parentid;
        private String usergroup;
        private String point;
        private String username;
        private String truename;
        private String usertruename;
        private String useranswer;
        private String userbank;
        private String ip;
        private String lasttime;
        private String isenable;
        private String newip;
        private String emailcount;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParentid() {
            return parentid;
        }

        public void setParentid(String parentid) {
            this.parentid = parentid;
        }

        public String getUsergroup() {
            return usergroup;
        }

        public void setUsergroup(String usergroup) {
            this.usergroup = usergroup;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTruename() {
            return truename;
        }

        public void setTruename(String truename) {
            this.truename = truename;
        }

        public String getUsertruename() {
            return usertruename;
        }

        public void setUsertruename(String usertruename) {
            this.usertruename = usertruename;
        }

        public String getUseranswer() {
            return useranswer;
        }

        public void setUseranswer(String useranswer) {
            this.useranswer = useranswer;
        }

        public String getUserbank() {
            return userbank;
        }

        public void setUserbank(String userbank) {
            this.userbank = userbank;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getLasttime() {
            return lasttime;
        }

        public void setLasttime(String lasttime) {
            this.lasttime = lasttime;
        }

        public String getIsenable() {
            return isenable;
        }

        public void setIsenable(String isenable) {
            this.isenable = isenable;
        }

        public String getNewip() {
            return newip;
        }

        public void setNewip(String newip) {
            this.newip = newip;
        }

        public String getEmailcount() {
            return emailcount;
        }

        public void setEmailcount(String emailcount) {
            this.emailcount = emailcount;
        }
    }
}
