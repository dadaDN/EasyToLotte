package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencynFHBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 2
     * table : [{"starttime":"2017-09-25 00:00:00","endtime":"2017-10-10 00:00:00","bet":"791922.8790","total":"10745.2889","per":"10.0000","inmoney":"1074.5289","stime":"2017-10-10 05:05:56"},{"starttime":"2017-09-10 00:00:00","endtime":"2017-09-25 00:00:00","bet":"1056675.3930","total":"48889.6334","per":"10.0000","inmoney":"4888.9633","stime":"2017-09-25 05:05:56"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * starttime : 2017-09-25 00:00:00
         * endtime : 2017-10-10 00:00:00
         * bet : 791922.8790
         * total : 10745.2889
         * per : 10.0000
         * inmoney : 1074.5289
         * stime : 2017-10-10 05:05:56
         */

        private String starttime;
        private String endtime;
        private String bet;
        private String total;
        private String per;
        private String inmoney;
        private String stime;

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getBet() {
            return bet;
        }

        public void setBet(String bet) {
            this.bet = bet;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPer() {
            return per;
        }

        public void setPer(String per) {
            this.per = per;
        }

        public String getInmoney() {
            return inmoney;
        }

        public void setInmoney(String inmoney) {
            this.inmoney = inmoney;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }
    }
}
