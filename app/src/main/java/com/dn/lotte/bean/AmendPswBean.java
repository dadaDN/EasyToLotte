package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/10/10.
 */

public class AmendPswBean implements Serializable {

    /**
     * result : 1
     * returnval : 密码修改成功
     */

    private String result;
    private String returnval;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }
}
