package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/12/28.
 */

public class QiyueBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * fhState : 0
     * gzState : 0
     */

    private String result;
    private String returnval;
    private String fhState;
    private String gzState;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getFhState() {
        return fhState;
    }

    public void setFhState(String fhState) {
        this.fhState = fhState;
    }

    public String getGzState() {
        return gzState;
    }

    public void setGzState(String gzState) {
        this.gzState = gzState;
    }
}
