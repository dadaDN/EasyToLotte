package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class RechargBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 5
     * table : [{"rowid":"1","username":"TestApp1","id":"58","ssid":"C_4922310600660483175","userid":"1004","username1":"TestApp1","money":"10100.0000","bankid":"3002","bankname":"泽圣扫码","checkcode":"","inmoney":"10000.00","dzmoney":"10000.00","stime":"2017-10-19 16:48:35","state":"1","statename":"已完成"},{"rowid":"2","username":"TestApp1","id":"55","ssid":"C_4949474357783662607","userid":"1004","username1":"TestApp1","money":"10100.0000","bankid":"888","bankname":"后台充值","checkcode":"","inmoney":"100000.00","dzmoney":"100000.00","stime":"2017-10-19 16:48:35","state":"1","statename":"已完成"},{"rowid":"3","username":"TestApp1","id":"54","ssid":"C_4763186362712192918","userid":"1004","username1":"TestApp1","money":"10100.0000","bankid":"888","bankname":"后台充值","checkcode":"","inmoney":"100000.00","dzmoney":"100000.00","stime":"2017-10-19 16:48:35","state":"1","statename":"已完成"},{"rowid":"4","username":"TestApp1","id":"53","ssid":"C_4874670497238607577","userid":"1004","username1":"TestApp1","money":"10100.0000","bankid":"888","bankname":"后台充值","checkcode":"","inmoney":"100000.00","dzmoney":"100000.00","stime":"2017-10-19 16:48:35","state":"1","statename":"已完成"},{"rowid":"5","username":"TestApp1","id":"51","ssid":"C_4710007315295343654","userid":"1004","username1":"TestApp1","money":"10100.0000","bankid":"888","bankname":"后台充值","checkcode":"","inmoney":"10000.00","dzmoney":"10000.00","stime":"2017-10-19 16:48:35","state":"1","statename":"已完成"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 1
         * username : TestApp1
         * id : 58
         * ssid : C_4922310600660483175
         * userid : 1004
         * username1 : TestApp1
         * money : 10100.0000
         * bankid : 3002
         * bankname : 泽圣扫码
         * checkcode :
         * inmoney : 10000.00
         * dzmoney : 10000.00
         * stime : 2017-10-19 16:48:35
         * state : 1
         * statename : 已完成
         */

        private String rowid;
        private String username;
        private String id;
        private String ssid;
        private String userid;
        private String username1;
        private String money;
        private String bankid;
        private String bankname;
        private String checkcode;
        private String inmoney;
        private String dzmoney;
        private String stime;
        private String state;
        private String statename;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername1() {
            return username1;
        }

        public void setUsername1(String username1) {
            this.username1 = username1;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getBankid() {
            return bankid;
        }

        public void setBankid(String bankid) {
            this.bankid = bankid;
        }

        public String getBankname() {
            return bankname;
        }

        public void setBankname(String bankname) {
            this.bankname = bankname;
        }

        public String getCheckcode() {
            return checkcode;
        }

        public void setCheckcode(String checkcode) {
            this.checkcode = checkcode;
        }

        public String getInmoney() {
            return inmoney;
        }

        public void setInmoney(String inmoney) {
            this.inmoney = inmoney;
        }

        public String getDzmoney() {
            return dzmoney;
        }

        public void setDzmoney(String dzmoney) {
            this.dzmoney = dzmoney;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStatename() {
            return statename;
        }

        public void setStatename(String statename) {
            this.statename = statename;
        }
    }
}
