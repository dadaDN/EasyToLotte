package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/10.
 */

public class SafeQuesstionBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 6
     * table : [{"id":"1","title":"你最想对自己说什么"},{"id":"2","title":"你最大的愿望是什么"},{"id":"3","title":"你的家乡在哪里"},{"id":"4","title":"你的宠物叫什么"},{"id":"5","title":"你最喜欢的地方"},{"id":"6","title":"你的QQ号码是什么"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1
         * title : 你最想对自己说什么
         */

        private String id;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
