package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DN on 2017/10/13.
 */

public class BettingBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 8
     * table : [{"isme":"1001","rowid":"1","id":"688","ssid":"B_5563991062825653583","userid":"1001","username":"lihuan001","usermoney":"0.0000","playid":"10096","playname":"直选定位胆","playcode":"P_DD","lotteryid":"1015","lotteryname":"菲律宾1.5分","issuenum":"20171013012","singlemoney":"1.0000","moshi":"元","times":"1.00","num":"5","dx":"0","ds":"","total":"5.0000","point":"0.0000","pointmoney":"0.0000","bonus":"19.6000","bonus2":"9.800000000","winnum":"0","winbonus":"0.0000","realget":"-5.0000","pos":"","stime":"2017-10-13 00:11:21","stime2":"2017-10-13 00:11:21","shorttime":"10-13 00:11","isopen":"0","state":"2","iswin":"-1","number":"5,6,8,8,8","poslen":"0"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean implements Serializable{
        /**
         * isme : 1001
         * rowid : 1
         * id : 688
         * ssid : B_5563991062825653583
         * userid : 1001
         * username : lihuan001
         * usermoney : 0.0000
         * playid : 10096
         * playname : 直选定位胆
         * playcode : P_DD
         * lotteryid : 1015
         * lotteryname : 菲律宾1.5分
         * issuenum : 20171013012
         * singlemoney : 1.0000
         * moshi : 元
         * times : 1.00
         * num : 5
         * dx : 0
         * ds :
         * total : 5.0000
         * point : 0.0000
         * pointmoney : 0.0000
         * bonus : 19.6000
         * bonus2 : 9.800000000
         * winnum : 0
         * winbonus : 0.0000
         * realget : -5.0000
         * pos :
         * stime : 2017-10-13 00:11:21
         * stime2 : 2017-10-13 00:11:21
         * shorttime : 10-13 00:11
         * isopen : 0
         * state : 2
         * iswin : -1
         * number : 5,6,8,8,8
         * poslen : 0
         */

        private String isme;
        private String rowid;
        private String id;
        private String ssid;
        private String userid;
        private String username;
        private String usermoney;
        private String playid;
        private String playname;
        private String playcode;
        private String lotteryid;
        private String lotteryname;
        private String issuenum;
        private String singlemoney;
        private String moshi;
        private String times;
        private String num;
        private String dx;
        private String ds;
        private String total;
        private String point;
        private String pointmoney;
        private String bonus;
        private String bonus2;
        private String winnum;
        private String winbonus;
        private String realget;
        private String pos;
        private String stime;
        private String stime2;
        private String shorttime;
        private String isopen;
        private String state;
        private String iswin;
        private String number;
        private String poslen;

        public String getIsme() {
            return isme;
        }

        public void setIsme(String isme) {
            this.isme = isme;
        }

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsermoney() {
            return usermoney;
        }

        public void setUsermoney(String usermoney) {
            this.usermoney = usermoney;
        }

        public String getPlayid() {
            return playid;
        }

        public void setPlayid(String playid) {
            this.playid = playid;
        }

        public String getPlayname() {
            return playname;
        }

        public void setPlayname(String playname) {
            this.playname = playname;
        }

        public String getPlaycode() {
            return playcode;
        }

        public void setPlaycode(String playcode) {
            this.playcode = playcode;
        }

        public String getLotteryid() {
            return lotteryid;
        }

        public void setLotteryid(String lotteryid) {
            this.lotteryid = lotteryid;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }

        public String getIssuenum() {
            return issuenum;
        }

        public void setIssuenum(String issuenum) {
            this.issuenum = issuenum;
        }

        public String getSinglemoney() {
            return singlemoney;
        }

        public void setSinglemoney(String singlemoney) {
            this.singlemoney = singlemoney;
        }

        public String getMoshi() {
            return moshi;
        }

        public void setMoshi(String moshi) {
            this.moshi = moshi;
        }

        public String getTimes() {
            return times;
        }

        public void setTimes(String times) {
            this.times = times;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getDx() {
            return dx;
        }

        public void setDx(String dx) {
            this.dx = dx;
        }

        public String getDs() {
            return ds;
        }

        public void setDs(String ds) {
            this.ds = ds;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getPointmoney() {
            return pointmoney;
        }

        public void setPointmoney(String pointmoney) {
            this.pointmoney = pointmoney;
        }

        public String getBonus() {
            return bonus;
        }

        public void setBonus(String bonus) {
            this.bonus = bonus;
        }

        public String getBonus2() {
            return bonus2;
        }

        public void setBonus2(String bonus2) {
            this.bonus2 = bonus2;
        }

        public String getWinnum() {
            return winnum;
        }

        public void setWinnum(String winnum) {
            this.winnum = winnum;
        }

        public String getWinbonus() {
            return winbonus;
        }

        public void setWinbonus(String winbonus) {
            this.winbonus = winbonus;
        }

        public String getRealget() {
            return realget;
        }

        public void setRealget(String realget) {
            this.realget = realget;
        }

        public String getPos() {
            return pos;
        }

        public void setPos(String pos) {
            this.pos = pos;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getStime2() {
            return stime2;
        }

        public void setStime2(String stime2) {
            this.stime2 = stime2;
        }

        public String getShorttime() {
            return shorttime;
        }

        public void setShorttime(String shorttime) {
            this.shorttime = shorttime;
        }

        public String getIsopen() {
            return isopen;
        }

        public void setIsopen(String isopen) {
            this.isopen = isopen;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getIswin() {
            return iswin;
        }

        public void setIswin(String iswin) {
            this.iswin = iswin;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getPoslen() {
            return poslen;
        }

        public void setPoslen(String poslen) {
            this.poslen = poslen;
        }
    }
}
