package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/12/4.
 */

public class TimeBean implements Serializable {

    /**
     * result : 1
     * UserName : lihuan001
     * Money : 9932.3317
     * Email : 0
     */

    private String result;
    private String UserName;
    private String Money;
    private String Email;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getMoney() {
        return Money;
    }

    public void setMoney(String Money) {
        this.Money = Money;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
}
