package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/12/28.
 */

public class BeforeqiyuBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"1496","userid":"106148","username":"zhen001","isused":"0","stime":"2017-12-28 05:51:29","stime2":""}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1496
         * userid : 106148
         * username : zhen001
         * isused : 0
         * stime : 2017-12-28 05:51:29
         * stime2 :
         */

        private String id;
        private String userid;
        private String username;
        private String isused;
        private String stime;
        private String stime2;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getIsused() {
            return isused;
        }

        public void setIsused(String isused) {
            this.isused = isused;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getStime2() {
            return stime2;
        }

        public void setStime2(String stime2) {
            this.stime2 = stime2;
        }
    }
}
