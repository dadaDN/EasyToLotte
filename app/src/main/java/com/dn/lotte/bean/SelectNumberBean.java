package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/25.
 */

public class SelectNumberBean implements Serializable {

    private List<SelectNumberBean.NumberBean> table;

    public List<SelectNumberBean.NumberBean> getTable() {
        return table;
    }

    public void setTable(List<SelectNumberBean.NumberBean> table) {
        this.table = table;
    }


    public static class NumberBean implements Serializable {

        String LotteryId;
        String PlayId;
        String Price;
        String Times;
        String Num;
        String SingelBouns;
        String Point;
        String Balls;
        String StrPos;
        String Alltotal;
        String mode="";

        public String getIsClear() {
            return IsClear;
        }

        public void setIsClear(String isClear) {
            IsClear = isClear;
        }

        String IsClear;
        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        String model;
        public String getFd() {
            return fd;
        }

        public void setFd(String fd) {
            this.fd = fd;
        }

        String fd;
        public String getPos() {
            return pos;
        }

        public void setPos(String pos) {
            this.pos = pos;
        }

        String pos;
        public String getmMinPos() {
            return mMinPos;
        }

        public void setmMinPos(String mMinPos) {
            this.mMinPos = mMinPos;
        }

        String mMinPos;
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        String name;

        public String getLotteryId() {
            return LotteryId;
        }

        public void setLotteryId(String lotteryId) {
            LotteryId = lotteryId;
        }

        public String getPlayId() {
            return PlayId;
        }

        public void setPlayId(String playId) {
            PlayId = playId;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            Price = price;
        }

        public String getTimes() {
            return Times;
        }

        public void setTimes(String times) {
            Times = times;
        }

        public String getNum() {
            return Num;
        }

        public void setNum(String num) {
            Num = num;
        }

        public String getSingelBouns() {
            return SingelBouns;
        }

        public void setSingelBouns(String singelBouns) {
            SingelBouns = singelBouns;
        }

        public String getPoint() {
            return Point;
        }

        public void setPoint(String point) {
            Point = point;
        }

        public String getBalls() {
            return Balls;
        }

        public void setBalls(String balls) {
            Balls = balls;
        }

        public String getStrPos() {
            return StrPos;
        }

        public void setStrPos(String strPos) {
            StrPos = strPos;
        }

        public String getAlltotal() {
            return Alltotal;
        }

        public void setAlltotal(String alltotal) {
            Alltotal = alltotal;
        }


    }
}
