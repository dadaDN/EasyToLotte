package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class TransFerrecordBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"rowid":"1","id":"3","ssid":"111111111","type":"0","userid":"1001","touserid":"1005","moneychange":"1.0000","stime":"2017-10-18 00:00:00","remark":"","username":"lihuan001","tousername":"TestApp2"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 1
         * id : 3
         * ssid : 111111111
         * type : 0
         * userid : 1001
         * touserid : 1005
         * moneychange : 1.0000
         * stime : 2017-10-18 00:00:00
         * remark :
         * username : lihuan001
         * tousername : TestApp2
         */

        private String rowid;
        private String id;
        private String ssid;
        private String type;
        private String userid;
        private String touserid;
        private String moneychange;
        private String stime;
        private String remark;
        private String username;
        private String tousername;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getTouserid() {
            return touserid;
        }

        public void setTouserid(String touserid) {
            this.touserid = touserid;
        }

        public String getMoneychange() {
            return moneychange;
        }

        public void setMoneychange(String moneychange) {
            this.moneychange = moneychange;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTousername() {
            return tousername;
        }

        public void setTousername(String tousername) {
            this.tousername = tousername;
        }
    }
}
