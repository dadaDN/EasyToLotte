package com.dn.lotte.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/24.
 */

public class TestBean {

    private List<五星直选复式Bean> 五星直选复式;

    public List<五星直选复式Bean> get五星直选复式() {
        return 五星直选复式;
    }

    public void set五星直选复式(List<五星直选复式Bean> 五星直选复式) {
        this.五星直选复式 = 五星直选复式;
    }

    public static class 五星直选复式Bean {
        /**
         * 0 : 111
         * 1 : 111
         * 2 : 111
         */

        @SerializedName("0")
        private String _$0;
        @SerializedName("1")
        private String _$1;
        @SerializedName("2")
        private String _$2;

        public String get_$0() {
            return _$0;
        }

        public void set_$0(String _$0) {
            this._$0 = _$0;
        }

        public String get_$1() {
            return _$1;
        }

        public void set_$1(String _$1) {
            this._$1 = _$1;
        }

        public String get_$2() {
            return _$2;
        }

        public void set_$2(String _$2) {
            this._$2 = _$2;
        }
    }
}
