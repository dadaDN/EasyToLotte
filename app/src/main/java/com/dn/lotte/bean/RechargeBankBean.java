package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/10.
 */

public class RechargeBankBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 2
     * table : [{"logo":"alipay","code":"alipay","bank":"支付宝","payname":"杨昊翰","payaccount":"6214837143721318"},{"logo":"bank_zsyh","code":"","bank":"招商银行","payname":"杨昊翰","payaccount":"6214837143721318"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * logo : alipay
         * code : alipay
         * bank : 支付宝
         * payname : 杨昊翰
         * payaccount : 6214837143721318
         */

        private String logo;
        private String code;
        private String bank;
        private String payname;
        private String payaccount;

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getBank() {
            return bank;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getPayname() {
            return payname;
        }

        public void setPayname(String payname) {
            this.payname = payname;
        }

        public String getPayaccount() {
            return payaccount;
        }

        public void setPayaccount(String payaccount) {
            this.payaccount = payaccount;
        }
    }
}
