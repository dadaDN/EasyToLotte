package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/12/28.
 */

public class DayratioBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * groupid : 3
     * prompt : 您的工资契约如下
     * recordcount : 1
     * table : [{"id":"1731","minmoney":"0.00","money":"0.30","isused":"0","stime":"2017-12-28 07:11:21"}]
     */

    private String result;
    private String returnval;
    private String groupid;
    private String prompt;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1731
         * minmoney : 0.00
         * money : 0.30
         * isused : 0
         * stime : 2017-12-28 07:11:21
         */

        private String id;
        private String minmoney;
        private String money;
        private String isused;
        private String stime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMinmoney() {
            return minmoney;
        }

        public void setMinmoney(String minmoney) {
            this.minmoney = minmoney;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getIsused() {
            return isused;
        }

        public void setIsused(String isused) {
            this.isused = isused;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }
    }
}
