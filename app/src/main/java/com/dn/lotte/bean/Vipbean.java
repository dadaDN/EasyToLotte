package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by DN on 2017/10/9.
 */

public class Vipbean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"1001","usergroup":"2","groupname":"直属","username":"lihuan001","money":"9995.7433","point":"13.00%","istruename":"1","ismobile":"1","isemail":"1","isanswer":"1","logintime":"2017-10-09 21:18:26","address":"北京市北京北大方正宽带网络科技有限公司","ip":"1.14.217.118","emailcount":"0","notice":"0","qq":"","email":"111@qq.com","mobile":"13000000000","truename":"张三"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1001
         * usergroup : 2
         * groupname : 直属
         * username : lihuan001
         * money : 9995.7433
         * point : 13.00%
         * istruename : 1
         * ismobile : 1
         * isemail : 1
         * isanswer : 1
         * logintime : 2017-10-09 21:18:26
         * address : 北京市北京北大方正宽带网络科技有限公司
         * ip : 1.14.217.118
         * emailcount : 0
         * notice : 0
         * qq :
         * email : 111@qq.com
         * mobile : 13000000000
         * truename : 张三
         */

        private String id;
        private String usergroup;
        private String groupname;
        private String username;
        private String money;
        private String point;
        private String istruename;
        private String ismobile;
        private String isemail;
        private String isanswer;
        private String logintime;
        private String address;
        private String ip;
        private String emailcount;
        private String notice;
        private String qq;
        private String email;
        private String mobile;
        private String truename;
        private String isbank;

        public String getIsbank() {
            return isbank;
        }

        public void setIsbank(String isbank) {
            this.isbank = isbank;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsergroup() {
            return usergroup;
        }

        public void setUsergroup(String usergroup) {
            this.usergroup = usergroup;
        }

        public String getGroupname() {
            return groupname;
        }

        public void setGroupname(String groupname) {
            this.groupname = groupname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getIstruename() {
            return istruename;
        }

        public void setIstruename(String istruename) {
            this.istruename = istruename;
        }

        public String getIsmobile() {
            return ismobile;
        }

        public void setIsmobile(String ismobile) {
            this.ismobile = ismobile;
        }

        public String getIsemail() {
            return isemail;
        }

        public void setIsemail(String isemail) {
            this.isemail = isemail;
        }

        public String getIsanswer() {
            return isanswer;
        }

        public void setIsanswer(String isanswer) {
            this.isanswer = isanswer;
        }

        public String getLogintime() {
            return logintime;
        }

        public void setLogintime(String logintime) {
            this.logintime = logintime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getEmailcount() {
            return emailcount;
        }

        public void setEmailcount(String emailcount) {
            this.emailcount = emailcount;
        }

        public String getNotice() {
            return notice;
        }

        public void setNotice(String notice) {
            this.notice = notice;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getTruename() {
            return truename;
        }

        public void setTruename(String truename) {
            this.truename = truename;
        }
    }
}
