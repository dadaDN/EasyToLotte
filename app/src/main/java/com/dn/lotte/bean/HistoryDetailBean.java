package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/18.
 */

public class HistoryDetailBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 120
     * table : [{"id":"939887","type":"1001","title":"20171018-029","number":"3,2,7,2,7","numberall":"3,2,7,2,7","total":"21","dx":"0","ds":"0","opentime":"2017-10-18 10:50:52","stime":"2017-10-18 10:50:56","flag":"0","flag2":"0","isfill":"1","lotteryname":"重庆时时彩"},{"id":"939675","type":"1001","title":"20171018-028","number":"0,6,4,3,1","numberall":"0,6,4,3,1","total":"14","dx":"0","ds":"0","opentime":"2017-10-18 10:40:47","stime":"2017-10-18 10:40:51","flag":"0","flag2":"0","isfill":"1","lotteryname":"重庆时时彩"},{"id":"939456","type":"1001","title":"20171018-027","number":"7,7,8,5,3","numberall":"7,7,8,5,3","total":"30","dx":"0","ds":"0","opentime":"2017-10-18 10:30:49","stime":"2017-10-18 10:30:51","flag":"0","flag2":"0","isfill":"1","lotteryname":"重庆时时彩"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 939887
         * type : 1001
         * title : 20171018-029
         * number : 3,2,7,2,7
         * numberall : 3,2,7,2,7
         * total : 21
         * dx : 0
         * ds : 0
         * opentime : 2017-10-18 10:50:52
         * stime : 2017-10-18 10:50:56
         * flag : 0
         * flag2 : 0
         * isfill : 1
         * lotteryname : 重庆时时彩
         */

        private String id;
        private String type;
        private String title;
        private String number;
        private String numberall;
        private String total;
        private String dx;
        private String ds;
        private String opentime;
        private String stime;
        private String flag;
        private String flag2;
        private String isfill;
        private String lotteryname;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getNumberall() {
            return numberall;
        }

        public void setNumberall(String numberall) {
            this.numberall = numberall;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getDx() {
            return dx;
        }

        public void setDx(String dx) {
            this.dx = dx;
        }

        public String getDs() {
            return ds;
        }

        public void setDs(String ds) {
            this.ds = ds;
        }

        public String getOpentime() {
            return opentime;
        }

        public void setOpentime(String opentime) {
            this.opentime = opentime;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getFlag2() {
            return flag2;
        }

        public void setFlag2(String flag2) {
            this.flag2 = flag2;
        }

        public String getIsfill() {
            return isfill;
        }

        public void setIsfill(String isfill) {
            this.isfill = isfill;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }
    }
}
