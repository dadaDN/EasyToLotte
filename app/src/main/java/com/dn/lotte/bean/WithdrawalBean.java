package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class WithdrawalBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 6
     * table : [{"rowid":"1","sxf":"0.0000","id":"28","ssid":"G_5229656270588026175","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"17","paymethod":"11","paybank":"广发银行","payname":"张三","tpayname":"张**","payaccount":"243545234234","tpayaccount":"************4234","paybankaddress":"北京","bankcode":"CGB","cashmoney":"100.0000","stime":"2017-10-12 23:54:57","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"},{"rowid":"2","sxf":"0.0000","id":"27","ssid":"G_5523359995359952230","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"3","paymethod":"7","paybank":"中国银行","payname":"张三","tpayname":"张**","payaccount":"987654321","tpayaccount":"************4321","paybankaddress":"杭州","bankcode":"BOC","cashmoney":"100.0000","stime":"2017-10-12 23:50:06","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"},{"rowid":"3","sxf":"0.0000","id":"26","ssid":"G_5468409333609819526","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"18","paymethod":"9","paybank":"中国邮政储蓄银行","payname":"张三","tpayname":"张**","payaccount":"1234567","tpayaccount":"************4567","paybankaddress":"上海","bankcode":"PSBC","cashmoney":"100.0000","stime":"2017-10-12 23:32:05","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"},{"rowid":"4","sxf":"0.0000","id":"24","ssid":"G_5618459901938611415","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"18","paymethod":"9","paybank":"中国邮政储蓄银行","payname":"张三","tpayname":"张**","payaccount":"1234567","tpayaccount":"************4567","paybankaddress":"上海","bankcode":"PSBC","cashmoney":"100.0000","stime":"2017-10-11 15:35:11","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"},{"rowid":"5","sxf":"0.0000","id":"23","ssid":"G_4763928861609872726","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"18","paymethod":"9","paybank":"中国邮政储蓄银行","payname":"张三","tpayname":"张**","payaccount":"1234567","tpayaccount":"************4567","paybankaddress":"上海","bankcode":"PSBC","cashmoney":"100.0000","stime":"2017-10-11 15:34:57","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"},{"rowid":"6","sxf":"0.0000","id":"22","ssid":"G_5590762694478568981","userid":"1001","username":"lihuan001","money":"7306.8063","bankid":"18","paymethod":"9","paybank":"中国邮政储蓄银行","payname":"张三","tpayname":"张**","payaccount":"1234567","tpayaccount":"************4567","paybankaddress":"上海","bankcode":"PSBC","cashmoney":"100.0000","stime":"2017-10-11 15:34:55","state":"0","statename":"正在处理","stime2":"","msg":"","bet":"218.0000"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 1
         * sxf : 0.0000
         * id : 28
         * ssid : G_5229656270588026175
         * userid : 1001
         * username : lihuan001
         * money : 7306.8063
         * bankid : 17
         * paymethod : 11
         * paybank : 广发银行
         * payname : 张三
         * tpayname : 张**
         * payaccount : 243545234234
         * tpayaccount : ************4234
         * paybankaddress : 北京
         * bankcode : CGB
         * cashmoney : 100.0000
         * stime : 2017-10-12 23:54:57
         * state : 0
         * statename : 正在处理
         * stime2 :
         * msg :
         * bet : 218.0000
         */

        private String rowid;
        private String sxf;
        private String id;
        private String ssid;
        private String userid;
        private String username;
        private String money;
        private String bankid;
        private String paymethod;
        private String paybank;
        private String payname;
        private String tpayname;
        private String payaccount;
        private String tpayaccount;
        private String paybankaddress;
        private String bankcode;
        private String cashmoney;
        private String stime;
        private String state;
        private String statename;
        private String stime2;
        private String msg;
        private String bet;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getSxf() {
            return sxf;
        }

        public void setSxf(String sxf) {
            this.sxf = sxf;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getBankid() {
            return bankid;
        }

        public void setBankid(String bankid) {
            this.bankid = bankid;
        }

        public String getPaymethod() {
            return paymethod;
        }

        public void setPaymethod(String paymethod) {
            this.paymethod = paymethod;
        }

        public String getPaybank() {
            return paybank;
        }

        public void setPaybank(String paybank) {
            this.paybank = paybank;
        }

        public String getPayname() {
            return payname;
        }

        public void setPayname(String payname) {
            this.payname = payname;
        }

        public String getTpayname() {
            return tpayname;
        }

        public void setTpayname(String tpayname) {
            this.tpayname = tpayname;
        }

        public String getPayaccount() {
            return payaccount;
        }

        public void setPayaccount(String payaccount) {
            this.payaccount = payaccount;
        }

        public String getTpayaccount() {
            return tpayaccount;
        }

        public void setTpayaccount(String tpayaccount) {
            this.tpayaccount = tpayaccount;
        }

        public String getPaybankaddress() {
            return paybankaddress;
        }

        public void setPaybankaddress(String paybankaddress) {
            this.paybankaddress = paybankaddress;
        }

        public String getBankcode() {
            return bankcode;
        }

        public void setBankcode(String bankcode) {
            this.bankcode = bankcode;
        }

        public String getCashmoney() {
            return cashmoney;
        }

        public void setCashmoney(String cashmoney) {
            this.cashmoney = cashmoney;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStatename() {
            return statename;
        }

        public void setStatename(String statename) {
            this.statename = statename;
        }

        public String getStime2() {
            return stime2;
        }

        public void setStime2(String stime2) {
            this.stime2 = stime2;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getBet() {
            return bet;
        }

        public void setBet(String bet) {
            this.bet = bet;
        }
    }
}
