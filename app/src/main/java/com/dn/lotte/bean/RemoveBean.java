package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/10/15.
 */

public class RemoveBean implements Serializable {

    /**
     * result : 0
     * returnval : 撤单失败!
     */

    private String result;
    private String returnval;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }
}
