package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/12/1.
 */

public class DiscountitemBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 1
     * table : [{"id":"1","typecode":"每月10号,25号自动派发","starttime":"2014-03-01 00:00:00","endtime":"2018-03-18 00:00:00","title":"前台名称","content":"活动介绍","remark":"活动规则"}]
     */

    private String result;
    private String returnval;
    private int recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public int getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(int recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1
         * typecode : 每月10号,25号自动派发
         * starttime : 2014-03-01 00:00:00
         * endtime : 2018-03-18 00:00:00
         * title : 前台名称
         * content : 活动介绍
         * remark : 活动规则
         */

        private String id;
        private String typecode;
        private String starttime;
        private String endtime;
        private String title;
        private String content;
        private String remark;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTypecode() {
            return typecode;
        }

        public void setTypecode(String typecode) {
            this.typecode = typecode;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
