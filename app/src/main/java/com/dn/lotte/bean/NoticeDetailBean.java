package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/13.
 */

public class NoticeDetailBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"120","pic":"","color":"","title":"3","content":"1","stime":"2017-09-29 11:03:00","isused":"1","isindex":"0"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 120
         * pic :
         * color :
         * title : 3
         * content : 1
         * stime : 2017-09-29 11:03:00
         * isused : 1
         * isindex : 0
         */

        private String id;
        private String pic;
        private String color;
        private String title;
        private String content;
        private String stime;
        private String isused;
        private String isindex;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getIsused() {
            return isused;
        }

        public void setIsused(String isused) {
            this.isused = isused;
        }

        public String getIsindex() {
            return isindex;
        }

        public void setIsindex(String isindex) {
            this.isindex = isindex;
        }
    }
}
