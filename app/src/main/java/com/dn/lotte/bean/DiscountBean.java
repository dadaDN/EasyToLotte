package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/12/1.
 */

public class DiscountBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 4
     * table : [{"id":"1","title":"前台名称"}]
     */

    private String result;
    private String returnval;
    private int recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public int getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(int recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1
         * title : 前台名称
         */

        private String id;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
