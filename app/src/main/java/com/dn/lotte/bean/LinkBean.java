package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 链接开户列表
 * Created by ASUS on 2017/12/11.
 */

public class LinkBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 3
     * table : [{"rowid":"1","id":"5","userid":"1000","point":"120.00","yxtime":"5","times":"2","url":"http://app.emccapi.com/register?u=rrc8Rg1OMSup3A6jkqEViA@2@2","stime":"2017-12-11 03:26:39"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 1
         * id : 5
         * userid : 1000
         * point : 120.00
         * yxtime : 5
         * times : 2
         * url : http://app.emccapi.com/register?u=rrc8Rg1OMSup3A6jkqEViA@2@2
         * stime : 2017-12-11 03:26:39
         */

        private String rowid;
        private String id;
        private String userid;
        private String point;
        private String yxtime;
        private String times;
        private String url;
        private String stime;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getYxtime() {
            return yxtime;
        }

        public void setYxtime(String yxtime) {
            this.yxtime = yxtime;
        }

        public String getTimes() {
            return times;
        }

        public void setTimes(String times) {
            this.times = times;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }
    }
}
