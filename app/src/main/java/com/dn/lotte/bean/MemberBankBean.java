package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 会员银行列表
 * Created by ASUS on 2017/10/11.
 */

public class MemberBankBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 5
     * table : [{"id":"18","userid":"1001","paymethod":"9","code":"PSBC","paybank":"中国邮政储蓄银行","payaccount":"1234567","payname":"张三","paybankaddress":"上海","addtime":"2017-10-10 01:51:16","islock":"1","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00","question":"你是哪里的人？","answer":"我来自伟大的中国","money":"8978.7013","txcs":"0","txje":"0.0000","charge":"9900.0000","bet":"5198.5140"},{"id":"17","userid":"1001","paymethod":"11","code":"CGB","paybank":"广发银行","payaccount":"243545234234","payname":"张三","paybankaddress":"北京","addtime":"2017-10-10 01:48:32","islock":"1","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00","question":"你是哪里的人？","answer":"我来自伟大的中国","money":"8978.7013","txcs":"0","txje":"0.0000","charge":"9900.0000","bet":"5198.5140"},{"id":"3","userid":"1001","paymethod":"7","code":"BOC","paybank":"中国银行","payaccount":"987654321","payname":"张三","paybankaddress":"杭州","addtime":"2017-09-06 18:18:29","islock":"1","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00","question":"你是哪里的人？","answer":"我来自伟大的中国","money":"8978.7013","txcs":"0","txje":"0.0000","charge":"9900.0000","bet":"5198.5140"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 18
         * userid : 1001
         * paymethod : 9
         * code : PSBC
         * paybank : 中国邮政储蓄银行
         * payaccount : 1234567
         * payname : 张三
         * paybankaddress : 上海
         * addtime : 2017-10-10 01:51:16
         * islock : 1
         * mincharge : 100.0000
         * maxcharge : 49999.0000
         * starttime : 12:00:00
         * endtime : 02:00:00
         * maxgetcash : 99
         * bindtime : 0
         * betpercheck : 30.00
         * question : 你是哪里的人？
         * answer : 我来自伟大的中国
         * money : 8978.7013
         * txcs : 0
         * txje : 0.0000
         * charge : 9900.0000
         * bet : 5198.5140
         */

        private String id;
        private String userid;
        private String paymethod;
        private String code;
        private String paybank;
        private String payaccount;
        private String payname;
        private String paybankaddress;
        private String addtime;
        private String islock;
        private String mincharge;
        private String maxcharge;
        private String starttime;
        private String endtime;
        private String maxgetcash;
        private String bindtime;
        private String betpercheck;
        private String question;
        private String answer;
        private String money;
        private String txcs;
        private String txje;
        private String charge;
        private String bet;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getPaymethod() {
            return paymethod;
        }

        public void setPaymethod(String paymethod) {
            this.paymethod = paymethod;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPaybank() {
            return paybank;
        }

        public void setPaybank(String paybank) {
            this.paybank = paybank;
        }

        public String getPayaccount() {
            return payaccount;
        }

        public void setPayaccount(String payaccount) {
            this.payaccount = payaccount;
        }

        public String getPayname() {
            return payname;
        }

        public void setPayname(String payname) {
            this.payname = payname;
        }

        public String getPaybankaddress() {
            return paybankaddress;
        }

        public void setPaybankaddress(String paybankaddress) {
            this.paybankaddress = paybankaddress;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public String getIslock() {
            return islock;
        }

        public void setIslock(String islock) {
            this.islock = islock;
        }

        public String getMincharge() {
            return mincharge;
        }

        public void setMincharge(String mincharge) {
            this.mincharge = mincharge;
        }

        public String getMaxcharge() {
            return maxcharge;
        }

        public void setMaxcharge(String maxcharge) {
            this.maxcharge = maxcharge;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getMaxgetcash() {
            return maxgetcash;
        }

        public void setMaxgetcash(String maxgetcash) {
            this.maxgetcash = maxgetcash;
        }

        public String getBindtime() {
            return bindtime;
        }

        public void setBindtime(String bindtime) {
            this.bindtime = bindtime;
        }

        public String getBetpercheck() {
            return betpercheck;
        }

        public void setBetpercheck(String betpercheck) {
            this.betpercheck = betpercheck;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getTxcs() {
            return txcs;
        }

        public void setTxcs(String txcs) {
            this.txcs = txcs;
        }

        public String getTxje() {
            return txje;
        }

        public void setTxje(String txje) {
            this.txje = txje;
        }

        public String getCharge() {
            return charge;
        }

        public void setCharge(String charge) {
            this.charge = charge;
        }

        public String getBet() {
            return bet;
        }

        public void setBet(String bet) {
            this.bet = bet;
        }
    }
}
