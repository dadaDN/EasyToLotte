package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 丁宁
 * on 2017/10/9.
 */

public class PurchaseBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 30
     * table : [{"id":"1001","title":"重庆时时彩","code":"cqssc","isopen":"0","sort":"1","indextype":"2"},{"id":"1015","title":"菲律宾1.5分","code":"flb90m","isopen":"0","sort":"1","indextype":"1"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1001
         * title : 重庆时时彩
         * code : cqssc
         * isopen : 0
         * sort : 1
         * indextype : 2
         */

        private String id;
        private String title;
        private String code;
        private String isopen;
        private String sort;
        private String indextype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getIsopen() {
            return isopen;
        }

        public void setIsopen(String isopen) {
            this.isopen = isopen;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public String getIndextype() {
            return indextype;
        }

        public void setIndextype(String indextype) {
            this.indextype = indextype;
        }
    }
}
