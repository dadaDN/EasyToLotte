package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/12/28.
 */

public class QiyueBiBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * minPer : 0
     * maxPer : 0
     */

    private String result;
    private String returnval;
    private String minPer;
    private String maxPer;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getMinPer() {
        return minPer;
    }

    public void setMinPer(String minPer) {
        this.minPer = minPer;
    }

    public String getMaxPer() {
        return maxPer;
    }

    public void setMaxPer(String maxPer) {
        this.maxPer = maxPer;
    }
}
