package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class RecordStateBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 15
     * table : [{"id":"","title":"所有类型"},{"id":"1","title":"账号充值"},{"id":"2","title":"账号取款"},{"id":"3","title":"投注扣款"},{"id":"4","title":"游戏返点"},{"id":"5","title":"奖金派送"},{"id":"6","title":"撤单返款"},{"id":"7","title":"转出资金"},{"id":"8","title":"转入资金"},{"id":"9","title":"活动礼金"},{"id":"10","title":"其他"},{"id":"11","title":"积分兑换"},{"id":"12","title":"分红"}]
     */

    private String result;
    private String returnval;
    private int recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public int getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(int recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id :
         * title : 所有类型
         */

        private String id;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
