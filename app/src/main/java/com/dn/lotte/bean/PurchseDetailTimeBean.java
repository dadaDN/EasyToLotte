package com.dn.lotte.bean;

/**
 * Created by 丁宁
 * on 2017/10/30.
 */

public class PurchseDetailTimeBean {

    /**
     * name : 重庆时时彩
     * lotteryid : 1001
     * ordertime : 266
     * closetime : 30
     * nestsn : 20171030-060
     * opennum : 59
     * cursn : 20171030-059
     */

    private String name;
    private String lotteryid;
    private String ordertime;
    private String closetime;
    private String nestsn;
    private String opennum;
    private String cursn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLotteryid() {
        return lotteryid;
    }

    public void setLotteryid(String lotteryid) {
        this.lotteryid = lotteryid;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public String getClosetime() {
        return closetime;
    }

    public void setClosetime(String closetime) {
        this.closetime = closetime;
    }

    public String getNestsn() {
        return nestsn;
    }

    public void setNestsn(String nestsn) {
        this.nestsn = nestsn;
    }

    public String getOpennum() {
        return opennum;
    }

    public void setOpennum(String opennum) {
        this.opennum = opennum;
    }

    public String getCursn() {
        return cursn;
    }

    public void setCursn(String cursn) {
        this.cursn = cursn;
    }
}
