package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/12/28.
 */

public class FenpeijsonBean implements Serializable {
    private String userid;
    private String money;
    private String per;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }
}
