package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by DN on 2017/10/12.
 */

public class PurchaseDetailGameBean {


    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 131
     * table : [{"id":"10001","radio":"1001","type":"1","title0":"五星直选","title":"五星复式","title2":"P_5FS","titlename":"五星直选复式","remark":"从万、千、百、十、个中各选一个号码组成一注。","example":"投注方案：23456；开奖号码：23456，即中五星直选。","help":"从万位、千位、百位、十位、个位中选择一个5位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。","maxbonus":"196000.00","minbonus":"170000.00","posbonus":"100.000000","maxbonus2":"0.00","minbonus2":"0.00","posbonus2":"0.000000","wznum":"0","minnum":"3500","maxnum":"80000","sort":"1"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 10001
         * radio : 1001
         * type : 1
         * title0 : 五星直选
         * title : 五星复式
         * title2 : P_5FS
         * titlename : 五星直选复式
         * remark : 从万、千、百、十、个中各选一个号码组成一注。
         * example : 投注方案：23456；开奖号码：23456，即中五星直选。
         * help : 从万位、千位、百位、十位、个位中选择一个5位数号码组成一注，所选号码与开奖号码全部相同，且顺序一致，即为中奖。
         * maxbonus : 196000.00
         * minbonus : 170000.00
         * posbonus : 100.000000
         * maxbonus2 : 0.00
         * minbonus2 : 0.00
         * posbonus2 : 0.000000
         * wznum : 0
         * minnum : 3500
         * maxnum : 80000
         * sort : 1
         */

        private String id;
        private String radio;
        private String type;
        private String title0;
        private String title;
        private String title2;
        private String titlename;
        private String remark;
        private String example;
        private String help;
        private String maxbonus;
        private String minbonus;
        private String posbonus;
        private String maxbonus2;
        private String minbonus2;
        private String posbonus2;
        private String wznum;
        private String minnum;
        private String maxnum;
        private String sort;
        private boolean isChecked;

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRadio() {
            return radio;
        }

        public void setRadio(String radio) {
            this.radio = radio;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle0() {
            return title0;
        }

        public void setTitle0(String title0) {
            this.title0 = title0;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle2() {
            return title2;
        }

        public void setTitle2(String title2) {
            this.title2 = title2;
        }

        public String getTitlename() {
            return titlename;
        }

        public void setTitlename(String titlename) {
            this.titlename = titlename;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getExample() {
            return example;
        }

        public void setExample(String example) {
            this.example = example;
        }

        public String getHelp() {
            return help;
        }

        public void setHelp(String help) {
            this.help = help;
        }

        public String getMaxbonus() {
            return maxbonus;
        }

        public void setMaxbonus(String maxbonus) {
            this.maxbonus = maxbonus;
        }

        public String getMinbonus() {
            return minbonus;
        }

        public void setMinbonus(String minbonus) {
            this.minbonus = minbonus;
        }

        public String getPosbonus() {
            return posbonus;
        }

        public void setPosbonus(String posbonus) {
            this.posbonus = posbonus;
        }

        public String getMaxbonus2() {
            return maxbonus2;
        }

        public void setMaxbonus2(String maxbonus2) {
            this.maxbonus2 = maxbonus2;
        }

        public String getMinbonus2() {
            return minbonus2;
        }

        public void setMinbonus2(String minbonus2) {
            this.minbonus2 = minbonus2;
        }

        public String getPosbonus2() {
            return posbonus2;
        }

        public void setPosbonus2(String posbonus2) {
            this.posbonus2 = posbonus2;
        }

        public String getWznum() {
            return wznum;
        }

        public void setWznum(String wznum) {
            this.wznum = wznum;
        }

        public String getMinnum() {
            return minnum;
        }

        public void setMinnum(String minnum) {
            this.minnum = minnum;
        }

        public String getMaxnum() {
            return maxnum;
        }

        public void setMaxnum(String maxnum) {
            this.maxnum = maxnum;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }
    }
}
