package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/10/15.
 */

public class CunModeBean implements Serializable {
    private String mode;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
