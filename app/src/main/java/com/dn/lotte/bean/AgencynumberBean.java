package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencynumberBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 98
     * table : [{"rowid":"10","id":"101","ssid":"Z_4810191005624210324","userid":"1001","username":"lihuan001","usercode":",1001,","parentid":"0","lotteryid":"1015","lotteryname":"菲律宾1.5分","startissuenum":"20171016891","isstop":"1","stime":"2017-10-16 23:16:18","alltotal":"5.0000","isstopname":"是","finishname":"5.0000/5.000","statename":"1/5","finishstate":"0","shorttime":"10-16 23:16"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 10
         * id : 101
         * ssid : Z_4810191005624210324
         * userid : 1001
         * username : lihuan001
         * usercode : ,1001,
         * parentid : 0
         * lotteryid : 1015
         * lotteryname : 菲律宾1.5分
         * startissuenum : 20171016891
         * isstop : 1
         * stime : 2017-10-16 23:16:18
         * alltotal : 5.0000
         * isstopname : 是
         * finishname : 5.0000/5.000
         * statename : 1/5
         * finishstate : 0
         * shorttime : 10-16 23:16
         */

        private String rowid;
        private String id;
        private String ssid;
        private String userid;
        private String username;
        private String usercode;
        private String parentid;
        private String lotteryid;
        private String lotteryname;
        private String startissuenum;
        private String isstop;
        private String stime;
        private String alltotal;
        private String isstopname;
        private String finishname;
        private String statename;
        private String finishstate;
        private String shorttime;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsercode() {
            return usercode;
        }

        public void setUsercode(String usercode) {
            this.usercode = usercode;
        }

        public String getParentid() {
            return parentid;
        }

        public void setParentid(String parentid) {
            this.parentid = parentid;
        }

        public String getLotteryid() {
            return lotteryid;
        }

        public void setLotteryid(String lotteryid) {
            this.lotteryid = lotteryid;
        }

        public String getLotteryname() {
            return lotteryname;
        }

        public void setLotteryname(String lotteryname) {
            this.lotteryname = lotteryname;
        }

        public String getStartissuenum() {
            return startissuenum;
        }

        public void setStartissuenum(String startissuenum) {
            this.startissuenum = startissuenum;
        }

        public String getIsstop() {
            return isstop;
        }

        public void setIsstop(String isstop) {
            this.isstop = isstop;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getAlltotal() {
            return alltotal;
        }

        public void setAlltotal(String alltotal) {
            this.alltotal = alltotal;
        }

        public String getIsstopname() {
            return isstopname;
        }

        public void setIsstopname(String isstopname) {
            this.isstopname = isstopname;
        }

        public String getFinishname() {
            return finishname;
        }

        public void setFinishname(String finishname) {
            this.finishname = finishname;
        }

        public String getStatename() {
            return statename;
        }

        public void setStatename(String statename) {
            this.statename = statename;
        }

        public String getFinishstate() {
            return finishstate;
        }

        public void setFinishstate(String finishstate) {
            this.finishstate = finishstate;
        }

        public String getShorttime() {
            return shorttime;
        }

        public void setShorttime(String shorttime) {
            this.shorttime = shorttime;
        }
    }
}
