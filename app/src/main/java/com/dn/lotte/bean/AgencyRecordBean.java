package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class AgencyRecordBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 704
     * table : [{"rowid":"1","id":"768","ssid":"Z_4901959512402062667","userid":"1001","usercode":",1001,","username":"lihuan001","parentid":"0","moneychange":"-0.0310","moneyago":"8642.1178","moneyafter":"8642.0868","content":"","stime":"2017-10-12 22:10:51","code":"3","codename":"投注扣款","issoft":"99","remark":"会员追号"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * rowid : 1
         * id : 768
         * ssid : Z_4901959512402062667
         * userid : 1001
         * usercode : ,1001,
         * username : lihuan001
         * parentid : 0
         * moneychange : -0.0310
         * moneyago : 8642.1178
         * moneyafter : 8642.0868
         * content :
         * stime : 2017-10-12 22:10:51
         * code : 3
         * codename : 投注扣款
         * issoft : 99
         * remark : 会员追号
         */

        private String rowid;
        private String id;
        private String ssid;
        private String userid;
        private String usercode;
        private String username;
        private String parentid;
        private String moneychange;
        private String moneyago;
        private String moneyafter;
        private String content;
        private String stime;
        private String code;
        private String codename;
        private String issoft;
        private String remark;

        public String getRowid() {
            return rowid;
        }

        public void setRowid(String rowid) {
            this.rowid = rowid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSsid() {
            return ssid;
        }

        public void setSsid(String ssid) {
            this.ssid = ssid;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsercode() {
            return usercode;
        }

        public void setUsercode(String usercode) {
            this.usercode = usercode;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getParentid() {
            return parentid;
        }

        public void setParentid(String parentid) {
            this.parentid = parentid;
        }

        public String getMoneychange() {
            return moneychange;
        }

        public void setMoneychange(String moneychange) {
            this.moneychange = moneychange;
        }

        public String getMoneyago() {
            return moneyago;
        }

        public void setMoneyago(String moneyago) {
            this.moneyago = moneyago;
        }

        public String getMoneyafter() {
            return moneyafter;
        }

        public void setMoneyafter(String moneyafter) {
            this.moneyafter = moneyafter;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCodename() {
            return codename;
        }

        public void setCodename(String codename) {
            this.codename = codename;
        }

        public String getIssoft() {
            return issoft;
        }

        public void setIssoft(String issoft) {
            this.issoft = issoft;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
