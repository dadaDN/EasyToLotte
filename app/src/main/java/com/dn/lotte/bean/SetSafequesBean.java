package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by ASUS on 2017/10/10.
 */

public class SetSafequesBean implements Serializable {
    //返回数据

    private String result;
    private String returnval;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }
}
