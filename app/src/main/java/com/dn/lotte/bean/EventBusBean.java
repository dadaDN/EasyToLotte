package com.dn.lotte.bean;

import java.io.Serializable;

/**
 * Created by 丁宁
 * on 2017/10/10.
 */

public class EventBusBean<T> implements Serializable {
    private String mTag;
    private T mData;

    public EventBusBean(String tag, T data) {
        this.mTag = tag;
        this.mData = data;
    }

    public T getData() {
        return mData;
    }

    public String getTag() {
        return mTag;
    }
}
