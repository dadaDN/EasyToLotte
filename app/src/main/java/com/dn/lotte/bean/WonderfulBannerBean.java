package com.dn.lotte.bean;

import java.util.List;

/**
 * Created by DN on 2017/10/12.
 */

public class WonderfulBannerBean {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 3
     * table : [{"id":"1","title":"测试banner1","url":"123.207.44.159:50090/statics/banner/banner1.png","link":"1001"},{"id":"2","title":"测试banner2","url":"123.207.44.159:50090/statics/banner/banner1.png","link":"1001"},{"id":"3","title":"测试banner3","url":"123.207.44.159:50090/statics/banner/banner1.png","link":"1001"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 1
         * title : 测试banner1
         * url : 123.207.44.159:50090/statics/banner/banner1.png
         * link : 1001
         */

        private String id;
        private String title;
        private String url;
        private String link;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
