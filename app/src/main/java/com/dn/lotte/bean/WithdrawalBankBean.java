package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 取款银行
 * Created by ASUS on 2017/10/10.
 */

public class WithdrawalBankBean implements Serializable {

    /**
     * result : 1
     * returnval : 加载完成
     * recordcount : 15
     * table : [{"id":"3","code":"ICBC","bank":"中国工商银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"4","code":"CCB","bank":"中国建设银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"5","code":"ABC","bank":"中国农业银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"6","code":"CMB","bank":"招商银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"7","code":"BOC","bank":"中国银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"8","code":"COMM","bank":"中国交通银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"9","code":"PSBC","bank":"中国邮政储蓄银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"10","code":"CEB","bank":"中国光大银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"11","code":"CGB","bank":"广发银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"12","code":"CIB","bank":"兴业银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"13","code":"SPDB","bank":"上海浦东发展银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"14","code":"CMBC","bank":"中国民生银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"15","code":"CNCB","bank":"中信银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"16","code":"PAB","bank":"平安银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"},{"id":"20","code":"HXB","bank":"华夏银行","mincharge":"100.0000","maxcharge":"49999.0000","starttime":"12:00:00","endtime":"02:00:00","maxgetcash":"99","bindtime":"0","betpercheck":"30.00"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean {
        /**
         * id : 3
         * code : ICBC
         * bank : 中国工商银行
         * mincharge : 100.0000
         * maxcharge : 49999.0000
         * starttime : 12:00:00
         * endtime : 02:00:00
         * maxgetcash : 99
         * bindtime : 0
         * betpercheck : 30.00
         */

        private String id;
        private String code;
        private String bank;
        private String mincharge;
        private String maxcharge;
        private String starttime;
        private String endtime;
        private String maxgetcash;
        private String bindtime;
        private String betpercheck;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getBank() {
            return bank;
        }

        public void setBank(String bank) {
            this.bank = bank;
        }

        public String getMincharge() {
            return mincharge;
        }

        public void setMincharge(String mincharge) {
            this.mincharge = mincharge;
        }

        public String getMaxcharge() {
            return maxcharge;
        }

        public void setMaxcharge(String maxcharge) {
            this.maxcharge = maxcharge;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getMaxgetcash() {
            return maxgetcash;
        }

        public void setMaxgetcash(String maxgetcash) {
            this.maxgetcash = maxgetcash;
        }

        public String getBindtime() {
            return bindtime;
        }

        public void setBindtime(String bindtime) {
            this.bindtime = bindtime;
        }

        public String getBetpercheck() {
            return betpercheck;
        }

        public void setBetpercheck(String betpercheck) {
            this.betpercheck = betpercheck;
        }
    }
}
