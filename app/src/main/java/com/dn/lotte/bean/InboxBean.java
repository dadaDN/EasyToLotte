package com.dn.lotte.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2017/10/17.
 */

public class InboxBean implements Serializable {

    /**
     * result : 1
     * returnval : 操作成功
     * recordcount : 1
     * table : [{"id":"24","sendid":"1001","receiveid":"1030","sendname":"lihuan001","receivename":"chen123","title":"1232133","contents":"123213213213213","stime":"2017-10-17 18:02:30","isread":"0"}]
     */

    private String result;
    private String returnval;
    private String recordcount;
    private List<TableBean> table;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReturnval() {
        return returnval;
    }

    public void setReturnval(String returnval) {
        this.returnval = returnval;
    }

    public String getRecordcount() {
        return recordcount;
    }

    public void setRecordcount(String recordcount) {
        this.recordcount = recordcount;
    }

    public List<TableBean> getTable() {
        return table;
    }

    public void setTable(List<TableBean> table) {
        this.table = table;
    }

    public static class TableBean implements Serializable{
        /**
         * id : 24
         * sendid : 1001
         * receiveid : 1030
         * sendname : lihuan001
         * receivename : chen123
         * title : 1232133
         * contents : 123213213213213
         * stime : 2017-10-17 18:02:30
         * isread : 0
         */

        private String id;
        private String sendid;
        private String receiveid;
        private String sendname;
        private String receivename;
        private String title;
        private String contents;
        private String stime;
        private String isread;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSendid() {
            return sendid;
        }

        public void setSendid(String sendid) {
            this.sendid = sendid;
        }

        public String getReceiveid() {
            return receiveid;
        }

        public void setReceiveid(String receiveid) {
            this.receiveid = receiveid;
        }

        public String getSendname() {
            return sendname;
        }

        public void setSendname(String sendname) {
            this.sendname = sendname;
        }

        public String getReceivename() {
            return receivename;
        }

        public void setReceivename(String receivename) {
            this.receivename = receivename;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContents() {
            return contents;
        }

        public void setContents(String contents) {
            this.contents = contents;
        }

        public String getStime() {
            return stime;
        }

        public void setStime(String stime) {
            this.stime = stime;
        }

        public String getIsread() {
            return isread;
        }

        public void setIsread(String isread) {
            this.isread = isread;
        }
    }
}
