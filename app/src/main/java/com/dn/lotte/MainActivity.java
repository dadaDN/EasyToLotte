package com.dn.lotte;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.dn.lotte.app.AppConstant;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.TabBean;
import com.dn.lotte.ui.mainFragment.HistoryFragment;
import com.dn.lotte.ui.mainFragment.PurchaseFragment;
import com.dn.lotte.ui.mainFragment.UsercenterFragment;
import com.dn.lotte.ui.mainFragment.WonderfulFragment;
import com.dn.lotte.ui.usercenter.activity.CardBundActivity;
import com.dn.lotte.ui.usercenter.activity.SafePressionActivity;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.SPUtils;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.tencent.bugly.beta.Beta;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import butterknife.Bind;

public class MainActivity extends BaseActivity {

    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    @Bind(R.id.tablayout)
    CommonTabLayout mTablayout;
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();

    private String[] mTitles = {"精彩推荐", "购彩中心", "历史开奖", "我的账户"};
    private int[] mIconUnselectIds = {
            R.drawable.icon_nav_rec, R.drawable.icon_nav_cart, R.drawable.icon_nav_history,
            R.drawable.icon_nav_user};
    private int[] mIconSelectIds = {
            R.drawable.icon_nav_rec_on, R.drawable.icon_nav_cart_on, R.drawable.icon_nav_history_on,
            R.drawable.icon_nav_user_on};
    private Socket socket;
    private PrintWriter pw;
    private BufferedReader br;
    private Handler handler;
    @Override
    public int getLayoutId() {
        return R.layout.act_main;
    }

    @Override
    public void initPresenter() {
    }

    @Override
    public void initView() {
        Beta.checkUpgrade();
        handler = new Handler();
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabBean(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }
        mFragments.add(new WonderfulFragment());
        mFragments.add(new PurchaseFragment());
        mFragments.add(new HistoryFragment());
        mFragments.add(new UsercenterFragment());
        MyPagerAdapter mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(mAdapter);
        setData();
        if (GlobalApplication.getInstance().getUserModel()!=null&&GlobalApplication.getInstance().getUserModel().getTable()!=null&&GlobalApplication.getInstance().getUserModel().getTable().get(0)!=null&&GlobalApplication.getInstance().getUserModel().getTable().get(0).getIsanswer().equals("0")) {
            showSafeAnsower();
        }
        if (SPUtils.getSharedStringData(mContext, AppConstant.BANK).equals("0")){
            showBoundBank();
        }
      //  if (GlobalApplication.getInstance().getUserModel().getTable().get(0).get)

//        new Thread()
//            @Override
//            public void run() {
//                super.run();
//                try {
//                    //连接到123.207.44.159的8200端口 :
//                    socket = new Socket("123.207.44.159", 8200);
//                } catch (UnknownHostException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    Log.e("socket", "unknown host");
//                } catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    Log.e("socket", "io execption");
//                }
//                if (socket == null) {
//                    Log.e("socket", "null");
//                } else
//                    try {
//                        pw = new PrintWriter(socket.getOutputStream());
//                        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//                        if (pw != null && br != null) {
//                            new Thread(){
//                                @Override
//                                public void run() {
//                                    super.run();
//                                    try {
//                                        String str;
//                                        while((str=br.readLine())!=null){
//                                            final String s=str;
//                                            handler.post(new Runnable(){
//
//                                                public void run() {
//                                                    Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
//
//                                                }});
//
//                                        }
//                                    } catch (IOException e) {
//                                        // TODO Auto-generated catch block
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }.start();
//                        }
//                    } catch (IOException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//            }
//        }.start();

    }

    private void showBoundBank() {
        new QMUIDialog.MessageDialogBuilder(mContext)
                .setTitle("您还没有绑定银行卡")
                .setMessage("是否现在绑定?")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        startActivity(CardBundActivity.class);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void showSafeAnsower() {
        new QMUIDialog.MessageDialogBuilder(mContext)
                .setTitle("您还没有绑定安全问题")
                .setMessage("是否现在绑定?")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        startActivity( SafePressionActivity.class);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void setData() {
        mTablayout.setTabData(mTabEntities);
        mTablayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mViewpager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                AppConstant.WHERETOGO = position;
                mTablayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewpager.setCurrentItem(AppConstant.WHERETOGO);
        //设置未读消息红点
//        mTablayout.showDot(2);
//        MsgView rtv_2_2 = mTablayout.getMsgView(2);
//        if (rtv_2_2 != null) {
//            UnreadMsgUtils.setSize(rtv_2_2, dp2px(7.5f));
//        }
    }

private class MyPagerAdapter extends FragmentPagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

}

    protected int dp2px(float dp) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
