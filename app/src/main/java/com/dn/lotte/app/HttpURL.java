package com.dn.lotte.app;

/**
 * Created by 丁宁
 * on 2017/10/9.
 */

public class HttpURL {

    public static boolean isTest = false;//测试 true
    //  http://119.28.192.135:50090
//  http://apis.buqip.cn/api/
    //http://apis.frpublicdata.xyz:50090/
    public static String Base_Url = "";

    static {
        if (isTest) {
            Base_Url = "http://119.28.192.135:50090";//测试地址
        } else {
            Base_Url = "http://apis.frpublicdata.xyz:50090";//正式地址
        }
    }

    public static String login = "/user/ajaxLogin.aspx?oper=login"; //登陆
    public static String login2 = "/user/ajaxLogin.aspx?oper=login2"; //登陆
    public static String code = "/plus/getcode.aspx"; //登陆验证码
    public static String vipInfo = "/center/ajaxList.aspx?oper=ajaxGetUserInfo"; //会员信息
    public static String purchase = "/lottery/ajaxList.aspx?oper=lotterys"; //购彩中心  --全部游戏
    public static String purchaseType = "/lottery/ajaxList.aspx?oper=playTypeById"; //游戏分类
    public static String homeBanner = "/global/index.aspx?oper=platBanner"; //首页轮播图
    public static String betting = "/bet/ajaxList.aspx?oper=ajaxGetBetList"; // 投注记录
    public static String numberRecord = "/bet/ajaxList.aspx?oper=ajaxGetZHList"; // 追号记录
    public static String purchaseGame = "/lottery/ajaxList.aspx?oper=playInfoById"; //游戏玩法
    public static String notice = "/news/ajaxList.aspx?oper=ajaxGetNewsList"; //平台公告
    public static String noticeyouhui = "/active/ajaxList.aspx?oper=platactives"; //平台优惠
    public static String noticeyouhuidetail = "/active/ajaxList.aspx?oper=activeById"; //平台优惠详情
    public static String noticeDetail = "/news/ajaxList.aspx?oper=ajaxGetNewsContent"; //公告详情
    public static String newHistory = "/lottery/ajaxList.aspx?oper=GetKaiJiangList"; //公告详情
    public static String historyDetail = "/lottery/ajaxList.aspx?oper=GetKaiJiangInfo"; //公告详情
    public static String purchaseDetailNumber = "/lottery/ajaxList.aspx?oper=playNumberById"; //公告详情
    public static String bettinData = "/lottery/ajaxOper.aspx?oper=ajaxBetting"; //投注

    public static String getSafeQuesstion = "/global/index.aspx?oper=GetSetQues";//设置里面的获取安全问题
    public static String getPassWordalter = "/center/ajaxOper.aspx?oper=changepass";//设置里面修改登录密码
    public static String setSafeQuesstion = "/center/ajaxOper.aspx?oper=ajaxVerify";//设置里面的设置安全问题
    public static String setMoneypsw = "/center/ajaxOper.aspx?oper=moneypass";//设置里面设置修改资金密码
    public static String boundrealname = "/center/ajaxOper.aspx?oper=saveTrueName";//设置里面绑定真实姓名
    public static String boundbankcar = "/center/ajaxOper.aspx?oper=ajaxAddBank";//设置里面绑定银行
    public static String allbank = "/global/index.aspx?oper=platbanks";//设置里面所有取款银行银行
    public static String getRecharge = "/global/index.aspx?oper=platIphonecharge";//个人中心的充值方式
    public static String getRechargebank = "/money/ajaxList.aspx?oper=ajaxGetChrBankList";//个人中心的充值银行列表
    public static String memberBank = "/center/ajaxList.aspx?oper=ajaxGetBankList";//提现绑定的银行列表
    public static String getbankmoney = "/money/ajaxOper.aspx?oper=ajaxCash";//提现
    public static String getopenrank = "/user/ajaxList.aspx?oper=GetUserRegUserGroup";//开户级别
    public static String openmember = "/user/ajaxOper.aspx?oper=regiter";//开户
    public static String groupstatement = "/user/ajaxList.aspx?oper=ajaxGetFKProListSub";//团队报表
    public static String memebermanage = "/user/ajaxList.aspx?oper=ajaxGetUserList";//会员管理列表
    public static String record = "/user/ajaxList.aspx?oper=ajaxGetHisList_User";//账变记录
    public static String recordstate = "/global/index.aspx?oper=plathistory";//账变流水类型
    public static String dropout = "/user/ajaxOper.aspx?oper=logout";//退出账号
    public static String pursenumberdetails = "/bet/ajaxList.aspx?oper=ajaxGetZHInfo";//追号详情
    public static String remove = "/lottery/ajaxOper.aspx?oper=ajaxBettingCancel";//单个撤单
    public static String allhistory = "/lottery/ajaxList.aspx?oper=GetKaiJiangList";//历史开奖总和
    public static String kjdetails = "/lottery/ajaxList.aspx?oper=GetKaiJiangInfo";//开奖明细
    public static String versionsdetection = "/global/index.aspx?oper=platVersion";//版本检测
    public static String inbox = "/email/ajaxList.aspx?oper=ajaxGetReceiveList";//收件箱
    public static String outbox = "/email/ajaxList.aspx?oper=ajaxGetSendList";//发件箱
    public static String subordinate = "/email/ajaxList.aspx?oper=ajaxGetEmailUserList";//会员下级列表
    public static String sendmessage = "/email/ajaxOper.aspx?oper=SendEmail";//会员下级列表
    public static String accountDetails = "/bet/ajaxHistory.aspx?oper=ajaxGetHisList";//账户明细(资金明细)
    public static String personage = "/user/ajaxList.aspx?oper=ajaxGetListDay";//个人报表(就是接口里面代理界面个人统计)
    public static String recharg = "/money/ajaxList.aspx?oper=ajaxGetChargeList";//充值记录
    public static String withdrawal = "/money/ajaxList.aspx?oper=ajaxGetCashList";//取款记录
    public static String transferrecord = "/money/ajaxList.aspx?oper=ajaxGetTranAccList";//转账记录
    public static String agencybet = "/user/ajaxList.aspx?oper=ajaxGetBetList_User";//代理管理游戏记录（团队投注记录）
    public static String agencynumber = "/user/ajaxList.aspx?oper=ajaxGetZHList_User";//团队追号记录
    public static String agencynfh = "/user/ajaxList.aspx?oper=ajaxGetAgentFHList_User";//团队分红
    public static String getzhlist = "/lottery/ajaxList.aspx?oper=ajaxGetZhIssueNum";//获取追号列表
    public static String mzhuihao = "/lottery/ajaxOper.aspx?oper=ajaxZHBetting";//追号
    public static String loadkefu = "/global/index.aspx?oper=platform";//获取客服地址平台配置里面
    public static String memberpoint = "/user/ajaxOper.aspx?oper=ajaxUpdatePoint";//会员返点
    public static String zzurl = "/user/ajaxOper.aspx?oper=ajaxTranAcc";//转账
    public static String touzdetails = "/bet/ajaxList.aspx?oper=ajaxGetBetInfo";//投注详情
    public static String game = "/lottery/ajaxOper.aspx?oper=ajaxBetting";//投注详情
    public static String time = "/lottery/ajaxList.aspx?oper=ajaxLotteryTime";//投注详情
    public static String getMoney = "/global/index.aspx?oper=ajaxUserTimerPage";//定时请求余额
    public static String LinkOpen = "/user/ajaxOper.aspx?oper=ajaxRegStr";//链接开户
    public static String Linklist = "/user/ajaxList.aspx?oper=ajaxGetRegStrList";//链接列表
    public static String Linklistz = "/user/ajaxRegister.aspx";//链接注册
    public static String verifynumber = "/user/ajaxPassWordOper.aspx?oper=ajaxCheckUserName";//忘记密码中的验证账号
    public static String verifypswanswnor = "/user/ajaxPassWordOper.aspx?oper=ajaxCheckAnswer";//忘记密码中的验证密保问题
    public static String setpsw = "/user/ajaxPassWordOper.aspx?oper=ajaxSetPassWord";//忘记密码中的重新设置密码
    public static String loadyanz = "/money/ajaxOper.aspx?oper=ajaxCharge";//充值验证 跳转
    public static String qiyue = "/contract/ajaxList.aspx?oper=ajaxIsContract";//是否存在契约
    public static String beforeqiyue = "/contract/ajaxGzList.aspx?oper=ajaxGetAlReadyList";//分配前判断是否存在契约工资
    public static String qiyuesmallbig = "/contract/ajaxList.aspx?oper=ajaxGetPerValue";//契约比例区间工资
    public static String dayratio = "/contract/ajaxGzList.aspx?oper=ajaxGetContractInfo";//我的工资契约信息
    public static String consentratio = "/contract/ajaxGzOper.aspx?oper=UpdateContractOne";//同意签订工资契约,和拒绝签订一样
    public static String rejectratio = "/contract/ajaxGzOper.aspx?oper=UpdateContractTwo";//拒绝签订工资契约
    public static String consentcheratio = "/contract/ajaxGzOper.aspx?oper=UpdateContractFour";//同意撤销工资契约
    public static String myfhratio = "/contract/ajaxList.aspx?oper=ajaxGetContractInfo";//我的分红契约信息
//    public static String myfhratio = "contract/ajaxList.aspx?oper=ajaxGetAgentFHList";//我的分红契约记录
    public static String myfhconsentratio = "/contract/ajaxOper.aspx?oper=UpdateContractOne";//我的分红契约信息同意签订契约
    public static String myfhjujueratio = "/contract/ajaxOper.aspx?oper=UpdateContractTwo";//我的分红契约信息拒绝签订契约
    public static String myfhchexiaoeratio = "/contract/ajaxOper.aspx?oper=UpdateContractFour";//我的分红契约信息同意撤销契约
    public static String gonzifenpei = "/contract/ajaxGzOper.aspx?oper=ajaxSaveContract";//工资契约分配
    public static String fhfenpei = "/contract/ajaxOper.aspx?oper=ajaxSaveContract";//分红契约分配
    public static String gonzichefenpei = "/contract/ajaxOper.aspx?oper=UpdateContractThree";//工资契约发起撤销
    public static String beforefhqiyue = "/contract/ajaxList.aspx?oper=ajaxGetAlReadyList";//分配前判断是否存在契约分红
    public static String qiyuesmallbigfh = "/contract/ajaxList.aspx?oper=ajaxGetAlReadyList";//契约比例区间分红
  //  public static String trendnumber = "//lottery/ajaxList.aspx?oper=GetZsList";//走势图号码

}
