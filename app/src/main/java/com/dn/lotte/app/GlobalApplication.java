package com.dn.lotte.app;

import android.os.Environment;
import android.util.Log;

import com.dn.lotte.R;
import com.dn.lotte.bean.CunModeBean;
import com.dn.lotte.bean.Vipbean;
import com.dn.lotte.ui.usercenter.activity.LoginActivity;
import com.easy.common.baseapp.BaseApplication;
import com.easy.common.commonutils.JsonUtils;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheEntity;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.cookie.CookieJarImpl;
import com.lzy.okgo.cookie.store.SPCookieStore;
import com.lzy.okgo.interceptor.HttpLoggingInterceptor;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.upgrade.UpgradeStateListener;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import okhttp3.OkHttpClient;

/**
 * Created by 丁宁
 * on 2017/5/3.
 */

public class GlobalApplication extends BaseApplication {
    private static GlobalApplication mContext;

    public static GlobalApplication getInstance() {
        return mContext;
    }

    public static final String APP_ID = "41d5850f69"; // TODO 替换成bugly上注册的appid
    private Vipbean mVipbean;
    private CunModeBean cunModeBean;
    private String TAG = "DNLOG";

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
//        initImagePicker();
//        initEMClient();
//        IntentService intentService = new IntentService("") {
//            @Override
//            protected void onHandleIntent(@Nullable Intent intent) {
//
//            }
//        };
//        Bugtags.start("5cd474027dfffb9f6a778d149ead1ddc", this, Bugtags.BTGInvocationEventBubble);
        initOkGo();
        initBuggly();
    }

    private void initBuggly() {
        /**** Beta高级设置*****/
        /**
         * true表示app启动自动初始化升级模块；
         * false不好自动初始化
         * 开发者如果担心sdk初始化影响app启动速度，可以设置为false
         * 在后面某个时刻手动调用
         */
        Beta.autoInit = true;

        Beta.upgradeCheckPeriod = 3 * 1000;
        /**
         * true表示初始化时自动检查升级
         * false表示不会自动检查升级，需要手动调用Beta.checkUpgrade()方法
         */
        Beta.autoCheckUpgrade = false;

        /**
         * 设置升级周期为60s（默认检查周期为0s），60s内SDK不重复向后天请求策略
         */
        Beta.initDelay = 1 * 1000;

        /**
         * 设置通知栏大图标，largeIconId为项目中的图片资源；
         */
        Beta.largeIconId = R.mipmap.kc;

        /**
         * 设置状态栏小图标，smallIconId为项目中的图片资源id;
         */
        Beta.smallIconId = R.mipmap.kc;


        /**
         * 设置更新弹窗默认展示的banner，defaultBannerId为项目中的图片资源Id;
         * 当后台配置的banner拉取失败时显示此banner，默认不设置则展示“loading“;
         */
        Beta.defaultBannerId = R.mipmap.kc;

        /**
         * 设置sd卡的Download为更新资源保存目录;
         * 后续更新资源会保存在此目录，需要在manifest中添加WRITE_EXTERNAL_STORAGE权限;
         */
        Beta.storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        /**
         * 点击过确认的弹窗在APP下次启动自动检查更新时会再次显示;
         */
        Beta.showInterruptedStrategy = false;

        /**
         * 只允许在MainActivity上显示更新弹窗，其他activity上不显示弹窗;
         * 不设置会默认所有activity都可以显示弹窗;
         */
        Beta.canShowUpgradeActs.add(LoginActivity.class);


        /**
         *  设置自定义升级对话框UI布局
         *  注意：因为要保持接口统一，需要用户在指定控件按照以下方式设置tag，否则会影响您的正常使用：
         *  标题：beta_title，如：android:tag="beta_title"
         *  升级信息：beta_upgrade_info  如： android:tag="beta_upgrade_info"
         *  更新属性：beta_upgrade_feature 如： android:tag="beta_upgrade_feature"
         *  取消按钮：beta_cancel_button 如：android:tag="beta_cancel_button"
         *  确定按钮：beta_confirm_button 如：android:tag="beta_confirm_button"
         *  详见layout/upgrade_dialog.xml
         */
        Beta.upgradeDialogLayoutId = R.layout.upgrade_dialog;

        /**
         * 设置自定义tip弹窗UI布局
         * 注意：因为要保持接口统一，需要用户在指定控件按照以下方式设置tag，否则会影响您的正常使用：
         *  标题：beta_title，如：android:tag="beta_title"
         *  提示信息：beta_tip_message 如： android:tag="beta_tip_message"
         *  取消按钮：beta_cancel_button 如：android:tag="beta_cancel_button"
         *  确定按钮：beta_confirm_button 如：android:tag="beta_confirm_button"
         *  详见layout/tips_dialog.xml
         */
//        Beta.tipsDialogLayoutId = R.layout.tips_dialog;

        /**
         *  如果想监听升级对话框的生命周期事件，可以通过设置OnUILifecycleListener接口
         *  回调参数解释：
         *  context - 当前弹窗上下文对象
         *  view - 升级对话框的根布局视图，可通过这个对象查找指定view控件
         *  upgradeInfo - 升级信息
         */
        Beta.upgradeStateListener = new UpgradeStateListener() {
            @Override
            public void onUpgradeFailed(boolean b) {

            }

            @Override
            public void onUpgradeSuccess(boolean b) {

            }

            /**
             * 没有更新
             * @param
             */
            @Override
            public void onUpgradeNoVersion(boolean b) {
                Log.d(TAG, "lllllllaaa" + b);
            }

            @Override
            public void onUpgrading(boolean b) {

            }

            @Override
            public void onDownloadCompleted(boolean b) {

            }
        };
//        Beta.upgradeDialogLifecycleListener = new UILifecycleListener<UpgradeInfo>() {
//            @Override
//            public void onCreate(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onCreate");
//                // 注：可通过这个回调方式获取布局的控件，如果设置了id，可通过findViewById方式获取，如果设置了tag，可以通过findViewWithTag，具体参考下面例子:
//
//                // 通过id方式获取控件，并更改imageview图片
////                ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
////                imageView.setImageResource(R.mipmap.ic_launcher);
////
////                // 通过tag方式获取控件，并更改布局内容
////                TextView textView = (TextView) view.findViewWithTag("textview");
////                textView.setText("my custom text");
////
////                // 更多的操作：比如设置控件的点击事件
////                imageView.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        Intent intent = new Intent(getApplicationContext(), OtherActivity.class);
////                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                        startActivity(intent);
////                    }
////                });
//            }
//
//            @Override
//            public void onStart(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onStart");
//            }
//
//            @Override
//            public void onResume(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onResume");
//            }
//
//            @Override
//            public void onPause(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onPause");
//            }
//
//            @Override
//            public void onStop(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onStop");
//            }
//
//            @Override
//            public void onDestroy(Context context, View view, UpgradeInfo upgradeInfo) {
//                Log.d(TAG, "onDestory");
//            }
//        };

        /**
         * 自定义Activity参考，通过回调接口来跳转到你自定义的Actiivty中。
         */
//        Beta.upgradeListener = new UpgradeListener() {
//
//            @Override
//            public void onUpgrade(int ret, UpgradeInfo strategy, boolean isManual, boolean isSilence) {
//                if (strategy != null) {
////                    Intent i = new Intent();
////                    i.setClass(getApplicationContext(), UpgradeActivity.class);
////                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                    startActivity(i);
//                } else {
//                    Toast.makeText(getApplicationContext(), "没有更新", Toast.LENGTH_SHORT).show();
//                }
//            }
//        };


        /**
         * 已经接入Bugly用户改用上面的初始化方法,不影响原有的crash上报功能;
         * init方法会自动检测更新，不需要再手动调用Beta.checkUpdate(),如需增加自动检查时机可以使用Beta.checkUpdate(false,false);
         * 参数1： applicationContext
         * 参数2：appId
         * 参数3：是否开启debug
         */
        Bugly.init(getApplicationContext(), APP_ID, true);

        /**
         * 如果想自定义策略，按照如下方式设置
         */

        /***** Bugly高级设置 *****/
        //        BuglyStrategy strategy = new BuglyStrategy();
        /**
         * 设置app渠道号
         */
        //        strategy.setAppChannel(APP_CHANNEL);

        //        Bugly.init(getApplicationContext(), APP_ID, true, strategy);
    }


    private void initOkGo() {
        //---------这里给出的是示例代码,告诉你可以这么传,实际使用的时候,根据需要传,不需要就不传-------------//
        HttpHeaders headers = new HttpHeaders();
//        headers.put("commonHeaderKey1", "commonHeaderValue1");    //header不支持中文，不允许有特殊字符
//        headers.put("commonHeaderKey2", "commonHeaderValue2");
        HttpParams params = new HttpParams();
//        params.put("commonParamsKey1", "commonParamsValue1");     //param支持中文,直接传,不要自己编码
//        params.put("commonParamsKey2", "这里支持中文参数");
        //----------------------------------------------------------------------------------------//
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //log相关
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor("OkGo");
        loggingInterceptor.setPrintLevel(HttpLoggingInterceptor.Level.BODY);        //log打印级别，决定了log显示的详细程度
        loggingInterceptor.setColorLevel(Level.INFO);                               //log颜色级别，决定了log在控制台显示的颜色
        builder.addInterceptor(loggingInterceptor);                                 //添加OkGo默认debug日志
        //第三方的开源库，使用通知显示当前请求的log，不过在做文件下载的时候，这个库好像有问题，对文件判断不准确
        //builder.addInterceptor(new ChuckInterceptor(this));

        //超时时间设置，默认60秒
        builder.readTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);      //全局的读取超时时间
        builder.writeTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);     //全局的写入超时时间
        builder.connectTimeout(OkGo.DEFAULT_MILLISECONDS, TimeUnit.MILLISECONDS);   //全局的连接超时时间

        //自动管理cookie（或者叫session的保持），以下几种任选其一就行
        builder.cookieJar(new CookieJarImpl(new SPCookieStore(this)));            //使用sp保持cookie，如果cookie不过期，则一直有效
//        builder.cookieJar(new CookieJarImpl(new DBCookieStore(this)));              //使用数据库保持cookie，如果cookie不过期，则一直有效
        //builder.cookieJar(new CookieJarImpl(new MemoryCookieStore()));            //使用内存保持cookie，app退出后，cookie消失
        // 其他统一的配置
        // 详细说明看GitHub文档：https://github.com/jeasonlzy/
        OkGo.getInstance().init(this)                           //必须调用初始化
                .setOkHttpClient(builder.build())               //建议设置OkHttpClient，不设置会使用默认的
                .setCacheMode(CacheMode.NO_CACHE)               //全局统一缓存模式，默认不使用缓存，可以不传
                .setCacheTime(CacheEntity.CACHE_NEVER_EXPIRE)   //全局统一缓存时间，默认永不过期，可以不传
                .setRetryCount(3)                               //全局统一超时重连次数，默认为三次，那么最差的情况会请求4次(一次原始请求，三次重连请求)，不需要可以设置为0
                .addCommonHeaders(headers)                      //全局公共头
                .addCommonParams(params);                       //全局公共参数
    }

//
//    private void initImagePicker() {
//        ImagePicker imagePicker = ImagePicker.getInstance();
//        imagePicker.setMultiMode(false);
//        imagePicker.setImageLoader(new GlideImageLoader());   //设置图片加载器
//        imagePicker.setShowCamera(false);                      //显示拍照按钮
//        imagePicker.setCrop(true);                           //允许裁剪（单选才有效）
//        imagePicker.setSaveRectangle(true);                   //是否按矩形区域保存
//        imagePicker.setSelectLimit(1);                       //选中数量限制
//        imagePicker.setStyle(CropImageView.Style.RECTANGLE);  //裁剪框的形状
//        imagePicker.setFocusWidth(800);                       //裁剪框的宽度。单位像素（圆形自动取宽高最小值）
//        imagePicker.setFocusHeight(800);                      //裁剪框的高度。单位像素（圆形自动取宽高最小值）
//        imagePicker.setOutPutX(1000);                         //保存文件的宽度。单位像素
//        imagePicker.setOutPutY(1000);                         //保存文件的高度。单位像素
//    }
//

    /**
     * 登录之后设置为true
     *
     * @param value
     */
    public void setLogin(boolean value) {
        SPUtils.setSharedBooleanData(mContext, "ISLOGIN", value);
    }

    /**
     * 是否登录  true登录
     *
     * @author 丁宁
     * @description
     */
    public boolean isLogin() {
        return SPUtils.getSharedBooleanData(mContext, "ISLOGIN");
    }
//
//    /**
//     * 设置accesstoken
//     *
//     * @param value
//     */
//    public void setAccessToken(String value) {
//        SPUtils.setSharedStringData(mContext, "AccessToken", value);
//    }
//
//    /**
//     * 获取accesstoken
//     *
//     * @author 丁宁
//     * @description
//     */
//    public String getAccessToken() {
//        return SPUtils.getSharedStringData(mContext, "AccessToken");
//    }
//
//    /**
//     * 设置RefreshToken
//     *
//     * @param value
//     */
//    public void setRefreshToken(String value) {
//        SPUtils.setSharedStringData(mContext, "RefreshToken", value);
//    }
//
//    /**
//     * 获取RefreshToken
//     *
//     * @author 丁宁
//     * @description
//     */
//    public String getRefreshToken() {
//        return SPUtils.getSharedStringData(mContext, "RefreshToken");
//    }
//
//    /**
//     * 设置USRRID
//     *
//     * @param value userid
//     */
//    public void setUserID(String value) {
//        SPUtils.setSharedStringData(mContext, "USRRID", value);
//    }
//
//    /**
//     * 获取USRRID
//     *
//     * @author 丁宁
//     * @description
//     */
//    public String getUserID() {
//        return SPUtils.getSharedStringData(mContext, "USRRID");
//    }
//
//    private UserInfoBean.UserBean userModel;
//

    /**
     * 存储对象
     *
     * @param bean 个人信息本地数据
     */
    public void setUserModel(Vipbean bean) {
        if (bean != null) {
            Gson gson = new Gson();
            String saveData = gson.toJson(bean);
            SPUtils.setSharedStringData(mContext, "USER_MODEL_CLASS", saveData);
        } else {
            SPUtils.setSharedStringData(mContext, "USER_MODEL_CLASS", null);
        }
    }

    /**
     * 获取对象
     *
     * @return 个人信息本地数据
     */
    public Vipbean getUserModel() {
        String sava = SPUtils.getSharedStringDatas(mContext, "USER_MODEL_CLASS");
        if (!StringUtils.isEmpty(sava)){
            return  (Vipbean) JsonUtils.fromJson(sava, Vipbean.class);
        }

        return null;
    }

    /**
     * 存储圆角模式
     *
     * @param bean 个人信息本地数据
     */
    public void setMoneyMode(CunModeBean bean) {
        if (bean != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(bean);
                String serStr = byteArrayOutputStream.toString("ISO-8859-1");
                serStr = java.net.URLEncoder.encode(serStr, "UTF-8");
                SPUtils.setSharedStringData(mContext, "USER_MODEL_CLASS", serStr);
                objectOutputStream.close();
                byteArrayOutputStream.close();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            SPUtils.setSharedStringData(mContext, "USER_MODEL_CLASS", null);
        }
        cunModeBean = bean;
    }

    /**
     * 获取圆角模式
     *
     * @return 个人信息本地数据
     */
    public CunModeBean getMoneyMode() {
        if (cunModeBean == null) {
            String temp = SPUtils.getSharedStringDatas(mContext, "USRRID");
            if (temp == null) {
                return null;
            }
            String redStr;
            try {
                redStr = java.net.URLDecoder.decode(temp, "UTF-8");
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(redStr.getBytes("ISO-8859-1"));
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                cunModeBean = (CunModeBean) objectInputStream.readObject();
                objectInputStream.close();
                byteArrayInputStream.close();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (StreamCorruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return cunModeBean;
    }
}

