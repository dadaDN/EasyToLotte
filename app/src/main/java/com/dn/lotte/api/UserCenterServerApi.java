package com.dn.lotte.api;

import android.util.Log;

import com.dn.lotte.app.HttpURL;
import com.dn.lotte.bean.AccountDetailsBean;
import com.dn.lotte.bean.AgencyRecordBean;
import com.dn.lotte.bean.AgencybetBean;
import com.dn.lotte.bean.AgencynFHBean;
import com.dn.lotte.bean.AgencynumberBean;
import com.dn.lotte.bean.AmendPswBean;
import com.dn.lotte.bean.BeforeqiyuBean;
import com.dn.lotte.bean.DayratioBean;
import com.dn.lotte.bean.GroupStatementBean;
import com.dn.lotte.bean.HistoryBean;
import com.dn.lotte.bean.KefuBean;
import com.dn.lotte.bean.LinkBean;
import com.dn.lotte.bean.MemberBankBean;
import com.dn.lotte.bean.MemberManageBean;
import com.dn.lotte.bean.OpenRankBean;
import com.dn.lotte.bean.PersonageBean;
import com.dn.lotte.bean.PurseNumberBean;
import com.dn.lotte.bean.QiyueBean;
import com.dn.lotte.bean.QiyueBiBean;
import com.dn.lotte.bean.RechargBean;
import com.dn.lotte.bean.RechargeBankBean;
import com.dn.lotte.bean.RechargeBean;
import com.dn.lotte.bean.RecordStateBean;
import com.dn.lotte.bean.RemoveBean;
import com.dn.lotte.bean.SafeQuesstionBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.bean.SubordinateBean;
import com.dn.lotte.bean.TimeBean;
import com.dn.lotte.bean.TransFerrecordBean;
import com.dn.lotte.bean.WithdrawalBankBean;
import com.dn.lotte.bean.WithdrawalBean;
import com.dn.lotte.bean.kjDetailsBean;
import com.dn.lotte.utils.CommonUtils;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.lzy.okrx2.adapter.ObservableResponse;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * 个人中心页的一些网络请求
 * Created by ASUS on 2017/10/9.
 */

public class UserCenterServerApi {


    //设置里面修改登录密码
    public static Observable<AmendPswBean> getPassWordalter(HttpParams param) {

        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.getPassWordalter)//
                .params(param)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        AmendPswBean amendPswBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return amendPswBean;
                    }
                });//
    }

    //设置里面的获取安全问题
    public static Observable<SafeQuesstionBean> getSafeQuesstion() {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.getSafeQuesstion)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SafeQuesstionBean>() {
                    @Override
                    public SafeQuesstionBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String str = stringResponse.body();
                        String substring = str.substring(1, str.length() - 1);
                        Gson gson = new Gson();
                        SafeQuesstionBean safeQuesstionBean = gson.fromJson(substring, SafeQuesstionBean.class);
                        return safeQuesstionBean;
                    }
                });//
    }

    //设置里面的设置安全问题
    public static Observable<SetSafequesBean> setSafeQuesstion(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.setSafeQuesstion)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //设置里面设置修改资金密码
    public static Observable<SetSafequesBean> setMoneypsw(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.setMoneypsw)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //设置里面设置真实姓名
    public static Observable<SetSafequesBean> setRealname(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.boundrealname)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //设置里面绑定银行
    public static Observable<SetSafequesBean> bundBankcar(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.boundbankcar)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //设置里面绑定银行中的所有取款银行
    public static Observable<WithdrawalBankBean> getBank() {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.allbank)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, WithdrawalBankBean>() {
                    @Override
                    public WithdrawalBankBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        WithdrawalBankBean withdrawalBankBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), WithdrawalBankBean.class);
                        return withdrawalBankBean;
                    }
                });
    }

    //个人中心中的充值方式选择
    public static Observable<RechargeBean> getRecharge() {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.getRecharge)

                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RechargeBean>() {
                    @Override
                    public RechargeBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RechargeBean rechargeBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RechargeBean.class);
                        return rechargeBean;
                    }
                });
    }

    //个人中心中的充值银行列表
    public static Observable<RechargeBankBean> getRechargeBank(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.getRechargebank)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RechargeBankBean>() {
                    @Override
                    public RechargeBankBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String string = stringResponse.body();
                        Gson gson = new Gson();
                        RechargeBankBean rechargeBankBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RechargeBankBean.class);
                        return rechargeBankBean;
                    }
                });
    }

    //取款银行列表
    public static Observable<MemberBankBean> getMemberBank() {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.memberBank)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, MemberBankBean>() {
                    @Override
                    public MemberBankBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String string = stringResponse.body();
                        Gson gson = new Gson();
                        MemberBankBean memberBankBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), MemberBankBean.class);
                        return memberBankBean;
                    }
                });
    }

    //提现
    public static Observable<SetSafequesBean> getbankMoney(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.getbankmoney)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //开户中心的开户级别
    public static Observable<OpenRankBean> getOpenrank() {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.getopenrank)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, OpenRankBean>() {
                    @Override
                    public OpenRankBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        OpenRankBean openRankBean = gson.
                                fromJson(CommonUtils.getSubStr(stringResponse.body()), OpenRankBean.class);
                        return openRankBean;
                    }
                });
    }

    //开户
    public static Observable<SetSafequesBean> getOpenMember(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.openmember)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean setSafequesBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return setSafequesBean;
                    }
                });
    }

    //团队报表
    public static Observable<GroupStatementBean> getGroupData(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.groupstatement)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, GroupStatementBean>() {
                    @Override
                    public GroupStatementBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        GroupStatementBean groupStatementBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), GroupStatementBean.class);
                        return groupStatementBean;
                    }
                });
    }

    //团队报表
    public static Observable<MemberManageBean> getMemebrMangement(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.memebermanage)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, MemberManageBean>() {
                    @Override
                    public MemberManageBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        MemberManageBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), MemberManageBean.class);
                        return member;
                    }
                });
    }
    //账变记录团队

    public static Observable<AgencyRecordBean> getRecord(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.record)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AgencyRecordBean>() {
                    @Override
                    public AgencyRecordBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        AgencyRecordBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AgencyRecordBean.class);
                        return member;
                    }
                });
    }   //账变类型

    public static Observable<RecordStateBean> getRecordstate() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.recordstate)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RecordStateBean>() {
                    @Override
                    public RecordStateBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RecordStateBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RecordStateBean.class);
                        return member;
                    }
                });
    }

    public static Observable<SetSafequesBean> cancellation() {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.dropout)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }
    //追号详情

    public static Observable<PurseNumberBean> pursedetails(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.pursenumberdetails)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurseNumberBean>() {
                    @Override
                    public PurseNumberBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String s = stringResponse.body();
                        Gson gson = new Gson();
                        PurseNumberBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurseNumberBean.class);
                        return member;
                    }
                });
    }
    //撤单单个

    public static Observable<RemoveBean> setRemove(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.remove)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RemoveBean>() {
                    @Override
                    public RemoveBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RemoveBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RemoveBean.class);
                        return member;
                    }
                });
    }    //历史开奖总和

    public static Observable<HistoryBean> getallHistory() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.allhistory)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, HistoryBean>() {
                    @Override
                    public HistoryBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        HistoryBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), HistoryBean.class);
                        return member;
                    }
                });
    } //历史开奖明细

    public static Observable<kjDetailsBean> getkjdetails(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.kjdetails)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, kjDetailsBean>() {
                    @Override
                    public kjDetailsBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        kjDetailsBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), kjDetailsBean.class);
                        return member;
                    }
                });
    }//检查版本

    public static Observable<RemoveBean> versiondetection() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.versionsdetection)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RemoveBean>() {
                    @Override
                    public RemoveBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RemoveBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RemoveBean.class);
                        return member;
                    }
                });
    }//站内信的下级列表

    public static Observable<SubordinateBean> getsubordinate() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.subordinate)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SubordinateBean>() {
                    @Override
                    public SubordinateBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SubordinateBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SubordinateBean.class);
                        return member;
                    }
                });
    }
    //站内信的发送

    public static Observable<SetSafequesBean> sendmessage(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.sendmessage)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }    //账变记录（资金明细）

    public static Observable<AccountDetailsBean> accountDetails(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.accountDetails)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AccountDetailsBean>() {
                    @Override
                    public AccountDetailsBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        AccountDetailsBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AccountDetailsBean.class);
                        return member;
                    }
                });
    }  //个人报表

    public static Observable<PersonageBean> getpersonage(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.personage)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PersonageBean>() {
                    @Override
                    public PersonageBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        String a = CommonUtils.getSubStr(stringResponse.body());
                        PersonageBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PersonageBean.class);
                        return member;
                    }
                });
    }//充值记录

    public static Observable<RechargBean> recharg(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.recharg)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RechargBean>() {
                    @Override
                    public RechargBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RechargBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RechargBean.class);
                        return member;
                    }
                });
    }//取款记录

    public static Observable<WithdrawalBean> withdrawal(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.withdrawal)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, WithdrawalBean>() {
                    @Override
                    public WithdrawalBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        WithdrawalBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), WithdrawalBean.class);
                        return member;
                    }
                });
    }//转账记录

    public static Observable<TransFerrecordBean> gettransferrecord(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.transferrecord)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, TransFerrecordBean>() {
                    @Override
                    public TransFerrecordBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        TransFerrecordBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), TransFerrecordBean.class);
                        return member;
                    }
                });
    }
    //代理管理界面的团队投注记录 （游戏记录）

    public static Observable<AgencybetBean> getagencybet(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.agencybet)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AgencybetBean>() {
                    @Override
                    public AgencybetBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String a = CommonUtils.getSubStr(stringResponse.body());
                        Gson gson = new Gson();
                        AgencybetBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AgencybetBean.class);
                        return member;
                    }
                });
    }
    //代理管理界面的团队追号记录

    public static Observable<AgencynumberBean> agencynumber(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.agencynumber)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AgencynumberBean>() {
                    @Override
                    public AgencynumberBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String a = CommonUtils.getSubStr(stringResponse.body());
                        Gson gson = new Gson();
                        AgencynumberBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AgencynumberBean.class);
                        return member;
                    }
                });
    }    //代理管理界面的团队分红

    public static Observable<AgencynFHBean> agencyfh(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.agencynfh)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AgencynFHBean>() {
                    @Override
                    public AgencynFHBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String a = CommonUtils.getSubStr(stringResponse.body());
                        Gson gson = new Gson();
                        AgencynFHBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AgencynFHBean.class);
                        return member;
                    }
                });
    }

    //获取客服地址
    public static Observable<KefuBean> loadkefu() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.loadkefu)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, KefuBean>() {
                    @Override
                    public KefuBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String a = CommonUtils.getSubStr(stringResponse.body());
                        Gson gson = new Gson();
                        KefuBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), KefuBean.class);
                        return member;
                    }
                });
    }  //会员升点

    public static Observable<SetSafequesBean> memberpoint(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.memberpoint)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }
    //转账接口

    public static Observable<SetSafequesBean> setaccounts(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.zzurl)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }    //链接开户

    public static Observable<SetSafequesBean> getLink(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.LinkOpen)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }  //链接开户

    public static Observable<LinkBean> getLinklist() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.Linklist)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, LinkBean>() {
                    @Override
                    public LinkBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        LinkBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), LinkBean.class);
                        return member;
                    }
                });
    }   //刷新余额

    public static Observable<TimeBean> getMoney() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.getMoney)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, TimeBean>() {
                    @Override
                    public TimeBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        TimeBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), TimeBean.class);
                        return member;
                    }
                });
    }
    //充值验证

    public static Observable<SetSafequesBean> Loadyanzheng(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.loadyanz)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        SetSafequesBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return member;
                    }
                });
    }
    //是否有契约验证

    public static Observable<QiyueBean> loawqiyue() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.qiyue)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, QiyueBean>() {
                    @Override
                    public QiyueBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        QiyueBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), QiyueBean.class);
                        return member;
                    }
                });
    }  //契约比例区间

    public static Observable<QiyueBiBean> qiyuesmallbig() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.qiyuesmallbig)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, QiyueBiBean>() {
                    @Override
                    public QiyueBiBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        QiyueBiBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), QiyueBiBean.class);
                        return member;
                    }
                });
    } //契约比例区间分红

    public static Observable<QiyueBiBean> qiyuesmallbigfh() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.qiyuesmallbigfh)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, QiyueBiBean>() {
                    @Override
                    public QiyueBiBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        QiyueBiBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), QiyueBiBean.class);
                        return member;
                    }
                });
    }//工资契约

    public static Observable<DayratioBean> dayratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.dayratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, DayratioBean>() {
                    @Override
                    public DayratioBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        DayratioBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), DayratioBean.class);
                        return member;
                    }
                });
    }//同意签订契约

    public static Observable<AmendPswBean> consentratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.consentratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//拒绝签订契约

    public static Observable<AmendPswBean> rejectratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.rejectratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//工资同意撤销契约

    public static Observable<AmendPswBean> consentcheratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.consentcheratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//我的分红契约信息

    public static Observable<DayratioBean> myfhratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.myfhratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, DayratioBean>() {
                    @Override
                    public DayratioBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        DayratioBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), DayratioBean.class);
                        return member;
                    }
                });
    }
    //分红同意签订契约

    public static Observable<AmendPswBean> myfhconsentratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.myfhconsentratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    } //分红拒绝签订契约

    public static Observable<AmendPswBean> myfhjujueratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.myfhjujueratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//分红同意撤销契约

    public static Observable<AmendPswBean> myfhchexiaoeratio() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.myfhchexiaoeratio)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//工资分配契约前判断是否存在契约

    public static Observable<BeforeqiyuBean> beforeqiyue(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.beforeqiyue)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, BeforeqiyuBean>() {
                    @Override
                    public BeforeqiyuBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        BeforeqiyuBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), BeforeqiyuBean.class);
                        return member;
                    }
                });
    }//工资分配契约

    public static Observable<AmendPswBean> gonzifenpei(String params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.gonzifenpei)
                .converter(new StringConvert())
                .upJson(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String body = stringResponse.body();
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//分红分配契约

    public static Observable<AmendPswBean> fhfenpei(String params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.fhfenpei)
                .converter(new StringConvert())
                .upJson(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String body = stringResponse.body();
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }//工资分配发起撤销契约

    public static Observable<AmendPswBean> gonzichefenpei(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.gonzichefenpei)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, AmendPswBean>() {
                    @Override
                    public AmendPswBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        String body = stringResponse.body();
                        Gson gson = new Gson();
                        AmendPswBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), AmendPswBean.class);
                        return member;
                    }
                });
    }
    //分红分配契约前判断是否存在契约

    public static Observable<BeforeqiyuBean> beforefhqiyue(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.beforefhqiyue)
                .converter(new StringConvert())
                .params(params)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, BeforeqiyuBean>() {
                    @Override
                    public BeforeqiyuBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Log.e("fandian", stringResponse + "");
                        Gson gson = new Gson();
                        BeforeqiyuBean member = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), BeforeqiyuBean.class);
                        return member;
                    }
                });
    }
}
