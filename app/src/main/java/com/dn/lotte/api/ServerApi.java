/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dn.lotte.api;

import com.dn.lotte.app.HttpURL;
import com.dn.lotte.bean.BettingBean;
import com.dn.lotte.bean.DiscountBean;
import com.dn.lotte.bean.DiscountitemBean;
import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.bean.InboxBean;
import com.dn.lotte.bean.NewHistoryBean;
import com.dn.lotte.bean.NoticeBean;
import com.dn.lotte.bean.NoticeDetailBean;
import com.dn.lotte.bean.NumberRecordBean;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.PurchseDetailTimeBean;
import com.dn.lotte.bean.PursezhlistBean;
import com.dn.lotte.bean.RemoveBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.bean.TzDetailsBean;
import com.dn.lotte.bean.UserInfoBean;
import com.dn.lotte.bean.Vipbean;
import com.dn.lotte.bean.WonderfulBannerBean;
import com.dn.lotte.utils.CommonUtils;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.lzy.okrx2.adapter.ObservableResponse;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ServerApi {


    //登录请求
    public static Observable<UserInfoBean> getLogoinData(HttpParams param) {

        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.login)//
                .params(param)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, UserInfoBean>() {
                    @Override
                    public UserInfoBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        UserInfoBean userInfoBean = gson.fromJson(stringResponse.body(), UserInfoBean.class);
                        return userInfoBean;
                    }
                });//
    }
    //登录请求
    public static Observable<UserInfoBean> getLogoinData2(HttpParams param) {

        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.login2)//
                .params(param)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, UserInfoBean>() {
                    @Override
                    public UserInfoBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        UserInfoBean userInfoBean = gson.fromJson(stringResponse.body(), UserInfoBean.class);
                        return userInfoBean;
                    }
                });//
    }
    //会员信息
    public static Observable<Vipbean> getVipInfoData() {

        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.vipInfo)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, Vipbean>() {
                    @Override
                    public Vipbean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        Vipbean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), Vipbean.class);
                        return userInfoBean;
                    }
                });//
    }

    //够菜中心请求
    public static Observable<PurchaseBean> getPurchaseData() {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.purchase)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchaseBean>() {
                    @Override
                    public PurchaseBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();

                        PurchaseBean purchaseBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchaseBean.class);
                        return purchaseBean;
                    }
                });//
    }

    //游戏分类
    public static Observable<PurchaseDetailTypeBean> getPurchaseTypeData(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.purchaseType)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchaseDetailTypeBean>() {
                    @Override
                    public PurchaseDetailTypeBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PurchaseDetailTypeBean purchaseBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchaseDetailTypeBean.class);
                        return purchaseBean;
                    }
                });
    }

    //游戏玩法
    public static Observable<PurchaseDetailGameBean> getPurchaseGameData(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.purchaseGame)
                .params(httpParams)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchaseDetailGameBean>() {
                    @Override
                    public PurchaseDetailGameBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PurchaseDetailGameBean purchaseBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchaseDetailGameBean.class);
                        return purchaseBean;
                    }
                });
    }


    //游戏玩法
    public static Observable<WonderfulBannerBean> getWonderfulBannerData() {

        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.homeBanner)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, WonderfulBannerBean>() {
                    @Override
                    public WonderfulBannerBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        WonderfulBannerBean purchaseBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), WonderfulBannerBean.class);
                        return purchaseBean;
                    }
                });
    }

    //投注记录
    public static Observable<BettingBean> getBettingData(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.betting)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, BettingBean>() {
                    @Override
                    public BettingBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        BettingBean bettingBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), BettingBean.class);
                        return bettingBean;
                    }
                });
    }

    //投注记录
    public static Observable<TzDetailsBean> gettzDetails(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.touzdetails)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, TzDetailsBean>() {
                    @Override
                    public TzDetailsBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        TzDetailsBean bettingBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), TzDetailsBean.class);
                        return bettingBean;
                    }
                });
    }

    //追号记录
    public static Observable<NumberRecordBean> getNumberRecordData(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.numberRecord)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, NumberRecordBean>() {
                    @Override
                    public NumberRecordBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        NumberRecordBean numberRecordBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), NumberRecordBean.class);
                        return numberRecordBean;
                    }
                });
    }

    //公告
    public static Observable<NoticeBean> getNoticeData(HttpParams httpParams) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.notice)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, NoticeBean>() {
                    @Override
                    public NoticeBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        NoticeBean noticeBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), NoticeBean.class);
                        return noticeBean;
                    }
                });
    }


    //公告详情
    public static Observable<NoticeDetailBean> getNoticeDetailData(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.noticeDetail)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, NoticeDetailBean>() {
                    @Override
                    public NoticeDetailBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        NoticeDetailBean noticeBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), NoticeDetailBean.class);
                        return noticeBean;
                    }
                });
    }

    //站内信收件箱
    public static Observable<InboxBean> getStandinbox(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.inbox)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, InboxBean>() {
                    @Override
                    public InboxBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        InboxBean noticeBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), InboxBean.class);
                        return noticeBean;
                    }
                });
    }

    //站内信发件箱
    public static Observable<InboxBean> getStandoutbox(HttpParams httpParams) {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.outbox)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, InboxBean>() {
                    @Override
                    public InboxBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        InboxBean noticeBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), InboxBean.class);
                        return noticeBean;
                    }
                });
    }

    //获取历史记录
    public static Observable<NewHistoryBean> getNewHistoryData() {

        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.newHistory)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, NewHistoryBean>() {
                    @Override
                    public NewHistoryBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        NewHistoryBean historyBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), NewHistoryBean.class);
                        return historyBean;
                    }
                });
    }

    //获取历史记录
    public static Observable<HistoryDetailBean> getHistoryDetailData(HttpParams httpParams) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.historyDetail)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, HistoryDetailBean>() {
                    @Override
                    public HistoryDetailBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        HistoryDetailBean historyBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), HistoryDetailBean.class);
                        return historyBean;
                    }
                });
    }

    //获取玩法
    public static Observable<PurchaseDetailNumberBean> getPurchaseDetailNumberData(HttpParams httpParams) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.purchaseDetailNumber)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchaseDetailNumberBean>() {
                    @Override
                    public PurchaseDetailNumberBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PurchaseDetailNumberBean purchaseDetailNumberBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchaseDetailNumberBean.class);
                        return purchaseDetailNumberBean;
                    }
                });
    }

    //获取追号数据
    public static Observable<PursezhlistBean> getpursecount(HttpParams httpParams) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.getzhlist)
                .converter(new StringConvert())
                .params(httpParams)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PursezhlistBean>() {
                    @Override
                    public PursezhlistBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PursezhlistBean purchaseDetailNumberBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PursezhlistBean.class);
                        return purchaseDetailNumberBean;
                    }
                });
    }

    //追号
    public static Observable<RemoveBean> setzhuihao(String string) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.mzhuihao)
                .converter(new StringConvert())
                .upJson(string)
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, RemoveBean>() {
                    @Override
                    public RemoveBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        RemoveBean purchaseDetailNumberBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), RemoveBean.class);
                        return purchaseDetailNumberBean;
                    }
                });
    }

    //获取玩法
    public static Observable<PurchaseDetailNumberBean> setBettingData(String josn) {

        return OkGo.<String>post(HttpURL.Base_Url+HttpURL.game)
                .upJson(josn)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchaseDetailNumberBean>() {
                    @Override
                    public PurchaseDetailNumberBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PurchaseDetailNumberBean purchaseDetailNumberBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchaseDetailNumberBean.class);
                        return purchaseDetailNumberBean;
                    }
                });
    }


    //获取彩票时间
    public static Observable<PurchseDetailTimeBean> getPurchaseDetailTime(HttpParams params) {

        return OkGo.<String>get(HttpURL.Base_Url+HttpURL.time)
                .params(params)
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, PurchseDetailTimeBean>() {
                    @Override
                    public PurchseDetailTimeBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        PurchseDetailTimeBean purchaseDetailNumberBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), PurchseDetailTimeBean.class);
                        return purchaseDetailNumberBean;
                    }
                });
    }

    //平台优惠
    public static Observable<DiscountBean> getDiscounts() {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.noticeyouhui)//
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<String>, DiscountBean>() {
                    @Override
                    public DiscountBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        DiscountBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), DiscountBean.class);
                        return userInfoBean;
                    }
                });//
    }    //平台优惠详情

    public static Observable<DiscountitemBean> getDiscountsdetails(HttpParams params) {
        return OkGo.<String>get(HttpURL.Base_Url + HttpURL.noticeyouhuidetail)//
                .params(params)
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())

                .map(new Function<Response<String>, DiscountitemBean>() {
                    @Override
                    public DiscountitemBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        DiscountitemBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), DiscountitemBean.class);
                        return userInfoBean;
                    }
                });
    } //验证账号

    public static Observable<SetSafequesBean> getverifynumber(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.verifynumber)//
                .params(params)
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())

                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return userInfoBean;
                    }
                });
    }
 //验证账号

    public static Observable<SetSafequesBean> getverifypswanswnor(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.verifypswanswnor)//
                .params(params)
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())

                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return userInfoBean;
                    }
                });
    } //设置新密码

    public static Observable<SetSafequesBean> setpsw(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.setpsw)//
                .params(params)
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())

                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return userInfoBean;
                    }
                });
    } //链接注册

    public static Observable<SetSafequesBean> Linklistz(HttpParams params) {
        return OkGo.<String>post(HttpURL.Base_Url + HttpURL.Linklistz)//
                .params(params)
                .converter(new StringConvert())//
                .adapt(new ObservableResponse<String>())//
                .subscribeOn(Schedulers.io())

                .map(new Function<Response<String>, SetSafequesBean>() {
                    @Override
                    public SetSafequesBean apply(@NonNull Response<String> stringResponse) throws Exception {
                        Gson gson = new Gson();
                        SetSafequesBean userInfoBean = gson.fromJson(CommonUtils.getSubStr(stringResponse.body()), SetSafequesBean.class);
                        return userInfoBean;
                    }
                });
    }

}
