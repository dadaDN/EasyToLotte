package com.dn.lotte.ui.purchase.model;

import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.ui.purchase.contract.PurchaseFSDetailContract;
import com.lzy.okgo.model.HttpParams;

import io.reactivex.Observable;

/**
 * Created by DN on 2017/10/12.
 */

public class PurchaseFSNewDetailModel implements PurchaseFSDetailContract.Model {
    @Override
    public Observable<PurchaseDetailNumberBean> getNumber(HttpParams httpParams) {
        return ServerApi.getPurchaseDetailNumberData(httpParams);
    }

    @Override
    public Observable<PurchaseDetailNumberBean> getBettingData(String json) {
        return ServerApi.setBettingData(json);
    }
}
