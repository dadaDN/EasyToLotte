package com.dn.lotte.ui.usercenter.activity;


import android.support.annotation.IdRes;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.bean.SubordinateBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/17.
 */

public class SendStrandActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rl_state)
    RelativeLayout rlState;
    @Bind(R.id.rb_up)
    RadioButton rbUp;
    @Bind(R.id.rb_down)
    RadioButton rbDown;
    @Bind(R.id.rg_state)
    RadioGroup rgState;
    @Bind(R.id.rl_people)
    RelativeLayout rlPeople;
    @Bind(R.id.tv_change_user)
    TextView tvChangeUser;
    @Bind(R.id.rv_people)
    RippleView rvPeople;
    @Bind(R.id.rl_visibility)
    RelativeLayout rlVisibility;
    @Bind(R.id.rl_title)
    RelativeLayout rlTitle;
    @Bind(R.id.them)
    ClearEditText them;
    @Bind(R.id.cet_content)
    ClearEditText cetContent;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    private int type = 1;
    private List<SubordinateBean.TableBean> mlist = new ArrayList<>();
    private QMUIListPopup mListPopup;
    private String userid;

    @Override
    public int getLayoutId() {
        return R.layout.activity_sendstrand;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle("发送消息");
        rgState.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (rbUp.getId() == checkedId) {
                    type = 1;
                    rlVisibility.setVisibility(View.GONE);
                } else if (rbDown.getId() == checkedId) {
                    type = 0;
                    rlVisibility.setVisibility(View.VISIBLE);
                }
            }
        });
        rvPeople.setOnRippleCompleteListener(this);
        okBtn.setOnRippleCompleteListener(this);
        getXlist();
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.rv_people:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(rippleView);
                break;
            case R.id.okBtn:
                if (type == 1) {//给上级发送
                    if (!StringUtils.isEmpty(them.getText().toString().trim())) {
                        if (!StringUtils.isEmpty(cetContent.getText().toString().trim())) {
                            HttpParams params = new HttpParams();
                            params.put("type", type);
                            params.put("name", "");
                            params.put("title", them.getText().toString().trim());
                            params.put("content", cetContent.getText().toString().trim());
                            getSend(params);
                        } else {
                            showShortToast("请输入内容");
                        }
                    } else {
                        showShortToast("请输入标题");
                    }

                } else if (type == 0) {//给下级发送
                    if (!tvChangeUser.getText().toString().equals("请选择您的下级")) {
                        if (!StringUtils.isEmpty(them.getText().toString().trim())) {
                            if (!StringUtils.isEmpty(cetContent.getText().toString().trim())) {
                                HttpParams params = new HttpParams();
                                params.put("type", type);
                                params.put("name", tvChangeUser.getText().toString());
                                params.put("title", them.getText().toString().trim());
                                params.put("content", cetContent.getText().toString().trim());
                                getSend(params);
                            } else {
                                showShortToast("请输入内容1");
                            }
                        } else {
                            showShortToast("请输入标题1");
                        }
                    } else {
                        showShortToast("请选择您的下级");

                    }
                }

                break;
        }
    }

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            List<String> date = new ArrayList<>();
            for (SubordinateBean.TableBean bean : mlist) {
                date.add(bean.getUsername());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, date);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 120), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvChangeUser.setText(mlist.get(i).getUsername());
                    userid = mlist.get(i).getId();
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getXlist() {
        UserCenterServerApi.getsubordinate()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SubordinateBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SubordinateBean subordinateBean) {
                        stopProgressDialog();
                        if (subordinateBean.getResult().equals("1")) {
                            mlist = subordinateBean.getTable();
                        } else {
                            showShortToast(subordinateBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    public void getSend(HttpParams params) {
        UserCenterServerApi.sendmessage(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")) {
                            them.setText("");
                            cetContent.setText("");
                        }
                        showShortToast(setSafequesBean.getReturnval());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
