package com.dn.lotte.ui.usercenter.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.bean.WithdrawalBankBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 绑定真实姓名需要一个弹窗后面加上
 * Created by ASUS on 2017/10/9.
 */

public class CardBundActivity extends BaseActivity {


    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_card_bound_bank)
    TextView tvCardBoundBank;
    @Bind(R.id.tv_bank)
    TextView tvBank;
    @Bind(R.id.rl_bound_bank)
    RelativeLayout rlBoundBank;
    @Bind(R.id.tv_card_bound_card)
    TextView tvCardBoundCard;
    @Bind(R.id.ed_bound_card)
    ClearEditText edBoundCard;
    @Bind(R.id.tv_bound_name)
    TextView tvBoundName;
    @Bind(R.id.tv_bound_realname)
    TextView tvBoundRealname;
    @Bind(R.id.tv_address)
    TextView tvAddress;
    @Bind(R.id.ed_adress)
    ClearEditText edAdress;
    @Bind(R.id.tv_withdrawle)
    TextView tvWithdrawle;
    @Bind(R.id.ed_withdrawal_password)
    ClearEditText edWithdrawalPassword;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    private List<WithdrawalBankBean.TableBean> mbanklist = new ArrayList<>();
    private String id1;
    private QMUIListPopup mListPopup;
    private Intent intent;

    @Override
    public int getLayoutId() {
        return R.layout.activity_boundcard;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_set_card_bound);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        String istruename = GlobalApplication.getInstance().getUserModel().getTable().get(0).getIstruename();
        if (istruename.equals("1")) {//1绑定0没有
            tvBoundRealname.setText(GlobalApplication.getInstance().getUserModel().getTable().get(0).getTruename());
        } else {
            showDialogstop();
        }
        getWithdrawalBank();
    }

    private void showDialogstop() {
        new QMUIDialog.MessageDialogBuilder(CardBundActivity.this)
                .setTitle("请先绑定真实姓名")
                .setMessage("现在绑定?")
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        intent = new Intent(getApplication(), Set_Bound_Realname.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();

    }
    @OnClick({R.id.rl_bound_bank, R.id.okBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_bound_bank:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(view);
                break;
            case R.id.okBtn:

                if (!tvBank.getText().toString().equals(R.string.usercenter_set_card_change_bank)) {
                    if (!StringUtils.isEmpty(edBoundCard.getText().toString().trim())) {
                        if (!tvBoundRealname.getText().toString().equals("取款姓名")) {
                            if (!StringUtils.isEmpty(edAdress.getText().toString().trim())) {
                                if (!StringUtils.isEmpty(edWithdrawalPassword.getText().toString())) {
                                    HttpParams params = new HttpParams();
                                    params.put("Bank", id1);
                                    params.put("BankName", tvBank.getText().toString());
                                    params.put("Address", edAdress.getText().toString().trim());
                                    params.put("PayAccount", edBoundCard.getText().toString().trim());
                                    params.put("PayName", tvBoundRealname.getText().toString());
                                    params.put("PayPwd", edWithdrawalPassword.getText().toString().trim());
                                    bundCardUrl(params);
                                } else {
                                    showShortToast("请输入取款密码");
                                }
                            } else {
                                showShortToast("开户地址不能为空");
                            }
                        } else {
                            showShortToast("请先绑定取款人姓名");
                        }
                    } else {
                        showShortToast("请填写银行卡号");
                    }
                } else {
                    showShortToast("请选择绑定银行");
                }
                break;
        }
    }

    //绑定卡号
    private void bundCardUrl(HttpParams params) {
        UserCenterServerApi.bundBankcar(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        showShortToast(setSafequesBean.getReturnval());
                        if (setSafequesBean.getResult().equals("1")){
                            finish();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }
    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            List<String> mlist = new ArrayList<>();
            for (WithdrawalBankBean.TableBean bean : mbanklist) {
                mlist.add(bean.getBank());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, mlist);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 180), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvBank.setText(mbanklist.get(i).getBank());
                    id1 = mbanklist.get(i).getId();
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getWithdrawalBank() {
        UserCenterServerApi.getBank()

                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        stopProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WithdrawalBankBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull WithdrawalBankBean withdrawalBankBean) {
                        if (withdrawalBankBean.getResult().equals("1")) {
                            mbanklist = withdrawalBankBean.getTable();
                        } else {
                            showShortToast(withdrawalBankBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

}
