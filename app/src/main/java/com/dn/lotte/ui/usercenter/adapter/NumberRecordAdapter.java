package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.NumberRecordBean;

import java.util.List;


/**
 * Created by DN on 2017/10/13.
 */

public class NumberRecordAdapter extends BaseQuickAdapter<NumberRecordBean.TableBean, BaseViewHolder> {

    public NumberRecordAdapter(@LayoutRes int layoutResId, @Nullable List<NumberRecordBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NumberRecordBean.TableBean bean) {
        helper.setText(R.id.tv_game_name, bean.getLotteryname())
                .setText(R.id.tv_user_name, bean.getUsername())
                .setText(R.id.tv_usr_number, "订单号：" + bean.getSsid())
                .setText(R.id.tv_do_time, "操作时间：" + bean.getStime())
                .setText(R.id.tv_game_record_number, "期号：" + bean.getStartissuenum())
                .setText(R.id.tv_game_record_time, "已追：" + bean.getStatename() + " 期")
                .setText(R.id.tv_stop, "中奖停止追号：" + bean.getIsstopname())
                .setText(R.id.tv_money, bean.getAlltotal()).setText(R.id.tv_game_money, bean.getFinishname());
//        helper.setImageResource(R.id.iv_content, CommonUtils.getImgLocation(bean.getLotteryname()));

    }
}
