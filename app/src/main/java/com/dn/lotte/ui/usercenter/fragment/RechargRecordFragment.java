package com.dn.lotte.ui.usercenter.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.RechargBean;
import com.dn.lotte.ui.usercenter.adapter.RechargAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 充值记录
 * Created by ASUS on 2017/10/18.
 */

public class RechargRecordFragment extends BaseFragment implements
        SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener, RippleView.OnRippleCompleteListener {
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    @Bind(R.id.tv_state)
    TextView tvState;
    @Bind(R.id.rl_start)
    RelativeLayout rlStart;
    @Bind(R.id.srl_state)
    RippleView srlState;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    private int page = 1, pagesize = 10;
    private RechargAdapter mAdapter;
    private List<RechargBean.TableBean> mlist = new ArrayList<>();
    private QMUIListPopup mListPopup;
    private String[] arr = {
            "所有类型", "待支付", "已完成"
    };
    private List<String> mstatelist = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_recharg;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        for (int i = 0; i < arr.length; i++) {
            mstatelist.add(arr[i]);
        }
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        srlState.setOnRippleCompleteListener(this);
        mAdapter = new RechargAdapter(R.layout.item_recharg, mlist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
//        mLoadMoreEndGone = true;
        mRecyclerView.setAdapter(mAdapter);
        isRefresh = 0;
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), "");
    }

    private void loadData(int page, String stime, String otime, String state) {
        if (stime.equals("开始时间")) {
            stime = "";
        }
        if (otime.equals("结束时间")) {
            otime = "";
        }
        if (state.equals("类别") || state.equals("所有类型")) {
            state = "";
        }
        if (state.equals("待支付")) {
            state = "0";
        }
        if (state.equals("已完成")) {
            state = "1";
        }
        HttpParams params = new HttpParams();
        params.put("page", page);
        params.put("pagesize", pagesize);
        params.put("d1", stime);
        params.put("d2", otime);
        params.put("state", state);
        UserCenterServerApi.recharg(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RechargBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RechargBean personageBean) {
                        stopProgressDialog();
                        if (personageBean.getResult().equals("1")) {
                            if (personageBean.getTable().size() < pagesize) {
                                mLoadMoreEndGone = true;
                                mAdapter.loadMoreEnd(mLoadMoreEndGone);
                            }
                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                mAdapter.addData(personageBean.getTable());
                            } else {
                                if (mSwipeLayout != null)
                                    mSwipeLayout.setRefreshing(false);
                                mAdapter.setNewData(personageBean.getTable());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        tvStarttime.setText("开始时间");
        tvOvertime.setText("结束时间");
        tvState.setText("类别");
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), "");
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        page = 1;
                        isRefresh=0;
                        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        page = 1;
                        isRefresh=0;
                        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());
                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
            case R.id.srl_state:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(rippleView);
                break;
        }
    }


    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, mstatelist);
            mListPopup = new QMUIListPopup(getActivity(), QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(getActivity(), 120), QMUIDisplayHelper.dp2px(getActivity(), 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvState.setText(mstatelist.get(i));
                    page = 1;
                    isRefresh=0;
                    loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

}
