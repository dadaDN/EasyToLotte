package com.dn.lotte.ui.usercenter.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.NoticeBean;
import com.dn.lotte.ui.usercenter.activity.MessageFragmentActivity;
import com.dn.lotte.ui.usercenter.adapter.NoticeAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░▄░░
 * ░░░░░░░░░▐█░░░░░░░░░░░▄▀▒▌░
 * ░░░░░░░░▐▀▒█░░░░░░░░▄▀▒▒▒▐
 * ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
 * ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
 * ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
 * ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒
 * ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
 * ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄
 * ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒
 * ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒
 * 单身狗就这样默默地看着你，一句话也不说。
 * 投注记录
 *
 * @author DN
 * @data 2017/10/13
 **/
public class MessageFragment extends BaseFragment
        implements SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    private int page = 1, pagesize = 10;
    private List<NoticeBean.TableBean> mList = new ArrayList<>();
    private NoticeAdapter mAdapter;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_betting;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new NoticeAdapter(R.layout.item_notice, mList);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
//        mLoadMoreEndGone = true;
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent=new Intent(getActivity(),MessageFragmentActivity.class);
                intent.putExtra("details",mAdapter.getData().get(position));
                startActivity(intent);
            }
        });
        isRefresh = 0;
        loadData();
    }

    private void loadData () {
        HttpParams httpParams = new HttpParams();
        httpParams.put("page", page);
        httpParams.put("pagesize", pagesize);
        ServerApi.getNoticeData(httpParams)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NoticeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull NoticeBean purchaseBean) {
                        if (purchaseBean.getResult().equals("1")) {
                            Log.i("DNlog", purchaseBean.getTable().size() + "");
                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                if (purchaseBean.getTable().size() < pagesize) {
                                    mLoadMoreEndGone = true;
                                    mAdapter.loadMoreEnd(mLoadMoreEndGone);
                                }
                                mAdapter.addData(purchaseBean.getTable());
                            } else {
                                mSwipeLayout.setRefreshing(false);
                                mAdapter.setNewData(purchaseBean.getTable());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        loadData();
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        loadData();
    }
}
