package com.dn.lotte.ui.mainFragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.ui.purchase.activity.PurchaseDetailActivity;
import com.dn.lotte.ui.purchase.adapter.PurchaseAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.DnToolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 111
 * 采购中心
 */
public class PurchaseFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    List<PurchaseBean.TableBean> mData = new ArrayList<>();
    private boolean mLoadMoreEndGone = false;
    private PurchaseAdapter mAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchase;
    }

    @Override
    public void initPresenter() {
//18762784351
    }

    @Override
    protected void initView() {
        mToolbar.setMainTitle(R.string.purchase_center);
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new PurchaseAdapter(R.layout.item_purchase, mData);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
//        mAdapter.setOnLoadMoreListener((BaseQuickAdapter.RequestLoadMoreListener) getActivity(), mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mLoadMoreEndGone = true;
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putString("id", mData.get(position).getId());
                bundle.putString("title", mData.get(position).getTitle());
                startActivity(PurchaseDetailActivity.class, bundle);
            }
        });
        ImageView imageView = new ImageView(getActivity());

        loadData();
    }

    private void loadData() {
        ServerApi.getPurchaseData()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull PurchaseBean purchaseBean) {
                        if (purchaseBean.getResult().equals("1")) {
                            mData = purchaseBean.getTable();
                            if (mSwipeLayout != null) mSwipeLayout.setRefreshing(false);
                            mAdapter.setNewData(mData);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
//                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
