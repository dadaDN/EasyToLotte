package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.BettingBean;
import com.dn.lotte.utils.CommonUtils;

import java.util.List;


/**
 * Created by DN on 2017/10/13.
 */

public class BettingAdapter extends BaseQuickAdapter<BettingBean.TableBean, BaseViewHolder> {

    public BettingAdapter(@LayoutRes int layoutResId, @Nullable List<BettingBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, BettingBean.TableBean bean) {
        helper.setText(R.id.tv_title, bean.getPlayname());
        if (bean.getState().equals("0")) {
            //未开奖
            helper.setText(R.id.tv_stutas, "未开奖");
        } else if (bean.getState().equals("1")) {
            helper.setText(R.id.tv_stutas, "已撤单");
        } else if (bean.getState().equals("2")) {
            helper.setText(R.id.tv_stutas, "未中奖");
        } else if (bean.getState().equals("3")) {
            helper.setText(R.id.tv_stutas, "已中奖");
//            helper.setText(R.id.tv_status_ok, bean.getTotal())
//                    .setVisible(R.id.linearStutas, true)
//                    .setVisible(R.id.tv_stutas, false);
        }
        helper.setImageResource(R.id.iv_head, CommonUtils.getImgLocation(bean.getLotteryname()));
        helper.setText(R.id.tv_stage, "第 " + bean.getIssuenum() + " 期");
        helper.setText(R.id.tv_time,bean.getStime2());
        helper.setText(R.id.tv_money,bean.getTotal());
    }
}
