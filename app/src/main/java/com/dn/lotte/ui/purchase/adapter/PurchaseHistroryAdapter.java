package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.HistoryDetailBean;
import com.easy.common.commonutils.StringUtils;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseHistroryAdapter extends BaseQuickAdapter<HistoryDetailBean.TableBean, BaseViewHolder> {

    public PurchaseHistroryAdapter(@LayoutRes int layoutResId, @Nullable List<HistoryDetailBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HistoryDetailBean.TableBean item) {
        helper.setText(R.id.tv_time_number, item.getTitle());
        FlexboxLayout flexboxLayout = helper.getView(R.id.countRecycler);
        showlabel(helper.getPosition(),flexboxLayout,item.getNumber());
    }

    /**
     * 显示标签
     *
     * @param flexboxLayout FlexboxLayout
     * @param str           “聪明，强，无敌” --> 以 ，隔开的字符串
     */
    private void showlabel(int position,FlexboxLayout flexboxLayout, String str) {
        flexboxLayout.removeAllViews();
        String[] allStr;

        if (!StringUtils.isEmpty(str)) {
            allStr = str.split(",");
            for (int i = 0; i < allStr.length; i++) {
                TextView textView = new TextView(mContext);
                textView.setText(allStr[i]);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(12);
                textView.setTextColor(mContext.getResources().getColor(R.color.white));
                if (position==0){
                    textView.setBackgroundResource(R.drawable.round_bule1);
                }else {
                    textView.setBackgroundResource(R.drawable.round_red1);
                }
//                textView.setBackgroundResource(R.drawable.round_bule1);
                flexboxLayout.addView(textView);
                ViewGroup.LayoutParams params = textView.getLayoutParams();
                if (params instanceof FlexboxLayout.LayoutParams) {
                    FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) params;
                    layoutParams.setMargins(0, 8, 30, 0);
                    textView.setLayoutParams(layoutParams);
                }
            }
        }
    }
}
