package com.dn.lotte.ui.usercenter.activity;

import android.app.DatePickerDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AgencyRecordBean;
import com.dn.lotte.bean.RecordStateBean;
import com.dn.lotte.ui.usercenter.adapter.AgencyMoneyRecordAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/12.
 */

public class AgencyMoneyRecordActivity extends BaseActivity implements RippleView.OnRippleCompleteListener, SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    @Bind(R.id.tv_state)
    TextView tvState;
    @Bind(R.id.rl_start)
    RelativeLayout rlStart;
    @Bind(R.id.srl_state)
    RippleView srlState;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private List<AgencyRecordBean.TableBean> mlist = new ArrayList<>();
    private List<RecordStateBean.TableBean> mrecordstate = new ArrayList<>();
    private AgencyMoneyRecordAdapter agencyMoneyRecordAdapter;
    private int page = 1;
    private QMUIListPopup mListPopup;
    private String recordstateid;
    private List<String> mstringlist;

    @Override
    public int getLayoutId() {
        return R.layout.activity_record;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.agency_account_record);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        srlState.setOnRippleCompleteListener(this);
        swipeLayout.setOnRefreshListener(this);
        rlStatementList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        agencyMoneyRecordAdapter = new AgencyMoneyRecordAdapter(R.layout.item_record, mlist);
        CustomLoadMoreView customLoadMoreView = new CustomLoadMoreView();
        agencyMoneyRecordAdapter.setLoadMoreView(customLoadMoreView);
        agencyMoneyRecordAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        agencyMoneyRecordAdapter.isFirstOnly(false);
        rlStatementList.setAdapter(agencyMoneyRecordAdapter);
        getUrl(page, "", "", "");
        getState();
    }

    @Override
    public void onRefresh() {
        page=1;
        tvStarttime.setText("开始时间");
        tvOvertime.setText("结束时间");
        tvState.setText("类别");
        getUrl(page, "", "", "");
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(AgencyMoneyRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        page=1;
                        if (tvOvertime.getText().toString().equals("结束时间")) {
                            getUrl(page, tvStarttime.getText().toString(), "", tvState.getText().toString());
                        } else {
                            getUrl(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());
                        }
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(AgencyMoneyRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        page=1;
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        if (tvStarttime.getText().toString().equals("开始时间")) {
                            getUrl(page, "", tvOvertime.getText().toString(), tvState.getText().toString());
                        } else {
                            getUrl(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), tvState.getText().toString());
                        }
                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
            case R.id.srl_state:
                  initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(rippleView);
                break;
        }
    }

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, mstringlist);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 120), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvState.setText(mrecordstate.get(i).getTitle());
                    recordstateid = mrecordstate.get(i).getId();
                    page=1;
                    getUrl(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), recordstateid);
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getUrl(int page, String stime, String otime, String state) {
        if (state.equals("类别")) {
            state="";
        }
        if (stime.equals("开始时间")){
            stime="";
        }
        if (otime.equals("结束时间")) {
            otime="";
        }
        HttpParams params = new HttpParams();
        params.put("page", page);
        params.put("pagesize", "10");
        params.put("d1", stime);
        params.put("d2", otime);
        params.put("tid", state);
        params.put("sel", "");
        params.put("u", "");
        getRecord(params);
    }

    private void getRecord(HttpParams params) {
        UserCenterServerApi.getRecord(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        stopProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AgencyRecordBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AgencyRecordBean agencyRecordBean) {
                        stopProgressDialog();
                        if (agencyRecordBean.getResult().equals("1")) {
                            mlist = agencyRecordBean.getTable();
                            swipeLayout.setRefreshing(false);
                            agencyMoneyRecordAdapter.setNewData(mlist);
                        } else {
                            showShortToast(agencyRecordBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    public void getState() {
        UserCenterServerApi.getRecordstate()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RecordStateBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RecordStateBean recordStateBean) {
                        stopProgressDialog();
                        if (recordStateBean.getResult().equals("1")) {
                            mrecordstate = recordStateBean.getTable();
                            mstringlist = new ArrayList<String>();
                            swipeLayout.setRefreshing(false);
                            for(RecordStateBean.TableBean bean:mrecordstate){
                                mstringlist.add(bean.getTitle());
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
