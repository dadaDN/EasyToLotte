package com.dn.lotte.ui.usercenter.activity;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.bean.TimeAndStateBean;
import com.dn.lotte.ui.usercenter.fragment.BumberRecordFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.flyco.tablayout.SlidingTabLayout;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░▄░░
 * ░░░░░░░░░▐█░░░░░░░░░░░▄▀▒▌░
 * ░░░░░░░░▐▀▒█░░░░░░░░▄▀▒▒▒▐
 * ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
 * ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
 * ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
 * ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒
 * ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
 * ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄
 * ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒
 * ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒
 * 单身狗就这样默默地看着你，一句话也不说。
 * 追号记录
 *
 * @author DN
 * @data 2017/10/13
 **/
public class NumberRecordActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {

    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.tv_starttime)
    TextView mTvStarttime;
    @Bind(R.id.imageView4)
    ImageView mImageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout mRlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView mSrlStarttime;
    @Bind(R.id.tv_overtime)
    TextView mTvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout mRlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView mSrlOvertime;
    @Bind(R.id.tv_state)
    TextView mTvState;
    @Bind(R.id.rl_start)
    RelativeLayout mRlStart;
    @Bind(R.id.srl_state)
    RippleView mSrlState;
    @Bind(R.id.tablayout)
    SlidingTabLayout mTablayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    private final String[] mTitles = {
            "全部状态", "已完成", "进行中"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private TimeAndStateBean timeAndStateBean;
    private List<PurchaseBean.TableBean> mgamelist = new ArrayList<>();
    private String gameid="";

    @Override
    public int getLayoutId() {
        return R.layout.activity_number_record;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        timeAndStateBean = new TimeAndStateBean();
        mToolbar.setMainTitle("追号记录");
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new BumberRecordFragment(""));
        mFragments.add(new BumberRecordFragment("0"));
        mFragments.add(new BumberRecordFragment("-1"));
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(mAdapter);
        mTablayout.setViewPager(mViewpager, mTitles, this, mFragments);
        mSrlStarttime.setOnRippleCompleteListener(this);
        mSrlOvertime.setOnRippleCompleteListener(this);
        mSrlState.setOnRippleCompleteListener(this);
        getAllGame();
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(NumberRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        mTvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        timeAndStateBean.setStime(mTvStarttime.getText().toString());
                        timeAndStateBean.setOtime(mTvOvertime.getText().toString());
                        timeAndStateBean.setState(gameid);
                        EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("timeandstate", timeAndStateBean));

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(NumberRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        mTvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        timeAndStateBean.setStime(mTvStarttime.getText().toString());
                        timeAndStateBean.setOtime(mTvOvertime.getText().toString());
                        timeAndStateBean.setState(gameid);
                        EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("timeandstate", timeAndStateBean));

                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
            case R.id.srl_state:
                if (mgamelist==null){
                    getAllGame();
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(rippleView);
                }else {
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(rippleView);
                }
                break;
        }
    }

    public void getAllGame() {
        ServerApi.getPurchaseData()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull PurchaseBean purchaseBean) {
                        stopProgressDialog();
                        if (purchaseBean.getResult().equals("1")) {
                            mgamelist = purchaseBean.getTable();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private QMUIListPopup mListPopup;

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            final List<String> data = new ArrayList<>();
            for (PurchaseBean.TableBean tableBean : mgamelist) {
                data.add(tableBean.getTitle());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 150), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    mTvState.setText(data.get(i));
                    gameid = mgamelist.get(i).getId();
                    timeAndStateBean.setStime(mTvStarttime.getText().toString());
                    timeAndStateBean.setOtime(mTvOvertime.getText().toString());
                    timeAndStateBean.setState(mgamelist.get(i).getId());
                    EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("timeandstate", timeAndStateBean));
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }
}
