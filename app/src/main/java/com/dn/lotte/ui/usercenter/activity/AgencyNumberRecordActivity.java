package com.dn.lotte.ui.usercenter.activity;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.bean.TimeAndStateBean;
import com.dn.lotte.ui.usercenter.fragment.AgencyNumberFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.flyco.tablayout.SlidingTabLayout;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 代理管理追号记录
 * Created by ASUS on 2017/10/19.
 */

public class AgencyNumberRecordActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {

    private final String[] mTitles = {
           "全部状态", "已完成", "进行中"};
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    @Bind(R.id.tv_state)
    TextView tvState;
    @Bind(R.id.rl_start)
    RelativeLayout rlStart;
    @Bind(R.id.srl_state)
    RippleView srlState;
    @Bind(R.id.tablayout)
    SlidingTabLayout tablayout;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    private QMUIListPopup mListPopup;
    private List<PurchaseBean.TableBean> mgamelist = new ArrayList<>();
    private TimeAndStateBean timeAndStateBean;
    private String gameid="";

    @Override
    public int getLayoutId() {
        return R.layout.activity_number_record;
    }

    @Override
    public void initPresenter() {


    }

    @Override
    public void initView() {
        tvState.setText("游戏");
        initTitle();
        toolbar.setMainTitle(R.string.agency_number_record);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        timeAndStateBean = new TimeAndStateBean();
        mFragments.add(new AgencyNumberFragment(""));
        mFragments.add(new AgencyNumberFragment("0"));
        mFragments.add(new AgencyNumberFragment("-1"));
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(mAdapter);
        tablayout.setViewPager(viewpager, mTitles, this, mFragments);
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        srlState.setOnRippleCompleteListener(this);
        getAllGame();
    }

    public void getAllGame() {
        ServerApi.getPurchaseData()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull PurchaseBean purchaseBean) {
                        stopProgressDialog();
                        if (purchaseBean.getResult().equals("1")) {
                            mgamelist = purchaseBean.getTable();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(AgencyNumberRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        timeAndStateBean.setStime(tvStarttime.getText().toString());
                        timeAndStateBean.setOtime(tvOvertime.getText().toString());
                        timeAndStateBean.setState(gameid);
                        EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("numbertimeandstate", timeAndStateBean));

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(AgencyNumberRecordActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        timeAndStateBean.setStime(tvStarttime.getText().toString());
                        timeAndStateBean.setOtime(tvOvertime.getText().toString());
                        timeAndStateBean.setState(gameid);
                        EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("numbertimeandstate", timeAndStateBean));

                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
            case R.id.srl_state:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(rippleView);
        }
    }

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            final List<String> data = new ArrayList<>();
            for (PurchaseBean.TableBean tableBean : mgamelist) {
                data.add(tableBean.getTitle());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 150), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvState.setText(data.get(i));
                    gameid = mgamelist.get(i).getId();
                    timeAndStateBean.setStime(tvStarttime.getText().toString());
                    timeAndStateBean.setOtime(tvOvertime.getText().toString());
                    timeAndStateBean.setState(gameid);
                    EventBus.getDefault().post(new EventBusBean<TimeAndStateBean>("numbertimeandstate", timeAndStateBean));

                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

}
