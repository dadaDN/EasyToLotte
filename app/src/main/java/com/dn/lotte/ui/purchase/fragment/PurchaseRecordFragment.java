package com.dn.lotte.ui.purchase.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.activity.BettingActivity;
import com.dn.lotte.ui.usercenter.fragment.AgencyNumberFragment;
import com.dn.lotte.ui.usercenter.fragment.BettingFragment;
import com.easy.common.base.BaseFragment;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 记录
 */
public class PurchaseRecordFragment extends BaseFragment {

    @Bind(R.id.tablayout)
    TabLayout mTablayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    private String mLid;
    private MyPagerAdapter mAdapter;
    private ArrayList<Fragment> mFragments;
    private String[] mTitles;

    public static PurchaseRecordFragment getInstance(String id) {
        PurchaseRecordFragment purchaseRecordFragment = new PurchaseRecordFragment();
        purchaseRecordFragment.mLid = id;
        return purchaseRecordFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchase_recored;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        mTitles = new String[]{
                "投注记录", "追号记录", "资金明细"};
        mFragments = new ArrayList<>();
        mFragments.add(new BettingFragment(""));
        mFragments.add(new AgencyNumberFragment(""));
        mFragments.add(new PurchaseAcconutDetailsFragment());
        mAdapter = new MyPagerAdapter(getChildFragmentManager());
        mViewpager.setAdapter(mAdapter);
        mViewpager.setOffscreenPageLimit(0);
        mTablayout.setTabMode(TabLayout.MODE_FIXED);
        mTablayout.setupWithViewPager(mViewpager);
//        mTablayout.setViewPager(mViewpager, mTitles, getActivity(), mFragments);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
