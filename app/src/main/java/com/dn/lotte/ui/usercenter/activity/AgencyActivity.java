package com.dn.lotte.ui.usercenter.activity;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ASUS on 2017/10/11.
 */

public class AgencyActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.iv_user_download)
    RelativeLayout ivUserDownload;
    @Bind(R.id.rl_agency_open_center)
    RelativeLayout rlAgencyOpenCenter;
    @Bind(R.id.iv_user_remark)
    RelativeLayout ivUserRemark;
    @Bind(R.id.rl_agency_group_statement)
    RelativeLayout rlAgencyGroupStatement;
    @Bind(R.id.iv_user_vip)
    RelativeLayout ivUserVip;
    @Bind(R.id.rl_agency_member_management)
    RelativeLayout rlAgencyMemberManagement;
    @Bind(R.id.iv_user_info)
    RelativeLayout ivUserInfo;
    @Bind(R.id.rl_agency_account_record)
    RelativeLayout rlAgencyAccountRecord;
    @Bind(R.id.iv_user_agency)
    RelativeLayout ivUserAgency;
    @Bind(R.id.rl_agency_game_record)
    RelativeLayout rlAgencyGameRecord;
    @Bind(R.id.iv_user_mesage)
    RelativeLayout ivUserMesage;
    @Bind(R.id.rl_agency_number_record)
    RelativeLayout rlAgencyNumberRecord;
    @Bind(R.id.iv_user_account_setting)
    RelativeLayout ivUserAccountSetting;
    @Bind(R.id.rl_agency_profit)
    RelativeLayout rlAgencyProfit;
    @Bind(R.id.iv_user_account_ratio)
    RelativeLayout ivUserAccountRatio;
    @Bind(R.id.rl_agency_ratio)
    RelativeLayout rlAgencyRatio;
    private Intent intent;

    @Override
    public int getLayoutId() {
        return R.layout.activity_remark;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_agency);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);

    }

    @OnClick({R.id.rl_agency_open_center, R.id.rl_agency_group_statement,
            R.id.rl_agency_member_management, R.id.rl_agency_account_record,
            R.id.rl_agency_game_record, R.id.rl_agency_number_record,
            R.id.rl_agency_profit, R.id.rl_agency_ratio})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_agency_open_center://开户中心
                startActivity(OpenChange.class);
                break;
            case R.id.rl_agency_group_statement://团队报表
                startActivity(GroupStatementActivity.class);
                break;
            case R.id.rl_agency_member_management://会员管理
                Intent intent = new Intent(getApplicationContext(), UserCenterManagementActivity.class);
                intent.putExtra("uid", "");
                startActivity(intent);
                break;
            case R.id.rl_agency_account_record://账变记录
                startActivity(AgencyMoneyRecordActivity.class);
                break;
            case R.id.rl_agency_game_record://游戏记录
                startActivity(AgencyBetActivity.class);
                break;
            case R.id.rl_agency_number_record://追号记录
                startActivity(AgencyNumberRecordActivity.class);
                break;
            case R.id.rl_agency_profit://代理分红
                startActivity(AgencyFHActivity.class);
                break;
            case R.id.rl_agency_ratio://我的契约
                startActivity(RatioActivity.class);
                break;
        }
    }

}
