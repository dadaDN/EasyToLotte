package com.dn.lotte.ui.usercenter.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.fragment.RechargRecordFragment;
import com.dn.lotte.ui.usercenter.fragment.TransferRecordFragment;
import com.dn.lotte.ui.usercenter.fragment.WithdrawalRecordFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 账户明细
 * Created by ASUS on 2017/10/18.
 */

public class AccountActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tablayout)
    SlidingTabLayout tablayout;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private final String[] mTitles = {
            "充值记录", "取款记录", "转账记录"
    };
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_betting;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        mToolbar.setMainTitle(R.string.usercenter_account_particulars);
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new RechargRecordFragment());
        mFragments.add(new WithdrawalRecordFragment());
        mFragments.add(new TransferRecordFragment());
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(mAdapter);
        tablayout.setViewPager(viewpager, mTitles, this, mFragments);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
