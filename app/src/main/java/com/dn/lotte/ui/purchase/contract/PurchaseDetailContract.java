package com.dn.lotte.ui.purchase.contract;

import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.PurchseDetailTimeBean;
import com.dn.lotte.bean.SelectedBean;
import com.easy.common.base.BaseModel;
import com.easy.common.base.BasePresenter;
import com.easy.common.base.BaseView;
import com.lzy.okgo.model.HttpParams;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by DN on 2017/10/12.
 */

public interface PurchaseDetailContract {


    interface Model extends BaseModel {
        Observable<PurchaseDetailTypeBean> getType(HttpParams httpParams);

        Observable<PurchaseDetailGameBean> getGame(HttpParams httpParams);

        Observable<PurchaseDetailNumberBean> getNumber(HttpParams httpParams);

        Observable<HistoryDetailBean> getHistory(HttpParams httpParams);

        Observable<PurchseDetailTimeBean> getGameTime(HttpParams httpParams);
    }

    interface View extends BaseView {
        void returnDetailTypeList(PurchaseDetailTypeBean typeBean);

        void returnDetailGameList(PurchaseDetailGameBean gameBean);

        void returnSelectData(PurchaseDetailGameBean.TableBean gameBean);

        void returnNumberData(PurchaseDetailNumberBean numberbean);

        void returnHistoryData(HistoryDetailBean historyDetailBean);

        void returnNumberSelectedData(int postion, List<SelectedBean> selectedBeanList);

        void returnGameTime(PurchseDetailTimeBean detailTimeBean);

    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void getTypeData(HttpParams httpParams,String id);

        public abstract void getGameData(HttpParams httpParams,String id);

        public abstract void getNumberData(HttpParams httpParams);

        public abstract void getHistoryData(HttpParams httpParams);

        public abstract void returnSelectData(PurchaseDetailGameBean.TableBean bean);

        public abstract void returnNumberSelectedData(int postion, List<SelectedBean> selectedBeanList);

        public abstract void getGameTime(HttpParams httpParams);
    }
}
