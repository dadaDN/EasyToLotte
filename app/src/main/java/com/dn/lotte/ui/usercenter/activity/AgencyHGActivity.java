package com.dn.lotte.ui.usercenter.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.fragment.MemberManagementFragment;
import com.dn.lotte.ui.usercenter.fragment.MemeberPointFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 代理管理中每个item的会员管理
 * Created by ASUS on 2017/11/5.
 */

public class AgencyHGActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tablayout)
    SlidingTabLayout tablayout;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private String[] mTitles = {"会员升点", "配额管理"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mMAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_message;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        Bundle extras = getIntent().getExtras();
        String selectData = extras.getString("membermanage");
        initTitle();
        mToolbar.setMainTitle("会员管理");
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new MemeberPointFragment(selectData));
        mFragments.add(new MemberManagementFragment(selectData));
        mMAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(mMAdapter);
        tablayout.setViewPager(viewpager, mTitles, this, mFragments);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

}
