package com.dn.lotte.ui.usercenter.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * chen
 * Created by ASUS on 2017/10/11.
 */

public class Personage_statement_Activity extends BaseActivity {
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_person_statement;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle(R.string.usercenter_statement);

    }

    @OnClick({R.id.rl_starttime, R.id.rl_overtime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_starttime:
                break;
            case R.id.rl_overtime:
                break;
        }
    }

}
