package com.dn.lotte.ui.wonderful.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.utils.CommonUtils;

import java.util.List;

/**
 * Created by DN on 2017/10/9.
 */

public class ToSelectAdapter extends BaseQuickAdapter<PurchaseBean.TableBean, BaseViewHolder> {

    public ToSelectAdapter(@LayoutRes int layoutResId, @Nullable List<PurchaseBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PurchaseBean.TableBean item) {
        helper.setText(R.id.text,item.getTitle());
        helper.setImageResource(R.id.iv_content, CommonUtils.getImgLocation(item.getTitle()));
    }
}
