package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.AgencybetBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencyBetAdapter extends BaseQuickAdapter<AgencybetBean.TableBean,BaseViewHolder> {

    private String statename;

    public AgencyBetAdapter(@LayoutRes int layoutResId, @Nullable List<AgencybetBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AgencybetBean.TableBean item) {
        if (item.getState().equals("0")){
            statename = "未开奖";
        }if (item.getState().equals("1")){
            statename = "已撤单";
        }if (item.getState().equals("2")){
            statename = "未中奖";
        }if (item.getState().equals("3")){
            statename = "已中奖";
        }
        helper.setText(R.id.tv_user_name,item.getUsername())
                .setText(R.id.tv_game_name,item.getLotteryname())
                .setText(R.id.tv_usr_number,"订单号："+item.getSsid())
                .setText(R.id.tv_do_time,"操作时间："+item.getStime())
                .setText(R.id.tv_game_record_number,"期号："+item.getIssuenum())
                .setText(R.id.tv_game_status,statename)
                .setText(R.id.tv_game_record_time,item.getPlayname())
                .setText(R.id.tv_game_money,item.getTotal()) .setText(R.id.tv_money,item.getRealget());

    }
}
