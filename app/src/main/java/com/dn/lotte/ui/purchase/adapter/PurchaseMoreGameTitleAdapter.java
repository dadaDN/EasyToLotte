package com.dn.lotte.ui.purchase.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;
import com.dn.lotte.utils.CommonUtils;
import com.dn.lotte.utils.NotifyDataState;
import com.easy.common.baseapp.BaseApplication;
import com.easy.common.commonutils.ToastUitl;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 *
 */

public class PurchaseMoreGameTitleAdapter extends BaseQuickAdapter<PurchaseDetailGameBean.TableBean, BaseViewHolder> {
    public PurchaseMoreGameTitleAdapter(@LayoutRes int layoutResId, @Nullable List<PurchaseDetailGameBean.TableBean> data) {
        super(layoutResId, data);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void convert(final BaseViewHolder helper, final PurchaseDetailGameBean.TableBean item) {
        helper.setText(R.id.cb_item, item.getTitle());
        if (item.isChecked()) {
            helper.setBackgroundRes(R.id.cb_item, R.drawable.radiobutton_background_checked);
            helper.setTextColor(R.id.cb_item, BaseApplication.getAppResources().getColor(R.color.white));
        } else {
            helper.setBackgroundRes(R.id.cb_item, R.drawable.radiobutton_background_unchecked);
            helper.setTextColor(R.id.cb_item, R.color.F2);
        }
    }

}
