package com.dn.lotte.ui.mainFragment;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.HistoryBean;
import com.dn.lotte.ui.history.activity.KjDetailsActivity;
import com.dn.lotte.ui.usercenter.adapter.HistoryAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.DnToolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 历史开奖
 */
public class HistoryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rv_list)
    RecyclerView rvList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private boolean mLoadMoreEndGone = false;
    private HistoryAdapter mAdapter;
    private List<HistoryBean.TableBean> mlist = new ArrayList<>();

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_history;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        toolbar.setMainTitle(R.string.usercenter_history);
        swipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        swipeLayout.setOnRefreshListener(this);
        rvList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new HistoryAdapter(R.layout.item_history, mlist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mLoadMoreEndGone = true;
        rvList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(getActivity(), KjDetailsActivity.class);
                intent.putExtra("id", mAdapter.getData().get(position).getLotteryid());
                intent.putExtra("name", mAdapter.getData().get(position).getLotteryname());
                startActivity(intent);
            }
        });
        loadData();
    }

    private void loadData() {
        UserCenterServerApi.getallHistory()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HistoryBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull HistoryBean historyBean) {
                        stopProgressDialog();
                        if (historyBean.getResult().equals("1")) {
                            mlist = historyBean.getTable();
                            if (swipeLayout != null) swipeLayout.setRefreshing(false);
                            mAdapter.setNewData(mlist);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }


    @Override
    public void onRefresh() {
        loadData();
    }
}
