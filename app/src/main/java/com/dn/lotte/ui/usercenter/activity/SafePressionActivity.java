package com.dn.lotte.ui.usercenter.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.SafeQuesstionBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


/**
 * 安全问题
 * Created by ASUS on 2017/10/9.
 */

public class SafePressionActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rl_question)
    RelativeLayout rlQuestion;
    @Bind(R.id.tv_quesstion)
    TextView tvQuesstion;
    @Bind(R.id.rl_quessition)
    RelativeLayout rlQuessition;
    @Bind(R.id.rl_answer)
    RelativeLayout rlAnswer;
    @Bind(R.id.ed_new_psw)
    ClearEditText edNewPsw;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.rl_change_quesstion)
    RelativeLayout rlChangeQuesstion;
    private List<SafeQuesstionBean.TableBean> mlist = new ArrayList<>();



    @Override
    public int getLayoutId() {
        return R.layout.activity_safe;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_set_safe_question);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        GetsafeQuesstion();

    }

    //请求安全问题
    private void GetsafeQuesstion() {
        UserCenterServerApi.getSafeQuesstion()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SafeQuesstionBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull SafeQuesstionBean safeQuesstionBean) {
                        if (safeQuesstionBean.getResult().equals("1")) {
                            mlist = safeQuesstionBean.getTable();
                        } else {
                            showShortToast(safeQuesstionBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @OnClick({R.id.rl_change_quesstion, R.id.okBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_change_quesstion:
                if (mlist == null) {
                    GetsafeQuesstion();
                } else {
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(view);
                }
                break;
            case R.id.okBtn:
                if (!StringUtils.isEmpty(edNewPsw.getText().toString().trim())) {
                    if (!tvQuesstion.getText().toString().equals("请选择安全问题")) {
                        HttpParams params = new HttpParams();
                        params.put("question", tvQuesstion.getText().toString());
                        params.put("answer", edNewPsw.getText().toString().trim());
                        setQuesstion(params);
                    } else {
                        showShortToast("请选择安全问题");
                    }
                } else {
                    showShortToast("请输入问题答案");
                }
                break;
        }
    }

    //修改密保问题
    public void setQuesstion(HttpParams params) {
        UserCenterServerApi.setSafeQuesstion(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")){
                            finish();
                        }
                        showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });

    }

    private QMUIListPopup mListPopup;

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            final List<String> data = new ArrayList<>();
            for (SafeQuesstionBean.TableBean tableBean : mlist) {
                data.add(tableBean.getTitle());
            }

            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 280), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvQuesstion.setText(data.get(i));
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }


}
