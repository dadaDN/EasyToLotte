package com.dn.lotte.ui.usercenter.activity;

import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;

import butterknife.Bind;

/**
 * Created by ASUS on 2017/12/11.
 */

public class OpenChange extends BaseActivity implements RippleView.OnRippleCompleteListener{
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.putong_open)
    RippleView putongOpen;
    @Bind(R.id.lian_open)
    RippleView lianOpen;

    @Override
    public int getLayoutId() {
        return R.layout.activityopenchange;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("我的消息");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        putongOpen.setOnRippleCompleteListener(this);
        lianOpen.setOnRippleCompleteListener(this);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.putong_open://普通开户
                startActivity(AgencyOpenCenterActivity.class);
                break;
            case R.id.lian_open://链接开户
                startActivity(LinkActivity.class);
                break;

        }
    }
}
