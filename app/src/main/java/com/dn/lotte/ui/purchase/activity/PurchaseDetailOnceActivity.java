package com.dn.lotte.ui.purchase.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.dn.lotte.R;
import com.dn.lotte.ui.purchase.fragment.PurchaseBettingFragment;
import com.dn.lotte.ui.purchase.fragment.PurchaseGoFragment;
import com.dn.lotte.ui.purchase.fragment.PurchaseHistoryRecFragment;
import com.dn.lotte.ui.purchase.fragment.PurchaseRecordFragment;
import com.dn.lotte.utils.CommonUtils;
import com.easy.common.base.BaseActivity;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.Bind;

public class PurchaseDetailOnceActivity extends BaseActivity {

    @Bind(R.id.tablayout)
    SegmentTabLayout mTabLayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    @Bind(R.id.rigthImg)
    RoundedImageView mRightImg;
    private final String[] mTitles = {
            "开奖", "走势", "投注", "记录"};

    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private String mTitle;

    @Override
    public int getLayoutId() {
        return R.layout.activity_purchase_detail_once;
    }

    @Override
    public void initPresenter() {
    }

    @Override
    public void initView() {
        Bundle extras = getIntent().getExtras();
        String id = extras.getString("id");
        mTitle = extras.getString("title");
        mRightImg.setImageResource(CommonUtils.getImgLocation(mTitle));
        mFragments.add(PurchaseHistoryRecFragment.getInstance(id));
        mFragments.add(PurchaseGoFragment.getInstance(id));
        mFragments.add(PurchaseBettingFragment.getInstance(id,mTitle));
        mFragments.add(PurchaseRecordFragment.getInstance(id));
        initTab();
    }

    private void initTab() {
        mViewpager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mViewpager.setOffscreenPageLimit(2);
        mTabLayout.setTabData(mTitles);
        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mViewpager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewpager.setCurrentItem(2);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
