package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.RechargBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class RechargAdapter extends BaseQuickAdapter<RechargBean.TableBean,BaseViewHolder> {
    public RechargAdapter(@LayoutRes int layoutResId, @Nullable List<RechargBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RechargBean.TableBean item) {
        helper.setText(R.id.tv_username,item.getSsid())
                .setText(R.id.tv_recharge, item.getInmoney())
                .setText(R.id.tv_withdrawal, item.getBankname())
                .setText(R.id.tv_consume, item.getDzmoney())
                .setText(R.id.tv_activity, item.getStatename())
                .setText(R.id.tv_return, item.getStime());
    }
}
