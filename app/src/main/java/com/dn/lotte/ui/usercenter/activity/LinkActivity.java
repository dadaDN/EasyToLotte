package com.dn.lotte.ui.usercenter.activity;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/11.
 */

public class LinkActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rl_open_state)
    RelativeLayout rlOpenState;
    @Bind(R.id.time)
    ClearEditText time;
    @Bind(R.id.rl_name)
    RelativeLayout rlName;
    @Bind(R.id.userName)
    ClearEditText userName;
    @Bind(R.id.rl_rebate)
    RelativeLayout rlRebate;
    @Bind(R.id.et_rebate)
    ClearEditText etRebate;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.okBtn_link)
    RippleView okBtnLink;

    @Override
    public int getLayoutId() {
        return R.layout.activity_link;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("链接开户");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        okBtn.setOnRippleCompleteListener(this);
        okBtnLink.setOnRippleCompleteListener(this);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn:
                if (!StringUtils.isEmpty(time.getText().toString())) {
                    if (!StringUtils.isEmpty(userName.getText().toString())) {
                        if (Double.parseDouble(userName.getText().toString()) > 0) {
                            if (!StringUtils.isEmpty(etRebate.getText().toString())) {
                                if (Double.parseDouble(etRebate.getText().toString()) < 10) {
                                    showShortToast("开户返点不能小于10");
                                } else {
                                    HttpParams params = new HttpParams();
                                    params.put("times", userName.getText().toString().trim());
                                    params.put("yxtime", time.getText().toString().trim());
                                    params.put("point", Double.parseDouble(etRebate.getText().toString().trim()));
                                    loadOpen(params);
                                }
                            } else {
                                showShortToast("请输入返点");
                            }
                        } else {
                            showShortToast("次数大于0");
                        }
                    } else {
                        showShortToast("请填写使用次数");
                    }
                } else {
                    showShortToast("请填写有效期数");
                }
                break;
            case R.id.okBtn_link:
                startActivity(Link_LieActivity.class);
                break;
        }
    }

    private void loadOpen(HttpParams params) {
        UserCenterServerApi.getLink(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

}
