package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PursezhlistBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/26.
 */

public class PursueAdapter extends BaseQuickAdapter<PursezhlistBean.TableBean, BaseViewHolder> {
    public PursueAdapter(@LayoutRes int layoutResId, @Nullable List<PursezhlistBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PursezhlistBean.TableBean item) {
        helper.setText(R.id.tv_number, item.getSn())
                .setText(R.id.tv_bei, item.getCount())
                .setText(R.id.tv_money, item.getPrice())
                .setText(R.id.tv_time, item.getStime());
    }
}
