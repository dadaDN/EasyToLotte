package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.SelectedBean;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class NumberAdapter extends BaseQuickAdapter<SelectedBean, BaseViewHolder> {

    public NumberAdapter(@LayoutRes int layoutResId, @Nullable List<SelectedBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SelectedBean item) {
        helper.setText(R.id.cb_item, item.getName()).addOnClickListener(R.id.cb_item);
        helper.setChecked(R.id.cb_item, item.isCheched());

    }

}
