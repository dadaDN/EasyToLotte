package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.AccountDetailsBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class AccountDetailsAdapter extends BaseQuickAdapter<AccountDetailsBean.TableBean, BaseViewHolder> {
    public AccountDetailsAdapter(@LayoutRes int layoutResId, @Nullable List<AccountDetailsBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AccountDetailsBean.TableBean item) {
        helper.setText(R.id.tv_number, item.getSsid())
                .setText(R.id.tv_money, item.getMoneychange())
                .setText(R.id.tv_qmoney, item.getMoneyago())
                .setText(R.id.tv_hmoney, item.getMoneyafter())
                .setText(R.id.tv_time, item.getStime())
                .setText(R.id.tv_sort, "操作类别："+item.getCodename())
                .setText(R.id.tv_state, "状态："+item.getRemark());
    }
}
