package com.dn.lotte.ui.purchase.presenter;

import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.ui.purchase.contract.PurchaseFSDetailContract;
import com.dn.lotte.utils.CommonUtils;
import com.easy.common.commonutils.ToastUitl;
import com.lzy.okgo.model.HttpParams;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by DN on 2017/10/12.
 */


public class PurchaseFSDetailPresenter extends PurchaseFSDetailContract.Presenter {

    @Override
    public void getNumberData(HttpParams httpParams, final String lid, final String titleName) {
        if (CommonUtils.getGameNumberData(titleName + lid) != null) {
            mView.returnNumberData(CommonUtils.getGameNumberData(titleName + lid));
        } else {
            mModel.getNumber(httpParams).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<PurchaseDetailNumberBean>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
//                        mView.showLoading("");
                        }

                        @Override
                        public void onNext(@NonNull PurchaseDetailNumberBean numberBean) {

                            mView.returnNumberData(numberBean);
                            CommonUtils.setGameNumberData(numberBean, titleName + lid);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }

    }

    @Override
    public void setPlayData(String palyCode, String selectNumber, String playStrPos, boolean isSha) {
        mView.returnPlayData(palyCode, selectNumber, playStrPos, isSha);
    }

    @Override
    public void getBettingData(String json) {
        mModel.getBettingData(json)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        mView.showLoading("");
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseDetailNumberBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull PurchaseDetailNumberBean numberBean) {
                        if (numberBean.getResult().equals("1")) {
                            mView.returenBettingData(numberBean);
                        } else
                            ToastUitl.showShort(numberBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.stopLoading();
                    }

                    @Override
                    public void onComplete() {
                        mView.stopLoading();

                    }
                });

    }

    @Override
    public void setNumberData(int number) {
        mView.returnSelectNumberData(number);
    }
}
