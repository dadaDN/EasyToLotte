package com.dn.lotte.ui.usercenter.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.AgencyRecordBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class AgencyMoneyRecordAdapter extends BaseQuickAdapter<AgencyRecordBean.TableBean, BaseViewHolder> {
    public AgencyMoneyRecordAdapter(int item_groupadapter, List<AgencyRecordBean.TableBean> mlist) {
        super(item_groupadapter, mlist);
    }

    @Override
    protected void convert(BaseViewHolder helper, AgencyRecordBean.TableBean item) {
        helper.setText(R.id.tv_num, item.getSsid())
                .setText(R.id.tv_name,item.getUsername())
                .setText(R.id.tv_bmoney,item.getMoneychange() )
                .setText(R.id.tv_qmoney,item.getMoneyago() )
                .setText(R.id.tv_hmoney, item.getMoneyafter())
                .setText(R.id.tv_state,item.getCodename());
    }
}
