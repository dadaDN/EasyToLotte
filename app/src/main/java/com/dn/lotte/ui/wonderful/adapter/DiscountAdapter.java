package com.dn.lotte.ui.wonderful.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.DiscountBean;

import java.util.List;

/**
 * Created by ASUS on 2017/12/1.
 */

public class DiscountAdapter extends BaseQuickAdapter<DiscountBean.TableBean, BaseViewHolder> {
    public DiscountAdapter(@LayoutRes int layoutResId, @Nullable List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DiscountBean.TableBean item) {
        helper.setText(R.id.tv_title, item.getTitle());
    }
}
