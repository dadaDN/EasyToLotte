package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.WithdrawalBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class WithdrawalRecordAdapter extends BaseQuickAdapter<WithdrawalBean.TableBean,BaseViewHolder> {
    public WithdrawalRecordAdapter(@LayoutRes int layoutResId, @Nullable List<WithdrawalBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, WithdrawalBean.TableBean item) {
        helper.setText(R.id.tv_username,item.getSsid())
                .setText(R.id.tv_recharge,item.getTpayname())
                .setText(R.id.tv_withdrawal,item.getPaybank())
                .setText(R.id.tv_consume,item.getCashmoney())
                .setText(R.id.tv_profit, item.getStime())
                .setText(R.id.tv_return, item.getMoney())
                .setText(R.id.tv_activity, item.getStatename());


    }
}
