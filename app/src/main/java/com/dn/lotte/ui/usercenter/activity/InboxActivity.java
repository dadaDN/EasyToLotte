package com.dn.lotte.ui.usercenter.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.InboxBean;
import com.dn.lotte.ui.usercenter.adapter.InboxAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/17.
 */

public class InboxActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,
        BaseQuickAdapter.RequestLoadMoreListener
        , RippleView.OnRippleCompleteListener {
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    private int page = 1, pagesize = 10;
    private List<InboxBean.TableBean> mList = new ArrayList<>();
    private InboxAdapter mAdapter;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    private String type;

    @Override
    public int getLayoutId() {
        return R.layout.inbox;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        type = getIntent().getStringExtra("type");
        toolbar.setMainTitle(type);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new InboxAdapter(R.layout.item_inbox, mList);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mRecyclerView.setAdapter(mAdapter);
        isRefresh = 0;
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, InboxDeatilsActivity.class);
                intent.putExtra("details", mAdapter.getData().get(position));
                intent.putExtra("name", type);
                startActivity(intent);
            }
        });
    }


    private void loadData(int pag, String stime, String otime) {
        HttpParams httpParams = new HttpParams();
        httpParams.put("page", pag);
        httpParams.put("pagesize", pagesize);
        if (stime.equals("开始时间")) {
            stime = "";
        }
        if (otime.equals("结束时间")) {
            otime = "";
        }
        httpParams.put("d1", stime);
        httpParams.put("d2", otime);
        if (type.equals("收件箱")) {
            getInbox(httpParams);
        } else if (type.equals("发件箱")) {
            getOutbox(httpParams);
        }
    }

    //发件箱
    private void getOutbox(HttpParams httpParams) {
        ServerApi.getStandoutbox(httpParams)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InboxBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull InboxBean inboxBean) {
                        stopProgressDialog();
                        if (inboxBean.getResult().equals("1")) {
                            Log.i("DNlog", inboxBean.getTable() + "");
                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                if (inboxBean.getTable().size() < pagesize) {
                                    mLoadMoreEndGone = true;
                                    mAdapter.loadMoreEnd(mLoadMoreEndGone);
                                }
                                mAdapter.addData(inboxBean.getTable());
                            } else {
                                mSwipeLayout.setRefreshing(false);
                                mAdapter.setNewData(inboxBean.getTable());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //收件箱
    private void getInbox(HttpParams httpParams) {
        ServerApi.getStandinbox(httpParams)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InboxBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull InboxBean inboxBean) {
                        if (inboxBean.getResult().equals("1")) {
                            Log.i("DNlog", inboxBean.getTable() + "");
                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                if (inboxBean.getTable().size() < pagesize) {
                                    mLoadMoreEndGone = true;
                                    mAdapter.loadMoreEnd(mLoadMoreEndGone);
                                }
                                mAdapter.addData(inboxBean.getTable());
                            } else {
                                mSwipeLayout.setRefreshing(false);
                                mAdapter.setNewData(inboxBean.getTable());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
    }


    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(InboxActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        isRefresh = 0;
                        page = 1;
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(InboxActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        isRefresh = 0;
                        page = 1;
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        loadData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
        }
    }
}
