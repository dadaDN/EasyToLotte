package com.dn.lotte.ui.purchase.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AccountDetailsBean;
import com.dn.lotte.bean.RecordStateBean;
import com.dn.lotte.ui.usercenter.activity.AccountDetailsActivity;
import com.dn.lotte.ui.usercenter.adapter.AccountDetailsAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class PurchaseAcconutDetailsFragment extends BaseFragment implements RippleView.OnRippleCompleteListener,
        SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    @Bind(R.id.tv_state)
    TextView tvState;
    @Bind(R.id.rl_start)
    RelativeLayout rlStart;
    @Bind(R.id.srl_state)
    RippleView srlState;
    @Bind(R.id.rl_statement_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    private int page = 1, pagesize = 10;
    private List<RecordStateBean.TableBean> mstatelist = new ArrayList<>();
    private List<AccountDetailsBean.TableBean> maccountlist = new ArrayList<>();
    private QMUIListPopup mListPopup;
    private AccountDetailsAdapter mAdapter;
    private String stateid = "";

    private String mLid;

    public static PurchaseAcconutDetailsFragment getInstance(String id) {
        PurchaseAcconutDetailsFragment purchaseGoFragment = new PurchaseAcconutDetailsFragment();
        purchaseGoFragment.mLid = id;
        return purchaseGoFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchase_account;
    }

    @Override
    public void initPresenter() {

    }

    protected boolean mIsVisible = false;

    /**
     * `
     * 在这里实现Fragment数据的缓加载.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mIsVisible = true;
            onVisible();
        } else {
            mIsVisible = false;
            onInvisible();
        }
    }

    protected void onInvisible() {
    }

    /**
     * 显示时加载数据,需要这样的使用
     * 注意声明 isPrepared，先初始化
     * 生命周期会先执行 setUserVisibleHint 再执行onActivityCreated
     * 在 onActivityCreated 之后第一次显示加载数据，只加载一次
     */
    protected void onVisible() {
        loadData();
    }

    @Override
    public void initView() {
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        srlState.setOnRippleCompleteListener(this);
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new AccountDetailsAdapter(R.layout.item_account, maccountlist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mLoadMoreEndGone = true;
        mRecyclerView.setAdapter(mAdapter);
        isRefresh = 0;
//        loadData();
        getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), stateid);

    }

    private void loadData() {
        UserCenterServerApi.getRecordstate()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RecordStateBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RecordStateBean recordStateBean) {
                        stopProgressDialog();
                        if (recordStateBean.getResult().equals("1")) {
                            mstatelist = recordStateBean.getTable();
                        } else {
                            showShortToast(recordStateBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }


    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            List<String> mlist = new ArrayList<>();
            for (RecordStateBean.TableBean bean : mstatelist) {
                mlist.add(bean.getTitle());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_list_item, mlist);
            mListPopup = new QMUIListPopup(getActivity(), QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(getActivity(), 120), QMUIDisplayHelper.dp2px(getActivity(), 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvState.setText(mstatelist.get(i).getTitle());
                    stateid = mstatelist.get(i).getId();
                    page = 1;
                    isRefresh = 0;
                    getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), stateid);
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        tvStarttime.setText("开始时间");
        tvOvertime.setText("结束时间");
        tvState.setText("类别");
        getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), "");
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), stateid);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        page = 1;
                        isRefresh = 0;
                        getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), stateid);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        page = 1;
                        isRefresh = 0;
                        getAccountDate(page, tvStarttime.getText().toString(), tvOvertime.getText().toString(), stateid);
                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
            case R.id.srl_state:
                if (mstatelist == null) {
                    loadData();
                } else {
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(rippleView);
                }
                break;
        }
    }

    public void getAccountDate(int pag, String stime, String otime, String state) {
        if (stime.equals("开始时间")) {
            stime = "";
        }
        if (otime.equals("结束时间")) {
            otime = "";
        }
        HttpParams params = new HttpParams();
        params.put("page", pag);
        params.put("pagesize", pagesize);
        params.put("d1", stime);
        params.put("d2", otime);
        params.put("tid", state);
        params.put("sel", "");
        params.put("u", "");
        UserCenterServerApi.accountDetails(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AccountDetailsBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AccountDetailsBean accountDetailsBean) {
                        stopProgressDialog();
                        if (accountDetailsBean.getResult().equals("1")) {
                            if (accountDetailsBean.getTable().size() < pagesize) {
                                mLoadMoreEndGone = true;
                                mAdapter.loadMoreEnd(mLoadMoreEndGone);
                            }
                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                mAdapter.addData(accountDetailsBean.getTable());
                            } else {
                                if (mSwipeLayout != null) mSwipeLayout.setRefreshing(false);
                                mAdapter.setNewData(accountDetailsBean.getTable());
                            }

                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
