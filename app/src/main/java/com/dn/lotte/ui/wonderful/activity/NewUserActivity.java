package com.dn.lotte.ui.wonderful.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.app.HttpURL;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.widget.ClearEditText;
import com.dn.lotte.widget.InputMethodRelativeLayout;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.BitmapCallback;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/11.
 */

public class NewUserActivity extends BaseActivity implements RippleView.OnRippleCompleteListener, InputMethodRelativeLayout.OnSizeChangedListenner {
    @Bind(R.id.iv_img_backgroud)
    ImageView ivImgBackgroud;
    @Bind(R.id.de_frm_backgroud)
    FrameLayout deFrmBackgroud;
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.logoImg)
    ImageView logoImg;
    @Bind(R.id.codeValue)
    TextView codeValue;
    @Bind(R.id.view)
    View view;
    @Bind(R.id.userName)
    ClearEditText userName;
    @Bind(R.id.usercenterPassword)
    TextView usercenterPassword;
    @Bind(R.id.pass)
    ClearEditText pass;
    @Bind(R.id.newusercenterPassword)
    TextView newusercenterPassword;
    @Bind(R.id.newpass)
    ClearEditText newpass;
    @Bind(R.id.uncode)
    TextView uncode;
    @Bind(R.id.et_code)
    ClearEditText etCode;
    @Bind(R.id.codeImg)
    ImageView codeImg;
    @Bind(R.id.VerifyPassword)
    TextView VerifyPassword;
    @Bind(R.id.password_once)
    EditText passwordOnce;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.content)
    LinearLayout content;
    @Bind(R.id.scroll)
    ScrollView scroll;
    @Bind(R.id.loginlayout)
    InputMethodRelativeLayout loginlayout;
    @Bind(R.id.cet_link)
    ClearEditText cetLink;
    private String mUserNameStr;
    private String mPassStr;
    private String newmPassStr;
    private String cetLinkstr;

    @Override
    public int getLayoutId() {
        return R.layout.activity_newuser;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("注册");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        //获取验证码
        getCode();
        okBtn.setOnRippleCompleteListener(this);
        loginlayout.setOnSizeChangedListenner(this);
        codeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();
            }
        });
    }

    @Override
    public void onSizeChange(boolean paramBoolean, int w, int h) {
        if (paramBoolean) {// 键盘弹出时
//            mPositionLiear.setVisibility(View.GONE);
            ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(content, "translationY", 0.0f, -10);
            mAnimatorTranslateY.setDuration(300);
            mAnimatorTranslateY.setInterpolator(new LinearInterpolator());
            mAnimatorTranslateY.start();
            toolbar.setVisibility(View.GONE);
            zoomIn(logoImg, 0.6f, 10);
        } else { // 键盘隐藏时
//            mPositionLiear.setVisibility(View.VISIBLE);
            ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(content, "translationY", content.getTranslationY(), 0);
            mAnimatorTranslateY.setDuration(300);
            mAnimatorTranslateY.setInterpolator(new LinearInterpolator());
            mAnimatorTranslateY.start();
            //键盘收回后，logo恢复原来大小，位置同样回到初始位置
            zoomOut(logoImg, 0.6f);
            toolbar.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn:
                mUserNameStr = userName.getText().toString().trim();
                mPassStr = pass.getText().toString().trim();
                newmPassStr = newpass.getText().toString().trim();
                cetLinkstr = cetLink.getText().toString().trim();
                String mCodeStr = etCode.getText().toString().trim();
                if (!StringUtils.isEmpty(mUserNameStr)) {
                    if (!StringUtils.isEmpty(mPassStr)) {
                        if (!StringUtils.isEmpty(mCodeStr)) {
                            if (!StringUtils.isEmpty(newmPassStr)) {
                                if (newmPassStr.equals(mPassStr)) {
                                    if (!StringUtils.isEmpty(cetLinkstr)) {
                                        HttpParams params=new HttpParams();
                                        params.put("u",cetLinkstr);
                                        params.put("name",mUserNameStr);
                                        params.put("pass",mPassStr);
                                        params.put("code",mCodeStr);
                                        loadlink(params);
                                    }else {
                                        showShortToast("请输入开户链接");
                                    }
                                } else {
                                    showShortToast("俩次密码不一致");
                                }
                            } else {
                                showShortToast("请再次输入密码");
                            }

                        } else
                            showShortToast("请输入验证码");
                    } else {
                        showShortToast("请输入密码");
                    }
                } else
                    showShortToast("请输入用户名");
                break;
        }
    }

    private void loadlink(HttpParams params) {
        ServerApi.Linklistz(params)
        .doOnSubscribe(new Consumer<Disposable>() {
            @Override
            public void accept(@NonNull Disposable disposable) throws Exception {
                startProgressDialog();
            }
        })
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<SetSafequesBean>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                stopProgressDialog();
            }

            @Override
            public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                stopProgressDialog();
                showShortToast(setSafequesBean.getReturnval());

            }

            @Override
            public void onError(@NonNull Throwable e) {
                stopProgressDialog();

            }

            @Override
            public void onComplete() {
                stopProgressDialog();
            }
        })
        ;

    }

    /**
     * 缩小
     *
     * @param view
     */
    public static void zoomIn(final View view, float scale, float dist) {
        view.setPivotY(view.getHeight());
        view.setPivotX(view.getWidth() / 2);
        AnimatorSet mAnimatorSet = new AnimatorSet();
        ObjectAnimator mAnimatorScaleX = ObjectAnimator.ofFloat(view, "scaleX", 1.0f, scale);
        ObjectAnimator mAnimatorScaleY = ObjectAnimator.ofFloat(view, "scaleY", 1.0f, scale);
        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(view, "translationY", 0.0f, -dist);

        mAnimatorSet.play(mAnimatorTranslateY).with(mAnimatorScaleX);
        mAnimatorSet.play(mAnimatorScaleX).with(mAnimatorScaleY);
        mAnimatorSet.setDuration(300);
        mAnimatorSet.start();
    }

    /**
     * f放大
     *
     * @param view
     */
    public static void zoomOut(final View view, float scale) {
        view.setPivotY(view.getHeight());
        view.setPivotX(view.getWidth() / 2);
        AnimatorSet mAnimatorSet = new AnimatorSet();

        ObjectAnimator mAnimatorScaleX = ObjectAnimator.ofFloat(view, "scaleX", scale, 1.0f);
        ObjectAnimator mAnimatorScaleY = ObjectAnimator.ofFloat(view, "scaleY", scale, 1.0f);
        ObjectAnimator mAnimatorTranslateY = ObjectAnimator.ofFloat(view, "translationY", view.getTranslationY(), 0);

        mAnimatorSet.play(mAnimatorTranslateY).with(mAnimatorScaleX);
        mAnimatorSet.play(mAnimatorScaleX).with(mAnimatorScaleY);
        mAnimatorSet.setDuration(300);
        mAnimatorSet.start();
    }

    private void getCode() {
        OkGo.<Bitmap>get(HttpURL.Base_Url + HttpURL.code)
                .execute(new BitmapCallback() {
                    @Override
                    public void onSuccess(Response<Bitmap> response) {
                        if (codeImg != null) codeImg.setImageBitmap(response.body());
                    }
                });
    }

}
