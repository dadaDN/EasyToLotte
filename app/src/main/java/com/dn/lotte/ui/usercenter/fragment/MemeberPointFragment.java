package com.dn.lotte.ui.usercenter.fragment;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.MemberManageBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.RippleView;
import com.google.gson.Gson;
import com.lzy.okgo.model.HttpParams;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 会员升点
 * Created by ASUS on 2017/11/5.
 */

public class MemeberPointFragment extends BaseFragment implements RippleView.OnRippleCompleteListener {


    @Bind(R.id.tv_zhanghao)
    TextView tvZhanghao;
    @Bind(R.id.now_fandian)
    TextView nowFandian;
    @Bind(R.id.tv_moeny)
    TextView tvMoeny;
    @Bind(R.id.rl_jian)
    RelativeLayout rlJian;
    @Bind(R.id.et_fandian)
    EditText etFandian;
    @Bind(R.id.rl_jia)
    RelativeLayout rlJia;
    @Bind(R.id.betting)
    RippleView betting;
    private String selectdata;
    private DecimalFormat df11;
    private String[] strings;
    private String point;
    private String[] zjpoint;
    private MemberManageBean.TableBean memberManageBean;

    public MemeberPointFragment(String selectData) {
        this.selectdata = selectData;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_agencyhg;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        point = GlobalApplication.getInstance().getUserModel().getTable().get(0).getPoint();
        Gson gson = new Gson();
        memberManageBean = gson.fromJson(selectdata, MemberManageBean.TableBean.class);
        tvZhanghao.setText(memberManageBean.getUsername());
        nowFandian.setText(memberManageBean.getPoint());
        tvMoeny.setText(memberManageBean.getMoney());
        zjpoint = point.split("%");
        strings = memberManageBean.getPoint().split("%");
        etFandian.setText(strings[0]);
        // 格式化小数
        df11 = new DecimalFormat("0.00");
        betting.setOnRippleCompleteListener(this);
    }

    @OnClick({R.id.rl_jian, R.id.rl_jia})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_jian:
                String point = etFandian.getText().toString();
                if (point.equals("")) {
                    point = 0.00 + "";
                }
                if (Double.parseDouble(point) <= 0) {
                    showShortToast("最低返点为0");
                } else {
                    etFandian.setText(df11.format(Double.parseDouble(point) - 0.1));
                }
                break;
            case R.id.rl_jia:
                String point1 = etFandian.getText().toString();
                if (point1.equals("")) {
                    point1 = 0.00 + "";
                }
                if (Double.parseDouble(point1) >= Double.parseDouble(zjpoint[0])) {
                    showShortToast("不能大于自身返点");
                } else {
                    etFandian.setText(df11.format(Double.parseDouble(point1) + 0.1));
                }
                break;
        }
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.betting:
                loadMemberPoint();
                break;
        }
    }

    private void loadMemberPoint() {
        String a = memberManageBean.getId();
        String b = etFandian.getText().toString();
        Log.e("fandian", a + "---------" + b);
        HttpParams params = new HttpParams();
        params.put("userid", memberManageBean.getId());
        params.put("point", Double.parseDouble(etFandian.getText().toString())*10);
        UserCenterServerApi.memberpoint(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean kefuBean) {
                        stopProgressDialog();
                        showShortToast(kefuBean.getReturnval());
                        if (kefuBean.getResult().equals("1")){
                            EventBus.getDefault().post(new EventBusBean<String>("pointsuccess", "pointsuccess"));
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
