package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.AgencynFHBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencyFHAdapte extends BaseQuickAdapter<AgencynFHBean.TableBean,BaseViewHolder> {
    public AgencyFHAdapte(@LayoutRes int layoutResId, @Nullable List<AgencynFHBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AgencynFHBean.TableBean item) {
        helper.setText(R.id.tv_stare_time,item.getStarttime())
        .setText(R.id.tv_end_time,item.getEndtime())
                .setText(R.id.tv_tuandui_yingkui,item.getTotal())
                .setText(R.id.rv_all_money,item.getBet())
                .setText(R.id.tv_fh_bilv,item.getPer())
                .setText(R.id.tv_fh_money,item.getInmoney());
    }
}
