package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.GroupStatementBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class GroupStatementAdapter extends BaseQuickAdapter<GroupStatementBean.TableBean, BaseViewHolder> {

    public GroupStatementAdapter(@LayoutRes int layoutResId, @Nullable List<GroupStatementBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, GroupStatementBean.TableBean item) {
        helper.setText(R.id.tv_name,item.getUsername());
        helper.setText(R.id.tv_recharge,item.getCharge());//充值
        helper.setText(R.id.tv_withdrawal,item.getGetcash());//提现
        helper.setText(R.id.tv_consume,item.getMoney());//余额
        helper.setText(R.id.tv_award,item.getWin());//派奖
        helper.setText(R.id.tv_return,item.getPoint());//返点
        helper.setText(R.id.tv_activity,item.getGive());//活动
        helper.setText(R.id.tv_profit,item.getTotal());//盈利
        helper.setText(R.id.tv_buy,item.getBet());//代购
        helper.setText(R.id.tv_other,item.getOther());//其他
        helper.setText(R.id.tv_chedan,item.getCancellation());//其他
        helper.setText(R.id.tv_daymoney,item.getDdmoney());//单单日结
    }
}
