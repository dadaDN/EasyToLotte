package com.dn.lotte.ui.usercenter.activity;

import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.bean.InboxBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.TimeUtil;
import com.easy.common.commonwidget.DnToolbar;

import java.util.Date;

import butterknife.Bind;

/**
 * Created by ASUS on 2017/10/17.
 */

public class InboxDeatilsActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.c)
    TextView c;
    @Bind(R.id.tv_data)
    TextView tvData;
    @Bind(R.id.rl_time)
    RelativeLayout rlTime;
    @Bind(R.id.ll_title)
    LinearLayout llTitle;
    @Bind(R.id.tv_notice_content)
    TextView tvNoticeContent;
    @Bind(R.id.tv_xinxi)
    TextView tvXinxi;
    @Bind(R.id.tv_time)
    TextView tvTime;
    @Bind(R.id.tv_name)
    TextView tvName;
    private InboxBean.TableBean details;

    @Override
    public int getLayoutId() {
        return R.layout.activitygonggao;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        details = (InboxBean.TableBean) getIntent().getSerializableExtra("details");
        String name = getIntent().getStringExtra("name");
        toolbar.setMainTitle(name);
        Date dateByFormat = TimeUtil.getDateByFormat(details.getStime(), TimeUtil.dateFormatYMDHMS);
        tvData.setText(TimeUtil.getStringByFormat(dateByFormat, "MM") + "月");
        tvData.setText(TimeUtil.getStringByFormat(dateByFormat, "dd"));
        tvNoticeContent.setText(details.getTitle());
        tvTime.setText(details.getStime());
//        tvXinxi.setText(details.getContents());
        tvXinxi.setText(Html.fromHtml(details.getContents()));
        llTitle.setVisibility(View.GONE);
        if (name.equals("发件箱")) {
            tvName.setText("收件人："+details.getReceivename());
        } else {
            tvName.setText("发件人："+details.getSendname());
        }

    }

}
