package com.dn.lotte.ui.usercenter.activity;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.OpenRankBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 开户
 * Created by ASUS on 2017/10/11.
 */

public class AgencyOpenCenterActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rl_open_state)
    RelativeLayout rlOpenState;
    @Bind(R.id.rv_change_state)
    TextView rvChangeState;
    @Bind(R.id.rl_change_open_state)
    RippleView rlChangeOpenState;
    @Bind(R.id.rl_name)
    RelativeLayout rlName;
    @Bind(R.id.userName)
    ClearEditText userName;
    @Bind(R.id.rl_rebate)
    RelativeLayout rlRebate;
    @Bind(R.id.et_rebate)
    ClearEditText etRebate;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.rl_password)
    RelativeLayout rlPassword;
    @Bind(R.id.et_member_psw)
    ClearEditText etMemberPsw;
    private List<OpenRankBean.TableBean> mranklist = new ArrayList<>();
    private String sid;
    private QMUIListPopup mListPopup;

    @Override
    public int getLayoutId() {
        return R.layout.activity_center_open_agency;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.agency_open_center);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        getOpenState();
        okBtn.setOnRippleCompleteListener(this);
        rlChangeOpenState.setOnRippleCompleteListener(this);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn:
                if (!rvChangeState.getText().toString().equals("请选择开户类别")) {
                    if (!StringUtils.isEmpty(userName.getText().toString().trim())) {
                        if (!StringUtils.isEmpty(etRebate.getText().toString().trim())) {
                            if (!StringUtils.isEmpty(etMemberPsw.getText().toString().trim())) {
                                if (Double.parseDouble(etRebate.getText().toString())<12){
                                    showShortToast("开户返点不能小于12");
                                }else {
                                    Log.e("kaihu", sid);
                                    HttpParams params = new HttpParams();
                                    params.put("type", sid);
                                    params.put("name", userName.getText().toString().trim());
                                    params.put("pwd", etMemberPsw.getText().toString().trim());
                                    params.put("point", Double.parseDouble(etRebate.getText().toString().trim()) * 10);
                                    openMember(params);
                                }
                            } else {
                                showShortToast("请输入密码");
                            }
                        } else {
                            showShortToast("请输入返点");
                        }
                    } else {
                        showShortToast("请输入用户名");
                    }
                } else {
                    showShortToast("请选择开户类别");
                }
                break;
            case R.id.rl_change_open_state:
                if (mranklist.size() != 0) {
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(rippleView);
                } else {
                    showShortToast("没有开户权限");
                }
                break;
        }
    }

    private void openMember(HttpParams params) {
            UserCenterServerApi.getOpenMember(params)
                    .doOnSubscribe(new Consumer<Disposable>() {
                        @Override
                        public void accept(@NonNull Disposable disposable) throws Exception {
                            startProgressDialog();
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SetSafequesBean>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            stopProgressDialog();
                        }

                        @Override
                        public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                            stopProgressDialog();
                            showShortToast(setSafequesBean.getReturnval());
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            stopProgressDialog();

                        }

                        @Override
                        public void onComplete() {
                            stopProgressDialog();

                        }
                    });
    }

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            List<String> mlist=new ArrayList<>();
            for (OpenRankBean.TableBean bean:mranklist){
                mlist.add(bean.getName());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, mlist);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 200), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    rvChangeState.setText(mranklist.get(i).getName());
                    sid = mranklist.get(i).getId();
                    Log.e("kaihui1",sid);
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getOpenState() {
        UserCenterServerApi.getOpenrank()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OpenRankBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull OpenRankBean openRankBean) {
                        stopProgressDialog();
                        if (openRankBean.getResult().equals("1")) {
                            mranklist = openRankBean.getTable();
                        } else {
                            showShortToast("没有开户权限");
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
