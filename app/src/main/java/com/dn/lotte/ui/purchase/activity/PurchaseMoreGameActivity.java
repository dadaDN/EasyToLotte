package com.dn.lotte.ui.purchase.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.MainActivity;
import com.dn.lotte.R;
import com.dn.lotte.app.AppConstant;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.ui.purchase.adapter.PurchaseMoreGameAdapter;
import com.dn.lotte.ui.usercenter.adapter.BettingAdapter;
import com.dn.lotte.utils.CommonUtils;
import com.dn.lotte.utils.NotifyDataState;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.dn.lotte.app.AppConstant.REFRESHBETTINGTAB;

public class PurchaseMoreGameActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView mLeftTitle;
    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private PurchaseMoreGameAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_purchase_more_game;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        mToolbar.setMainTitle("选择玩法");
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        Bundle bundle = getIntent().getExtras();
        String mID = bundle.getString("id");
        PurchaseDetailTypeBean gameTypeDataList = CommonUtils.getGameTypeDataList(mID);
        List<PurchaseDetailTypeBean.TableBean> gameList = gameTypeDataList.getTable();

        PurchaseDetailGameBean gameData = CommonUtils.getGameData("DNID" + mID);
        List<PurchaseDetailGameBean.TableBean> mList = gameData.getTable();

        List<PurchaseDetailGameBean.TableBean> selectGameList = CommonUtils.getSelectGameList("DNID" + mID);

        // 通知列表选中的数据
        NotifyDataState.updataMoreGameChecked(mList, selectGameList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new PurchaseMoreGameAdapter(R.layout.item_purchse_more_game, gameList, mList, mID);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mRecyclerView.setAdapter(mAdapter);
        mToolbar.setOnLeftTitleClickListener(new DnToolbar.OnLeftTitleClickListener() {
            @Override
            public void onLeftClick(View view) {
                EventBus.getDefault().post(new EventBusBean<>(AppConstant.REFRESHBETTINGTAB, ""));
                finish();

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            EventBus.getDefault().post(new EventBusBean<>(AppConstant.REFRESHBETTINGTAB, ""));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
