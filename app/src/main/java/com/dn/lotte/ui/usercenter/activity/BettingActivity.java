package com.dn.lotte.ui.usercenter.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.fragment.BettingFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 追号记录
 */
public class BettingActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.tablayout)
    SlidingTabLayout mTablayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    private final String[] mTitles = {
            "全部", "未开奖", "未中奖"
            , "已中奖"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_betting;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        mToolbar.setMainTitle("投注记录");
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new BettingFragment(""));
        mFragments.add(new BettingFragment("0"));
        mFragments.add(new BettingFragment("2"));
        mFragments.add(new BettingFragment("3"));
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(mAdapter);
        mTablayout.setViewPager(mViewpager,mTitles,this,mFragments);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
