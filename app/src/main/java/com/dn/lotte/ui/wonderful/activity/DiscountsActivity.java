package com.dn.lotte.ui.wonderful.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.DiscountBean;
import com.dn.lotte.ui.wonderful.adapter.DiscountAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 平台优惠活动
 * Created by ASUS on 2017/12/1.
 */

public class DiscountsActivity extends BaseActivity
        implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.rv_list)
    RecyclerView rvList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    private List<DiscountBean.TableBean> mlist = new ArrayList<>();
    private int isRefresh = 0;
    private DiscountAdapter discountAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_discounts;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("平台优惠");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        swipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        swipeLayout.setOnRefreshListener(this);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        discountAdapter = new DiscountAdapter(R.layout.item_discount, mlist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        discountAdapter.setLoadMoreView(mLoadMoreView);
        discountAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        discountAdapter.isFirstOnly(false);
        rvList.setAdapter(discountAdapter);
        discountAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(DiscountsActivity.this, DiscountItemActivity.class);
                intent.putExtra("tid", discountAdapter.getData().get(position).getId());
                startActivity(intent);
            }
        });
        isRefresh = 0;
        loadDate();

    }

    private void loadDate() {
        ServerApi.getDiscounts()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DiscountBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull DiscountBean discountBean) {
                        stopProgressDialog();
                        if (discountBean.getResult().equals("1")) {
                            mlist = discountBean.getTable();
                            if (isRefresh == 1) {
                                discountAdapter.loadMoreComplete();
                                discountAdapter.addData(discountBean.getTable());
                            } else {
                                swipeLayout.setRefreshing(false);
                                discountAdapter.setNewData(discountBean.getTable());
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });

    }

    @Override
    public void onRefresh() {
        loadDate();
    }

}
