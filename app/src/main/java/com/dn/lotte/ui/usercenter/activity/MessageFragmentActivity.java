package com.dn.lotte.ui.usercenter.activity;

import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.NoticeBean;
import com.dn.lotte.bean.NoticeDetailBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.TimeUtil;
import com.easy.common.commonwidget.DnToolbar;
import com.lzy.okgo.model.HttpParams;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/17.
 */

public class MessageFragmentActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.c)
    TextView c;
    @Bind(R.id.tv_data)
    TextView tvData;
    @Bind(R.id.rl_time)
    RelativeLayout rlTime;
    @Bind(R.id.ll_title)
    LinearLayout llTitle;
    @Bind(R.id.tv_notice_content)
    TextView tvNoticeContent;
    @Bind(R.id.tv_xinxi)
    TextView tvXinxi;
    @Bind(R.id.tv_time)
    TextView tvTime;
    @Bind(R.id.tv_name)
    TextView tvName;
    private NoticeBean.TableBean bean;
    private List<NoticeDetailBean.TableBean> details;

    @Override
    public int getLayoutId() {
        return R.layout.activitygonggao;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle("公告");
        bean = (NoticeBean.TableBean) getIntent().getSerializableExtra("details");
        Date dateByFormat = TimeUtil.getDateByFormat(bean.getStime(), TimeUtil.dateFormatYMDHMS);
        tvData.setText(TimeUtil.getStringByFormat(dateByFormat, "MM") + "月");
        tvData.setText(TimeUtil.getStringByFormat(dateByFormat, "dd"));
        tvNoticeContent.setText(bean.getTitle());
        tvTime.setText(bean.getStime());
        tvName.setVisibility(View.GONE);
        HttpParams params = new HttpParams();
        params.put("Id", bean.getId());
        getGgdate(params);
    }

    private void getGgdate(HttpParams params) {
        ServerApi.getNoticeDetailData(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NoticeDetailBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull NoticeDetailBean noticeDetailBean) {
                        stopProgressDialog();
                        if (noticeDetailBean.getResult().equals("1")) {
                            setDetails(noticeDetailBean.getTable());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();


                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });

    }

    public void setDetails(List<NoticeDetailBean.TableBean> details) {
        this.details = details;
        //tvXinxi.setText(details.get(0).getContent());
        tvXinxi.setText(Html.fromHtml(details.get(0).getContent()==null?"":details.get(0).getContent())==null?"":Html.fromHtml(details.get(0).getContent()==null?"":details.get(0).getContent()));
    }
}
