package com.dn.lotte.ui.purchase.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.activity.KeFuActivity;
import com.easy.common.base.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PurchaseGoFragment extends BaseFragment {
    @Bind(R.id.webview)
    WebView mWebview;
    private String mLid;

    public static PurchaseGoFragment getInstance(String id) {
        PurchaseGoFragment purchaseGoFragment = new PurchaseGoFragment();
        purchaseGoFragment.mLid = id;
        return purchaseGoFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchase_go;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        String url = "http://www.happy916.com/chart.html?lid=" + mLid;
        WebSettings webSettings = mWebview.getSettings();
        // 设置WebView属性，能够执行Javascript脚本
        webSettings.setJavaScriptEnabled(true);
        // 设置可以访问文件
        webSettings.setAllowFileAccess(true);
        // 设置支持缩放
        webSettings.setBuiltInZoomControls(true);
        // 加载需要显示的网页
        mWebview.loadUrl(url);
        // 设置Web视图
        // wv.setWebViewClient(new webViewClient ());
        mWebview.setWebViewClient(new webViewClient());
    }

    // Web视图
    private class webViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
//            showShortToast("ddddd");
            return true;

        }

    }
}
