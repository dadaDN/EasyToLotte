package com.dn.lotte.ui.usercenter.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.CunModeBean;
import com.dn.lotte.bean.RemoveBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.UpgradeInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 设置
 * Created by ASUS on 2017/10/9.
 */

public class SettingActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.iv_user_set_alter_login_password)
    RelativeLayout ivUserSetAlterLoginPassword;
    @Bind(R.id.relative_user_set_alter_login_password)
    RelativeLayout relativeUserSetAlterLoginPassword;
    @Bind(R.id.iv_user_set_setting_money_password)
    RelativeLayout ivUserSetSettingMoneyPassword;
    @Bind(R.id.relative_user_set_setting_money_password)
    RelativeLayout relativeUserSetSettingMoneyPassword;
    @Bind(R.id.iv_set_safe_question)
    RelativeLayout ivSetSafeQuestion;
    @Bind(R.id.relative_set_safe_question)
    RelativeLayout relativeSetSafeQuestion;
    @Bind(R.id.iv_set_card_bound)
    RelativeLayout ivSetCardBound;
    @Bind(R.id.relative_set_card_bound)
    RelativeLayout relativeSetCardBound;
    @Bind(R.id.iv_set_inform_setting)
    RelativeLayout ivSetInformSetting;
    @Bind(R.id.relative_set_inform_setting)
    RelativeLayout relativeSetInformSetting;
    @Bind(R.id.iv_set_money_mode)
    RelativeLayout ivSetMoneyMode;
    @Bind(R.id.relative_set_money_mode)
    RelativeLayout relativeSetMoneyMode;
    @Bind(R.id.iv_set_examine_update)
    RelativeLayout ivSetExamineUpdate;
    @Bind(R.id.relative_set_examine_update)
    RelativeLayout relativeSetExamineUpdate;
    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.iv_user_realname)
    RelativeLayout ivUserRealname;
    @Bind(R.id.rl_user_realname)
    RelativeLayout rlUserRealname;
    @Bind(R.id.tv_money_mode)
    TextView tvMoneyMode;
    @Bind(R.id.ll_money_mode)
    LinearLayout llMoneyMode;
    private Intent intent;
    private QMUIListPopup mListPopup;
    private String[] arr = {
            "元", "角", "分", "厘"
    };

    @Override
    public int getLayoutId() {
        return R.layout.user_activitysetting;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        mToolbar.setMainTitle(R.string.usercenter_account_setting);
        CunModeBean moneyMode = GlobalApplication.getInstance().getMoneyMode();
        if (moneyMode != null) {
            tvMoneyMode.setText(moneyMode.getMode());
        }
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
    }

    @OnClick({R.id.rl_user_realname, R.id.ll_money_mode, R.id.toolbar, R.id.relative_user_set_alter_login_password, R.id.relative_user_set_setting_money_password, R.id.relative_set_safe_question, R.id.relative_set_card_bound, R.id.relative_set_inform_setting, R.id.relative_set_examine_update})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                break;
            case R.id.relative_user_set_alter_login_password://修改登录密码
                startActivity(AmendPassword.class);
                break;
            case R.id.relative_user_set_setting_money_password://设置资金密码
                startActivity( SetMoneyPasswordActivity.class);
                break;
            case R.id.relative_set_safe_question://安全问题
                if (GlobalApplication.getInstance().getUserModel()!=null&&GlobalApplication.getInstance().getUserModel().getTable()!=null&&GlobalApplication.getInstance().getUserModel().getTable().get(0).getIsanswer()!=null){
                    if (GlobalApplication.getInstance().getUserModel().getTable().get(0).getIsanswer().equals("1")) {
                        showShortToast("您已经绑定过密保了");
                    } else {
                        startActivity( SafePressionActivity.class);
                    }
                }else {
                    startActivity( SafePressionActivity.class);
                }

                break;
            case R.id.relative_set_card_bound://卡号绑定
                startActivity(CardBundActivity.class);
                break;
            case R.id.rl_user_realname:
                String   istruename = GlobalApplication.getInstance().getUserModel().getTable().get(0).getIstruename();
                if (istruename.equals("1")) {
                    showShortToast("已绑定过姓名");
                } else {
                    startActivity(Set_Bound_Realname.class);
                }
                break;
            case R.id.relative_set_inform_setting:
                break;
            case R.id.ll_money_mode:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(view);
                break;
            case R.id.relative_set_examine_update://检查最新版本
                Beta.checkUpgrade();
                Beta.canShowUpgradeActs.add(SettingActivity.class);
                UpgradeInfo upgradeInfo = Beta.getUpgradeInfo();
//                getVersion();
                if (upgradeInfo == null) {
                    showShortToast("已经是最新版本");
                }
                break;
        }
    }

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            final List<String> data = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                data.add(arr[i]);
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 80), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvMoneyMode.setText(data.get(i));
                    CunModeBean cunModeBean = new CunModeBean();
                    cunModeBean.setMode(data.get(i));
                    GlobalApplication.getInstance().setMoneyMode(cunModeBean);
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getVersion() {
        UserCenterServerApi.versiondetection()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RemoveBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RemoveBean removeBean) {
                        stopProgressDialog();
                        showShortToast(removeBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }
}
