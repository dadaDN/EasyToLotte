package com.dn.lotte.ui.wonderful.activity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.ui.wonderful.adapter.SelectAdapter;
import com.dn.lotte.ui.wonderful.adapter.ToSelectAdapter;
import com.dn.lotte.utils.CommonUtils;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░▄░░
 * ░░░░░░░░░▐█░░░░░░░░░░░▄▀▒▌░
 * ░░░░░░░░▐▀▒█░░░░░░░░▄▀▒▒▒▐
 * ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
 * ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
 * ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
 * ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒
 * ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
 * ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄
 * ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒
 * ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒
 * 单身狗就这样默默地看着你，一句话也不说。
 *
 * @author DN
 * @data 2017/10/10
 **/
public class MoregameActivity extends BaseActivity implements DnToolbar.OnRightTitleClickListener {


    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.selectRecycler)
    RecyclerView mSelectRecycler;
    @Bind(R.id.toSelectRecycler)
    RecyclerView mToSelectRecycler;
    List<PurchaseBean.TableBean> mToSelectData = new ArrayList<>();
    List<PurchaseBean.TableBean> mSelectData = new ArrayList<>();
    private ToSelectAdapter mToSelectAdapter;
    private SelectAdapter mSelectAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.act_moregame;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {

        initTitle();
        mToolbar.setMainTitle("自定义彩种");
        mToolbar.setMainTitleRightText("完成");
        mToolbar.setOnRightTitleClickListener(this);
        mSelectData = CommonUtils.getMoreGameList();
        if( CommonUtils.getMoreGameList()==null)
            mSelectData = new ArrayList<>();
        mSelectRecycler.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false));
        mToSelectRecycler.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false));
        mSelectRecycler.setNestedScrollingEnabled(false);
        mToSelectRecycler.setNestedScrollingEnabled(false);
        mSelectAdapter = new SelectAdapter(R.layout.item_select, mSelectData);
        mToSelectAdapter = new ToSelectAdapter(R.layout.item_toselect, mToSelectData);

        mSelectRecycler.setAdapter(mSelectAdapter);
        mToSelectRecycler.setAdapter(mToSelectAdapter);
        loadData();
        mToSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                mSelectData.add(mToSelectData.get(position));
                mSelectAdapter.addData(mToSelectData.get(position));
                mToSelectAdapter.remove(position);


            }
        });
        mSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                mToSelectData.add(mSelectData.get(position));
                mToSelectAdapter.addData(mSelectData.get(position));
                mSelectAdapter.remove(position);


            }
        });


    }

    private void loadData() {
        ServerApi.getPurchaseData()
                .map(new Function<PurchaseBean, List<PurchaseBean.TableBean>>() {
                    @Override
                    public List<PurchaseBean.TableBean> apply(@NonNull PurchaseBean purchaseBean) throws Exception {
                        List<PurchaseBean.TableBean> allBeanList = purchaseBean.getTable();
                        if (mSelectData.size() > 0) {
                            for (PurchaseBean.TableBean bean : mSelectData) {
                                for (Iterator<PurchaseBean.TableBean> it = allBeanList.iterator(); it.hasNext(); ) {
                                    PurchaseBean.TableBean s = it.next();
                                    if (bean.getId().equals(s.getId())) {
                                        it.remove();
                                    }
                                }
                            }
                        } else {
                            allBeanList = purchaseBean.getTable();
                        }
                        Log.i("DNlog", allBeanList.size() + "-----------");
                        return allBeanList;
                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<PurchaseBean.TableBean>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<PurchaseBean.TableBean> list) {
                        mToSelectData = list;
                        mToSelectAdapter.setNewData(mToSelectData);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }


    @Override
    public void onRightClick(View view) {
        CommonUtils.setMoreGameList(mSelectData);
        EventBus.getDefault().post(new EventBusBean<List<PurchaseBean.TableBean>>("SelectDataToWonder", mSelectData));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
