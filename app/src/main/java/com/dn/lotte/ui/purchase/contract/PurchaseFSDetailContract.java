package com.dn.lotte.ui.purchase.contract;

import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.easy.common.base.BaseModel;
import com.easy.common.base.BasePresenter;
import com.easy.common.base.BaseView;
import com.lzy.okgo.model.HttpParams;

import io.reactivex.Observable;

/**
 * Created by DN on 2017/10/12.
 */

public interface PurchaseFSDetailContract {


    interface Model extends BaseModel {

        Observable<PurchaseDetailNumberBean> getNumber(HttpParams httpParams);

        Observable<PurchaseDetailNumberBean> getBettingData(String json);
    }

    interface View extends BaseView {

        void returnNumberData(PurchaseDetailNumberBean numberbean);

        void returnSelectNumberData(int selectNumber);

        void returnPlayData(String palyCode, String selectNumber, String playStrPos, boolean isSha);

        void returenBettingData(PurchaseDetailNumberBean bean);
    }

    abstract class Presenter extends BasePresenter<View, Model> {
        public abstract void getNumberData(HttpParams httpParams, String lid, String titleName);

        public abstract void setNumberData(int number);

        public abstract void setPlayData(String palyCode, String selectNumber, String playStrPos, boolean isSha);

        public abstract void getBettingData(String json);
    }
}
