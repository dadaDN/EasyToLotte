package com.dn.lotte.ui.purchase.activity;

import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.TimeBean;
import com.dn.lotte.ui.usercenter.activity.BettingActivity;
import com.dn.lotte.ui.usercenter.activity.NumberRecordActivity;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/12.
 */

public class BetSuccessActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_betmoney)
    TextView tvBetmoney;
    @Bind(R.id.tv_accountmoney)
    TextView tvAccountmoney;
    @Bind(R.id.tv_oneBbetting)
    TextView tvOneBbetting;
    @Bind(R.id.oneBbetting)
    RippleView oneBbetting;
    @Bind(R.id.tv_onebuy)
    TextView tvOnebuy;
    @Bind(R.id.onebuy)
    RippleView onebuy;
    private String money;
    private String tag;//0是投注 1是追号

    @Override
    public int getLayoutId() {
        return R.layout.activity_successbet;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        money = getIntent().getStringExtra("strmoney");
        tag = getIntent().getStringExtra("tag");
        tvBetmoney.setText(money);
        requestMoney();
        if (tag.equals("1")) {
            tvOneBbetting.setText("追号记录");
            tvOnebuy.setText("继续追号");
        } else {
            tvOneBbetting.setText("投注记录");
            tvOnebuy.setText("继续购彩");
        }
        onebuy.setOnRippleCompleteListener(this);
        oneBbetting.setOnRippleCompleteListener(this);
    }

    private void requestMoney() {
        UserCenterServerApi.getMoney()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TimeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull TimeBean timeBean) {
                        stopProgressDialog();
                        if (timeBean.getResult().equals("1")) {
                            if (tvAccountmoney != null)
                                tvAccountmoney.setText(timeBean.getMoney() == null ? "" : timeBean.getMoney());
                            EventBus.getDefault().post(new EventBusBean<String>("bww", "bww"));
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.oneBbetting://记录
                if (tag.equals("1")) {//1追号0投注
                    startActivity(NumberRecordActivity.class);
                } else {
                    startActivity(BettingActivity.class);
                }
                finish();
                break;
            case R.id.onebuy://购彩

                finish();
                break;
        }
    }
}
