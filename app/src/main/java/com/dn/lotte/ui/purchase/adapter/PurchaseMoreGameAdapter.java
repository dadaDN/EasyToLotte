package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.utils.CommonUtils;
import com.dn.lotte.utils.NotifyDataState;
import com.easy.common.commonutils.ToastUitl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/1/25 0025.
 */

public class PurchaseMoreGameAdapter extends BaseQuickAdapter<PurchaseDetailTypeBean.TableBean, BaseViewHolder> {
    private List<PurchaseDetailGameBean.TableBean> mList;
    private String mID;

    public PurchaseMoreGameAdapter(int layoutResId, @Nullable List<PurchaseDetailTypeBean.TableBean> data, List<PurchaseDetailGameBean.TableBean> mList, String mID) {
        super(layoutResId, data);
        this.mList = mList;
        this.mID = mID;
    }

    @Override
    protected void convert(BaseViewHolder helper, PurchaseDetailTypeBean.TableBean item) {
        helper.setText(R.id.tv, item.getTitle());
        RecyclerView recyclerView = helper.getView(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));

        final List<PurchaseDetailGameBean.TableBean> mBranchList = new ArrayList<>();

        for (PurchaseDetailGameBean.TableBean tableBean : mList) {
            if (item.getId().equals(tableBean.getRadio())) {
                mBranchList.add(tableBean);
            }
        }
        final PurchaseMoreGameTitleAdapter mAdapter = new PurchaseMoreGameTitleAdapter(R.layout.item_purchase_more_game_detail_title, mBranchList);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                final List<PurchaseDetailGameBean.TableBean> selectGameList = CommonUtils.getSelectGameList("DNID" + mID);
                final List<PurchaseDetailGameBean.TableBean> mSelectGameList = CommonUtils.getSelectGameList("DNID" + mID);
                PurchaseDetailGameBean.TableBean bean = mBranchList.get(position);
                if (bean.isChecked()) {
                    //选中 点击变成未选中
                    if (selectGameList.size() == 1) {
                        ToastUitl.showShort("最少保留一个玩法！");
                    } else {
                        bean.setChecked(false);
                        for (int i = 0; i < selectGameList.size(); i++) {
                            //取消选中 如果列表里有 移出
                            if (selectGameList.get(i).getTitle().equals(bean.getTitle())) {
                                mSelectGameList.remove(i);
                            }
                        }
                        CommonUtils.setSelectGameList("DNID" +mID, mSelectGameList);
                    }
                } else {
                    bean.setChecked(true);
                    selectGameList.add(bean);
                    CommonUtils.setSelectGameList("DNID" +mID, selectGameList);
                }
                mAdapter.notifyItemChanged(position);
            }
        });
    }
}
