package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.SelectedBean;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;
import com.dn.lotte.ui.purchase.presenter.PurchaseFSDetailPresenter;
import com.dn.lotte.utils.JsonUtil;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonutils.ToastUitl;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.dn.lotte.utils.TestCodeUtils.BetNumerice;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseFSNumberAdapter extends BaseQuickAdapter<PurchaseDetailNumberBean.TableBean.BallsBean, BaseViewHolder> {

    private String value;
    private String key;
    PurchaseFSDetailPresenter mPresenter;
    private String[] mListItems = {};
    boolean isRenShi = false; //是否是任式
    boolean isSDZ = false; //是否是杀对子
    boolean isSha = false; //是否杀对子
    boolean isDPC = false; //是否是低频彩
    boolean isFG = false; //是否是 以“_”线分割的
    final List<String> selectNumberList = new ArrayList<>();
    private String mPlayCode;

    public PurchaseFSNumberAdapter(@LayoutRes int layoutResId
            , @Nullable List<PurchaseDetailNumberBean.TableBean.BallsBean> data
            , String mPlayCode, PurchaseFSDetailPresenter mPresenter, boolean isRenShi, boolean isSDZ, boolean isDPC, boolean isFG) {
        super(layoutResId, data);
        this.mPresenter = mPresenter;
        this.isRenShi = isRenShi;
        this.isSDZ = isSDZ;
        this.isDPC = isDPC;
        this.isFG = isFG;
        this.mPlayCode = mPlayCode;

    }

    public void refreshSDZ(boolean isSDZ) {
        this.isSDZ = isSDZ;
//        calculateNumber(mSelectedBeanList, parentPosition);
    }

    public void refreshData(List<PurchaseDetailNumberBean.TableBean.BallsBean> data) {
        selectNumberList.clear();
        for (int i = 0; i < data.size(); i++) {
            selectNumberList.add("");
        }
        initList();
        mPresenter.setNumberData(0);
        this.setNewData(data);
    }

    private void initList() {
        selectNumberList.clear();
        for (int i = 0; i < mData.size(); i++) {
            selectNumberList.add("");
        }
    }

    @Override
    protected void convert(final BaseViewHolder helper, PurchaseDetailNumberBean.TableBean.BallsBean item) {
        String ballStr = JsonUtil.toJson(item);
        //动态获取bean的  key + value
        final HashMap<String, Object> stringObjectHashMap = JsonUtil.parseJsonToMap(ballStr);
        Iterator<Map.Entry<String, Object>> iterator = stringObjectHashMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            key = next.getKey();
            Object v = next.getValue();
            value = v + "";
        }
        helper.setText(R.id.tv_title, key);

        if (value != null) {
            if (StringUtils.isContainChinese(value)) {
                helper.setVisible(R.id.linearGone, false);
            } else {
                helper.setVisible(R.id.linearGone, true);
            }
            mListItems = value.split(",");
        }
        final List<SelectedBean> mSelectedBeanList = new ArrayList<>();
        for (String s : mListItems) {
            SelectedBean selectedBean = new SelectedBean();
            selectedBean.setName(s);
            mSelectedBeanList.add(selectedBean);
        }

        if (selectNumberList == null || selectNumberList.size() != mData.size()) {
            initList();
        }

        // 号码
        RecyclerView mNumberRecyclerView = helper.getView(R.id.numberRecyclerView);
        final NumberAdapter mNumberAdapter = new NumberAdapter(R.layout.item_number, mSelectedBeanList);
        mNumberRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 6));
        mNumberRecyclerView.setAdapter(mNumberAdapter);
        final int parentPosition = helper.getPosition();
        if (parentPosition == 0 && isSDZ) {
            helper.setVisible(R.id.re_sdz, true);
        } else {
            helper.setVisible(R.id.re_sdz, false);
        }

        helper.setOnClickListener(R.id.cb_sdz, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox view = (CheckBox) helper.getView(R.id.cb_sdz);
                isSha = view.isChecked();
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        //每注點擊事件
        mNumberAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.cb_item:
                        CheckBox checkBox = (CheckBox) view;
                        SelectedBean selectedBean = mSelectedBeanList.get(position);
                        if (selectedBean.isCheched()) {
                            selectedBean.setCheched(false);
                            checkBox.setChecked(false);
                        } else {
                            selectedBean.setCheched(true);
                            checkBox.setChecked(true);
                        }
                        calculateNumber(mSelectedBeanList, parentPosition);
                        break;
                }
            }
        });

        /**
         * 大小单双奇偶
         */
        helper.setOnClickListener(R.id.allBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (SelectedBean bean : mSelectedBeanList) {
                    bean.setCheched(true);
                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        helper.setOnClickListener(R.id.bigBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < mSelectedBeanList.size(); i++) {
                    if (i < mSelectedBeanList.size() / 2) {
                        mSelectedBeanList.get(i).setCheched(false);
                    } else {
                        mSelectedBeanList.get(i).setCheched(true);
                    }
                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        helper.setOnClickListener(R.id.smallBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < mSelectedBeanList.size(); i++) {
                    if (i < mSelectedBeanList.size() / 2) {
                        mSelectedBeanList.get(i).setCheched(true);
                    } else {
                        mSelectedBeanList.get(i).setCheched(false);
                    }
                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        helper.setOnClickListener(R.id.singleBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < mSelectedBeanList.size(); i++) {
                    if (Integer.parseInt(mSelectedBeanList.get(i).getName()) % 2 == 1) {
                        mSelectedBeanList.get(i).setCheched(true);
                    } else
                        mSelectedBeanList.get(i).setCheched(false);
                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        helper.setOnClickListener(R.id.doubleBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < mSelectedBeanList.size(); i++) {
                    if (Integer.parseInt(mSelectedBeanList.get(i).getName()) % 2 == 1) {
                        mSelectedBeanList.get(i).setCheched(false);
                    } else mSelectedBeanList.get(i).setCheched(true);

                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }
        });

        helper.setOnClickListener(R.id.clearBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (SelectedBean bean : mSelectedBeanList) {
                    bean.setCheched(false);
                }
                mNumberAdapter.setNewData(mSelectedBeanList);
                //筛选选中的条目
                calculateNumber(mSelectedBeanList, parentPosition);
            }

        });
    }

    /**
     * 计算注数
     *
     * @param mSelectedBeanList 当前列表的整体数据
     * @param parentPosition    父 第几个列表
     */
    private void calculateNumber(List<SelectedBean> mSelectedBeanList, int parentPosition) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mSelectedBeanList.size(); i++) {
            if (mSelectedBeanList.get(i).isCheched()) {
                if (isFG) {
                    sb.append(mSelectedBeanList.get(i).getName() + "_");
                } else
                    sb.append(mSelectedBeanList.get(i).getName());
            }
        }
        if (sb.toString().endsWith("_")) {
            String ss = sb.substring(0, sb.length() - 1);
            sb = new StringBuffer(ss);
        }
        if (selectNumberList == null || selectNumberList.size() != mData.size()) {
            initList();
        }
        selectNumberList.set(parentPosition, sb.toString());
        Gson gson = new Gson();
        String numberStr = gson.toJson(selectNumberList);
        Log.i("DNLOG--", parentPosition + "选中数据--w位置：" + parentPosition + "数据" + numberStr);
        List<String> strPosList = new ArrayList<>();
        for (int i = 0; i < selectNumberList.size(); i++) {
            if (selectNumberList.get(i).equals("")) {
                strPosList.add("0");
            } else
                strPosList.add("1");
        }
        StringBuffer selectNumberBuffer = new StringBuffer();
        for (int i = 0; i < selectNumberList.size(); i++) {
            if (i == selectNumberList.size() - 1) {
                selectNumberBuffer.append(selectNumberList.get(i));
            } else
                selectNumberBuffer.append(selectNumberList.get(i) + ",");
        }
        StringBuffer posNumberBuffer = new StringBuffer();
        for (int i = 0; i < strPosList.size(); i++) {
            if (i == strPosList.size() - 1) {
                posNumberBuffer.append(strPosList.get(i));
            } else
                posNumberBuffer.append(strPosList.get(i) + ",");
        }

        String s = gson.toJson(strPosList);
//        String seletNumberStr = numberStr.substring(2, numberStr.length() - 2);
//        String posStr = s.substring(2, s.length() - 2);
        int stakeNumber = BetNumerice(mPlayCode, selectNumberBuffer.toString(), posNumberBuffer.toString(), isSha);//注数
        Log.i("DNLOG--", "注数---：" + stakeNumber);
        mPresenter.setNumberData(stakeNumber);
        mPresenter.setPlayData(mPlayCode, selectNumberBuffer.toString(), posNumberBuffer.toString(), isSha);

    }
}
