package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;
import com.dn.lotte.utils.CommonUtils;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseDetailTitleAdapter extends BaseQuickAdapter<PurchaseDetailTypeBean.TableBean, BaseViewHolder> {
    PurchaseDetailPresenter presenter;
    PurchaseDetailTypeAdapter adapter;

    public PurchaseDetailTitleAdapter(@LayoutRes int layoutResId, @Nullable List<PurchaseDetailTypeBean.TableBean> data, PurchaseDetailPresenter presenter, PurchaseDetailTypeAdapter adapter) {
        super(layoutResId, data);
        this.presenter = presenter;
        this.adapter = adapter;
    }

    @Override
    protected void convert(final BaseViewHolder helper, final PurchaseDetailTypeBean.TableBean item) {
        helper.setText(R.id.cb_item, item.getTitle());
        final CheckBox mCheckBox = helper.getView(R.id.cb_item);
        mCheckBox.setChecked(item.isChecked());
        if (item.isChecked()) {
            List<PurchaseDetailGameBean.TableBean> gameTypeDetailList = CommonUtils.getGameTypeDetailList(item.getId());
            adapter.setNewData(gameTypeDetailList);
        }
        mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.isChecked()) {
                    mCheckBox.setChecked(true);
                    item.setChecked(true);
                } else {
                    for (PurchaseDetailTypeBean.TableBean data : mData) {
                        data.setChecked(false);
                    }
                    mCheckBox.setChecked(true);
                    item.setChecked(true);
                    notifyDataSetChanged();
                    List<PurchaseDetailGameBean.TableBean> gameTypeDetailList = CommonUtils.getGameTypeDetailList(item.getId());
                    adapter.setNewData(gameTypeDetailList);
                }

//                Toast.makeText(mContext, "sss" + helper.getPosition(), Toast.LENGTH_SHORT).show();
//                HttpParams httpParams = new HttpParams();
//                httpParams.put("lid", item.getId());
//                presenter.getGameData(httpParams);
            }
        });
//        helper.setOnItemClickListener(R.id.cb_item, new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(mContext, "sss" + i, Toast.LENGTH_SHORT).show();
//            }
//
//        });
    }

}
