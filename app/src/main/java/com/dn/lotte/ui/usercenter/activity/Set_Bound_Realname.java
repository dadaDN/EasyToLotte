package com.dn.lotte.ui.usercenter.activity;

import android.view.View;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.bean.Vipbean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 1
 * Created by ASUS on 2017/10/10.
 */

public class Set_Bound_Realname extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.ed_realname)
    ClearEditText edRealname;
    @Bind(R.id.okBtn)
    RippleView okBtn;

    @Override
    public int getLayoutId() {
        return R.layout.realname;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_set_bound_realname);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);

    }

    @OnClick(R.id.okBtn)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.okBtn:
                if (!StringUtils.isEmpty(edRealname.getText().toString().trim())) {
                    HttpParams params = new HttpParams();
                    params.put("name", edRealname.getText().toString().trim());
                    realNameUrl(params);

                } else {
                    showShortToast("请输入真实姓名");
                }
                break;
        }

    }

    private void getVipInfoData() {
        ServerApi.getVipInfoData()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Vipbean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Vipbean vipbean) {
                        GlobalApplication.getInstance().setUserModel(vipbean);

                        finish();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

    private void realNameUrl(HttpParams params) {
        UserCenterServerApi.setRealname(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        stopProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")) {
                            GlobalApplication.getInstance().getUserModel().getTable().get(0).setTruename(edRealname.getText().toString().trim());
                          //  GlobalApplication.getInstance().getUserModel().getTable().get(0).setIstruename("1");
                            getVipInfoData();

                        }
                        showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });

    }

}
