package com.dn.lotte.ui.usercenter.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.kjDetailsBean;
import com.easy.common.commonutils.StringUtils;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;

/**
 * Created by ASUS on 2017/10/15.
 */

public class KjDetailsAdapter extends BaseQuickAdapter<kjDetailsBean.TableBean, BaseViewHolder> {
    private String[] allStr;
    private String s;
    private String s1;
    private String s2;

    public KjDetailsAdapter(@LayoutRes int layoutResId, @Nullable List<kjDetailsBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, kjDetailsBean.TableBean item) {
        helper.setText(R.id.tv_qh_number, item.getTitle())
                .setText(R.id.tv_time, item.getOpentime());
        String substring = item.getNumber().substring(item.getNumber().length() - 1, item.getNumber().length());
        String[] split = item.getNumber().split(",");
        for (int i=0;i<split.length;i++){
            s = split[split.length - 1];
            s1 = split[split.length - 2];
            s2 = split[split.length - 3];
        }
        Log.e("ddddd",s+"--"+s1+"--"+s2);
        if (s.equals(s1)&&s.equals(s2)){
            helper.setText(R.id.tv_smallname, "豹子");
        }else {
            if (s.equals(s1)||s.equals(s2)||s1.equals(s2)){
                helper.setText(R.id.tv_smallname, "组三");
            }else {
                helper.setText(R.id.tv_smallname, "组六");
            }
        }
        showlabel((FlexboxLayout) helper.getView(R.id.fl_self_introduction), item.getNumber());

    }

    /**
     * 显示标签
     *
     * @param flexboxLayout FlexboxLayout
     * @param str           “聪明，强，无敌” --> 以 ，隔开的字符串
     */
    private void showlabel(FlexboxLayout flexboxLayout, String str) {
        flexboxLayout.removeAllViews();
        if (!StringUtils.isEmpty(str)) {
            allStr = str.split(",");
            for (int i = 0; i < allStr.length; i++) {
                TextView textView = new TextView(mContext);
                textView.setText(allStr[i]);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(16);
                textView.setTextColor(Color.WHITE);
                textView.setBackgroundResource(R.drawable.ball_ssc);
                flexboxLayout.addView(textView);
                ViewGroup.LayoutParams params = textView.getLayoutParams();
                if (params instanceof FlexboxLayout.LayoutParams) {
                    FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) params;
                    layoutParams.setMargins(30, 40, 0, 0);
                    textView.setLayoutParams(layoutParams);
                }
            }
        }
    }
}
