package com.dn.lotte.ui.usercenter.activity;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;

public class SplashActivity extends BaseActivity {


    private static final String TAG = "DNLOG";

    @Override
    public int getLayoutId() {
        return R.layout.act_splash;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        startActivity(LoginActivity.class);
        finish();
    }
}
