package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.AgencynumberBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencyNumberAdapter extends BaseQuickAdapter<AgencynumberBean.TableBean,BaseViewHolder>{

    private String name;

    public AgencyNumberAdapter(@LayoutRes int layoutResId, @Nullable List<AgencynumberBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AgencynumberBean.TableBean item) {
        if (item.getFinishstate().equals("-1")){
            name = "进行中";
        }else {
            name="已完成";
        }
        helper.setText(R.id.tv_user_name,item.getUsername())
                .setText(R.id.tv_usr_number,"订单号："+item.getSsid())
                .setText(R.id.tv_game_name,item.getLotteryname())
                .setText(R.id.tv_game_status,name)
                .setText(R.id.tv_game_record_time,"已追:"+item.getStatename()+"期")
                .setText(R.id.tv_game_money,item.getFinishname())
                .setText(R.id.tv_game_record_number,item.getStartissuenum())
                .setText(R.id.tv_do_time,item.getStime());
    }
}
