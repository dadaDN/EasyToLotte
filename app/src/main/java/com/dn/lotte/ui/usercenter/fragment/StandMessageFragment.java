package com.dn.lotte.ui.usercenter.fragment;

import android.content.Intent;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.activity.InboxActivity;
import com.dn.lotte.ui.usercenter.activity.SendStrandActivity;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.RippleView;

import butterknife.Bind;

/**
 * Created by ASUS on 2017/10/17.
 */

public class StandMessageFragment extends BaseFragment implements RippleView.OnRippleCompleteListener {

    @Bind(R.id.inbox)
    RippleView inbox;
    @Bind(R.id.outbox)
    RippleView outbox;
    @Bind(R.id.send_strand)
    RippleView sendStrand;
    private Intent intent;

    @Override
    protected int getLayoutResource() {
        return R.layout.standmessage;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        inbox.setOnRippleCompleteListener(this);
        outbox.setOnRippleCompleteListener(this);
        sendStrand.setOnRippleCompleteListener(this);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.inbox:
                intent = new Intent(getActivity(), InboxActivity.class);
                intent.putExtra("type", "收件箱");
                startActivity(intent);
                break;
            case R.id.outbox:
                intent = new Intent(getActivity(), InboxActivity.class);
                intent.putExtra("type", "发件箱");
                startActivity(intent);
                break;
            case R.id.send_strand:
                startActivity(SendStrandActivity.class);
                break;

        }
    }

}
