package com.dn.lotte.ui.purchase.model;

import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.PurchseDetailTimeBean;
import com.dn.lotte.ui.purchase.contract.PurchaseDetailContract;
import com.lzy.okgo.model.HttpParams;

import io.reactivex.Observable;

/**
 * Created by DN on 2017/10/12.
 */

public class PurchaseDetailModel implements PurchaseDetailContract.Model {
    @Override
    public Observable<PurchaseDetailTypeBean> getType(HttpParams httpParams) {
        return ServerApi.getPurchaseTypeData(httpParams);
    }

    @Override
    public Observable<PurchaseDetailGameBean> getGame(HttpParams httpParams) {
        return ServerApi.getPurchaseGameData(httpParams);
    }

    @Override
    public Observable<PurchaseDetailNumberBean> getNumber(HttpParams httpParams) {
        return ServerApi.getPurchaseDetailNumberData(httpParams);
    }

    @Override
    public Observable<HistoryDetailBean> getHistory(HttpParams httpParams) {
        return ServerApi.getHistoryDetailData(httpParams);
    }

    @Override
    public Observable<PurchseDetailTimeBean> getGameTime(HttpParams httpParams) {
        return ServerApi.getPurchaseDetailTime(httpParams);
    }
}
