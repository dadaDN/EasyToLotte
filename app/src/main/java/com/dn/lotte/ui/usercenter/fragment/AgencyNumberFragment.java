package com.dn.lotte.ui.usercenter.fragment;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AgencynumberBean;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.TimeAndStateBean;
import com.dn.lotte.ui.usercenter.adapter.AgencyNumberAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.lzy.okgo.model.HttpParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/19.
 */
@SuppressLint("ValidFragment")
public class AgencyNumberFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    private String mTag;
    private List<AgencynumberBean.TableBean> mList = new ArrayList<>();
    private int page = 1, pagesize = 10;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    private AgencyNumberAdapter mAdapter;
    private EventBusBean<TimeAndStateBean> bean;

    public AgencyNumberFragment(String s) {
        this.mTag = s;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_agency;
    }

    @Override
    public void initPresenter() {

    }

    protected boolean mIsVisible = false;

    /**
     * `
     * 在这里实现Fragment数据的缓加载.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mIsVisible = true;
            onVisible();
        } else {
            mIsVisible = false;
            onInvisible();
        }
    }

    protected void onInvisible() {
    }

    /**
     * 显示时加载数据,需要这样的使用
     * 注意声明 isPrepared，先初始化
     * 生命周期会先执行 setUserVisibleHint 再执行onActivityCreated
     * 在 onActivityCreated 之后第一次显示加载数据，只加载一次
     */
    protected void loadData() {
    }

    protected void onVisible() {
        loadData("", "", "");
    }

    @Override
    protected void initView() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new AgencyNumberAdapter(R.layout.item_number_record, mList);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mRecyclerView.setAdapter(mAdapter);
        isRefresh = 0;

    }

    private void loadData(String stime, String otime, String state) {
        HttpParams httpParams = new HttpParams();
        httpParams.put("page", page);
        httpParams.put("pagesize", pagesize);
        httpParams.put("d1", stime);
        httpParams.put("d2", otime);
        httpParams.put("lid", state);
        httpParams.put("state", mTag);
        httpParams.put("u", "");
        UserCenterServerApi.agencynumber(httpParams)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                        startProgressDialog();
                        if (mSwipeLayout != null) mSwipeLayout.setRefreshing(false);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AgencynumberBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull AgencynumberBean agencynumberBean) {
                        if (agencynumberBean.getResult().equals("1")) {
                            Log.i("DNlog", agencynumberBean.getTable().size() + "");
                            if (agencynumberBean.getResult().equals("1")) {
                                mList = agencynumberBean.getTable();
                                if (isRefresh == 1) {
                                    mAdapter.loadMoreComplete();
                                    mAdapter.addData(mList);
                                } else {
                                    mSwipeLayout.setRefreshing(false);
                                    mAdapter.setNewData(mList);
                                }
                                if (agencynumberBean.getTable().size() < pagesize) {
                                    mLoadMoreEndGone = true;
                                    mAdapter.loadMoreEnd(mLoadMoreEndGone);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (mSwipeLayout != null) mSwipeLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        loadData("", "", "");
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        loadData("", "", "");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainEventBus(EventBusBean<TimeAndStateBean> bean) {
        if ("numbertimeandstate".equals(bean.getTag())) {
            this.bean = bean;
            TimeAndStateBean data = bean.getData();
            String stime = data.getStime();
            String state = data.getState();
            String otime = data.getOtime();
            if (stime.equals("开始时间")) {
                stime = "";
            }
            if (otime.equals("结束时间")) {
                otime = "";
            }
            page = 1;
            isRefresh = 0;
            loadData(stime, otime, state);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
