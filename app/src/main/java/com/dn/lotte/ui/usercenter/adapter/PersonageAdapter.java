package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PersonageBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/18.
 */

public class PersonageAdapter extends BaseQuickAdapter<PersonageBean.TableBean,BaseViewHolder> {
    public PersonageAdapter(@LayoutRes int layoutResId, @Nullable List<PersonageBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PersonageBean.TableBean item) {
        helper.setText(R.id.tv_username,item.getStime());
        helper.setText(R.id.tv_recharge,item.getCharge());//充值
        helper.setText(R.id.tv_withdrawal,item.getGetcash());//提现
        helper.setText(R.id.tv_consume,item.getAgentfh());//余额
        helper.setText(R.id.tv_award,item.getWin());//派奖
        helper.setText(R.id.tv_return,item.getPoint());//返点
        helper.setText(R.id.tv_activity,item.getGive());//活动
        helper.setText(R.id.tv_profit,item.getTotal());//盈利
        helper.setText(R.id.tv_buy,item.getBet());//代购
        helper.setText(R.id.tv_chedan,item.getCancellation());//代购
        helper.setText(R.id.tv_jie,item.getDdmoney());//单单日结
        helper.setText(R.id.tv_other,item.getOther());//其他
    }
}
