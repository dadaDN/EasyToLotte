package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.SelectNumberBean;

import java.util.List;


/**
 * Created by DN on 2017/10 /8.
 */

public class NumberCAdapter extends BaseQuickAdapter<SelectNumberBean.NumberBean, BaseViewHolder> {

    public NumberCAdapter(@LayoutRes int layoutResId, @Nullable List<SelectNumberBean.NumberBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SelectNumberBean.NumberBean item) {
        helper.setText(R.id.tv_number, item.getBalls())
                .setText(R.id.tv_name, item.getName())
                .setText(R.id.tv_bs, item.getTimes())
                .setText(R.id.tv_pos, item.getModel()+"模式")
                .setText(R.id.tv_money, item.getAlltotal()+"元")
                .addOnClickListener(R.id.iv_delete);
        // 金額 = 倍數 * 注數 * 模式

    }

}
