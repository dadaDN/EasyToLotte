package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.DayratioBean;

import java.util.List;

/**
 * Created by ASUS on 2018/1/18.
 */

public class RatioFhAdapter extends BaseQuickAdapter<DayratioBean.TableBean,BaseViewHolder> {
    public RatioFhAdapter(@LayoutRes int layoutResId, @Nullable List<DayratioBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DayratioBean.TableBean item) {
        helper.setText(R.id.tvchange,"半月销量" + item.getMinmoney()+ "万");
        helper.setText(R.id.tvbili,item.getMoney() + "%");
    }
}
