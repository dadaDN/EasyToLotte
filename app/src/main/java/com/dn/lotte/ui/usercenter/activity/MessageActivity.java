package com.dn.lotte.ui.usercenter.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.dn.lotte.R;
import com.dn.lotte.ui.usercenter.fragment.MessageFragment;
import com.dn.lotte.ui.usercenter.fragment.StandMessageFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;

public class MessageActivity extends BaseActivity {


    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.tablayout)
    SlidingTabLayout mTablayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;

    private String[] mTitles = {"公告", "站内信"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mMAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_message;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        mToolbar.setMainTitle("我的消息");
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new MessageFragment());
        mFragments.add(new StandMessageFragment());
        mMAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewpager.setAdapter(mMAdapter);
        mTablayout.setViewPager(mViewpager,mTitles,this,mFragments);

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

}
