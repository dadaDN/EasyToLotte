package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.SelectedBean;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;
import com.dn.lotte.utils.JsonUtil;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseNumberAdapter extends BaseQuickAdapter<PurchaseDetailNumberBean.TableBean.BallsBean, BaseViewHolder> {
    private String value;
    private String key;
    private String[] mListItems = {};
    PurchaseDetailPresenter mPresenter;
    private List<Integer> mPositionList = new ArrayList<>();
    private int mTag = 1;
    private String remberName;

    public PurchaseNumberAdapter(@LayoutRes int layoutResId
            , @Nullable List<PurchaseDetailNumberBean.TableBean.BallsBean> data
            , PurchaseDetailPresenter mPresenter) {
        super(layoutResId, data);
        this.mPresenter = mPresenter;
    }

    public void randomData(int tag) {
        this.mTag = tag;
        notifyDataSetChanged();
    }

    public void remeberData(String remberName) {
        this.remberName = remberName;
        try {
            saveAllData.put(remberName, saveData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void convert(final BaseViewHolder helper, PurchaseDetailNumberBean.TableBean.BallsBean item) {

        String ballStr = JsonUtil.toJson(item);
        Log.i("DOI",ballStr);
        //动态获取bean的  key + value
        final HashMap<String, Object> stringObjectHashMap = JsonUtil.parseJsonToMap(ballStr);
        Iterator<Map.Entry<String, Object>> iterator = stringObjectHashMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            key = next.getKey();
            Log.i("DNLOG", key + "--------" + helper.getPosition());
            Object v = next.getValue();
            value = v + "";
            Log.i("DNLOG", value + "--------" + helper.getPosition());
        }
        helper.setText(R.id.tv_title, key);
        List<String> data = new ArrayList<>();

        if (value != null) {
            if (StringUtils.isContainChinese(value)) {
                helper.setVisible(R.id.linearGone, false);
            } else {
                helper.setVisible(R.id.linearGone, true);
            }
            mListItems = value.split(",");
        }

        List<SelectedBean> mSelectedBeanList = new ArrayList<>();
        Gson gson = new Gson();
//        String jsonStr = SPUtils.getSharedStringData(mContext, remberName + helper.getPosition());

        for (String s : mListItems) {
            SelectedBean selectedBean = new SelectedBean();
            selectedBean.setName(s);
            mSelectedBeanList.add(selectedBean);
        }

        // 号码
        RecyclerView mNumberRecyclerView = helper.getView(R.id.numberRecyclerView);
        final NumberAdapter mNumberAdapter = new NumberAdapter(R.layout.item_number, mSelectedBeanList);
        mNumberRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 6));
        mNumberRecyclerView.setAdapter(mNumberAdapter);

        final List<SelectedBean> finalMSelectedBeanList = mSelectedBeanList;

        if (mTag == 2) {
            Random random = new Random();
            int i = random.nextInt(finalMSelectedBeanList.size());
            finalMSelectedBeanList.get(i).setCheched(true);
            List<SelectedBean> selectedBeanLists = new ArrayList<>();
            //筛选选中的条目
            for (SelectedBean s : finalMSelectedBeanList) {
                if (s.isCheched()) {
                    selectedBeanLists.add(s);
                }
            }
            mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
        } else if (mTag == 3) {
            for (SelectedBean s : finalMSelectedBeanList) {
                s.setCheched(false);
            }
            List<SelectedBean> selectedBeanLists = new ArrayList<>();
            //筛选选中的条目
            for (SelectedBean s : finalMSelectedBeanList) {
                if (s.isCheched()) {
                    selectedBeanLists.add(s);
                }
            }
            mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
        }

        mNumberAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.cb_item:
                        mTag = 1;
                        CheckBox checkBox = (CheckBox) view;
                        SelectedBean selectedBean = finalMSelectedBeanList.get(position);
                        List<SelectedBean> selectedBeanLists = new ArrayList<>();
                        List<Integer> positionList = new ArrayList<>();
                        if (selectedBean.isCheched()) {
                            selectedBean.setCheched(false);
                            checkBox.setChecked(false);
                        } else {
                            selectedBean.setCheched(true);
                            checkBox.setChecked(true);
                        }
                        for (int i = 0; i < finalMSelectedBeanList.size(); i++) {
                            if (finalMSelectedBeanList.get(i).isCheched()) {
                                finalMSelectedBeanList.get(i).setPosition(i);
                                selectedBeanLists.add(finalMSelectedBeanList.get(i));
                            }
                        }

//                        //筛选选中的条目
//                        for (SelectedBean s : mSelectedBeanList) {
//                            if (s.isCheched()) {
//                                selectedBeanLists.add(s);
//                            }
//                        }

                        mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
                        break;
                }
            }
        });

        helper.setOnClickListener(R.id.allBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (SelectedBean bean : finalMSelectedBeanList) {
                    bean.setCheched(true);
                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
            }
        });

        helper.setOnClickListener(R.id.bigBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < finalMSelectedBeanList.size(); i++) {
                    if (i < finalMSelectedBeanList.size() / 2) {
                        finalMSelectedBeanList.get(i).setCheched(false);
                    } else {
                        finalMSelectedBeanList.get(i).setCheched(true);
                    }
                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);

            }
        });

        helper.setOnClickListener(R.id.smallBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < finalMSelectedBeanList.size(); i++) {
                    if (i < finalMSelectedBeanList.size() / 2) {
                        finalMSelectedBeanList.get(i).setCheched(true);
                    } else {
                        finalMSelectedBeanList.get(i).setCheched(false);
                    }
                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
            }
        });

        helper.setOnClickListener(R.id.singleBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < finalMSelectedBeanList.size(); i++) {
                    if (Integer.parseInt(finalMSelectedBeanList.get(i).getName()) % 2 == 1) {
                        finalMSelectedBeanList.get(i).setCheched(true);
                    } else
                        finalMSelectedBeanList.get(i).setCheched(false);
                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
            }
        });

        helper.setOnClickListener(R.id.doubleBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (int i = 0; i < finalMSelectedBeanList.size(); i++) {
                    if (Integer.parseInt(finalMSelectedBeanList.get(i).getName()) % 2 == 1) {
                        finalMSelectedBeanList.get(i).setCheched(false);
                    } else finalMSelectedBeanList.get(i).setCheched(true);

                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
            }
        });

        helper.setOnClickListener(R.id.clearBtn, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTag = 1;
                List<SelectedBean> selectedBeanLists = new ArrayList<>();
                for (SelectedBean bean : finalMSelectedBeanList) {
                    bean.setCheched(false);
                }
                mNumberAdapter.setNewData(finalMSelectedBeanList);
                //筛选选中的条目
                for (SelectedBean s : finalMSelectedBeanList) {
                    if (s.isCheched()) {
                        selectedBeanLists.add(s);
                    }
                }
                mPresenter.returnNumberSelectedData(helper.getPosition(), selectedBeanLists);
            }

        });

    }

    JSONObject saveAllData = new JSONObject();
    JSONObject saveData = new JSONObject();
    ArrayList<String> nameList = new ArrayList<>();

    public void saveData(int position, List<SelectedBean> selectedBeanLists) {
        Gson gson = new Gson();
        String json = gson.toJson(selectedBeanLists);
        SPUtils.setSharedStringData(mContext, remberName + position, json);
        Log.i("hhhh", SPUtils.getSharedStringData(mContext, remberName + position));

        nameList.add(remberName + position);
        String nameStr = gson.toJson(nameList);
        SPUtils.setSharedStringData(mContext, "CLEAR", nameStr);

//      l  try {
//            JSONObject jsonObject = (JSONObject) saveAllData.get(remberName);
//            Log.i("hhhh", remberName + jsonObject.toString());
//
//            jsonObject.put(position + "", "123" + "----" + position);
////            Log.i("hhhh", saveAllData.toString());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

//        try {
//
//
////            JSONObject node = new JSONObject();
////            node.put(position + "", "111");
////            saveArrayData.put(node);
////
////            Log.i("hhhh", saveAllData.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

}
