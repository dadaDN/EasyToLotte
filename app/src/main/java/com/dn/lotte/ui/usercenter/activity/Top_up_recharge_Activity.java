package com.dn.lotte.ui.usercenter.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.RechargeBankBean;
import com.dn.lotte.bean.RechargeBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 充值
 * Created by ASUS on 2017/10/10.
 */

public class Top_up_recharge_Activity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_yumoney)
    TextView tvYumoney;
    @Bind(R.id.iv_shuaxin)
    ImageView ivShuaxin;
    @Bind(R.id.tv_usercenter_change_recharge_pattern)
    TextView tvUsercenterChangeRechargePattern;
    @Bind(R.id.rl_usercenter_change_recharge_pattern)
    RelativeLayout rlUsercenterChangeRechargePattern;
    @Bind(R.id.tv_usercenter_recharge_pattern)
    TextView tvUsercenterRechargePattern;
    @Bind(R.id.tv_change_recharge_bank)
    TextView tvChangeRechargeBank;
    @Bind(R.id.rl_bank)
    RelativeLayout rlBank;
    @Bind(R.id.rl_bank1)
    RelativeLayout rlBank1;
    @Bind(R.id.view)
    View mview;
    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.ll_name)
    LinearLayout llName;
    @Bind(R.id.view1)
    View view1;
    @Bind(R.id.tv_moneynumber)
    TextView tvMoneynumber;
    @Bind(R.id.ll_number)
    LinearLayout llNumber;
    @Bind(R.id.et_money)
    EditText etMoney;
    @Bind(R.id.tv_maxmoney)
    TextView tvMaxmoney;
    @Bind(R.id.tv_minmoney)
    TextView tvMinmoney;
    @Bind(R.id.port)
    TextView port;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.tv_name)
    TextView tvName;
    private List<RechargeBean.TableBean> mlist = new ArrayList<>();
    private List<RechargeBankBean.TableBean> mbanklist = new ArrayList<>();
    private String stateid;
    private String bankCode;
    private QMUIListPopup mListPopup;
    private QMUIListPopup ListPopup;
    private String posturl;
    private String userid;
    private String type = "";

    @Override
    public int getLayoutId() {
        return R.layout.recharge_topup;
    }

    @Override
    public void initPresenter() {
    }

    @Override
    public void initView() {
        getRecharge();
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle(R.string.usercenter_power_up);
        tvYumoney.setText(GlobalApplication.getInstance().getUserModel().getTable().get(0).getMoney());
        userid = GlobalApplication.getInstance().getUserModel().getTable().get(0).getId();

    }

    //充值银行选择
    public void getRechargeBank(HttpParams params) {
        UserCenterServerApi.getRechargeBank(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        stopProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RechargeBankBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RechargeBankBean rechargeBankBean) {
                        stopProgressDialog();
                        if (rechargeBankBean.getResult().equals("1")) {
                            mbanklist = rechargeBankBean.getTable();
                        } else {
                            showShortToast(rechargeBankBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //充值方式选择
    public void getRecharge() {
        UserCenterServerApi.getRecharge()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RechargeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RechargeBean rechargeBean) {
                        stopProgressDialog();
                        if (rechargeBean.getResult().equals("1")) {
                            mlist = rechargeBean.getTable();
                        } else {
                            showShortToast(rechargeBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @OnClick({R.id.iv_shuaxin, R.id.rl_usercenter_change_recharge_pattern, R.id.rl_bank, R.id.okBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_shuaxin:
                break;
            case R.id.rl_usercenter_change_recharge_pattern:
                if (mlist == null) {
                    getRecharge();
                } else {
                    if (mListPopup != null) {
                        mListPopup = null;
                    }
                    initListPopupIfNeed();
                    mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopup.show(view);
                }
                port.setVisibility(View.GONE);
                break;
            case R.id.rl_bank:
                String aa = tvUsercenterChangeRechargePattern.getText().toString();
                port.setVisibility(View.GONE);
                if (aa.equals("请选择充值方式")) {
                    showShortToast("请先选择充值方式");
                } else {
                    if (ListPopup != null) {
                        ListPopup = null;
                    }
                    initListPopupIfNeedo();
                    ListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    ListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    ListPopup.show(view);
                }
                break;
            case R.id.okBtn:
                String tvchanger = tvUsercenterChangeRechargePattern.getText().toString();
                String tvbank = tvChangeRechargeBank.getText().toString();
                String name="";
                if (!StringUtils.isEmpty(etMoney.getText().toString().trim())) {
                    if (!tvchanger.equals("请选择充值方式")) {
                        if (!tvbank.equals("请选择充值银行")) {
                            if (Double.parseDouble(etMoney.getText().toString()) > Double.parseDouble(tvMinmoney.getText().toString()) &&
                                    Double.parseDouble(etMoney.getText().toString()) < Double.parseDouble(tvMaxmoney.getText().toString())) {
                                if (type.equals("1")) {
                                    if (!StringUtils.isEmpty(etName.getText().toString())) {
                                        name=etName.getText().toString();
                                    } else {
                                        showShortToast("充值人不能为空");
                                    }
                                } else {
                                    name="";
                                }
                                loadyanzheng(stateid, etMoney.getText().toString().trim(),name);
                            } else {
                                showShortToast("充值金额有误，请重新输入!");
                            }
                        } else {
                            showShortToast(R.string.usercenter_change_recharge_bank);
                        }
                    } else {
                        showShortToast(R.string.usercenter_change_recharge_pattern);
                    }
                } else {
                    showShortToast("请输入充值金额");
                }
                break;
        }
    }

    //充值提交前的验证111
    private void loadyanzheng(String stateid, String money,String name) {
        HttpParams params = new HttpParams();
        params.put("setid", stateid);
        params.put("name", name);
        params.put("amount", money);
        UserCenterServerApi.Loadyanzheng(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")) {
                            if (type.equals("1")) {
                                Intent intent = new Intent(getApplication(), Chongzhiweb.class);
                                intent.putExtra("name", tvName.getText().toString());
                                intent.putExtra("bankname", tvUsercenterChangeRechargePattern.getText().toString());
                                intent.putExtra("number", tvMoneynumber.getText().toString());
                                intent.putExtra("money", etMoney.getText().toString());
                                startActivity(intent);
                                port.setText("为了保证成功，请在30分钟之内手动到支付宝充值！");
                                port.setVisibility(View.VISIBLE);
                            } else {
                                String stringurl = posturl + "?Ssid=" + setSafequesBean.getReturnval() + "&Bank=" + bankCode;
                               /* Intent intent = new Intent(getApplication(), Chongzhiweb.class);
                                intent.putExtra("stringurl", stringurl);
                                startActivity(intent);*/
                                Uri uri = Uri.parse(stringurl);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        } else {
                            showShortToast(setSafequesBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //充值方式
    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            List<String> mylist = new ArrayList<>();
            for (RechargeBean.TableBean bean : mlist) {
                mylist.add(bean.getMername());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, mylist);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 150), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvUsercenterChangeRechargePattern.setText(mlist.get(i).getMername());
              /*      if (mlist.get(i).getMername().contains("支付宝")) {
                        rlBank1.setVisibility(View.GONE);
                    } else {*/
                    if (mlist.get(i).getType().equals("1")) {//0第三方1手动充值
                        mview.setVisibility(View.VISIBLE);
                        view1.setVisibility(View.VISIBLE);
                        llName.setVisibility(View.VISIBLE);
                        type = "1";
                    } else {
                        type = "0";
                        mview.setVisibility(View.GONE);
                        view1.setVisibility(View.GONE);
                        llName.setVisibility(View.GONE);
                    }
                    tvMaxmoney.setText(mlist.get(i).getMaxcharge());
                    tvMinmoney.setText(mlist.get(i).getMincharge());
                    posturl = mlist.get(i).getPosturl();
                    stateid = mlist.get(i).getId();
                    HttpParams params = new HttpParams();
                    params.put("sId", stateid);//银行编号
                    getRechargeBank(params);
                    //  }
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    //充值银行
    private void initListPopupIfNeedo() {
        if (ListPopup == null) {
            List<String> mylist = new ArrayList<>();
            for (RechargeBankBean.TableBean bean : mbanklist) {
                mylist.add(bean.getBank());
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, mylist);
            ListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            ListPopup.create(QMUIDisplayHelper.dp2px(mContext, 200), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvMoneynumber.setText(mbanklist.get(i).getPayaccount());
                    tvName.setText(mbanklist.get(i).getPayname());
                    tvChangeRechargeBank.setText(mbanklist.get(i).getBank());
                    bankCode = mbanklist.get(i).getCode();
                    ListPopup.dismiss();
                }
            });
            ListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }
}
