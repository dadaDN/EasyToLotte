package com.dn.lotte.ui.usercenter.activity;

import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.PurchseDetailTimeBean;
import com.dn.lotte.bean.SelectedBean;
import com.dn.lotte.bean.TimeBean;
import com.dn.lotte.ui.purchase.contract.PurchaseDetailContract;
import com.dn.lotte.ui.purchase.contract.PurchaseNewDetailContract;
import com.dn.lotte.utils.CommonUtils;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * Created by DN on 2017/10/12.
 */


public class PurchaseNewDetailPresenter extends PurchaseNewDetailContract.Presenter {

    @Override
    public void getTypeData(HttpParams httpParams, final String id) {
        if (CommonUtils.getGameTypeDataList(id) != null) {
//            mView.showLoading("");
            mView.returnDetailTypeList(CommonUtils.getGameTypeDataList(id));
        } else {
            mModel.getType(httpParams)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<PurchaseDetailTypeBean>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                        }

                        @Override
                        public void onNext(@NonNull PurchaseDetailTypeBean purchaseBean) {
                            if (purchaseBean.getResult().equals("1")) {
                                mView.returnDetailTypeList(purchaseBean);
                                CommonUtils.setGameTypeDataList(purchaseBean, id);
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

    @Override
    public void getGameData(HttpParams httpParams, final String id) {
        if (CommonUtils.getGameData(id) != null && CommonUtils.getSelectGameList(id) != null) {
            mView.returnDetailGameList(CommonUtils.getGameData(id));
        } else {
            mModel.getGame(httpParams)
                    .map(new Function<PurchaseDetailGameBean, PurchaseDetailGameBean>() {
                        @Override
                        public PurchaseDetailGameBean apply(PurchaseDetailGameBean purchaseDetailGameBean) throws Exception {
                            List<PurchaseDetailGameBean.TableBean> table = purchaseDetailGameBean.getTable();
                            List<PurchaseDetailGameBean.TableBean> mList = new ArrayList<>();
                            if (table != null && table.size() > 0) {
                                mList.add(table.get(0));
                                CommonUtils.setSelectGameList(id, mList);
                            }
                            return purchaseDetailGameBean;
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<PurchaseDetailGameBean>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onNext(@NonNull PurchaseDetailGameBean purchaseBean) {
                            if (purchaseBean.getResult().equals("1")) {
                                mView.returnDetailGameList(purchaseBean);
                                CommonUtils.setGameData(purchaseBean, id);
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }

    }

    @Override
    public void getNumberData(HttpParams httpParams) {
        mModel.getNumber(httpParams).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseDetailNumberBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull PurchaseDetailNumberBean numberBean) {
                        mView.returnNumberData(numberBean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getHistoryData(HttpParams httpParams) {

        mModel.getHistory(httpParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HistoryDetailBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull HistoryDetailBean numberBean) {
                        mView.returnHistoryData(numberBean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void returnSelectData(PurchaseDetailGameBean.TableBean bean) {
        mView.returnSelectData(bean);
    }

    @Override
    public void returnNumberSelectedData(int postion, List<SelectedBean> selectedBeanList) {
        mView.returnNumberSelectedData(postion, selectedBeanList);
    }

    @Override
    public void getGameTime(HttpParams httpParams) {
        mModel.getGameTime(httpParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchseDetailTimeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull PurchseDetailTimeBean timeBean) {
                        mView.returnGameTime(timeBean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void getMoneyData() {
        mModel.getMoney()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TimeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull TimeBean timeBean) {
                        mView.returnMoney(timeBean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }
}
