package com.dn.lotte.ui.purchase.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IdRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.CunModeBean;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.PurchseDetailTimeBean;
import com.dn.lotte.bean.SelectNumberBean;
import com.dn.lotte.bean.SelectedBean;
import com.dn.lotte.bean.TimeBean;
import com.dn.lotte.ui.purchase.adapter.HistroryDetailDataAdapter;
import com.dn.lotte.ui.purchase.adapter.PurchaseDetailCountAdapter;
import com.dn.lotte.ui.purchase.adapter.PurchaseDetailTitleAdapter;
import com.dn.lotte.ui.purchase.adapter.PurchaseDetailTypeAdapter;
import com.dn.lotte.ui.purchase.adapter.PurchaseNumberAdapter;
import com.dn.lotte.ui.purchase.contract.PurchaseDetailContract;
import com.dn.lotte.ui.purchase.model.PurchaseDetailModel;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;
import com.dn.lotte.ui.usercenter.activity.KeFuActivity;
import com.dn.lotte.utils.CommonUtils;
import com.dn.lotte.utils.TestCodeUtils;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.CustomPopWindow;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.lzy.okgo.model.HttpParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.dn.lotte.utils.TestCodeUtils.BetNumerice;

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░▄░░
 * ░░░░░░░░░▐█░░░░░░░░░░░▄▀▒▌░
 * ░░░░░░░░▐▀▒█░░░░░░░░▄▀▒▒▒▐
 * ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
 * ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
 * ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
 * ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒
 * ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
 * ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄
 * ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒
 * ▀▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒
 * 单身狗就这样默默地看着你，一句话也不说。
 * 购彩详情
 *
 * @author DN
 * @data 2017/10/11
 **/
public class PurchaseDetailActivity extends
        BaseActivity<PurchaseDetailPresenter, PurchaseDetailModel>
        implements PurchaseDetailContract.View, View.OnClickListener, RippleView.OnRippleCompleteListener, DnToolbar.OnRightTitleClickListener {

    @Bind(R.id.toolbar)
    DnToolbar mToolbar;
    @Bind(R.id.tv_title)
    LinearLayout mTvTitle;
    @Bind(R.id.countRecycler)
    FlexboxLayout mCountRecyclerView;
    @Bind(R.id.tv_time_number)
    TextView mTvTimeNumber;
    @Bind(R.id.tv_content_title)
    TextView mTvContentTitle;
    @Bind(R.id.title)
    TextView mDetailTitle;
    @Bind(R.id.tv_content_tow_name)
    TextView mTvContentTowName;
    @Bind(R.id.tv_last_time)
    TextView mTvLastTime;
    @Bind(R.id.iv_shake)
    ImageView mIvShake;
    @Bind(R.id.gameLinear)
    LinearLayout mGameLinear;
    @Bind(R.id.oneRecyclerView)
    RecyclerView mOneRecyclerView;
    @Bind(R.id.oneLinear)
    LinearLayout mOneLinear;
    @Bind(R.id.etTwo)
    EditText mEtTwo;
    @Bind(R.id.twoLinear)
    LinearLayout mTwoLinear;
    @Bind(R.id.linear)
    RelativeLayout mLinear;
    @Bind(R.id.randomView)
    RippleView mRandomView;
    @Bind(R.id.bettingCard)
    RippleView mBettingCard;
    @Bind(R.id.okBbetting)
    RippleView mOkBbetting;
    @Bind(R.id.oneBbetting)
    RippleView mOneBbetting;
    @Bind(R.id.tv_moneyNumber)
    TextView tvMoneyNumber;
    @Bind(R.id.tv_money)
    TextView mMoney;
    @Bind(R.id.ds_linear)
    LinearLayout mDSLinear;
    @Bind(R.id.tv_show)
    TextView mShow;
    @Bind(R.id.cb_sdz)
    CheckBox mSdz;
    @Bind(R.id.tv_ye)
    TextView mYE;
    @Bind(R.id.rl_trend)
    RelativeLayout rlTrend;
    private PurchaseDetailCountAdapter mPurchaseDetailCountAdapter;
    private String id;
    private CustomPopWindow mCustomPopWindow;
    private List<PurchaseDetailTypeBean.TableBean> mTitleList = new ArrayList<>();
    private List<PurchaseDetailGameBean.TableBean> mContentList = new ArrayList<>();

    private PurchaseDetailTitleAdapter mDetailTitleAdapter;
    private String mTitle;
    private CustomPopWindow mHistoryPopup;
    private List<HistoryDetailBean.TableBean> mHistoryDetailList = new ArrayList<>();
    private List<PurchaseDetailNumberBean.TableBean> mNumberList = new ArrayList<>();
    private String mTowTitles;
    private List<PurchaseDetailNumberBean.TableBean.BallsBean> mDetailNumberList = new ArrayList<>();
    private PurchaseNumberAdapter mPurchaseNumberAdapter;
    private String mDataStr;
    private String mWid;

    private List<SelectNumberBean.NumberBean> mSelectNumberList;
    private String mPlayCode;
    private int moneyNumber;
    private String point;
    private String minbonus;
    private String posbonus;
    private CountDownTimer mTimer;
    private String mMore = "";
    private int anInt = -1;
    private String strPos = "";
    private SeekBar seekBar;

    @Override
    public int getLayoutId() {
        return R.layout.act_purchase_detail;
    }

    @Override
    public void initPresenter() {
        mPresenter.setVM(this, mModel);
    }


    @Override
    public void initView() {
        initTitle();
        mToolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mToolbar.setToolbarRightBackImageRes(R.drawable.morei);
        mToolbar.setOnRightTitleClickListener(this);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        mTitle = extras.getString("title");
        mToolbar.setMainTitle(mTitle);
        mDetailTitle.setText(mTitle);
        mPurchaseNumberAdapter = new PurchaseNumberAdapter(R.layout.item_purchase_number, mDetailNumberList, mPresenter);
        mOneRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mOneRecyclerView.setAdapter(mPurchaseNumberAdapter);
        mSelectNumberList = new ArrayList<>();
        CommonUtils.setSelectNumberList("");
        point = SPUtils.getSharedStringData(mContext, "POINT");

        HttpParams httpParams = new HttpParams();
        httpParams.put("lid", id);
        mPresenter.getGameTime(httpParams);
        mPresenter.getTypeData(httpParams, id);
        mPresenter.getHistoryData(httpParams);

        mLinear.setOnClickListener(this);
        mTvTitle.setOnClickListener(this);
        mRandomView.setOnRippleCompleteListener(this);
        mBettingCard.setOnRippleCompleteListener(this);
        mOkBbetting.setOnRippleCompleteListener(this);
        mOneBbetting.setOnRippleCompleteListener(this);
        requestMoney();
        mSdz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strPos.equals("")) {
                    moneyNumber = TestCodeUtils.BetNumerice(mPlayCode, mDataStr, "", mSdz.isChecked());
                } else {
                    moneyNumber = TestCodeUtils.BetNumerice(mPlayCode, mDataStr, strPos, mSdz.isChecked());
                }
                mMoney.setText(moneyNumber + "注  ");
                double model = 1;
                switch (mModels) {
                    case "元":
                        model = 1;
                        break;
                    case "角":
                        model = 0.1;
                        break;
                    case "分":
                        model = 0.01;
                        break;
                    case "厘":
                        model = 0.001;
                        break;
                }
                if (moneyNumber > 0) {
                    double mulL = CommonUtils.mul(model, moneyNumber);
                    tvMoneyNumber.setText(CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "元");
                } else {
                    tvMoneyNumber.setText(0 + "元");
                }
            }
        });

        mEtTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                String reg = "[\u4e00-\u9fa5]";
                Pattern pat = Pattern.compile(reg);

                Matcher mat = pat.matcher(s.toString());
                String editStr = mat.replaceAll("");
                if (!StringUtils.isContainChinese(editStr)) {
                    String saa = editStr;
                    saa = editStr.replace("\n", " ");
                    editStr = saa;
                    if (mTitle.contains("11选5") || mTitle.contains("北京")) {
                        if (editStr.contains(" ")) {
                            saa = editStr.replace(" ", "_");
                        }
                    } else if (mTitle.contains("赛车")) {
                        if (editStr.contains(" ")) {
                            String ss = editStr.replace(" ", "");
                            saa = ss.replace("0", "");
                        } else {
                            saa = editStr.replace("0", "");
                        }
                    } else {
                        if (saa.contains(" ")) {
                            saa = editStr.replace(" ", ",");
                        } else if (saa.contains(";")) {
                            saa = editStr.replace(";", ",");
                        } else if (saa.contains("；")) {
                            saa = editStr.replace("；", ",");
                        } else if (saa.contains(" ")) {
                            saa = editStr.replace(" ", ",");
                        }
                    }

                    editStr = saa;
                    Log.i("DDD", editStr);
                    if (mPlayCode != null) {

                        if (strPos.equals("")) {
                            moneyNumber = TestCodeUtils.BetNumerice(mPlayCode, editStr, "", mSdz.isChecked());
                        } else {
                            moneyNumber = TestCodeUtils.BetNumerice(mPlayCode, editStr, strPos, mSdz.isChecked());
                        }
                    } else {
                        moneyNumber = 0;
                    }

                    List<String> listStr = Arrays.asList(editStr.split(","));
                    List<String> newList = new ArrayList(new HashSet(listStr));
                    List<String> newList1 = new ArrayList(new HashSet(listStr));
                    if (mPlayCode.equals("R_3HX")) {
                        for (int i = 0; i < newList1.size(); i++) {
                            String[] strArrary = TestCodeUtils.getStrArr(newList1.get(i).split(""));
                            if (strArrary.length == 3) {
                                if (strArrary[0].equals(strArrary[1]) && strArrary[1].equals(strArrary[2])) {
                                    newList1.remove(i);
                                }
                            }
                        }
                    }
                    newList = newList1;
                    String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
                    StringBuffer sb = new StringBuffer();

                    for (String s2 : strArray2) {
                        sb.append(s2 + ",");
                    }

                    if (moneyNumber > 0) {
                        mDataStr = sb.substring(0, sb.toString().length() - 1);
                    }
                    mMoney.setText(moneyNumber + "注  ");
//                Log.i("Dddd", TestCodeUtils.BetNumerice(mPlayCode, editStr, "") + "");
                    double model = 1;
                    switch (mModels) {
                        case "元":
                            model = 1;
                            break;
                        case "角":
                            model = 0.1;
                            break;
                        case "分":
                            model = 0.01;
                            break;
                        case "厘":
                            model = 0.001;
                            break;
                    }
                    if (moneyNumber > 0) {
                        double mulL = CommonUtils.mul(model, moneyNumber);
                        tvMoneyNumber.setText(CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "元");
                    } else {
                        tvMoneyNumber.setText(0 + "元");
                    }
                } else {

                }

//                String s = "123,123";
//                Log.i("DDD",editStr.replace(",",":"));
//                editStr.replace(",", "7");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //点击跳转走势图
        rlTrend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                startActivity(KeFuActivity.class,bundle);
           /*   String kefuurl= "http://www.happy916.com/chart.html?lid="+id;
                Uri uri = Uri.parse(kefuurl);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        oncancel(mTvLastTime);
    }

    /**
     * 取消倒计时
     *
     * @param v
     */
    public void oncancel(View v) {
        if (mTimer != null) mTimer.cancel();
    }

    /**
     * 开始倒计时
     *
     * @param v
     */
    public void restart(View v) {
        if (mTimer != null) mTimer.start();
    }

    private void initTime(final long l1, final long l2) {
        //            mTvShow.setEnabled(true);
//            mTvShow.setText("获取验证码");

        mTimer = new CountDownTimer(l1, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                HttpParams httpParams = new HttpParams();
                httpParams.put("lid", id);
                mPresenter.getHistoryData(httpParams);
                //62
                long time = millisUntilFinished / 1000;
//                if (time<59){
//                    if (mTvLastTime != null) mTvLastTime.setText((millisUntilFinished / 1000) + "秒");
//                }else if (){}
                long time1 = time % 60;

                long time2 = (time - time1) / 60 % 60;

                long time3 = ((time - time1) / 60 - time2) / 60;

                if (mTvLastTime != null)
                    mTvLastTime.setText(time3 + (time2 < 10 ? ":0" : ":") + time2 + (time1 < 10 ? ":0" : ":") + time1);
            }

            @Override
            public void onFinish() {
//            mTvShow.setEnabled(true);
//            mTvShow.setText("获取验证码");
                if (mTvLastTime != null) mTvLastTime.setText("本期投注已经截止");
                HttpParams httpParams = new HttpParams();
                httpParams.put("lid", id);
                mPresenter.getGameTime(httpParams);
            }
        };
    }

    ;

    /**
     * @param typeBean 五星
     */
    @Override
    public void returnDetailTypeList(PurchaseDetailTypeBean typeBean) {
        mTitleList = typeBean.getTable();
        if (mTitleList != null && mTitleList.size() > 0) {
            mTitleList.get(0).setChecked(true);
            HttpParams httpParams = new HttpParams();
            httpParams.put("lid", id);
            mPresenter.getGameData(httpParams, "DNID" + id);
            //  mPresenter.getGameData(httpParams, "DNID" + mTitleList.get(0).getId());
        }
    }

    /**
     * @param gameBean 五星下
     */
    @Override
    public void returnDetailGameList(PurchaseDetailGameBean gameBean) {
        mContentList = gameBean.getTable();
        if (mContentList != null && mContentList.size() > 0) {
            CommonUtils.setGameTypeList(mContentList);
//            mContentList = CommonUtils.getGameTypeList();
//            if (!StringUtils.isEmpty(mTitleList.get(0).getTitle())) {
//                if (mTvContentTitle != null) mTvContentTitle.setText(mTitleList.get(0).getTitle());
//            }
            if (mContentList != null && mContentList.size() > 0) {
                mTowTitles = mContentList.get(0).getTitle();
                if (mTvContentTowName != null) {
                    mTvContentTowName.setText(mTowTitles == null ? "" : mTowTitles);
                    if (mTvContentTitle != null) {
                        for (PurchaseDetailTypeBean.TableBean bean : mTitleList) {
                            if (bean.getId().equals(mContentList.get(0).getRadio())) {
                                mTvContentTitle.setText(bean.getTitle());
                            }
                        }
                    }
                }
            } else
                mTvContentTowName.setText("");


            View contentView = LayoutInflater.from(this).inflate(R.layout.purchase_detail_foot, null);
            View contentView2 = LayoutInflater.from(this).inflate(R.layout.foot_detail_rs, null);
            String title = mTvContentTitle.getText().toString();
            if (title.equals("前二") || title.equals("后二") || title.equals("任二")) {
                String contentTile = mContentList.get(0).getTitlename();
                if (contentTile.equals("直选复式") || contentTile.equals("直选单式") || contentTile.equals("任二复式") || contentTile.equals("任二单式")) {
                    mSdz.setVisibility(View.VISIBLE);
                }
            } else {
                mSdz.setVisibility(View.GONE);
                mSdz.setChecked(false);
            }
            //改了
            if (mContentList.get(0).getTitlename().contains("单式") || mContentList.get(0).getTitlename().contains("混选") || mContentList.get(0).getTitlename().contains("混合")) {
                if (mTitle.contains("11选5") || mTitle.contains("秒赛车") || mTitle.contains("北京PK10")) {
                    mShow.setText("每注之间用逗号[,]隔开,号码之间用空格[ ]隔开，不足2位要在前面加0。比如 09。");
                } else {
                    mShow.setText("请输入投注内容,每注选号请使用逗号,空格或 “ , ” 分隔开。");
                }
                mRandomView.setVisibility(View.GONE);
                mOneLinear.setVisibility(View.GONE);
                mTwoLinear.setVisibility(View.VISIBLE);
                mDSLinear.removeAllViews();
                if (mTvContentTitle.getText().toString().equals("任四") || mTvContentTitle.getText().toString().equals("任三") || mTvContentTitle.getText().toString().equals("任二")) {
                    mDSLinear.addView(contentView2);
                    setRSStatus(contentView2, mTvContentTitle.getText().toString());

                } else {
                    strPos = "";
                    anInt = -1;
                    mEtTwo.setText("");
                }

                mDSLinear.addView(contentView);
            } else {
                mPurchaseNumberAdapter.removeAllFooterView();
                if (mTvContentTitle.getText().toString().equals("任四") || mTvContentTitle.getText().toString().equals("任三") || mTvContentTitle.getText().toString().equals("任二")) {
                    mPurchaseNumberAdapter.addFooterView(contentView2);
                    setRSStatus(contentView2, mTvContentTitle.getText().toString());
                } else {
                    anInt = -1;
                    strPos = "";
                }
                mPurchaseNumberAdapter.addFooterView(contentView);
                mOneLinear.setVisibility(View.VISIBLE);
                mRandomView.setVisibility(View.VISIBLE);
                mTwoLinear.setVisibility(View.GONE);
            }
//            if (mTvContentTitle.getText().toString().equals("组选60")) {
//                minbonus = gameBean.getTable().get(0).getMinbonus2();
//                posbonus = gameBean.getTable().get(0).getPosbonus2();
//            } else {
            minbonus = gameBean.getTable().get(0).getMinbonus();
            posbonus = gameBean.getTable().get(0).getPosbonus();
//            }

            adjustStatus(contentView);
            mMore = gameBean.getTable().get(0).getExample();
            HttpParams httpParams = new HttpParams();
            httpParams.put("lid", id);
            mPresenter.getNumberData(httpParams);
            mMoney.setText(moneyNumber + "注  ");
            tvMoneyNumber.setText(0 + " 元");
        } else {
            stopProgressDialog();
        }
    }

    private void setRSStatus(View view, String name) {
        CheckBox cbW = (CheckBox) view.findViewById(R.id.cb_w);
        CheckBox cbQ = (CheckBox) view.findViewById(R.id.cb_q);
        CheckBox cbB = (CheckBox) view.findViewById(R.id.cb_b);
        CheckBox cbS = (CheckBox) view.findViewById(R.id.cb_s);
        CheckBox cbG = (CheckBox) view.findViewById(R.id.cb_g);
        final TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        int tag = 0;
        if (name.equals("任四")) {
            cbQ.setChecked(true);
            cbB.setChecked(true);
            cbS.setChecked(true);
            cbG.setChecked(true);
            tag = 4;
        } else if (name.equals("任三")) {
            cbB.setChecked(true);
            cbS.setChecked(true);
            cbG.setChecked(true);
            tag = 3;
        } else if (name.equals("任二")) {
            cbS.setChecked(true);
            cbG.setChecked(true);
            tag = 2;
        }
        final List<CheckBox> list = new ArrayList<>();
        list.add(cbW);
        list.add(cbQ);
        list.add(cbB);
        list.add(cbS);
        list.add(cbG);
        getSelectNumbner(tag, list, tvNumber);
        final int finalTag = tag;
        cbW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getSelectNumbner(finalTag, list, tvNumber);
            }
        });
        cbQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getSelectNumbner(finalTag, list, tvNumber);
            }
        });
        cbB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getSelectNumbner(finalTag, list, tvNumber);
            }
        });
        cbS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getSelectNumbner(finalTag, list, tvNumber);
            }
        });

        cbG.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getSelectNumbner(finalTag, list, tvNumber);
            }
        });
    }

    private void getSelectNumbner(int tag, List<CheckBox> list, TextView numerText) {
        List<String> numberList = new ArrayList<>();
        anInt = 0;
        StringBuilder sb = new StringBuilder();
        for (CheckBox checkBox : list) {
            if (checkBox.isChecked()) {
                sb.append("1" + ",");
            } else {
                sb.append("0" + ",");
            }
        }
        strPos = sb.toString().substring(0, sb.length() - 1);
        if (tag == 4) {
            for (CheckBox checkBox : list) {
                if (checkBox.isChecked()) {
                    numberList.add("");
                }
            }
            if (numberList.size() == 5) {
                anInt = 5;
            } else if (numberList.size() == 4) {
                anInt = 1;
            } else {
                anInt = 0;
            }
        } else if (tag == 3) {
            for (CheckBox checkBox : list) {
                if (checkBox.isChecked()) {
                    numberList.add("");
                }
            }
            if (numberList.size() == 5) {
                anInt = 10;
            } else if (numberList.size() == 4) {
                anInt = 4;
            } else if (numberList.size() == 3) {
                anInt = 1;
            } else {
                anInt = 0;
            }
        } else if (tag == 2) {
            for (CheckBox checkBox : list) {
                if (checkBox.isChecked()) {
                    numberList.add("");
                }
            }
            if (numberList.size() == 5) {
                anInt = 10;
            } else if (numberList.size() == 4) {
                anInt = 6;
            } else if (numberList.size() == 3) {
                anInt = 3;
            } else if (numberList.size() == 2) {
                anInt = 1;
            } else {
                anInt = 0;
            }
        }
        numerText.setText("共有 " + anInt + " 种方案");
        moneyNumber = BetNumerice(mPlayCode, mDataStr, strPos, mSdz.isChecked());
        mMoney.setText(moneyNumber + "注  ");
        double model = 1;
        switch (mModels) {
            case "元":
                model = 1;
                break;
            case "角":
                model = 0.1;
                break;
            case "分":
                model = 0.01;
                break;
            case "厘":
                model = 0.001;
                break;
        }
        if (moneyNumber > 0) {
            double mulL = CommonUtils.mul(model, moneyNumber);
            tvMoneyNumber.setText(CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "元");
        } else {
            tvMoneyNumber.setText(0 + "元");
        }

    }

    /**
     * 返回选中的 游戏  直选 + 直选复式
     *
     * @param gameBean
     */
    @Override
    public void returnSelectData(PurchaseDetailGameBean.TableBean gameBean) {
        if (mCustomPopWindow != null) {
            mCustomPopWindow.dissmiss();
        }
        for (PurchaseDetailTypeBean.TableBean bean : mTitleList) {
            if (bean.isChecked()) {
                mTvContentTitle.setText(bean.getTitle());
            }
        }
        minbonus = gameBean.getMinbonus();
        posbonus = gameBean.getPosbonus();
//
//        minbonus = gameBean.getMinbonus();
//        posbonus = gameBean.getPosbonus();
        mTvContentTowName.setText(gameBean.getTitle());
        mWid = gameBean.getId();
        mPlayCode = gameBean.getTitle2();
        mRemberName = gameBean.getTitlename();
        mMore = gameBean.getExample();
        View contentView = LayoutInflater.from(this).inflate(R.layout.purchase_detail_foot, null);
        View contentView2 = LayoutInflater.from(this).inflate(R.layout.foot_detail_rs, null);
        String title = mTvContentTitle.getText().toString();
        if (title.equals("前二") || title.equals("后二") || title.equals("任二")) {
            String contentTile = gameBean.getTitlename();
            if (contentTile.equals("前二直选复式") || contentTile.equals("前二直选单式") || contentTile.equals("后二直选复式") || contentTile.equals("后二直选单式") || contentTile.equals("任二直选复式") || contentTile.equals("任二直选单式")) {
                mSdz.setVisibility(View.VISIBLE);
            }
        } else {
            mSdz.setVisibility(View.GONE);
            mSdz.setChecked(false);
        }
        if (gameBean.getTitlename().contains("单式") || gameBean.getTitlename().contains("混选") || gameBean.getTitlename().contains("混合")) {
            mOneLinear.setVisibility(View.GONE);
            mTwoLinear.setVisibility(View.VISIBLE);
            mRandomView.setVisibility(View.GONE);
            if (mTitle.contains("11选5") || mTitle.contains("秒赛车") || mTitle.contains("北京PK10")) {
                mShow.setText("每注之间用逗号[,]隔开,号码之间用空格[ ]隔开，不足2位要在前面加0。比如 09。");
            } else {
                mShow.setText("请输入投注内容,每注选号请使用逗号,空格或 “ , ” 分隔开。");
            }
            mDSLinear.removeAllViews();
            if (mTvContentTitle.getText().toString().equals("任四") || mTvContentTitle.getText().toString().equals("任三") || mTvContentTitle.getText().toString().equals("任二")) {
                mDSLinear.addView(contentView2);
                setRSStatus(contentView2, mTvContentTitle.getText().toString());
            } else {
                strPos = "";
                anInt = -1;
            }
            mDSLinear.addView(contentView);
        } else {
            mPurchaseNumberAdapter.removeAllFooterView();
            if (mTvContentTitle.getText().toString().equals("任四") || mTvContentTitle.getText().toString().equals("任三") || mTvContentTitle.getText().toString().equals("任二")) {
                mPurchaseNumberAdapter.addFooterView(contentView2);
                setRSStatus(contentView2, mTvContentTitle.getText().toString());
            } else
                anInt = -1;
            mPurchaseNumberAdapter.addFooterView(contentView);
            mOneLinear.setVisibility(View.VISIBLE);
            mTwoLinear.setVisibility(View.GONE);
            mRandomView.setVisibility(View.VISIBLE);
            for (PurchaseDetailNumberBean.TableBean tableBean : mNumberList) {
                if (tableBean.getTitleName().equals(gameBean.getTitlename())) {
                    mDetailNumberList = tableBean.getBalls();
                    list.clear();
                    list11.clear();
                    for (int a = 0; a < mDetailNumberList.size(); a++) {
                        list.add("");
                        list11.add("");
                    }
                    mDataStr = "";
                    mPurchaseNumberAdapter.setNewData(mDetailNumberList);
                    mPurchaseNumberAdapter.randomData(1);
                }
            }
        }
        adjustStatus(contentView);
        moneyNumber = 0;
        mMoney.setText(moneyNumber + "注  ");
        tvMoneyNumber.setText(0 + " 元");
    }

    private String mModels = "元";
    private String mAlltotal = "";
    private String mPrice = "";
    private String mTimes = "1";
    private String nowPoint = "";
    private String mSingelBouns = "";

    private void adjustStatus(View contentView) {
        seekBar = (SeekBar) contentView.findViewById(R.id.seekbar);
        final TextView nowBounds = (TextView) contentView.findViewById(R.id.tv_now_bounds);
        final TextView mFD = (TextView) contentView.findViewById(R.id.tv_fd);
        final TextView mtvBuunds = (TextView) contentView.findViewById(R.id.tv_bounds);
        ImageButton addButton = (ImageButton) contentView.findViewById(R.id.add);
        ImageButton redButton = (ImageButton) contentView.findViewById(R.id.red);
        final EditText number = (EditText) contentView.findViewById(R.id.number);

        RadioGroup radioGroup = (RadioGroup) contentView.findViewById(R.id.group);
        RadioButton element = (RadioButton) contentView.findViewById(R.id.element);
        RadioButton angle = (RadioButton) contentView.findViewById(R.id.angle);
        RadioButton points = (RadioButton) contentView.findViewById(R.id.points);
        RadioButton deter = (RadioButton) contentView.findViewById(R.id.deter);
        CunModeBean moneyMode = GlobalApplication.getInstance().getMoneyMode();


        if (moneyMode != null && moneyMode.getMode() != null) {
            switch (moneyMode.getMode()) {
                case "元":
                    mModels = "元";
                    break;
                case "角":
                    mModels = "角";
                    break;
                case "分":
                    mModels = "分";
                    break;
                case "厘":
                    mModels = "厘";
                    break;
            }
        }

        switch (mModels) {
            case "元":
                element.setChecked(true);
                break;
            case "":
                element.setChecked(true);
                break;
            case "角":
                angle.setChecked(true);

                break;
            case "分":
                points.setChecked(true);
                break;
            case "厘":
                deter.setChecked(true);
                break;
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.element:
                        mModels = "元";
                        mPrice = CommonUtils.mul(1, Double.parseDouble(moneyNumber + "")) + "";
                        double mulY = CommonUtils.mul(1, Double.parseDouble(moneyNumber + ""));
//                        mAlltotal = CommonUtils.mul(mulY, Double.parseDouble(mTimes)) + "";
                        setNowBound(nowBounds);
                        break;
                    case R.id.angle:
                        mModels = "角";
                        mPrice = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + "")) + "";
                        double mulJ = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + ""));
//                        mAlltotal = CommonUtils.mul(mulJ, Double.parseDouble(mTimes)) + "";

                        setNowBound(nowBounds);
                        break;
                    case R.id.points:
                        mModels = "分";
                        mPrice = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + "")) + "";
                        double mulF = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + ""));
//                        mAlltotal = CommonUtils.mul(mulF, Double.parseDouble(mTimes)) + "";
                        setNowBound(nowBounds);
                        break;
                    case R.id.deter:

                        mModels = "厘";
                        mPrice = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + "")) + "";
                        double mulL = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + ""));
//                        mAlltotal = CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "";
                        setNowBound(nowBounds);
                        break;
                }
            }
        });
        nowPoint = point;
        Log.i("DNlog1", (int) (Double.parseDouble(point) * 10) + "::::" + point);

        seekBar.setMax((int) (Double.parseDouble(point) * 10));

        seekBar.setProgress((int) (Double.parseDouble(point) * 10));

        ititBounds(point, Double.parseDouble(nowPoint) * 10 + "", nowBounds, mFD, mtvBuunds);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                ititBounds(point, progress + "", nowBounds, mFD, mtvBuunds);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        number.setText(mTimes);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(number.getText().toString());
                a = a + 1;
                number.setText(a + "");
                mTimes = a + "";
                setNowBound(nowBounds);
            }
        });

        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(number.getText().toString());
                if (a == 1) {
                    mTimes = 1 + "";
                } else {
                    a = a - 1;
                    mTimes = a + "";
                }
                number.setText(a + "");

                setNowBound(nowBounds);
            }
        });

        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (StringUtils.isEmpty(s.toString())) {
//                    number.setText("1");
                    mTimes = "1";
                } else {
                    mTimes = number.getText().toString();
                }

                setNowBound(nowBounds);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    float cha ;
    private void ititBounds(String point, String progress, TextView nowBounds, TextView mFD, TextView mtvBuunds) {
        nowPoint = (float) Double.parseDouble(progress) / 10 + "";
        float inum = (float) Double.parseDouble(point) - (float) Double.parseDouble(progress) / 10;
        DecimalFormat df1 = new DecimalFormat("0");// 格式化小数
        Log.i("DNlog", progress + "----" + point);
        String s = (Double.parseDouble(point) * 10 - Double.parseDouble(progress)) / 10 + "";
        double inum2 ;
        if ((float) Double.parseDouble(progress)>145){
            cha=       (float) Double.parseDouble(progress) -145;
            inum2 = 14.5;
            nowPoint = "14.5";
            s = (float) (Double.parseDouble(progress)-145)/10+"";
//            s = (float) Double.parseDouble(progress)/10-14.5+"";
        }else if ((float) Double.parseDouble(progress)>120){
            inum2 = (float) Double.parseDouble(point) - inum;
//            s = (float) Double.parseDouble(progress)/10-12+"";
        }else {
            inum2 = (float)12;
            nowPoint = "12";
            s = (float) (Double.parseDouble(point) * 10-120)/10+"";
        }
        String s1 = df1.format(1700 + inum2 * 10 * 2);// 返回的是String类型

        mtvBuunds.setText(s1);
        mFD.setText(s);
        setNowBound(nowBounds);
    }

    public void setNowBound(TextView nowBounds) {
        double du = CommonUtils.sub(Double.parseDouble(point), Double.parseDouble(nowPoint));

        double v = Double.parseDouble(nowPoint);
        if (mTitle.equals("福彩3D") || mTitle.equals("体彩P3")) {
            if (v == 13.0) {
                v = 11.0;
            }
        }

        double mul = CommonUtils.mul(20, v);
        double mul2 = CommonUtils.mul(mul, Double.parseDouble(posbonus));
        double add = CommonUtils.add(Double.parseDouble(minbonus), mul2);
        double times = Double.parseDouble(mTimes);
        double model = 1;
        switch (mModels) {
            case "元":
                model = 1;
                break;
            case "角":
                model = 0.1;
                break;
            case "分":
                model = 0.01;
                break;
            case "厘":
                model = 0.001;
                break;
        }

        double timeModel = CommonUtils.mul(times, model); //倍数*圆角模式
        double nowMoney = CommonUtils.mul(timeModel, add); //金钱数量
        nowMoney = CommonUtils.div(nowMoney, (double) 2, 3);

        nowBounds.setText(nowMoney + "");
        if (moneyNumber > 0) {
            double mulL = CommonUtils.mul(model, moneyNumber);
            tvMoneyNumber.setText(CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "元");
        } else {
            tvMoneyNumber.setText(0 + "元");
        }
        mSingelBouns = nowMoney + "";
    }

    //选号总数据
    @Override
    public void returnNumberData(PurchaseDetailNumberBean numberbean) {
        if (numberbean.getResult().equals("1")) {
            mNumberList = numberbean.getTable();
            mWid = mContentList.get(0).getId();
            mPlayCode = mContentList.get(0).getTitle2();
            for (PurchaseDetailNumberBean.TableBean tableBean : mNumberList) {
                if (tableBean.getTitleName().equals(mContentList.get(0).getTitlename())) {
                    mRemberName = tableBean.getTitleName();
                    mDetailNumberList = tableBean.getBalls();
                    list.clear();
                    list11.clear();
                    for (int a = 0; a < mDetailNumberList.size(); a++) {
                        list.add("");
                        list11.add("");
                    }
                    mDataStr = "";
                    mPurchaseNumberAdapter.setNewData(mDetailNumberList);
                }
            }
            stopLoading();
        }
    }

    //历史数据
    @Override
    public void returnHistoryData(HistoryDetailBean historyDetailBean) {
        if (historyDetailBean.getResult().equals("1")) {
            mHistoryDetailList = historyDetailBean.getTable();
            if (mHistoryDetailList != null && mHistoryDetailList.size() > 0) {
                String number = mHistoryDetailList.get(0).getNumber();
                showlabel(mCountRecyclerView, number);
                if (mTvTimeNumber != null)
                    mTvTimeNumber.setText(mHistoryDetailList.get(0).getTitle());
            }
        }
    }

    List<String> list = new ArrayList<>();
    List<String> list11 = new ArrayList<>();
    List<Integer> mPositionlist = new ArrayList<>();
    Map<String, String> mRemberMap = new HashMap<>();
    String mRemberName = "";

    @Override
    public void returnNumberSelectedData(int postion, List<SelectedBean> selectedBeanList) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb11 = new StringBuilder();
        for (SelectedBean s : selectedBeanList) {
            sb.append(s.getName());
            sb11.append(s.getName() + "_");
            mPositionlist.add(s.getPosition());
        }
        StringBuilder sb2 = new StringBuilder();
        list.add(postion, sb.toString());
        list.remove(postion + 1);

        list11.add(postion, sb11.toString());
        list11.remove(postion + 1);

        for (String s : list) {
            sb2.append(s + ",");
        }
        StringBuilder sb12 = new StringBuilder();
        for (String s : list11) {
            if (s.endsWith("_")) {
                sb12.append(s.substring(0, s.length() - 1) + ",");
            } else
                sb12.append(s + ",");
        }
        String towTitle = mTvContentTowName.getText().toString();
        if (towTitle.contains("和值") || mTitle.contains("11选5") || mTitle.contains("赛车")
                || mTitle.contains("北京PK10") || towTitle.equals("组选120") || towTitle.equals("组选60") || towTitle.equals("组选30") ||
                towTitle.equals("组选20") || towTitle.equals("组选10") || towTitle.equals("组选5") || towTitle.equals("组选24") || towTitle.equals("组选12") ||
                towTitle.equals("组选6") || towTitle.equals("组选4")) {
            mDataStr = sb12.toString().substring(0, sb12.toString().length() - 1);
        }
//        if (mTitle.contains("11选5") || mTitle.contains("赛车") || mTitle.contains("北京PK10")) {
//        }
        else {
            mDataStr = sb2.toString().substring(0, sb2.toString().length() - 1);
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String s : list) {
            if (s.equals("")) {
                stringBuffer.append("0" + ",");
            } else
                stringBuffer.append("1" + ",");
        }
        if (mTvContentTitle.getText().toString().contains("任")) {
            moneyNumber = BetNumerice(mPlayCode, mDataStr, strPos, mSdz.isChecked());
        } else {
            moneyNumber = BetNumerice(mPlayCode, mDataStr, stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1), mSdz.isChecked());
//            if (mTitle.contains("11选5")) {
//                moneyNumber = BetNumerice(mPlayCode, sb12.toString().substring(0, sb12.toString().length() - 1), stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1));
//            } else {
//            }
        }


        mMoney.setText(moneyNumber + "注  ");
        double model = 1;
        switch (mModels) {
            case "元":
                model = 1;
                break;
            case "角":
                model = 0.1;
                break;
            case "分":
                model = 0.01;
                break;
            case "厘":
                model = 0.001;
                break;
        }
        if (moneyNumber > 0) {
            double mulL = CommonUtils.mul(model, moneyNumber);
            tvMoneyNumber.setText(CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "元");
        } else {
            tvMoneyNumber.setText(0 + "元");
        }
    }

    @Override
    public void returnGameTime(PurchseDetailTimeBean timeBean) {
        String ordertime = timeBean.getOrdertime();
        String closetime = timeBean.getClosetime();
        int i = Integer.parseInt(ordertime);
        int i2 = Integer.parseInt(closetime);
        i = i - i2;

        if (i == 0) {
//            mTvLastTime.setText("本期投注已经截止");
            HttpParams httpParams = new HttpParams();
            httpParams.put("lid", id);
            mPresenter.getGameTime(httpParams);
        } else {
            initTime(i * 1000, i2 * 1000);
            restart(mTvLastTime);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
                showHistoryDataPopup();
                break;
            case R.id.linear:
                showPopTopWithDarkBg();
                break;
        }
    }

    //历史popwindow
    private void showHistoryDataPopup() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_purchase_detail_history, null);
        RecyclerView historyRecyclerView = (RecyclerView) contentView.findViewById(R.id.historyRecyclerView);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        HistroryDetailDataAdapter histroryDetailDataAdapter = new HistroryDetailDataAdapter(R.layout.item_history_detail, mHistoryDetailList.size() > 8 ? mHistoryDetailList : mHistoryDetailList);

        historyRecyclerView.setAdapter(histroryDetailDataAdapter);
        mHistoryPopup = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .setAnimationStyle(R.style.CustomPopWindowStyle)
                .enableBackgroundDark(true) //弹出popWindow时，背景是否变暗
                .setBgDarkAlpha(0.5f) // 控制亮度
                .setOnDissmissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Log.e("TAG", "onDismiss");
                    }
                })
                .create()
                .showAsDropDown(mTvTitle, -15, 30);
    }

    /**
     * 显示标签
     *
     * @param flexboxLayout FlexboxLayout
     * @param str           “聪明，强，无敌” --> 以 ，隔开的字符串
     */
    private void showlabel(FlexboxLayout flexboxLayout, String str) {
        if (flexboxLayout != null) {
            flexboxLayout.removeAllViews();
            String[] allStr;
            if (!StringUtils.isEmpty(str)) {
                allStr = str.split(",");
                for (int i = 0; i < allStr.length; i++) {
                    TextView textView = new TextView(mContext);
                    textView.setText(allStr[i]);
                    textView.setGravity(Gravity.CENTER);
                    textView.setTextSize(12);
                    textView.setTextColor(getResources().getColor(R.color.white));
                    textView.setBackgroundResource(R.drawable.round_bg);
                    flexboxLayout.addView(textView);
                    ViewGroup.LayoutParams params = textView.getLayoutParams();
                    if (params instanceof FlexboxLayout.LayoutParams) {
                        FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) params;
                        layoutParams.setMargins(10, 8, 0, 0);
                        textView.setLayoutParams(layoutParams);
                    }
                }
            }
        }

    }

    /**
     * 显示PopupWindow 五星+ 窗口
     */
    private void showPopTopWithDarkBg() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_purchase_detail_type, null);
        //处理popWindow 显示内容
        handleLogic(contentView);
        //创建并显示popWindow
        mCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .setAnimationStyle(R.style.CustomPopWindowStyle)
                .enableBackgroundDark(true) //弹出popWindow时，背景是否变暗
                .setBgDarkAlpha(0.5f) // 控制亮度
                .setOnDissmissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Log.e("TAG", "onDismiss");
                    }
                })
                .create()
                .showAsDropDown(mTvContentTitle, -15, 30);
    }

    /**
     * 处理弹出显示内容、点击事件等逻辑
     *
     * @param contentView
     */
    private void handleLogic(View contentView) {
        RecyclerView typeRecyclerView = (RecyclerView) contentView.findViewById(R.id.typeRecyclerView);
        typeRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        PurchaseDetailTypeAdapter mDetailTypeAdapter = new PurchaseDetailTypeAdapter(R.layout.item_purchase_detail_title, mContentList, mPresenter);
        typeRecyclerView.setAdapter(mDetailTypeAdapter);

        RecyclerView titleRecyclerView = (RecyclerView) contentView.findViewById(R.id.titleRecyclerView);
        titleRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        mDetailTitleAdapter = new PurchaseDetailTitleAdapter(R.layout.item_purchase_detail_title, mTitleList, mPresenter, mDetailTypeAdapter);
        titleRecyclerView.setAdapter(mDetailTitleAdapter);
    }

    @Override
    public void showLoading(String title) {
        startProgressDialog();
    }

    @Override
    public void stopLoading() {
        stopProgressDialog();
    }

    @Override
    public void showErrorTip(String msg) {

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mSelectNumberList = CommonUtils.getSelectNumberList();
//        restart(mTvLastTime);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.bettingCard:
                //查看投注单
                if (mSelectNumberList != null && mSelectNumberList.size() > 0) {
                    Gson gson = new Gson();
                    String json = gson.toJson(mSelectNumberList);
                    Bundle bundle = new Bundle();
                    bundle.putString("selectData", json);
                    bundle.putString("lid", id);
                    startActivity(OKBettingActivity.class, bundle);
                } else
                    showShortToast("请先添加投注再查看");
                break;
            case R.id.okBbetting:
                //添加 投注单
                if (mPurchaseNumberAdapter != null && !mRemberName.contains("单式")) {
                    if (moneyNumber > 0) {
                        setData(0, 1);
                        showShortToast("添加成功");
                    } else {
                        if (moneyNumber == -1) {
                            showShortToast("添加失败,定位胆单个位置最多允许投注8码！");
                        } else
                            showShortToast("请选择投注号码");
                    }

                    //记录添加的账号
                } else {
                    //单式
                    if (moneyNumber > 0) {
                        setData(1, 1);
                        mEtTwo.setText("");
                        showShortToast("添加成功");
                    } else {
                        showShortToast("请填写投注号码");
                    }
                }
                break;
            case R.id.randomView:
                //摇一摇 随机
                if (mPurchaseNumberAdapter != null && !mRemberName.contains("单式")) {
                    mPurchaseNumberAdapter.randomData(2);
//                  mPurchaseNumberAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
                    mOneRecyclerView.smoothScrollToPosition(mDetailNumberList.size());
//                  mOneRecyclerView.smoothScrollToPosition(0);
                }
                break;
            case R.id.oneBbetting:
                if (moneyNumber > 0) {
                    if (mPurchaseNumberAdapter != null && !mRemberName.contains("单式")) {
                        setData(0, 3);
                    } else {
                        setData(1, 3);
                    }
                } else {
//                    if (moneyNumber==-1){
//                        showShortToast("投注失败,定位胆单个位置最多允许投注8码！");
//                    }else
                    showShortToast("请选择投注号码");
                }

                break;
        }

    }

    public void setData(int tag, int tag2) {
        SelectNumberBean.NumberBean numberBean = new SelectNumberBean.NumberBean();
        StringBuffer stringBuffer = new StringBuffer();
        for (String s : list) {
            if (s.equals("")) {
                stringBuffer.append("0" + ",");
            } else
                stringBuffer.append("1" + ",");
        }
        numberBean.setLotteryId(id);
        numberBean.setName(mRemberName);
        numberBean.setBalls(mDataStr);
        numberBean.setPlayId(mWid);
        numberBean.setmMinPos(minbonus);
//        numberBean.setPos(posbonus);
        numberBean.setNum(moneyNumber + "");
        numberBean.setFd(point);
        numberBean.setTimes(mTimes);
      double b1  =    Double.parseDouble(point);
        double b2  =    Double.parseDouble(nowPoint);
        double br = b1-b2;
        double br2 =   CommonUtils.sub(b1,b2);
        if (cha>0){
            br2 = CommonUtils.sub(br2, CommonUtils .div(cha,(double)10,2)) ;
        }
        numberBean.setPoint(br2 + "");
        numberBean.setModel(mModels);
        numberBean.setIsClear(mSdz.isChecked() ? "1" : "0");
        switch (mModels) {
            case "元":
                mPrice = "1";
//                mPrice = CommonUtils.mul(1, Double.parseDouble(moneyNumber + "")) + "";
                double mulY = CommonUtils.mul(1, Double.parseDouble(moneyNumber + ""));
                mAlltotal = CommonUtils.mul(mulY, Double.parseDouble(mTimes)) + "";
                break;
            case "角":
//                mPrice = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + "")) + "";
                mPrice = "0.1";
                double mulJ = CommonUtils.mul(0.1, Double.parseDouble(moneyNumber + ""));
                mAlltotal = CommonUtils.mul(mulJ, Double.parseDouble(mTimes)) + "";
                break;
            case "分":
                mPrice = "0.01";
//                mPrice = CommonUtils.mul(0.01, Double.parseDouble(moneyNumber + "")) + "";
                double mulF = CommonUtils.mul(0.01, Double.parseDouble(moneyNumber + ""));
                mAlltotal = CommonUtils.mul(mulF, Double.parseDouble(mTimes)) + "";
                break;
            case "厘":
                mPrice = "0.001";
//                mPrice = CommonUtils.mul(0.001, Double.parseDouble(moneyNumber + "")) + "";
                double mulL = CommonUtils.mul(0.001, Double.parseDouble(moneyNumber + ""));
                mAlltotal = CommonUtils.mul(mulL, Double.parseDouble(mTimes)) + "";
                break;
        }
        numberBean.setPrice(mPrice);
        numberBean.setAlltotal(mAlltotal);
        numberBean.setSingelBouns(mSingelBouns);
        if (tag == 0) {
            if (mRemberName.contains("任式")) {
                numberBean.setStrPos(stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1));
            } else if (mRemberName.contains("任")) {
                numberBean.setStrPos(strPos);
            } else {
                numberBean.setStrPos("");
            }
        } else if (tag == 1) {
            if (anInt != -1) {
                numberBean.setStrPos(strPos);
            } else
                numberBean.setStrPos("");
        }


        if (tag2 == 3) {
            setNowBetting(numberBean);
        } else {
            mSelectNumberList.add(numberBean);
        }
        mPurchaseNumberAdapter.randomData(3);
        mOneRecyclerView.smoothScrollToPosition(mDetailNumberList.size());
    }

    public void setNowBetting(final SelectNumberBean.NumberBean numberBean1) {
        Gson gson = new Gson();
        List<SelectNumberBean.NumberBean> list = new ArrayList<>();
        list.add(numberBean1);
//        Log.i("DDDDD", mSelectNumberList.size() + "");
        String json = gson.toJson(list);
        ServerApi.setBettingData(json)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurchaseDetailNumberBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull PurchaseDetailNumberBean numberBean) {
                        if (numberBean.getResult().equals("1")) {
                            if (mEtTwo != null) mEtTwo.setText("");
                            showShortToast(numberBean.getReturnval());
                            String alltotal = numberBean1.getAlltotal();
                            Intent intent = new Intent(getApplicationContext(), BetSuccessActivity.class);
                            intent.putExtra("strmoney", alltotal);
                            intent.putExtra("tag", "0");
                            startActivity(intent);
                            //投注成功发送消息到我的账户页刷新余额
                            EventBus.getDefault().post(new EventBusBean<String>("TiXianSuccess", "TiXianSuccess"));
                        } else {
                            showShortToast(numberBean.getReturnval());
                        }
//                        mSelectNumberList.clear();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private void requestMoney() {
        UserCenterServerApi.getMoney()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TimeBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull TimeBean timeBean) {
                        if (timeBean.getResult().equals("1")) {
                            if (mYE != null) mYE.setText("账户余额：" + timeBean.getMoney() + "元");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainEventBus(EventBusBean<String> bean) {
        if ("aww".equals(bean.getTag())) {
            if (mPurchaseNumberAdapter != null && !mRemberName.contains("单式")) {
                mPurchaseNumberAdapter.randomData(2);
                mOneRecyclerView.smoothScrollToPosition(0);
                mOneRecyclerView.smoothScrollToPosition(mDetailNumberList.size());
                mOneRecyclerView.smoothScrollToPosition(0);
            }
            requestMoney();
        } else if ("bww".equals(bean.getTag())) {
            requestMoney();
        }
    }

    @Override
    public void onRightClick(View view) {
        showPopMoreBg(view);
    }

    /**
     * 显示PopupWindow 五星+ 窗口
     *
     * @param view
     */
    private void showPopMoreBg(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.top_more, null);
        //创建并显示popWindow
        TextView more = (TextView) contentView.findViewById(R.id.more);
        more.setText(mMore);
        mCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this)
                .setView(contentView)
                .setAnimationStyle(R.style.CustomPopWindowStyle)
                .enableBackgroundDark(true) //弹出popWindow时，背景是否变暗
                .setBgDarkAlpha(0.5f) // 控制亮度
                .setOnDissmissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Log.e("TAG", "onDismiss");
                    }
                })
                .create()
                .showAsDropDown(mTvContentTitle, -15, 30);
    }

}
