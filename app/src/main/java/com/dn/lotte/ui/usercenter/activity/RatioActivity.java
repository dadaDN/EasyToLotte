package com.dn.lotte.ui.usercenter.activity;

import android.view.View;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.QiyueBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/28.
 */

public class RatioActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.okBtn_day)
    RippleView okBtnDay;
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_ratio;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle("我的契约");
        okBtn.setOnRippleCompleteListener(this);
        okBtnDay.setOnRippleCompleteListener(this);
       loadqiyu();
    }

    private void loadqiyu() {
        UserCenterServerApi.loawqiyue().
                doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<QiyueBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull QiyueBean qiyueBean) {
                        stopProgressDialog();
                        if (qiyueBean.getFhState().equals("1")) {//有分红契约
                            okBtn.setVisibility(View.VISIBLE);
                        } else {
                            okBtn.setVisibility(View.GONE);

                        }
                        if (qiyueBean.getGzState().equals("1")) {//有工资契约
                            okBtnDay.setVisibility(View.VISIBLE);
                        } else {
                            okBtnDay.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn_day://工资契约
                startActivity(DayRatioActivity.class);
                break;
            case R.id.okBtn://分红契约
                startActivity(RatioFhActivity.class);
                break;
        }
    }

}
