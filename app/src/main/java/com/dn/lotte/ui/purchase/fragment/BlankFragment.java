package com.dn.lotte.ui.purchase.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dn.lotte.R;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class BlankFragment extends Fragment {

    @Bind(R.id.tablayout)
    SlidingTabLayout mTablayout;
    @Bind(R.id.viewpager)
    ViewPager mViewpager;
    private String mTitle;
    private final String[] mTitles = {"个人", "单位"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;

    public static BlankFragment getInstance(String title) {
        BlankFragment sf = new BlankFragment();
        sf.mTitle = title;
        return sf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_purchase_recored, null);
            ButterKnife.bind(this, v);
            for (String title : mTitles) {
                mFragments.add(new PurchaseGoFragment());
            }
            mAdapter = new MyPagerAdapter(getFragmentManager());
            mViewpager.setAdapter(mAdapter);
            mTablayout.setViewPager(mViewpager, mTitles, getActivity(), mFragments);
        }
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
