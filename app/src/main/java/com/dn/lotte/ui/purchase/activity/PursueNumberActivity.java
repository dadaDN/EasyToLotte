package com.dn.lotte.ui.purchase.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.ui.purchase.fragment.PursueFragment;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * 追号
 * Created by ASUS on 2017/10/26.
 */

public class PursueNumberActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tablayout)
    SlidingTabLayout tablayout;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private final String[] mTitles = {
            "利润率追号", "同倍追号", "翻倍追号"};
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private MyPagerAdapter mAdapter;
    @Override
    public int getLayoutId() {
        return R.layout.activity_pursuenumber;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        Bundle extras = getIntent().getExtras();
        String selectData = extras.getString("pursuenumber");
        initTitle();
        toolbar.setMainTitle("我要追号");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mFragments.add(new PursueFragment("1",selectData));
        mFragments.add(new PursueFragment("2",selectData));
        mFragments.add(new PursueFragment("3",selectData));
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(mAdapter);
        tablayout.setViewPager(viewpager,mTitles,this,mFragments);
    }
    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }
}
