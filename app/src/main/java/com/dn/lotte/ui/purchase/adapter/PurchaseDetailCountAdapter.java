package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseDetailCountAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public PurchaseDetailCountAdapter(@LayoutRes int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_count, item);

    }

}
