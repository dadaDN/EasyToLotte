package com.dn.lotte.ui.usercenter.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.PurseNumberBean;
import com.dn.lotte.ui.usercenter.adapter.ZnumberDetailsAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/14.
 */

public class ZnumberDetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rv_list)
    RecyclerView rvList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private String xid;
    private List<PurseNumberBean.TableBean> mpurselist = new ArrayList<>();
    private int page = 1;
    private int pagesize = 10;
    private ZnumberDetailsAdapter mAdapter;
    private int isRefresh = 0;
    private boolean mLoadMoreEndGone = false;

    @Override
    public int getLayoutId() {
        return R.layout.activity_detailsznumber;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        isRefresh = 0;
        toolbar.setMainTitle(R.string.user_details);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        swipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        swipeLayout.setOnRefreshListener(this);
        rvList.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new ZnumberDetailsAdapter(R.layout.item_number_zhuiaho, mpurselist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        mAdapter.setLoadMoreView(mLoadMoreView);
        mAdapter.setOnLoadMoreListener(this, rvList);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
//        mLoadMoreEndGone = true;
        rvList.setAdapter(mAdapter);
        initDate();
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra("details", mAdapter.getData().get(position));
                startActivity(intent);
            }
        });
    }

    private void initDate() {
        xid = getIntent().getStringExtra("id");
        getpursedetails(xid);
    }

    private void getpursedetails(String xid) {
        final HttpParams params = new HttpParams();
        params.put("id", xid);//测试用86id
        params.put("page", page);
        params.put("pagesize", pagesize);
        UserCenterServerApi.pursedetails(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PurseNumberBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull PurseNumberBean purseNumberBean) {
                        stopProgressDialog();
                        if (purseNumberBean.getResult().equals("1")) {

                            if (isRefresh == 1) {
                                mAdapter.loadMoreComplete();
                                mAdapter.addData(purseNumberBean.getTable());
                            } else {
                                swipeLayout.setRefreshing(false);
                                mAdapter.setNewData(purseNumberBean.getTable());
                            }
                            if (purseNumberBean.getTable().size() < pagesize) {
                                mLoadMoreEndGone = true;
                                mAdapter.loadMoreEnd(mLoadMoreEndGone);
                            }
                        } else {
                            showShortToast(purseNumberBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }


    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        initDate();
    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        initDate();
    }
}
