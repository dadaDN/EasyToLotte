package com.dn.lotte.ui.purchase.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import butterknife.Bind;

/**
 * Created by ASUS on 2018/1/21.
 */

public class TrendActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.webview)
    WebView webview;
    private String id;

    @Override
    public int getLayoutId() {
        return R.layout.activity_trend;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        toolbar.setMainTitle("号码走势图");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        String url = "http://www.happy916.com/chart.html?lid=" + id;
        WebSettings webSettings = webview.getSettings();
        // 设置WebView属性，能够执行Javascript脚本
        webSettings.setJavaScriptEnabled(true);
        // 设置可以访问文件
        webSettings.setAllowFileAccess(true);
        // 设置支持缩放
        webSettings.setBuiltInZoomControls(true);
        // 加载需要显示的网页
        webview.loadUrl(url);
        // 设置Web视图
        // wv.setWebViewClient(new webViewClient ());
        webview.setWebViewClient(new webViewClient());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    // 设置回退
    // 覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack(); // goBack()表示返回WebView的上一页面
            return true;
        }
        finish();// 结束退出程序
        return false;
    }

    // Web视图
    private class webViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            showShortToast("ddddd");
            return true;

        }

    }
}
