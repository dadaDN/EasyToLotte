package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.ui.purchase.presenter.PurchaseDetailPresenter;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseDetailTypeAdapter extends BaseQuickAdapter<PurchaseDetailGameBean.TableBean, BaseViewHolder> {
    PurchaseDetailPresenter presenter;

    public PurchaseDetailTypeAdapter(@LayoutRes int layoutResId,
                                     @Nullable List<PurchaseDetailGameBean.TableBean> data,
                                     PurchaseDetailPresenter presenter) {
        super(layoutResId, data);
        this.presenter = presenter;
    }

    @Override
    protected void convert(final BaseViewHolder helper, final PurchaseDetailGameBean.TableBean item) {
        helper.setText(R.id.cb_item, item.getTitle());
        final CheckBox mCheckBox = helper.getView(R.id.cb_item);

        mCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.returnSelectData(item);
            }
        });

    }

}
