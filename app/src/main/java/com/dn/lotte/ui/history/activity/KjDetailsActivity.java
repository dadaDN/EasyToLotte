package com.dn.lotte.ui.history.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.kjDetailsBean;
import com.dn.lotte.ui.usercenter.adapter.KjDetailsAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/15.
 */

public class KjDetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rv_list)
    RecyclerView rvList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private String kjid;
    private String name;
    private List<kjDetailsBean.TableBean> mlist=new ArrayList<>();
    private KjDetailsAdapter kjDetailsAdapter;
    private boolean mLoadMoreEndGone = false;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_history;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        kjid = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        initTitle();
        toolbar.setMainTitle(name);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        swipeLayout.setOnRefreshListener(this);
        rvList.setLayoutManager(new LinearLayoutManager(mContext));
        kjDetailsAdapter = new KjDetailsAdapter(R.layout.item_kjdetails,mlist);
        CustomLoadMoreView mLoadMoreView = new CustomLoadMoreView();
        kjDetailsAdapter.setLoadMoreView(mLoadMoreView);
        kjDetailsAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        kjDetailsAdapter.isFirstOnly(false);
        mLoadMoreEndGone = true;
        rvList.setAdapter(kjDetailsAdapter);
        initDate();
    }

    private void initDate() {
        HttpParams params=new HttpParams();
        params.put("lid",kjid);
        UserCenterServerApi.getkjdetails(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<kjDetailsBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull kjDetailsBean kjDetailsBean) {
                        stopProgressDialog();
                        if (kjDetailsBean.getResult().equals("1")) {
                            mlist = kjDetailsBean.getTable();
                            swipeLayout.setRefreshing(false);
                            kjDetailsAdapter.setNewData(mlist);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onRefresh() {
        initDate();
    }
}
