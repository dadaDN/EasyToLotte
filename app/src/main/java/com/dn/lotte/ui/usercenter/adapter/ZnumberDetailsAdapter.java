package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurseNumberBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/14.
 */

public class ZnumberDetailsAdapter extends BaseQuickAdapter<PurseNumberBean.TableBean,BaseViewHolder> {
    public ZnumberDetailsAdapter(@LayoutRes int layoutResId, @Nullable List<PurseNumberBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PurseNumberBean.TableBean bean) {
        helper.setText(R.id.tv_game_name,bean.getLotteryname())
                .setText(R.id.tv_user_name,bean.getUsername())
                .setText(R.id.tv_usr_number,"订单号："+bean.getSsid())
                .setText(R.id.tv_do_time,"操作时间："+bean.getStime())
                .setText(R.id.tv_game_record_number,"期号："+bean.getIssuenum())
                .setText(R.id.tv_game_record_time,"状态："+bean.getStatename())
                .setText(R.id.tv_game_money,bean.getTotal());
    }
}
