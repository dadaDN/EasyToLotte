package com.dn.lotte.ui.wonderful.activity;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.DiscountitemBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.lzy.okgo.model.HttpParams;

import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/1.
 */

public class DiscountItemActivity extends BaseActivity {

    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.text)
    TextView text;
    @Bind(R.id.tv_start_time)
    TextView tvStartTime;
    @Bind(R.id.text1)
    TextView text1;
    @Bind(R.id.tv_end_time)
    TextView tvEndTime;
    @Bind(R.id.text2)
    TextView text2;
    @Bind(R.id.tv_regulation)
    TextView tvRegulation;
    @Bind(R.id.text3)
    TextView text3;
    @Bind(R.id.tv_timeandstate)
    TextView tvTimeandstate;
    @Bind(R.id.tv_xinxi)
    TextView tvXinxi;
    @Bind(R.id.tv_title)
    TextView tvTitle;
    @Bind(R.id.ll_title)
    LinearLayout llTitle;
    private String tid;

    @Override
    public int getLayoutId() {
        return R.layout.activity_discountitem;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        tid = getIntent().getStringExtra("tid");
        toolbar.setMainTitle("优惠详情");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        loadDate(tid);
    }

    private void loadDate(String tid) {
        HttpParams params = new HttpParams();
        params.put("id", tid);
        ServerApi.getDiscountsdetails(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DiscountitemBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull DiscountitemBean discountitemBean) {
                        stopProgressDialog();
                        if (discountitemBean.getResult().equals("1")) {
                            initData(discountitemBean.getTable());
                        } else {
                            showShortToast(discountitemBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private void initData(List<DiscountitemBean.TableBean> table) {
        tvTitle.setText(table.get(0).getTitle());
        tvStartTime.setText(table.get(0).getStarttime());
        tvEndTime.setText(table.get(0).getEndtime());
        tvRegulation.setText(table.get(0).getRemark());
        tvTimeandstate.setText(table.get(0).getTypecode());
        tvXinxi.setText(table.get(0).getContent());
    }

}
