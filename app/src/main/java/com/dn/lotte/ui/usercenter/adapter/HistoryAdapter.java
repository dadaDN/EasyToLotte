package com.dn.lotte.ui.usercenter.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.HistoryBean;
import com.easy.common.commonutils.StringUtils;
import com.google.android.flexbox.FlexboxLayout;

import java.util.List;

/**
 * Created by ASUS on 2017/10/15.
 */

public class HistoryAdapter extends BaseQuickAdapter<HistoryBean.TableBean, BaseViewHolder> {

    private String[] allStr;

    public HistoryAdapter(@LayoutRes int layoutResId, @Nullable List<HistoryBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HistoryBean.TableBean item) {
        helper.setText(R.id.tv_big_name, item.getLotteryname())
                .setText(R.id.tv_qh_number, item.getIssuenum());
        showlabel((FlexboxLayout) helper.getView(R.id.fl_self_introduction), item.getNumber());

    }

    /**
     * 显示标签
     *
     * @param flexboxLayout FlexboxLayout
     * @param str           “聪明，强，无敌” --> 以 ，隔开的字符串
     */
    private void showlabel(FlexboxLayout flexboxLayout, String str) {
        flexboxLayout.removeAllViews();
        if (!StringUtils.isEmpty(str)) {
            allStr = str.split(",");

            for (int i = 0; i < allStr.length; i++) {
                TextView textView = new TextView(mContext);
                textView.setText(allStr[i]);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(16);
                textView.setTextColor(Color.WHITE);
                textView.setBackgroundResource(R.drawable.ball_ssc);
                flexboxLayout.addView(textView);
                ViewGroup.LayoutParams params = textView.getLayoutParams();
                if (params instanceof FlexboxLayout.LayoutParams) {
                    FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) params;
                    layoutParams.setMargins(30, 40, 0, 0);
                    textView.setLayoutParams(layoutParams);
                }
            }
        }
    }
}
