package com.dn.lotte.ui.usercenter.activity;

import android.view.View;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.widget.ClearEditText;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 设置资金密码
 * Created by ASUS on 2017/10/9.
 */

public class SetMoneyPasswordActivity extends BaseActivity {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.ed_old_psw)
    ClearEditText edOldPsw;
    @Bind(R.id.ed_new_psw)
    ClearEditText edNewPsw;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.ed_affirm_psw)
    ClearEditText edAffirmPsw;

    @Override
    public int getLayoutId() {
        return R.layout.activity_money_psw_set;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_set_money_password_title);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
    }

    @OnClick(R.id.okBtn)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.okBtn:
                if (!StringUtils.isEmpty(edOldPsw.getText().toString().trim())) {
                    if (!StringUtils.isEmpty(edNewPsw.getText().toString().trim())) {
                        if (!StringUtils.isEmpty(edAffirmPsw.getText().toString())) {
                            if (edAffirmPsw.getText().toString().trim().equals(edNewPsw.getText().toString().trim())) {
                                HttpParams params=new HttpParams();
                                params.put("oldpass",edOldPsw.getText().toString().trim());
                                params.put("newpass",edNewPsw.getText().toString().trim());
                                params.put("newpass2",edAffirmPsw.getText().toString().trim());
                                affirmPassword(params);
                            } else {
                                showShortToast("两次密码不一致");
                            }
                        }
                    } else {
                        showShortToast("新密码不能为空");
                    }
                } else {
                    showShortToast("旧密码不能为空");
                }
                break;
        }
    }

    private void affirmPassword(HttpParams params) {
        UserCenterServerApi.setMoneypsw(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                            showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
