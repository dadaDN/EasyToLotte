package com.dn.lotte.ui.purchase.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.CunModeBean;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.SelectNumberBean;
import com.dn.lotte.ui.purchase.activity.BetSuccessActivity;
import com.dn.lotte.ui.purchase.adapter.PurchaseFSNumberAdapter;
import com.dn.lotte.ui.purchase.contract.PurchaseFSDetailContract;
import com.dn.lotte.ui.purchase.model.PurchaseFSNewDetailModel;
import com.dn.lotte.ui.purchase.presenter.PurchaseFSDetailPresenter;
import com.dn.lotte.utils.CommonUtils;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonutils.CommonUtil;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.RippleView;
import com.google.gson.Gson;
import com.lzy.okgo.model.HttpParams;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.dn.lotte.utils.TestCodeUtils.BetNumerice;

public class PurchaseFSFragment extends BaseFragment<PurchaseFSDetailPresenter, PurchaseFSNewDetailModel> implements PurchaseFSDetailContract.View, RippleView.OnRippleCompleteListener {

    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.purchaseNumber)
    TextView mPurchaseNumber;
    @Bind(R.id.purchaseMoney)
    TextView mPurchaseMoney;
    @Bind(R.id.oneBbetting)
    RippleView mOkBetting;
    private PurchaseDetailGameBean.TableBean mGameDataBean;
    private HttpParams httpParams;
    private String mLid;
    private String mCPName; //彩票名字
    private List<PurchaseDetailNumberBean.TableBean.BallsBean> mDetailNumberList = new ArrayList<>();
    private PurchaseFSNumberAdapter mPurchaseNumberAdapter;
    protected boolean mIsVisible = false;
    private View mSDZHeader;
    private View mAllFooter; // 倍数 返点调节 布局
    private View mRenshiFooter;//任式 个十 布局
    private String mPoint;
    private String minbonus;
    private String posbonus;
    private int precept;
    private double mFDCha;
    private double mSingelBounds;

    /**
     * 在这里实现Fragment数据的缓加载.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mIsVisible = true;
            onVisible();
        } else {
            mIsVisible = false;
            onInvisible();
        }
    }

    protected void onInvisible() {
    }

    /**
     * 显示时加载数据,需要这样的使用
     * 注意声明 isPrepared，先初始化
     * 生命周期会先执行 setUserVisibleHint 再执行onActivityCreated
     * 在 onActivityCreated 之后第一次显示加载数据，只加载一次
     */
    protected void onVisible() {
        if (mPurchaseNumberAdapter != null && mDetailNumberList != null) {
            mPurchaseNumberAdapter.refreshData(mDetailNumberList);
        }
    }

    public static PurchaseFSFragment getInstance(PurchaseDetailGameBean.TableBean tableBean, String id, String cpName) {
        PurchaseFSFragment purchaseFSFragment = new PurchaseFSFragment();
        purchaseFSFragment.mGameDataBean = tableBean;
        purchaseFSFragment.mLid = id;
        purchaseFSFragment.mCPName = cpName;
        return purchaseFSFragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_purchase_fs;
    }

    @Override
    public void initPresenter() {
        mPresenter.setVM(this, mModel);
    }

    boolean isFirst = true;//是否是第一次进入
    boolean isRenShi = false; //是否是任式
    boolean isSDZ = false; //是否杀对子
    boolean isDPC = false; //是否是低频彩
    boolean isFG = false; //是否是 以“_”线分割的

    @Override
    protected void initView() {
        if (mCPName.equals("福彩3D") || mCPName.equals("体彩P3"))
            isDPC = true;
        if (mGameDataBean != null)
            isRenShiState();
        if (mCPName.contains("11选5") || mCPName.equals("PK10"))
            isFG = true;
        mPurchaseNumberAdapter = new PurchaseFSNumberAdapter(R.layout.item_purchase_number, mDetailNumberList, mGameDataBean.getTitle2(), mPresenter, isRenShi, isSDZ, isDPC, isFG);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mPurchaseNumberAdapter);
        if (isFirst) {
            loadData();
            isFirst = false;
        }
        minbonus = mGameDataBean.getMinbonus();
        posbonus = mGameDataBean.getPosbonus();
//        initSDZHeader();
        initAllFooter();
        //任式
        initRenshiFooter();
        addFooter();
        mOkBetting.setOnRippleCompleteListener(this);
    }

    /**
     * 任式方案初始化
     */
    private void initRenshiFooter() {
        mRenshiFooter = LayoutInflater.from(getActivity()).inflate(R.layout.foot_detail_rs, null);
        CheckBox cbW = (CheckBox) mRenshiFooter.findViewById(R.id.cb_w);
        CheckBox cbQ = (CheckBox) mRenshiFooter.findViewById(R.id.cb_q);
        CheckBox cbB = (CheckBox) mRenshiFooter.findViewById(R.id.cb_b);
        CheckBox cbS = (CheckBox) mRenshiFooter.findViewById(R.id.cb_s);
        CheckBox cbG = (CheckBox) mRenshiFooter.findViewById(R.id.cb_g);
        final TextView tvNumber = (TextView) mRenshiFooter.findViewById(R.id.tvNumber);
        final List<CheckBox> checkBoxList = new ArrayList<>();
        checkBoxList.add(cbG);
        checkBoxList.add(cbS);
        checkBoxList.add(cbB);
        checkBoxList.add(cbQ);
        checkBoxList.add(cbW);
        //设置默认选项
        if (!StringUtils.isEmpty(parentTitle)) {
            switch (parentTitle) {
                case "任四":
                    setChecked(checkBoxList, 1);
                    break;
                case "任三":
                    setChecked(checkBoxList, 2);
                    break;
                case "任二":
                    setChecked(checkBoxList, 3);
                    break;
            }
        }
        multiplePrecept(parentTitle, checkBoxList, tvNumber);
        cbW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                multiplePrecept(parentTitle, checkBoxList, tvNumber);
            }
        });
        cbQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                multiplePrecept(parentTitle, checkBoxList, tvNumber);
            }
        });
        cbB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                multiplePrecept(parentTitle, checkBoxList, tvNumber);
            }
        });
        cbS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                multiplePrecept(parentTitle, checkBoxList, tvNumber);
            }
        });

        cbG.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                multiplePrecept(parentTitle, checkBoxList, tvNumber);
            }
        });
    }

    /**
     * 智障方法计算方案数
     *
     * @param parentTitle
     * @param checkBoxList
     * @param tvNumber
     */
    private void multiplePrecept(String parentTitle, List<CheckBox> checkBoxList, TextView tvNumber) {
        List<String> list = new ArrayList<>();
        for (CheckBox checkBox : checkBoxList) {
            if (checkBox.isChecked()) {
                list.add("");
            }
        }
        precept = 0;
        switch (parentTitle) {
            case "任四":
                if (list.size() < 4) {
                    precept = 0;
                }
                if (list.size() == 4) {
                    precept = 1;
                }
                if (list.size() == 5) {
                    precept = 5;
                }
                break;
            case "任三":
                if (list.size() < 3) {
                    precept = 0;
                }
                if (list.size() == 3) {
                    precept = 1;
                }
                if (list.size() == 4) {
                    precept = 4;
                }
                if (list.size() == 5) {
                    precept = 10;
                }
                break;
            case "任二":
                if (list.size() < 2) {
                    precept = 0;
                }
                if (list.size() == 2) {
                    precept = 1;
                }
                if (list.size() == 3) {
                    precept = 3;
                }
                if (list.size() == 4) {
                    precept = 6;
                }
                if (list.size() == 5) {
                    precept = 10;
                }
                break;
        }
        tvNumber.setText("共有 " + precept + " 种方案");
        returnSelectNumberData(mSelectNumber);
    }

    private void setChecked(List<CheckBox> checkBoxList, int strPos) {
        for (int i = 0; i < checkBoxList.size(); i++) {
            if (i < checkBoxList.size() - strPos) {
                checkBoxList.get(i).setChecked(true);
            } else
                checkBoxList.get(i).setChecked(false);
        }
    }

    /**
     * 杀对子Header
     * 换成了footer 位置变化太多 无奈
     */
    private void initSDZHeader() {
        mSDZHeader = LayoutInflater.from(getActivity()).inflate(R.layout.item_purchase_fs_header, null);
        CheckBox mSDZ = (CheckBox) mSDZHeader.findViewById(R.id.cb_sdz);
        mSDZ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPurchaseNumberAdapter.refreshSDZ(isChecked);
            }
        });
    }

    private String mModels = "元";//圆角模式
    private double model = 1;
    private String mTimes = "1";// 倍数
    private double mNowPoint;

    private void initAllFooter() {
        mAllFooter = LayoutInflater.from(getActivity()).inflate(R.layout.purchase_detail_foot, null);
        ImageButton redButton = (ImageButton) mAllFooter.findViewById(R.id.red);//倍数 减
        ImageButton addButton = (ImageButton) mAllFooter.findViewById(R.id.add);//倍数 加
        final EditText foldNumber = (EditText) mAllFooter.findViewById(R.id.number);//倍数

        RadioGroup radioGroup = (RadioGroup) mAllFooter.findViewById(R.id.group);
        RadioButton element = (RadioButton) mAllFooter.findViewById(R.id.element);
        RadioButton angle = (RadioButton) mAllFooter.findViewById(R.id.angle);
        RadioButton points = (RadioButton) mAllFooter.findViewById(R.id.points);
        RadioButton deter = (RadioButton) mAllFooter.findViewById(R.id.deter);

        final TextView mNowBounds = (TextView) mAllFooter.findViewById(R.id.tv_now_bounds);
        final TextView mFD = (TextView) mAllFooter.findViewById(R.id.tv_fd);
        final TextView mBuunds = (TextView) mAllFooter.findViewById(R.id.tv_bounds);
        SeekBar seekBar = (SeekBar) mAllFooter.findViewById(R.id.seekbar);

        /**
         *  倍数调节模式处理过程 ------------start-------------
         */
        foldNumber.setText(mTimes);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(foldNumber.getText().toString());
                a = a + 1;
                foldNumber.setText(a + "");
                mTimes = a + "";
            }
        });

        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(foldNumber.getText().toString());
                if (a == 1) {
                    mTimes = 1 + "";
                } else {
                    a = a - 1;
                    mTimes = a + "";
                }
                foldNumber.setText(a + "");
            }
        });

        foldNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (StringUtils.isEmpty(s.toString())) {
//                    number.setText("1");
                    mTimes = "1";
                } else {
                    mTimes = foldNumber.getText().toString();
                }
                setNowMoney(mNowBounds, mFD, mBuunds);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        /**
         *  倍数调节模式处理过程 ------------end-------------
         */

        /**
         *  初始化圆角模式 ------------start-------------
         */
        CunModeBean moneyMode = GlobalApplication.getInstance().getMoneyMode();
        if (moneyMode != null && StringUtils.isEmpty(moneyMode.getMode())) {
            switch (moneyMode.getMode()) {
                case "元":
                    model = 1;
                    element.setChecked(true);
                    break;
                case "角":
                    model = 0.1;
                    angle.setChecked(true);
                    break;
                case "分":
                    model = 0.01;
                    points.setChecked(true);
                    break;
                case "厘":
                    model = 0.001;
                    deter.setChecked(true);
                    break;
            }
        } else {
            model = 1;
            element.setChecked(true);
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.element:
                        mModels = "元";
                        model = 1;
                        setNowMoney(mNowBounds, mFD, mBuunds);
                        break;
                    case R.id.angle:
                        mModels = "角";
                        model = 0.1;
                        setNowMoney(mNowBounds, mFD, mBuunds);
                        break;
                    case R.id.points:
                        mModels = "分";
                        model = 0.01;
                        setNowMoney(mNowBounds, mFD, mBuunds);
                        break;
                    case R.id.deter:
                        mModels = "厘";
                        model = 0.001;
                        setNowMoney(mNowBounds, mFD, mBuunds);
                        break;
                }
            }
        });
        /**
         *  初始化圆角模式 ------------end-------------
         */

        /**
         *  返点调节处理 ------------start-------------
         */

        mPoint = SPUtils.getSharedStringData(getActivity(), "POINT");
        mFDCha = CommonUtils.sub(Double.parseDouble(mPoint), 14.5);
        final int mPoint = (int) (Double.parseDouble(this.mPoint) * 10) - 120;
        seekBar.setMax(25);
        seekBar.setProgress(25);
        mNowPoint = CommonUtils.div((mPoint - seekBar.getProgress()), 10, 2);
        setNowMoney(mNowBounds, mFD, mBuunds);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mNowPoint = CommonUtils.div((mPoint - progress), 10, 2);
                setNowMoney(mNowBounds, mFD, mBuunds);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        /**
         *  返点调节处理 ------------end-------------
         */

    }

    /**
     * 计算并显示金额
     *
     * @param mNowBounds
     * @param mFD
     * @param mBuunds
     */
    private void setNowMoney(TextView mNowBounds, TextView mFD, TextView mBuunds) {
        mFD.setText(mNowPoint + "");
        DecimalFormat df1 = new DecimalFormat("0");// 格式化小数
        double lastFD = CommonUtils.sub(Double.parseDouble(mPoint), mNowPoint);
        String now = df1.format(1700 + CommonUtils.sub(Double.parseDouble(mPoint), mNowPoint) * 10 * 2);// 返回的是String类型
        mBuunds.setText(now);
        if (mCPName.equals("福彩3D") || mCPName.equals("体彩P3")) {
            if (lastFD == 13.0) {
                lastFD = 11.0;
            }
        }
        //规则 20*返点*系数 + minBonus
        double mul = CommonUtils.mul(20, lastFD);
        double mul2 = CommonUtils.mul(mul, Double.parseDouble(posbonus));
        double add = CommonUtils.add(Double.parseDouble(minbonus), mul2);
        //  * 倍数 * 圆角模式

        double timeModel = CommonUtils.mul(Double.parseDouble(mTimes), model); //倍数*圆角模式
        double nowMoney = CommonUtils.mul(timeModel, add); //金钱数量
        nowMoney = CommonUtils.div(nowMoney, (double) 2, 3);
        mSingelBounds = nowMoney;
        mNowBounds.setText(nowMoney + "");
        multipleMoney();
    }

    /**
     * 添加脚布局
     */
    private void addFooter() {
        if (isRenShi)
            //个十百千万 任式脚布局
            mPurchaseNumberAdapter.addFooterView(mRenshiFooter);
        //添加脚布局
        mPurchaseNumberAdapter.addFooterView(mAllFooter);
    }

    /**
     * 判断是否是 任四 任三。。。
     */
    private String parentTitle = "";

    /**
     * 判断各种奇奇怪怪的情况
     * sb 后台 在此诅咒你妈妈要爆炸
     */
    public void isRenShiState() {
        PurchaseDetailTypeBean gameTypeDataList = CommonUtils.getGameTypeDataList(mLid);
        if (gameTypeDataList != null) {
            List<PurchaseDetailTypeBean.TableBean> titleList = gameTypeDataList.getTable();
            for (PurchaseDetailTypeBean.TableBean bean : titleList) {
                if (mGameDataBean.getRadio().equals(bean.getId())) {

                    if (bean.getTitle().equals("任四") || bean.getTitle().equals("任三") || bean.getTitle().equals("任二")) {
                        parentTitle = bean.getTitle();
                        isRenShi = true;
                    }


                    if (bean.getTitle().equals("前二") || bean.getTitle().equals("后二") || bean.getTitle().equals("任二")) {
                        if (mGameDataBean.getTitle().equals("直选复式") || mGameDataBean.getTitle().equals("直选单式")
                                || mGameDataBean.getTitle().equals("任二复式") || mGameDataBean.getTitle().equals("任二单式"))
                            isSDZ = true;
                    }

                    if (bean.getTitle().equals("五星")) {
                        if (mGameDataBean.getTitle().equals("组选120") || mGameDataBean.getTitle().equals("组选60")
                                || mGameDataBean.getTitle().equals("组选30") || mGameDataBean.getTitle().equals("组选20")
                                || mGameDataBean.getTitle().equals("组选10") || mGameDataBean.getTitle().equals("组选5")) {
                            isFG = true;
                        }
                    }
                    if (bean.getTitle().equals("四星")) {
                        if (mGameDataBean.getTitle().equals("组选12") || mGameDataBean.getTitle().equals("组选6")
                                || mGameDataBean.getTitle().equals("组选4"))
                            isFG = true;
                    }
                    if (bean.getTitle().equals("前三") || bean.getTitle().equals("中三") || bean.getTitle().equals("后三")
                            || bean.getTitle().equals("任三")) {
                        if (mGameDataBean.getTitle().equals("和值尾数") || mGameDataBean.getTitle().equals("组选包胆")
                                || mGameDataBean.getTitle().equals("直选跨度")) {
                            isFG = true;
                        }
                    }

                    if (bean.getTitle().equals("前二") || bean.getTitle().equals("后二") || bean.getTitle().equals("任二")) {
                        if (mGameDataBean.getTitle().equals("组选包胆") || mGameDataBean.getTitle().equals("直选跨度")) {
                            isFG = true;
                        }
                    }
                    if (bean.getTitle().equals("龙虎") || bean.getTitle().equals("趣味")) {
                        isFG = true;
                    }
                }
            }
        }
    }

    private void loadData() {
        httpParams = new HttpParams();
        httpParams.put("lid", mLid);
        mPresenter.getNumberData(httpParams, mLid, mGameDataBean.getTitle());//获取选号的总数据
    }


    /**
     * 返回选号数据 万位 千位。。
     *
     * @param numberbean
     */
    @Override
    public void returnNumberData(PurchaseDetailNumberBean numberbean) {
        if (numberbean.getResult().equals("1")) {
            List<PurchaseDetailNumberBean.TableBean> mNumberList = numberbean.getTable();

            for (PurchaseDetailNumberBean.TableBean tableBean : mNumberList) {
                if (tableBean.getTitleName().equals(mGameDataBean.getTitlename())) {
                    mDetailNumberList = tableBean.getBalls();
                    mPurchaseNumberAdapter.refreshData(mDetailNumberList);
                }
            }
        }
    }


    private int mSelectNumber = 0;

    @Override
    public void returnSelectNumberData(int selectNumber) {
        if (isRenShi) {
            //任式 注数= 注数*方案数
            mSelectNumber = selectNumber * precept;
        } else {
            mSelectNumber = selectNumber;
        }
        if (mPurchaseNumber != null) {
            mPurchaseNumber.setText(mSelectNumber + " 注");
            multipleMoney();
        }
    }

    private String mPlayCode = "";
    private String mBalls = "";
    private String mStrPos = "";
    private String mIsClear = "0";

    @Override
    public void returnPlayData(String palyCode, String selectNumber, String playStrPos, boolean isSha) {
        mPlayCode = palyCode;
        mBalls = selectNumber;
        mStrPos = playStrPos;
        mIsClear = isSha ? "1" : "0";
    }

    //投注成功 返回结果
    @Override
    public void returenBettingData(PurchaseDetailNumberBean bean) {
        showShortToast("投注成功");
        showShortToast(bean.getReturnval());
        Intent intent = new Intent(getActivity(), BetSuccessActivity.class);
        intent.putExtra("strmoney", mAlltotal);
        intent.putExtra("tag", "0");
        startActivity(intent);
        //投注成功发送消息到我的账户页刷新余额
        EventBus.getDefault().post(new EventBusBean<String>("TiXianSuccess", "TiXianSuccess"));
        if (mPurchaseNumberAdapter != null && mDetailNumberList != null) {
            mPurchaseNumberAdapter.refreshData(mDetailNumberList);
        }
    }

    private double mAlltotal = 0;

    private void multipleMoney() {
        double timeModel = CommonUtils.mul(Double.parseDouble(mTimes), model); //倍数*圆角模式
        double nowMoney = CommonUtils.mul(mSelectNumber, timeModel); //倍数*圆角模式
        mPurchaseMoney.setText(nowMoney + " 元");
        mAlltotal = nowMoney;
    }


    public String getBettingData() {
        List<SelectNumberBean.NumberBean> bettingList = new ArrayList<>();
        SelectNumberBean.NumberBean numberBean = new SelectNumberBean.NumberBean();
        numberBean.setAlltotal(mAlltotal + "");
        numberBean.setBalls(mBalls);
        numberBean.setIsClear(mIsClear);
        numberBean.setLotteryId(mLid);
        numberBean.setNum(mSelectNumber + "");
        numberBean.setPlayId(mGameDataBean.getId());
        numberBean.setPoint((mFDCha > 0 ? CommonUtils.sub(mNowPoint, mFDCha) : mPoint) + "");
        numberBean.setPrice(model + "");
        numberBean.setSingelBouns(mSingelBounds + "");
        if (isRenShi)
            numberBean.setStrPos(mStrPos);
        else
            numberBean.setStrPos("");
        numberBean.setTimes(mTimes);
        numberBean.setmMinPos(minbonus);
        numberBean.setModel(mModels);
        numberBean.setName(mGameDataBean.getTitlename());
        bettingList.add(numberBean);
        Gson gson = new Gson();
        String bettingStr = gson.toJson(bettingList);
        Log.i("DNLOG--:", bettingStr);
        return bettingStr;
    }

    @Override
    public void showLoading(String title) {
        startProgressDialog();
    }

    @Override
    public void stopLoading() {
        stopProgressDialog();
    }

    @Override
    public void showErrorTip(String msg) {

    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.oneBbetting:
                if (mSelectNumber > 0)
                    mPresenter.getBettingData(getBettingData());
                else
                    showShortToast("请填写正确的投注号码！");
                break;
        }
    }
}
