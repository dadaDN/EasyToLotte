package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.InboxBean;
import com.easy.common.commonutils.TimeUtil;

import java.util.Date;
import java.util.List;


/**
 * Created by DN on 2017/10/13.
 */

public class InboxAdapter extends BaseQuickAdapter<InboxBean.TableBean, BaseViewHolder> {

    public InboxAdapter(@LayoutRes int layoutResId, @Nullable List<InboxBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InboxBean.TableBean bean) {
        Date dateByFormat = TimeUtil.getDateByFormat(bean.getStime(), TimeUtil.dateFormatYMDHMS);
//        String mm = TimeUtil.getStringByFormat(bean.getStime(), "MM");
//        Log.i("DNlog" ,TimeUtil.getStringByFormat(dateByFormat,"dd")+"");
        helper.setText(R.id.tv_mouth, TimeUtil.getStringByFormat(dateByFormat, "MM") + "月")
                .setText(R.id.tv_data, TimeUtil.getStringByFormat(dateByFormat, "dd"))
                .setText(R.id.tv_title, bean.getTitle());

    }
}
