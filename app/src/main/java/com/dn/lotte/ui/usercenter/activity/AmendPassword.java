package com.dn.lotte.ui.usercenter.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AmendPswBean;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 修改登录密码
 * Created by ASUS on 2017/10/9.
 */

public class AmendPassword extends BaseActivity {

    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.ed_now_password)
    EditText edNowPassword;
    @Bind(R.id.ed_new_psw)
    EditText edNewPsw;
    @Bind(R.id.ed_affirm_password)
    EditText edAffirmPassword;
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.tv_now_password)
    RelativeLayout tvNowPassword;
    @Bind(R.id.relative_usercenter_download_resume)
    RelativeLayout relativeUsercenterDownloadResume;
    @Bind(R.id.rl_new_password)
    RelativeLayout rlNewPassword;
    @Bind(R.id.rl_affirm_password)
    RelativeLayout rlAffirmPassword;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    private String medNowPassword;

    @Override
    public int getLayoutId() {
        return R.layout.passwordamend;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.usercenter_set_alter_password);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
    }

    @OnClick(R.id.okBtn)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.okBtn:
                medNowPassword = edNowPassword.getText().toString().trim();
                String mnewpsw = edNewPsw.getText().toString().trim();
                String maffpsw = edAffirmPassword.getText().toString().trim();
                if (!StringUtils.isEmpty(medNowPassword)) {
                    if (!StringUtils.isEmpty(mnewpsw)) {
                        if (!StringUtils.isEmpty(maffpsw)) {
                            if (mnewpsw.equals(maffpsw)){
                                HttpParams params = new HttpParams();
                                params.put("oldpass", medNowPassword);
                                params.put("newpass", mnewpsw);
                                params.put("newpass2", maffpsw);
                                Amendpsw(params);
                            }else {
                                showShortToast("俩次密码不一致");
                            }

                        } else
                            showShortToast("请再次输入新密码");
                    } else {
                        showShortToast("请输入新密码");
                    }
                } else {
                    showShortToast("请输入当前密码");
                }
                break;
        }
    }

    //修改密码
    private void Amendpsw(HttpParams params) {
        UserCenterServerApi.getPassWordalter(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean amendPswBean) {
                        showShortToast(amendPswBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
