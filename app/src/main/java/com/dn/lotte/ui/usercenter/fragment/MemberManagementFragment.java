package com.dn.lotte.ui.usercenter.fragment;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseFragment;
import com.easy.common.commonwidget.RippleView;

import butterknife.Bind;

/**
 * 配额管理
 * Created by ASUS on 2017/11/5.
 */

public class MemberManagementFragment extends BaseFragment implements RippleView.OnRippleCompleteListener {
    @Bind(R.id.tv_zhanghao)
    TextView tvZhanghao;
    @Bind(R.id.now_fandian)
    TextView nowFandian;
    @Bind(R.id.tv_moeny)
    TextView tvMoeny;
    @Bind(R.id.tv_kh_jibie1)
    TextView tvKhJibie1;
    @Bind(R.id.tv_my_number1)
    TextView tvMyNumber1;
    @Bind(R.id.tv_xiji_number1)
    TextView tvXijiNumber1;
    @Bind(R.id.rl_add_number1)
    RelativeLayout rlAddNumber1;
    @Bind(R.id.tv_add_number1)
    TextView tvAddNumber1;
    @Bind(R.id.rl_jian_number1)
    RelativeLayout rlJianNumber1;
    @Bind(R.id.tv_jian_number1)
    TextView tvJianNumber1;
    @Bind(R.id.tv_kh_jibie2)
    TextView tvKhJibie2;
    @Bind(R.id.tv_my_number2)
    TextView tvMyNumber2;
    @Bind(R.id.tv_xiaji_number2)
    TextView tvXiajiNumber2;
    @Bind(R.id.rl_add_number2)
    RelativeLayout rlAddNumber2;
    @Bind(R.id.tv_add_number2)
    TextView tvAddNumber2;
    @Bind(R.id.rl_jian_number2)
    RelativeLayout rlJianNumber2;
    @Bind(R.id.tv_jian_number2)
    TextView tvJianNumber2;
    @Bind(R.id.tv_kh_jibie3)
    TextView tvKhJibie3;
    @Bind(R.id.tv_my_number3)
    TextView tvMyNumber3;
    @Bind(R.id.tv_xiaji_number3)
    TextView tvXiajiNumber3;
    @Bind(R.id.rl_add_number3)
    RelativeLayout rlAddNumber3;
    @Bind(R.id.tv_add_number3)
    TextView tvAddNumber3;
    @Bind(R.id.rl_jian_number3)
    RelativeLayout rlJianNumber3;
    @Bind(R.id.tv_jian_number3)
    TextView tvJianNumber3;
    @Bind(R.id.betting)
    RippleView betting;
    private String selectdata;

    public MemberManagementFragment(String selectData) {
        this.selectdata = selectData;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_manager;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        betting.setOnRippleCompleteListener(this);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.betting:
                showShortToast("暂无数据");
                break;
        }
    }
}
