package com.dn.lotte.ui.purchase.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.HistoryDetailBean;
import com.dn.lotte.ui.purchase.adapter.HistroryDetailDataAdapter;
import com.dn.lotte.ui.purchase.adapter.PurchaseHistroryAdapter;
import com.dn.lotte.ui.usercenter.adapter.BettingAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseFragment;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class PurchaseHistoryRecFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    private String mLid;
    private List<HistoryDetailBean.TableBean> mList = new ArrayList<>();
    private PurchaseHistroryAdapter mAdapter;

    public static PurchaseHistoryRecFragment getInstance(String id) {
        PurchaseHistoryRecFragment purchaseHistoryRecFragment = new PurchaseHistoryRecFragment();
        purchaseHistoryRecFragment.mLid = id;
        return purchaseHistoryRecFragment;
    }

    protected boolean mIsVisible = false;
    /**
     * `
     * 在这里实现Fragment数据的缓加载.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mIsVisible = true;
            onVisible();
        } else {
            mIsVisible = false;
            onInvisible();
        }
    }
    protected void onInvisible() {
    }

    /**
     * 显示时加载数据,需要这样的使用
     * 注意声明 isPrepared，先初始化
     * 生命周期会先执行 setUserVisibleHint 再执行onActivityCreated
     * 在 onActivityCreated 之后第一次显示加载数据，只加载一次
     */
    protected void onVisible() {
        loadData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_pruchase_history;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initView() {
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new PurchaseHistroryAdapter(R.layout.item_purchase_history, mList);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
//        mLoadMoreEndGone = true;
        mRecyclerView.setAdapter(mAdapter);
//        loadData();
    }

    private void loadData() {
        HttpParams httpParams = new HttpParams();
        httpParams.put("lid", mLid);
        ServerApi.getHistoryDetailData(httpParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HistoryDetailBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
//                        startProgressDialog();
                        if (mSwipeLayout != null) mSwipeLayout.setRefreshing(true);
                    }

                    @Override
                    public void onNext(@NonNull HistoryDetailBean historyDetailBean) {
                        if (historyDetailBean.getResult().equals("1")) {
                            mList = historyDetailBean.getTable();
                            mAdapter.setNewData(mList);
                            if (mSwipeLayout != null) mSwipeLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });

    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
