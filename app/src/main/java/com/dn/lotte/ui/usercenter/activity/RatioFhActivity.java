package com.dn.lotte.ui.usercenter.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AmendPswBean;
import com.dn.lotte.bean.DayratioBean;
import com.dn.lotte.ui.usercenter.adapter.RatioFhAdapter;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/28.
 */

public class RatioFhActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_big_name)
    TextView tvBigName;
    @Bind(R.id.tv_small_name)
    TextView tvSmallName;
    @Bind(R.id.ll_layout2)
    LinearLayout llLayout2;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.rlv_jujue)
    RippleView rlvJujue;
    @Bind(R.id.ll_layout)
    LinearLayout llLayout;
    @Bind(R.id.okBtn1)
    RippleView okBtn1;
    @Bind(R.id.rlv_jujue1)
    RippleView rlvJujue1;
    @Bind(R.id.ll_layout1)
    LinearLayout llLayout1;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    private DayratioBean date;
    private List<DayratioBean.TableBean> mlist = new ArrayList<>();
    private RatioFhAdapter ratioFhAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_ratiofh;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("我的分红契约");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        okBtn1.setOnRippleCompleteListener(this);
        okBtn.setOnRippleCompleteListener(this);
        rlvJujue.setOnRippleCompleteListener(this);
        rlvJujue1.setOnRippleCompleteListener(this);
        rlStatementList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        ratioFhAdapter = new RatioFhAdapter(R.layout.item_ratiofh, mlist);
        ratioFhAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);;
        rlStatementList.setAdapter(ratioFhAdapter);
        loaddayratio();
    }

    private void loaddayratio() {
        UserCenterServerApi.myfhratio()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DayratioBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull DayratioBean qiyueBiBean) {
                        stopProgressDialog();
                        if (qiyueBiBean.getResult().equals("1")) {
                               setdate(qiyueBiBean);
                            mlist = qiyueBiBean.getTable();
                            ratioFhAdapter.setNewData(mlist);
                        } else {
                            showShortToast(qiyueBiBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    public void setdate(DayratioBean date) {
        this.date = date;
        tvBigName.setText(date.getPrompt());
       /* tvChange.setText("每日销量" + date.getTable().get(0).getMinmoney() + "万");
        tvBili.setText(date.getTable().get(0).getMoney() + "%");*/
        String isused = date.getTable().get(0).getIsused();
        if (isused.equals("0")) {//上级分配请确认
            llLayout1.setVisibility(View.VISIBLE);
            llLayout.setVisibility(View.GONE);
            llLayout2.setVisibility(View.VISIBLE);
            tvSmallName.setVisibility(View.VISIBLE);

        } else if (isused.equals("1")) {//签订成功
            llLayout1.setVisibility(View.GONE);
            llLayout.setVisibility(View.GONE);
            llLayout2.setVisibility(View.VISIBLE);
            tvSmallName.setVisibility(View.VISIBLE);
        } else if (isused.equals("2")) {//契约已拒绝，请联系上级重新分配
            llLayout2.setVisibility(View.GONE);
            tvSmallName.setVisibility(View.GONE);
            llLayout1.setVisibility(View.GONE);
            llLayout.setVisibility(View.GONE);
            tvBigName.setText("契约已拒绝请联系上级重新分配");
        } else if (isused.equals("3")) {//上级要求撤销契约，请您确认
            llLayout1.setVisibility(View.GONE);
            llLayout.setVisibility(View.VISIBLE);
            llLayout2.setVisibility(View.VISIBLE);
            tvSmallName.setVisibility(View.VISIBLE);
        } else {
            llLayout2.setVisibility(View.GONE);
            tvSmallName.setVisibility(View.GONE);
            llLayout1.setVisibility(View.GONE);
            llLayout.setVisibility(View.GONE);
            tvBigName.setText("已同意撤销请联系上级重新分配");
        }
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn://同意撤销
                loadcheratio();
                break;
            case R.id.okBtn1://同意契约
                loadconsent();
                break;
            case R.id.rlv_jujue://拒绝撤销
                loadconsent();
                break;
            case R.id.rlv_jujue1://拒绝契约
                loadreject();
                break;
        }
    }

    //同意撤销契约
    private void loadcheratio() {
        UserCenterServerApi.myfhchexiaoeratio()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean amendPswBean) {
                        stopProgressDialog();
                        if (amendPswBean.getResult().equals("1")) {
                            llLayout2.setVisibility(View.GONE);
                            tvSmallName.setVisibility(View.GONE);
                            llLayout1.setVisibility(View.GONE);
                            llLayout.setVisibility(View.GONE);
                            tvBigName.setText("已同意撤销请联系上级重新分配");
                        }
                        showShortToast(amendPswBean.getReturnval());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //拒绝签订契约
    private void loadreject() {
        UserCenterServerApi.myfhjujueratio()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean amendPswBean) {
                        stopProgressDialog();
                        if (amendPswBean.getResult().equals("1")) {
                            llLayout2.setVisibility(View.GONE);
                            tvSmallName.setVisibility(View.GONE);
                            llLayout1.setVisibility(View.GONE);
                            llLayout.setVisibility(View.GONE);
                            tvBigName.setText("契约已拒绝请联系上级重新分配");
                        }
                        showShortToast(amendPswBean.getReturnval());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //同意签订契约
    private void loadconsent() {
        UserCenterServerApi.myfhconsentratio()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean amendPswBean) {
                        stopProgressDialog();
                        if (amendPswBean.getResult().equals("1")) {
                            llLayout1.setVisibility(View.GONE);
                            llLayout.setVisibility(View.GONE);
                        }
                        showShortToast(amendPswBean.getReturnval());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

}
