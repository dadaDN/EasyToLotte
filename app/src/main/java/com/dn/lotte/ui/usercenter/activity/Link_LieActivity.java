package com.dn.lotte.ui.usercenter.activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.LinkBean;
import com.dn.lotte.ui.usercenter.adapter.LinkAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/12/11.
 */

public class Link_LieActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private List<LinkBean.TableBean> mlist = new ArrayList<>();
    private int isRefresh = 0;
    private LinkAdapter linkAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_link_lie;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle("链接列表");
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        swipeLayout.setOnRefreshListener(this);
        rlStatementList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        linkAdapter = new LinkAdapter(R.layout.item_link, mlist);
        CustomLoadMoreView customLoadMoreView = new CustomLoadMoreView();
        linkAdapter.setLoadMoreView(customLoadMoreView);
        linkAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        linkAdapter.isFirstOnly(false);
        rlStatementList.setAdapter(linkAdapter);
        isRefresh = 0;
        loadLink();
        linkAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()){
                    case R.id.ll_copy:
                        // 从API11开始android推荐使用android.content.ClipboardManager
                        // 为了兼容低版本我们这里使用旧版的android.text.ClipboardManager，虽然提示deprecated，但不影响使用。
                        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        // 将文本内容放到系统剪贴板里。
                        String url = linkAdapter.getData().get(position).getUrl();
                        String[] split = url.split("=");
                        String s = split[1];
                        cm.setText(s);
                        showShortToast("已成功复制!");
                    break;
                }
            }
        });
    }

    private void loadLink() {
        UserCenterServerApi.getLinklist()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<LinkBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull LinkBean linkBean) {
                        stopProgressDialog();
                        if (linkBean.getResult().equals("1")) {
                            if (swipeLayout != null) swipeLayout.setRefreshing(false);
                            linkAdapter.setNewData(linkBean.getTable());

                        } else {
                            showShortToast(linkBean.getReturnval());
                        }


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onRefresh() {
        loadLink();
    }
}
