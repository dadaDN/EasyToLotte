package com.dn.lotte.ui.usercenter.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.AmendPswBean;
import com.dn.lotte.bean.BeforeqiyuBean;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.FenpeijsonBean;
import com.dn.lotte.bean.MemberManageBean;
import com.dn.lotte.bean.QiyueBiBean;
import com.dn.lotte.bean.SafeQuesstionBean;
import com.dn.lotte.bean.SetSafequesBean;
import com.dn.lotte.ui.usercenter.adapter.MemberManagementAdapter;
import com.dn.lotte.widget.ClearEditText;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonutils.StringUtils;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.google.gson.Gson;
import com.lzy.okgo.model.HttpParams;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.qmuiteam.qmui.widget.popup.QMUIListPopup;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/12.
 */

public class UserCenterManagementActivity extends BaseActivity implements
        RippleView.OnRippleCompleteListener, SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_state)
    TextView tvState;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_state)
    RippleView srlState;
    @Bind(R.id.ll_seek)
    ImageView llSeek;
    @Bind(R.id.et_seek_name)
    ClearEditText etSeekName;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @Bind(R.id.et_max_money)
    EditText etMaxMoney;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.et_min_money)
    EditText etMinMoney;
    @Bind(R.id.rl_relative)
    RelativeLayout rlRelative;
    @Bind(R.id.rl_sousuo)
    RelativeLayout rlSousuo;
    private int page = 1, pagesize = 10;
    private boolean mLoadMoreEndGone = false;
    private int isRefresh = 0;
    private List<MemberManageBean.TableBean> mlist = new ArrayList<>();
    private List<SafeQuesstionBean.TableBean> mysafelist = new ArrayList<>();
    private MemberManagementAdapter memberManagementAdapter;
    private QMUIListPopup mListPopup;
    private String[] arr = {
            "全部", "离线", "在线"
    };
    private String[] arr1 = {
            "充值转账", "活动转账", "其他转账"
    };
    private int tag = 0;
    private AlertDialog dialog;
    private RippleView rl_change_open_state;
    private QMUIListPopup mListPopupl;
    private TextView rv_change_state;
    private TextView tv_name;
    private ClearEditText et_money;
    private ClearEditText et_psw;
    private RippleView rlv_question;
    private ClearEditText et_answer;
    private TextView tv_question;
    private QMUIListPopup mListPopupsafe;
    private String uid;//会员id
    private TextView tv_qujian;
    private ClearEditText et_bili;
    private List<FenpeijsonBean> mSelectNumberList = new ArrayList();
    private FenpeijsonBean fenpeijsonBean;
    private RelativeLayout rl_layout;
    private String type = "";
    private RelativeLayout rl_layout1;
    private TextView tv_text;

    @Override
    public int getLayoutId() {
        return R.layout.activity_membermangement;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        fenpeijsonBean = new FenpeijsonBean();
        uid = getIntent().getStringExtra("uid");
        EventBus.getDefault().register(this);
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle(R.string.agency_member_management);
        srlState.setOnRippleCompleteListener(this);
        swipeLayout.setOnRefreshListener(this);
        rlStatementList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        memberManagementAdapter = new MemberManagementAdapter(R.layout.item_membermanage, mlist);
        CustomLoadMoreView customLoadMoreView = new CustomLoadMoreView();
        memberManagementAdapter.setLoadMoreView(customLoadMoreView);
        memberManagementAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        memberManagementAdapter.isFirstOnly(false);
        memberManagementAdapter.setOnLoadMoreListener(this, rlStatementList);
        rlStatementList.setAdapter(memberManagementAdapter);
        getMemeber(page, "", "", "", "", uid);
        initMoneyChange();
        memberManagementAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

                switch (view.getId()) {
                    case R.id.ib_money:
                        if (mlist.get(position).getIstranacc().equals("1")) {
                            if (GlobalApplication.getInstance().getUserModel().getTable().get(0).getIstruename().equals("0")) {
                                showPopTrueName();
                            } else {
                                showpopup(mlist.get(position).getUsername(), mlist.get(position).getId());
                            }
                        } else {
                            showShortToast("暂无转账权限，请联系客服");
                        }
                        break;
                 /*   case R.id.ll_gongzi://工资
                        loadwqiyu("1", mlist.get(position).getId());
                        break;*/
                    case R.id.ll_fenhong://分红
                        if (mlist.get(position).getIsfhcontract().equals("1")) {
                            beforeFHqiyu(uid,mlist.get(position).getId());
                        } else {
                            showShortToast("暂无分红权限");
                        }
                        break;
                    case R.id.ib_bianji:
                        MemberManageBean.TableBean tableBean = mlist.get(position);
                        Gson gson = new Gson();
                        String json = gson.toJson(tableBean);
                        Bundle bundle = new Bundle();
                        bundle.putString("membermanage", json);
                        startActivity(AgencyHGActivity.class, bundle);
                        break;
                    case R.id.ll_xiaji:
                        String id = mlist.get(position).getId();
                        Intent intent = new Intent(getApplicationContext(), UserCenterManagementActivity.class);
                        intent.putExtra("uid", id);
                        startActivity(intent);
                        break;
                }
            }
        });
        GetsafeQuesstion();
    }


    //分红契约分配钱判断
    private void beforeFHqiyu(final String uid, final String ussid) {
        HttpParams params = new HttpParams();
        params.put("page", "1");
        params.put("pagesize", "10");
        params.put("userid", uid);
        UserCenterServerApi.beforefhqiyue(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BeforeqiyuBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull BeforeqiyuBean amendPswBean) {
                        if (amendPswBean.getResult().equals("1")) {
                            if (amendPswBean.getTable().size() == 0) {
                                showShortToast("暂无分红契约");
                            } else {
                                qiyuefhratio(amendPswBean.getTable().get(0).getIsused(), uid,ussid);
                            }
                        } else {
                            showShortToast(amendPswBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
//请求分红契约的分配比例

    private void qiyuefhratio(final String state, final String uid, final String ussid) {
        UserCenterServerApi.qiyuesmallbig()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<QiyueBiBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull QiyueBiBean qiyueBiBean) {
                        stopProgressDialog();
                        showfhqiyue(qiyueBiBean.getMinPer().toString(), qiyueBiBean.getMaxPer().toString()
                                ,state,uid,ussid);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //弹出绑定真是姓名对话框，强制绑定
    private void showPopTrueName() {
        final QMUIDialog.EditTextDialogBuilder builder = new QMUIDialog.EditTextDialogBuilder(mContext);
        builder.setTitle("真实姓名没有绑定，请绑定!")
                .setPlaceholder("输入真实姓名")
                .setInputType(InputType.TYPE_CLASS_TEXT)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        String text = builder.getEditText().getText().toString();
                        Log.i("DNLOG", text);
                        if (text != null && text.length() > 0) {
                            HttpParams params = new HttpParams();
                            params.put("name", text);
                            realNameUrl(params, text);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(mContext, "请填入您的真实姓名", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }

    //帮定真实姓名
    private void realNameUrl(HttpParams params, final String name) {
        UserCenterServerApi.setRealname(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        stopProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")) {
                            GlobalApplication.getInstance().getUserModel().getTable().get(0).setTruename(name);
                            GlobalApplication.getInstance().getUserModel().getTable().get(0).setIstruename("1");
                        }
                        showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });

    }


    //请求安全问题
    private void GetsafeQuesstion() {
        UserCenterServerApi.getSafeQuesstion()
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SafeQuesstionBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull SafeQuesstionBean safeQuesstionBean) {
                        if (safeQuesstionBean.getResult().equals("1")) {
                            mysafelist = safeQuesstionBean.getTable();
                        } else {
                            showShortToast(safeQuesstionBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    //分红契约分配
    private void showfhqiyue(final String small, final String max, String state, final String uid, final String ussid) {
        //state是1是工资0是分红
        dialog = new AlertDialog.Builder(this)
                .setView(R.layout.fenpeilayout)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d("tag", "setPositiveButton");
                        try {
                            Field field = dialog.getClass()
                                    .getSuperclass().getSuperclass().getDeclaredField(
                                            "mShowing");
                            field.setAccessible(true);
                            //   将mShowing变量设为false，表示对话框已关闭
                            field.set(dialog, false);
                            dialog.dismiss();

                        } catch (Exception e) {

                        }
                    }
                })
                .setNegativeButton("cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        try {
                            Field field = dialog.getClass()
                                    .getSuperclass().getSuperclass().getDeclaredField(
                                            "mShowing");
                            field.setAccessible(true);
                            //   将mShowing变量设为false，表示对话框已关闭
                            field.set(dialog, true);
                            dialog.dismiss();

                        } catch (Exception e) {

                        }
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
        tv_name = (TextView) dialog.findViewById(R.id.tv_name);
        tv_name.setText("分红契约");
        tv_qujian = (TextView) dialog.findViewById(R.id.tv_qujian);
        tv_text = (TextView) dialog.findViewById(R.id.tv_text);
        et_money = (ClearEditText) dialog.findViewById(R.id.et_money);
        et_bili = (ClearEditText) dialog.findViewById(R.id.et_bili);
        rl_layout = (RelativeLayout) dialog.findViewById(R.id.rl_layout);
        rl_layout1 = (RelativeLayout) dialog.findViewById(R.id.rl_layout1);
        rl_layout1.setVisibility(View.VISIBLE);
        tv_qujian.setText("分配区间" + small + "%--" + max + "%");
        tv_text.setText("半月总销量");
        Button mNegativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        Button mPOstiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        if (state.equals("0")) {
            type = "1";
            rl_layout.setVisibility(View.VISIBLE);
            rl_layout1.setVisibility(View.VISIBLE);
            mNegativeButton.setVisibility(View.VISIBLE);
            mNegativeButton.setText("确定");
            mPOstiveButton.setText("取消");
        } else if (state.equals("1")) {
            type = "0";
            rl_layout.setVisibility(View.GONE);
            rl_layout1.setVisibility(View.GONE);
            mNegativeButton.setVisibility(View.VISIBLE);
            mNegativeButton.setText("撤销契约");
            mPOstiveButton.setText("取消");
        } else if (state.equals("2")) {
            type = "1";
            mNegativeButton.setVisibility(View.VISIBLE);
            rl_layout.setVisibility(View.VISIBLE);
            rl_layout1.setVisibility(View.VISIBLE);
            mNegativeButton.setText("重新分配");
            mPOstiveButton.setText("取消");
        } else if (state.equals("3")) {
            type = "1";
            mNegativeButton.setVisibility(View.GONE);
            rl_layout.setVisibility(View.GONE);
            rl_layout1.setVisibility(View.GONE);
            mNegativeButton.setText("确定");
            mPOstiveButton.setText("取消");
        } else {
            type = "1";
            mNegativeButton.setVisibility(View.VISIBLE);
            rl_layout.setVisibility(View.VISIBLE);
            rl_layout1.setVisibility(View.VISIBLE);
            mNegativeButton.setText("重新分配");
            mPOstiveButton.setText("取消");
        }
        mNegativeButton.setTextColor(Color.parseColor("#0079ff"));
        mPOstiveButton.setTextColor(Color.parseColor("#a3a3a3"));
        mPOstiveButton.setTextAppearance(this, R.style.Mycustomtab);
        mPOstiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("0")) {//发起撤销契约
                    HttpParams params = new HttpParams();
                    params.put("userid", uid);
                    loadfenpeigzche(params);
                    dialog.dismiss();
                } else {
                    if (!StringUtils.isEmpty(et_bili.getText().toString())) {
                        if (Double.parseDouble(et_bili.getText().toString()) < Double.parseDouble(max)) {
                            if (Double.parseDouble(et_bili.getText().toString()) > Double.parseDouble(small)) {
                                if (!StringUtils.isEmpty(et_money.getText().toString())) {
                                    fenpeijsonBean.setUserid(ussid);
                                    fenpeijsonBean.setMoney(et_money.getText().toString());
                                    fenpeijsonBean.setPer(et_bili.getText().toString());
                                    mSelectNumberList.add(fenpeijsonBean);
                                    Gson gson = new Gson();
                                    String json = gson.toJson(mSelectNumberList);
                                    loadfenpeigz(json);
                                    dialog.dismiss();
                                } else {
                                    showShortToast("请输入金额");
                                }

                            } else {
                                showShortToast("请输入正确比例");
                            }
                        } else {
                            showShortToast("请输入正确比例");
                        }
                    } else {
                        showShortToast("请输入比例");
                    }

                }
            }
        });
    }

    //发起撤销契约工资
    private void loadfenpeigzche(HttpParams json) {
        UserCenterServerApi.gonzichefenpei(json)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean beforeqiyuBean) {
                        stopProgressDialog();
                        showShortToast(beforeqiyuBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    //分配工资
    private void loadfenpeigz(String json) {//1工资契约0.分红

        UserCenterServerApi.fhfenpei(json)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                       // startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AmendPswBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull AmendPswBean beforeqiyuBean) {
                        stopProgressDialog();
                        showShortToast(beforeqiyuBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private void showpopup(String name, final String tid) {
        dialog = new AlertDialog.Builder(this)
                .setView(R.layout.zzdialog)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.d("tag", "setPositiveButton");
                        try {
                            Field field = dialog.getClass()
                                    .getSuperclass().getSuperclass().getDeclaredField(
                                            "mShowing");
                            field.setAccessible(true);
                            //   将mShowing变量设为false，表示对话框已关闭
                            field.set(dialog, false);
                            dialog.dismiss();

                        } catch (Exception e) {

                        }
                    }
                })
                .setNegativeButton("cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        try {
                            Field field = dialog.getClass()
                                    .getSuperclass().getSuperclass().getDeclaredField(
                                            "mShowing");
                            field.setAccessible(true);
                            //   将mShowing变量设为false，表示对话框已关闭
                            field.set(dialog, true);
                            dialog.dismiss();

                        } catch (Exception e) {

                        }
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
        rl_change_open_state = (RippleView) dialog.findViewById(R.id.rl_change_open_state);
        rlv_question = (RippleView) dialog.findViewById(R.id.rlv_question);
        rv_change_state = (TextView) dialog.findViewById(R.id.rv_change_state);
        tv_name = (TextView) dialog.findViewById(R.id.tv_name);
        tv_question = (TextView) dialog.findViewById(R.id.tv_question);
        et_money = (ClearEditText) dialog.findViewById(R.id.et_money);
        et_psw = (ClearEditText) dialog.findViewById(R.id.et_psw);
        et_answer = (ClearEditText) dialog.findViewById(R.id.et_answer);
        tv_name.setText(name);
        Button mNegativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        Button mPOstiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        mNegativeButton.setText("确定");
        mPOstiveButton.setText("取消");
        mNegativeButton.setTextColor(Color.parseColor("#0079ff"));
        mPOstiveButton.setTextColor(Color.parseColor("#a3a3a3"));
        mPOstiveButton.setTextAppearance(this, R.style.Mycustomtab);
        mPOstiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!rv_change_state.getText().toString().equals("请选择类别")) {
                    if (!tv_question.getText().toString().equals("请选择密保问题")) {
                        if (!et_answer.getText().toString().trim().equals("")) {
                            if (!et_money.getText().toString().trim().equals("")) {
                                if (!et_psw.getText().toString().trim().equals("")) {
                                    loadZH(tid);
                                } else {
                                    showShortToast("请输入资金密码");
                                }
                            } else {
                                showShortToast("请输入金额");
                            }
                        } else {
                            showShortToast("请输入安全问题");
                        }
                    } else {
                        showShortToast("请选择安全问题");
                    }
                } else {
                    showShortToast("请选择类别");
                }
            }
        });
        rl_change_open_state.setOnRippleCompleteListener(this);
        rlv_question.setOnRippleCompleteListener(this);
    }

    private void loadZH(String tid) {
        String state = rv_change_state.getText().toString();

        if (state.equals("充值转账")) {
            state = "0";
        } else if (state.equals("活动转账")) {
            state = "1";
        } else if (state.equals("其他转账")) {
            state = 2 + "";
        }
        HttpParams params = new HttpParams();
        params.put("type", state);
        params.put("userid", tid);
        params.put("money", et_money.getText().toString());
        params.put("pass", et_psw.getText().toString().trim());
        params.put("question", tv_question.getText().toString());
        params.put("answer", et_answer.getText().toString().trim());
        UserCenterServerApi.setaccounts(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SetSafequesBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull SetSafequesBean setSafequesBean) {
                        stopProgressDialog();
                        if (setSafequesBean.getResult().equals("1")) {
//                            EventBus.getDefault().post(new EventBusBean<String>("TiXianSuccess", "TiXianSuccess"));
                            isRefresh = 0;
                            page = 1;
                            tag = 1;
                            getMemeber(page, "", "", "", "", uid);
                            dialog.dismiss();
                        }
                        showShortToast(setSafequesBean.getReturnval());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private void initMoneyChange() {
        etMinMoney.addTextChangedListener(mtextwatcher);//余额小于监听
        etMaxMoney.addTextChangedListener(mtextwatcher);//余额大于监听
    }

    //余额小于监听
    private TextWatcher mtextwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (tag == 0) {
                page = 1;
                if (tvState.getText().toString().equals("全部") || tvState.getText().toString().equals("全部状态")) {
                    getMemeber(page, "", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);
                }
                if (tvState.getText().toString().equals("在线")) {
                    getMemeber(page, "1", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);

                }
                if (tvState.getText().toString().equals("离线")) {
                    getMemeber(page, "0", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);

                }
            }
        }
    };

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_state:
                initListPopupIfNeed();
                mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopup.show(rippleView);
                break;
            case R.id.rl_change_open_state:
                initListPopuplei();
                mListPopupl.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                mListPopupl.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                mListPopupl.show(rippleView);
                break;
            case R.id.rlv_question:
                if (mysafelist == null) {
                    GetsafeQuesstion();
                } else {
                    initListPopupIfsafe();
                    mListPopupsafe.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_LEFT);
                    mListPopupsafe.setPreferredDirection(QMUIPopup.DIRECTION_BOTTOM);
                    mListPopupsafe.show(rippleView);
                }
                break;
        }
    }

    //转账中的安全问题
    private void initListPopupIfsafe() {
        if (mListPopupsafe == null) {
            final List<String> data = new ArrayList<>();
            for (SafeQuesstionBean.TableBean tableBean : mysafelist) {
                data.add(tableBean.getTitle());
            }

            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopupsafe = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopupsafe.create(QMUIDisplayHelper.dp2px(mContext, 180), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tv_question.setText(data.get(i));
                    mListPopupsafe.dismiss();
                }
            });
            mListPopupsafe.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    //转账中的类别
    private void initListPopuplei() {
        if (mListPopupl == null) {
            final List<String> data = new ArrayList<>();
            for (int i = 0; i < arr1.length; i++) {
                data.add(arr1[i]);
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopupl = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopupl.create(QMUIDisplayHelper.dp2px(mContext, 140), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    rv_change_state.setText(data.get(i));
                    mListPopupl.dismiss();
                }
            });
            mListPopupl.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }
//状态选择

    private void initListPopupIfNeed() {
        if (mListPopup == null) {
            final List<String> data = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                data.add(arr[i]);
            }
            ArrayAdapter adapter = new ArrayAdapter<>(mContext, R.layout.simple_list_item, data);
            mListPopup = new QMUIListPopup(mContext, QMUIPopup.DIRECTION_NONE, adapter);
            mListPopup.create(QMUIDisplayHelper.dp2px(mContext, 80), QMUIDisplayHelper.dp2px(mContext, 220), new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    tvState.setText(data.get(i));
                    getChangeState(data.get(i));
                    mListPopup.dismiss();
                }
            });
            mListPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                }
            });
        }
    }

    public void getMemeber(int page, String state, String minmoney, String maxmoney, String member, String uid) {
        HttpParams params = new HttpParams();
        params.put("page", page);
        if (!uid.isEmpty()) {
            params.put("Id", uid);
        }
        params.put("pagesize", pagesize);
        params.put("online", state);
        params.put("money1", minmoney);
        params.put("money2", maxmoney);
        params.put("username", member);
        getMemeberMange(params);
    }

    private void getMemeberMange(HttpParams params) {
        UserCenterServerApi.getMemebrMangement(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MemberManageBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull MemberManageBean memberManageBean) {
                        stopProgressDialog();
                        if (memberManageBean.getResult().equals("1")) {
                            if (isRefresh == 1) {
                                memberManagementAdapter.loadMoreComplete();
                                memberManagementAdapter.addData(memberManageBean.getTable());
                            } else {
                                mlist = memberManageBean.getTable();
                                swipeLayout.setRefreshing(false);
                                memberManagementAdapter.setNewData(mlist);
                            }
                            if (memberManageBean.getTable().size() < pagesize) {
                                mLoadMoreEndGone = true;
                                memberManagementAdapter.loadMoreEnd(mLoadMoreEndGone);
                            }
                            if (tag == 1) {
                                etMinMoney.setText("");
                                etSeekName.setText("");
                                etMaxMoney.setText("");
                                tvState.setText("全部状态");
                                tag = 0;
                            }
                        } else {
                            showShortToast(memberManageBean.getReturnval());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    public void getChangeState(String state) {
        isRefresh = 0;
        page = 1;
        if (state.equals("全部")) {
            getMemeber(page, "", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);
        }
        if (state.equals("在线")) {
            getMemeber(page, "1", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);

        }
        if (state.equals("离线")) {
            getMemeber(page, "0", etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);

        }

    }

    @Override
    public void onRefresh() {
        isRefresh = 0;
        page = 1;
        tag = 1;
        getMemeber(page, "", "", "", "", uid);

    }

    @Override
    public void onLoadMoreRequested() {
        isRefresh = 1;
        page++;
        getMemeber(page, "", "", "", "", uid);
    }

    //会员升点发送的消息
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainEventBus(EventBusBean<String> bean) {
        if ("pointsuccess".equals(bean.getTag())) {
            isRefresh = 0;
            page = 1;
            tag = 1;
            getMemeber(page, "", "", "", "", uid);
        }
    }


    @OnClick(R.id.rl_sousuo)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_sousuo:
                String stringstate = "";
                if (tvState.getText().toString().equals("全部") || tvState.getText().toString().equals("全部状态")) {
                    stringstate = "";
                }
                if (tvState.getText().toString().equals("在线")) {
                    stringstate = "1";
                }
                if (tvState.getText().toString().equals("离线")) {
                    stringstate = "0";
                }
                getMemeber(page, stringstate, etMinMoney.getText().toString().trim(), etMaxMoney.getText().toString().trim(), etSeekName.getText().toString().trim(), uid);
                break;
        }
    }
}
