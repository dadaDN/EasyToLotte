package com.dn.lotte.ui.usercenter.activity;

import android.app.DatePickerDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.PersonageBean;
import com.dn.lotte.ui.usercenter.adapter.PersonageAdapter;
import com.dn.lotte.widget.CustomLoadMoreView;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 个人报表
 * Created by ASUS on 2017/10/18.
 */

public class PersonageActivity extends BaseActivity implements RippleView.OnRippleCompleteListener, SwipeRefreshLayout.OnRefreshListener
        , BaseQuickAdapter.RequestLoadMoreListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_starttime)
    TextView tvStarttime;
    @Bind(R.id.imageView4)
    ImageView imageView4;
    @Bind(R.id.rl_starttime)
    RelativeLayout rlStarttime;
    @Bind(R.id.srl_starttime)
    RippleView srlStarttime;
    @Bind(R.id.tv_overtime)
    TextView tvOvertime;
    @Bind(R.id.rl_overtime)
    RelativeLayout rlOvertime;
    @Bind(R.id.srl_overtime)
    RippleView srlOvertime;
    @Bind(R.id.rl_statement_list)
    RecyclerView rlStatementList;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    private int pagesize = 10;
    private int isRefresh = 0;
    private boolean mLoadMoreEndGone = false;
    private int page = 1;
    private PersonageAdapter personageAdapter;
    private List<PersonageBean.TableBean> mlist = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activitystatementgroup;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle(R.string.usercenter_statement);
        swipeLayout.setOnRefreshListener(this);
        srlStarttime.setOnRippleCompleteListener(this);
        srlOvertime.setOnRippleCompleteListener(this);
        rlStatementList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        personageAdapter = new PersonageAdapter(R.layout.item_personage, mlist);
        CustomLoadMoreView customLoadMoreView = new CustomLoadMoreView();
        personageAdapter.setLoadMoreView(customLoadMoreView);
        personageAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        personageAdapter.isFirstOnly(false);
        rlStatementList.setAdapter(personageAdapter);
        isRefresh = 0;
        groupData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
    }

    private void groupData(int page, String stime, String otime) {
        if (stime.equals("开始时间")) {
            stime = "";
        }
        if (otime.equals("结束时间")) {
            otime = "";
        }
        HttpParams params = new HttpParams();
        params.put("page", page);
        params.put("pagesize", pagesize);
        params.put("d1", stime);
        params.put("d2", otime);
        UserCenterServerApi.getpersonage(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PersonageBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull PersonageBean personageBean) {
                        stopProgressDialog();
                        if (personageBean.getResult().equals("1")) {
                            if (personageBean.getTable().size()<pagesize){
                                mLoadMoreEndGone = true;
                                personageAdapter.loadMoreEnd(mLoadMoreEndGone);
                            }
                            if (isRefresh==1){
                                personageAdapter.loadMoreComplete();
                                personageAdapter.addData(personageBean.getTable());
                            }else {
                                mlist = personageBean.getTable();
                                swipeLayout.setRefreshing(false);
                                personageAdapter.setNewData(mlist);
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    @Override
    public void onRefresh() {
        page=1;
        isRefresh=0;
        tvStarttime.setText("开始时间");
        tvOvertime.setText("结束时间");
        groupData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
    }

    @Override
    public void onLoadMoreRequested() {
        page++;
        isRefresh=1;
        groupData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.srl_starttime:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(PersonageActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c.set(year, monthOfYear, dayOfMonth);
                        tvStarttime.setText(DateFormat.format("yyy-MM-dd", c));
                        page=1;
                        isRefresh=0;
                        groupData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());

                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.srl_overtime:
                final Calendar c1 = Calendar.getInstance();
                DatePickerDialog dialog1 = new DatePickerDialog(PersonageActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c1.set(year, monthOfYear, dayOfMonth);
                        tvOvertime.setText(DateFormat.format("yyy-MM-dd", c1));
                        page=1;
                        isRefresh=0;
                        groupData(page, tvStarttime.getText().toString(), tvOvertime.getText().toString());

                    }
                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                dialog1.show();
                break;
        }
    }

}
