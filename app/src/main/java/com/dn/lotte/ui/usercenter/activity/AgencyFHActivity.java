package com.dn.lotte.ui.usercenter.activity;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dn.lotte.R;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.AgencynFHBean;
import com.dn.lotte.ui.usercenter.adapter.AgencyFHAdapte;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.lzy.okgo.model.HttpParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by ASUS on 2017/10/19.
 */

public class AgencyFHActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.rv_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeLayout)
    SwipeRefreshLayout mSwipeLayout;
    private int page = 1, pagesize = 10;
    private String day1;
    private String time;
    private List<AgencynFHBean.TableBean> mlist = new ArrayList<>();
    private AgencyFHAdapte mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_fh;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setMainTitle(R.string.agency_profit);
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        mSwipeLayout.setColorSchemeColors(Color.rgb(147, 147, 147));
        mSwipeLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new AgencyFHAdapte(R.layout.item_agencyfh, mlist);
        mAdapter.openLoadAnimation(BaseQuickAdapter.SCALEIN);
        mAdapter.isFirstOnly(false);
        mRecyclerView.setAdapter(mAdapter);
        //过去15天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date time1 = c.getTime();
        c.add(Calendar.DATE, -15);
        Date d = c.getTime();
        day1 = format.format(d);
        time = format.format(time1);

        getFH(day1, this.time);
    }

    public void getFH(String stime, String time) {
        HttpParams params = new HttpParams();
        params.put("page", page);
        params.put("pagesize", pagesize);
        params.put("d1", "");
        params.put("d2", "");
        UserCenterServerApi.agencyfh(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AgencynFHBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AgencynFHBean agencynFHBean) {

                        if (agencynFHBean.getResult().equals("1")) {
                            mSwipeLayout.setRefreshing(false);
                            mAdapter.setNewData(agencynFHBean.getTable());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }

    @Override
    public void onRefresh() {
        getFH(day1, time);
    }


}
