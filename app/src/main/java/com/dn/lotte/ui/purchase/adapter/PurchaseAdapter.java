package com.dn.lotte.ui.purchase.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.utils.CommonUtils;

import java.util.List;


/**
 * Created by DN on 2017/10/8.
 */

public class PurchaseAdapter extends BaseQuickAdapter<PurchaseBean.TableBean, BaseViewHolder> {

    public PurchaseAdapter(@LayoutRes int layoutResId, @Nullable List<PurchaseBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PurchaseBean.TableBean item) {
        helper.setText(R.id.tv_purchase_name,item.getTitle());
        if (item.getTitle().equals("重庆时时彩")){
            Log.i("xinjiang",CommonUtils.getImgLocation(item.getTitle())+"");
        }
        helper.setImageResource(R.id.iv_purchase_headImg, CommonUtils.getImgLocation(item.getTitle()));

    }

}
