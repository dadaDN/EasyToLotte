package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.TransFerrecordBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/19.
 */

public class TransferRecordAdapter extends BaseQuickAdapter<TransFerrecordBean.TableBean, BaseViewHolder> {

    private String namestate;

    public TransferRecordAdapter(@LayoutRes int layoutResId, @Nullable List<TransFerrecordBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TransFerrecordBean.TableBean item) {
        if (item.getType().equals("0")) {
            namestate = "充值转账";
        } else if (item.getType().equals("1")) {
             namestate = "活动转账";

        } else if (item.getType().equals("2")) {
             namestate = "其他转账";

        }
        helper.setText(R.id.tv_username, item.getSsid())
                .setText(R.id.tv_recharge, item.getMoneychange())
                .setText(R.id.tv_withdrawal, namestate)
                .setText(R.id.tv_consume, item.getUsername())
                .setText(R.id.tv_activity, item.getTousername())
                .setText(R.id.tv_return, item.getStime());
    }
}
