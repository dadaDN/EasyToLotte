package com.dn.lotte.ui.usercenter.activity;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.bean.NoticeDetailBean;
import com.easy.common.base.BaseActivity;
import com.lzy.okgo.model.HttpParams;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class MessageDetailActivity extends BaseActivity {


    @Override
    public int getLayoutId() {
        return R.layout.activity_message_detail;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        loadData();
    }

    private void loadData() {
        HttpParams httpParams = new HttpParams();
//        httpParams.put("id", page);
        ServerApi.getNoticeDetailData(httpParams)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
//                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NoticeDetailBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull NoticeDetailBean purchaseBean) {
                        if (purchaseBean.getResult().equals("1")) {
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
//                        stopProgressDialog();
                    }
                });
    }
}
