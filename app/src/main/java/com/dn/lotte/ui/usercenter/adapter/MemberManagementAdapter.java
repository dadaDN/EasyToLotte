package com.dn.lotte.ui.usercenter.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.MemberManageBean;

import java.util.List;

/**
 * Created by ASUS on 2017/10/12.
 */

public class MemberManagementAdapter extends BaseQuickAdapter<MemberManageBean.TableBean, BaseViewHolder> {
    public MemberManagementAdapter(int item_membermanage, List<MemberManageBean.TableBean> mlist) {
        super(item_membermanage, mlist);
    }

    @Override
    protected void convert(BaseViewHolder helper, MemberManageBean.TableBean item) {

        helper.setText(R.id.tv_name, item.getUsername());

        if (item.getIsonline().equals("0")) {//0离线1在线
            helper.setText(R.id.tv_now_state, "离线");//当前状态
            helper.setTextColor(R.id.tv_now_state, Color.parseColor("#9a9a9a"));
            helper.setVisible(R.id.iv_lixian,true);
            helper.setVisible(R.id.iv_zaixian,false);
        } else {
            helper.setText(R.id.tv_now_state, "在线");//当前状态
            helper.setTextColor(R.id.tv_now_state, Color.parseColor("#48a7e7"));
            helper.setVisible(R.id.iv_lixian,false);
            helper.setVisible(R.id.iv_zaixian,true);
        }


        if (item.getIsfhcontract().equals("0")) {//0无 1 有
            helper.setVisible(R.id.ll_fenhong,false);
        } else {
            helper.setVisible(R.id.ll_fenhong,true);
        }

//        helper;//最后登录
        helper.setText(R.id.tv_return, item.getPoint());//彩票返点
        helper.setText(R.id.tv_account_money, item.getMoney());//账户余额
        helper.setText(R.id.tv_change_name, item.getUsergroupname())
                .setText(R.id.tv_last_time, item.getLasttime())
                .addOnClickListener(R.id.ib_money).addOnClickListener(R.id.ib_bianji)
                .addOnClickListener(R.id.ll_xiaji)
                .addOnClickListener(R.id.ll_fenhong);

    }

}
