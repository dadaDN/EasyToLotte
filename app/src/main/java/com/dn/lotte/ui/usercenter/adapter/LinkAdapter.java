package com.dn.lotte.ui.usercenter.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dn.lotte.R;
import com.dn.lotte.bean.LinkBean;

import java.util.List;

/**
 * Created by ASUS on 2017/12/11.
 */

public class LinkAdapter extends BaseQuickAdapter<LinkBean.TableBean,BaseViewHolder> {
    public LinkAdapter(@LayoutRes int layoutResId, @Nullable List<LinkBean.TableBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, LinkBean.TableBean item) {
        helper.setText(R.id.tv_name,"剩余有效期:"+item.getYxtime()+"天")
                .setText(R.id.tv_time,"剩余次数:"+item.getTimes()+"次")
                .setText(R.id.tv_last_time,item.getStime())
                .setText(R.id.tv_point,"会员返点:"+item.getPoint())
                .addOnClickListener(R.id.ll_copy);
    }
}
