package com.dn.lotte.ui.usercenter.activity;

import android.content.Context;
import android.text.ClipboardManager;
import android.widget.TextView;

import com.dn.lotte.R;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;

import butterknife.Bind;

/**
 * Created by ASUS on 2017/12/8.
 */

public class Chongzhiweb extends BaseActivity implements RippleView.OnRippleCompleteListener{
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.text)
    TextView text;
    @Bind(R.id.tv_bank)
    TextView tvBank;
    @Bind(R.id.text1)
    TextView text1;
    @Bind(R.id.tv_money)
    TextView tvMoney;
    @Bind(R.id.text2)
    TextView text2;
    @Bind(R.id.tv_name)
    TextView tvName;
    @Bind(R.id.text3)
    TextView text3;
    @Bind(R.id.tv_number)
    TextView tvNumber;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    private String url;
    private String bankname;
    private String name;
    private String number;
    private String money;

    @Override
    public int getLayoutId() {
        return R.layout.activity_chognzhi;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        toolbar.setMainTitle("充值确认");
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        okBtn.setOnRippleCompleteListener(this);
        bankname = getIntent().getStringExtra("bankname");
        name = getIntent().getStringExtra("name");
        number = getIntent().getStringExtra("number");
        money = getIntent().getStringExtra("money");
        tvBank.setText(bankname);
        tvName.setText(name);
        tvMoney.setText(money);
        tvNumber.setText(number);
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()){
            case R.id.okBtn:
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                // 将文本内容放到系统剪贴板里。
                cm.setText(number);
                showShortToast("已成功复制,快去充值吧!");
                break;
        }
    }
}
