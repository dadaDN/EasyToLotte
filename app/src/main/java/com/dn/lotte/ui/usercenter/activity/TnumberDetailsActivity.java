package com.dn.lotte.ui.usercenter.activity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.api.ServerApi;
import com.dn.lotte.api.UserCenterServerApi;
import com.dn.lotte.bean.BettingBean;
import com.dn.lotte.bean.EventBusBean;
import com.dn.lotte.bean.RemoveBean;
import com.dn.lotte.bean.TzDetailsBean;
import com.dn.lotte.utils.CommonUtils;
import com.easy.common.base.BaseActivity;
import com.easy.common.commonwidget.DnToolbar;
import com.easy.common.commonwidget.RippleView;
import com.lzy.okgo.model.HttpParams;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 11
 * 投注详情
 * Created by ASUS on 2017/10/15.
 */

public class TnumberDetailsActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {
    @Bind(R.id.lt_main_title_left)
    TextView ltMainTitleLeft;
    @Bind(R.id.lt_main_title)
    TextView ltMainTitle;
    @Bind(R.id.lt_main_title_right)
    TextView ltMainTitleRight;
    @Bind(R.id.toolbar)
    DnToolbar toolbar;
    @Bind(R.id.tv_big_name)
    TextView tvBigName;
    @Bind(R.id.tv_center_name)
    TextView tvCenterName;
    @Bind(R.id.tv_small_name)
    TextView tvSmallName;
    @Bind(R.id.tv_qs_number)
    TextView tvQsNumber;
    @Bind(R.id.tv_time_n)
    TextView tvTimeN;
    @Bind(R.id.tv_time)
    TextView tvTime;
    @Bind(R.id.iv_touzhu)
    RelativeLayout ivTouzhu;
    @Bind(R.id.tv_states)
    TextView tvStates;
    @Bind(R.id.tv_dd_number)
    TextView tvDdNumber;
    @Bind(R.id.tv_qh_number)
    TextView tvQhNumber;
    @Bind(R.id.tv_buy_money)
    TextView tvBuyMoney;
    @Bind(R.id.tv_zhone_money)
    TextView tvZhoneMoney;
    @Bind(R.id.tv_sy_money)
    TextView tvSyMoney;
    @Bind(R.id.tv_buy_beishu)
    TextView tvBuyBeishu;
    @Bind(R.id.tv_tz_number)
    TextView tvTzNumber;
    @Bind(R.id.tv_zhong_zs)
    TextView tvZhongZs;
    @Bind(R.id.tv_buy_fandian)
    TextView tvBuyFandian;
    @Bind(R.id.tv_fandian_money)
    TextView tvFandianMoney;
    @Bind(R.id.tv_jj_mode)
    TextView tvJjMode;
    @Bind(R.id.tv_kaijiang_number)
    TextView tvKaijiangNumber;
    @Bind(R.id.tv_tou_time_n)
    TextView tvTouTimeN;
    @Bind(R.id.tv_tou_time_s)
    TextView tvTouTimeS;
    @Bind(R.id.tv_touzhu_number)
    TextView tvTouzhuNumber;
    @Bind(R.id.okBtn)
    RippleView okBtn;
    @Bind(R.id.iv_head)
    ImageView mHeadImg;
    @Bind(R.id.tv_buy_moshi)
    TextView tvBuyMoshi;
    private BettingBean.TableBean mlist;

    @Override
    public int getLayoutId() {
        return R.layout.activity_tdetails;
    }

    @Override
    public void initPresenter() {

    }

    @Override
    public void initView() {
        initTitle();
        toolbar.setToolbarLeftBackImageRes(R.drawable.icon_back);
        toolbar.setMainTitle(R.string.user_details);
        okBtn.setOnRippleCompleteListener(this);
        mlist = (BettingBean.TableBean) getIntent().getSerializableExtra("tdetails");
        initDetailsDate();

    }

    private void initDetailsDate() {
        HttpParams params = new HttpParams();
        params.put("Id", mlist.getId());
        ServerApi.gettzDetails(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TzDetailsBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull TzDetailsBean tzDetailsBean) {
                        stopProgressDialog();
                        if (tzDetailsBean.getResult().equals("1")) {
                            initDate(tzDetailsBean.getTable().get(0));
                        } else {
                            showShortToast(tzDetailsBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();

                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();

                    }
                });
    }

    private void initDate(TzDetailsBean.TableBean tzDetailsBean) {
        tvBigName.setText(tzDetailsBean.getLotteryname());
        mHeadImg.setImageResource(CommonUtils.getImgLocation(mlist.getLotteryname()));
        tvSmallName.setText(tzDetailsBean.getPlayname());
        tvCenterName.setVisibility(View.GONE);
        tvQsNumber.setText(tzDetailsBean.getIssuenum());
        tvTimeN.setText(tzDetailsBean.getStime());
        tvBuyMoshi.setText(tzDetailsBean.getMoshi());
        if (tzDetailsBean.getState().equals("0")) {
            //未开奖
            tvStates.setText("未开奖");
            okBtn.setVisibility(View.VISIBLE);
        } else if (tzDetailsBean.getState().equals("1")) {
            okBtn.setVisibility(View.GONE);
            tvStates.setText("已撤单");
        } else if (tzDetailsBean.getState().equals("2")) {
            okBtn.setVisibility(View.GONE);
            tvStates.setText("未中奖");
        } else if (tzDetailsBean.getState().equals("3")) {
            okBtn.setVisibility(View.GONE);
            tvStates.setText("已中奖");
        }

        tvDdNumber.setText(tzDetailsBean.getSsid());
        tvQhNumber.setText(tzDetailsBean.getIssuenum());
        tvBuyMoney.setText(tzDetailsBean.getTotal2());
        tvZhoneMoney.setText(tzDetailsBean.getWinbonus());
        tvSyMoney.setText(tzDetailsBean.getRealget());
        tvBuyBeishu.setText(tzDetailsBean.getTimes());
        tvTzNumber.setText(tzDetailsBean.getNum());
        tvKaijiangNumber.setText(tzDetailsBean.getKjnumber());
        tvJjMode.setText(tzDetailsBean.getBonus());
        tvZhongZs.setText(tzDetailsBean.getWinnum());//中奖注数
        tvBuyFandian.setText(tzDetailsBean.getPoint2());//购买返点
        tvFandianMoney.setText(tzDetailsBean.getPointmoney());//返点金额
        tvTouTimeN.setText(tzDetailsBean.getStime2());//投注时间
        tvTouzhuNumber.setText(tzDetailsBean.getStrdetail());//投注号码
    }

    @Override
    public void onComplete(RippleView rippleView) {
        switch (rippleView.getId()) {
            case R.id.okBtn:
                setMove();
                break;
        }
    }

    private void setMove() {
        final HttpParams params = new HttpParams();
        params.put("Id", mlist.getId());
        UserCenterServerApi.setRemove(params)
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        startProgressDialog();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RemoveBean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onNext(@NonNull RemoveBean purseNumberBean) {
                        stopProgressDialog();
                        Log.e("dddd", "ddddd");
                        if (purseNumberBean.getResult().equals("1")) {
                            showShortToast(purseNumberBean.getReturnval());
                            finish();
                            EventBus.getDefault().post(new EventBusBean<String>("cedansuccess", "cedansuccess"));
                        } else {
                            showShortToast(purseNumberBean.getReturnval());
                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        stopProgressDialog();
                    }

                    @Override
                    public void onComplete() {
                        stopProgressDialog();
                    }
                });
    }
}
