package com.dn.lotte.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dn.lotte.R;
import com.dn.lotte.app.AppConstant;
import com.dn.lotte.app.GlobalApplication;
import com.dn.lotte.bean.PurchaseBean;
import com.dn.lotte.bean.PurchaseDetailGameBean;
import com.dn.lotte.bean.PurchaseDetailNumberBean;
import com.dn.lotte.bean.PurchaseDetailTypeBean;
import com.dn.lotte.bean.SelectNumberBean;
import com.easy.common.commonutils.JsonUtils;
import com.easy.common.commonutils.SPUtils;
import com.easy.common.commonutils.StringUtils;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 丁宁
 * on 2017/10/9.
 */

public class CommonUtils {

    private static SharedPreferences sp;
    private static List<PurchaseDetailGameBean.TableBean> mGameTypeList;

    public static String getSubStr(String str) {
        if (str.startsWith("(")) {
            return StringUtils.isEmpty(str) ? "" : str.substring(1, str.length() - 1);
        } else
            return str;

    }

    public static void setGameNumberData(PurchaseDetailNumberBean list, String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(id, json);
        editor.commit();
    }

    public static PurchaseDetailNumberBean getGameNumberData(String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        String json = sp.getString(id, null);
        return (PurchaseDetailNumberBean) JsonUtils.fromJson(json, PurchaseDetailNumberBean.class);
    }

    /**
     * 第一次进入的时候 缓存游戏玩法分类的第一项
     * 之后缓存保存 的数据
     */
    public static void setSelectGameList(String id, List<PurchaseDetailGameBean.TableBean> list) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(AppConstant.SELECTGAMELIST + id, json);
        editor.commit();
    }

    public static List<PurchaseDetailGameBean.TableBean> getSelectGameList(String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        Gson gson = new Gson();
        String json = sp.getString(AppConstant.SELECTGAMELIST + id, null);
        Type type = new TypeToken<List<PurchaseDetailGameBean.TableBean>>() {
        }.getType();
        List<PurchaseDetailGameBean.TableBean> arrayList = gson.fromJson(json, type);
        return arrayList;
    }

    public static void setMoreGameList(List<PurchaseBean.TableBean> list) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(AppConstant.MOREGAME, json);
        editor.commit();
    }

    public static List<PurchaseBean.TableBean> getMoreGameList() {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        Gson gson = new Gson();
        String json = sp.getString(AppConstant.MOREGAME, null);
        Type type = new TypeToken<List<PurchaseBean.TableBean>>() {
        }.getType();
        List<PurchaseBean.TableBean> arrayList = gson.fromJson(json, type);
        return arrayList;
    }

    //根據ID緩存當前玩法的請求數據
    public static void setGameTypeDataList(PurchaseDetailTypeBean list, String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(id, json);
        editor.commit();
    }

    //根據ID獲取緩存數據
    public static PurchaseDetailTypeBean getGameTypeDataList(String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        String json = sp.getString(id, null);
        return (PurchaseDetailTypeBean) JsonUtils.fromJson(json, PurchaseDetailTypeBean.class);
    }

    //根據ID緩存當前玩法的請求數據
    public static void setGameData(PurchaseDetailGameBean list, String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(id, json);
        editor.commit();
    }

    //根據ID獲取緩存數據
    public static PurchaseDetailGameBean getGameData(String id) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        String json = sp.getString(id, null);
        return (PurchaseDetailGameBean) JsonUtils.fromJson(json, PurchaseDetailGameBean.class);
    }

    public static void setGameTypeList(List<PurchaseDetailGameBean.TableBean> list) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(AppConstant.GAMETYPE, json);
        editor.commit();
    }

    public static List<PurchaseDetailGameBean.TableBean> getGameTypeList() {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        Gson gson = new Gson();
        String json = sp.getString(AppConstant.GAMETYPE, null);
        Type type = new TypeToken<List<PurchaseDetailGameBean.TableBean>>() {
        }.getType();
        mGameTypeList = gson.fromJson(json, type);
        return mGameTypeList;
    }

    public static List<PurchaseDetailGameBean.TableBean> getGameTypeDetailList(String id) {
        List<PurchaseDetailGameBean.TableBean> list = new ArrayList<>();
        mGameTypeList = getGameTypeList();
        for (PurchaseDetailGameBean.TableBean bean : mGameTypeList) {
            if (bean.getRadio().equals(id)) {
                list.add(bean);
            }
        }
        return list;
    }


    public static void setNumberList(List<PurchaseDetailNumberBean.TableBean> list) {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(AppConstant.NumberList, json);
        editor.commit();
    }

    public static List<PurchaseDetailNumberBean.TableBean> getNumberGameList() {
        if (sp == null) {
            sp = GlobalApplication.getInstance().getSharedPreferences("config", MODE_PRIVATE);
        }
        Gson gson = new Gson();
        String json = sp.getString(AppConstant.NumberList, null);
        Type type = new TypeToken<List<PurchaseDetailNumberBean.TableBean>>() {
        }.getType();
        List<PurchaseDetailNumberBean.TableBean> arrayList = gson.fromJson(json, type);
        return arrayList;
    }

    public static void setSelectNumberList(String selectData) {
        SPUtils.setSharedStringData(GlobalApplication.getInstance(), "NUMBERSELECTDATA", selectData);
    }

    public static List<SelectNumberBean.NumberBean> getSelectNumberList() {
        String selectData = SPUtils.getSharedStringData(GlobalApplication.getInstance(), "NUMBERSELECTDATA");
        if (!StringUtils.isEmpty(selectData)) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<SelectNumberBean.NumberBean>>() {
            }.getType();
            return gson.fromJson(selectData, type);
        } else
            return new ArrayList<>();
    }


    /**
     * 傻逼代码  列表图片本地写死图片
     *
     * @param selectName 匹配title名字
     * @return
     */
    public static int getImgLocation(String selectName) {
        int location = 0;
        switch (selectName.trim()) {
            case "韩国1.5分":
                location = R.drawable.logo_cp_3d_hg;
                break;
            case "纽约30秒":
                location = R.drawable.logo_cp_3d_ny;
                break;
            case "一分3D":
                location = R.drawable.logo_cp_3d_yf;
                break;
            case "重庆11选5":
                location = R.drawable.logo_cp_11_cq;
                break;
            case "广东11选5":
                location = R.drawable.logo_cp_11_gd;
                break;
            case "韩国11选5":
                location = R.drawable.logo_cp_11_hg;
                break;
            case "江西11选5":
                location = R.drawable.logo_cp_11_jx;
                break;
            case "纽约11选5":
                location = R.drawable.logo_cp_11_ny;
                break;
            case "山东11选5":
                location = R.drawable.logo_cp_11_sd;
                break;
            case "上海11选5":
                location = R.drawable.logo_cp_11_sh;
                break;
            case "2分彩":
                location = R.drawable.logo_cp_efc;
                break;
            case "福彩3D":
                location = R.drawable.logo_cp_fc3d;
                break;
            case "菲律宾1.5分彩":
                location = R.drawable.logo_cp_flb_15;
                break;
            case "韩国1.5分彩":
                location = R.drawable.logo_cp_hg_15;
                break;
            case "纽约30秒彩":
                location = R.drawable.logo_cp_ny_30;
                break;
            case "重庆时时彩":
                location = R.drawable.logo_cp_ssc_cq;
                break;
            case "菲律宾1.5分":
                location = R.drawable.logo_cp_flb_15;
                break;
            case "新加坡30秒":
                location = R.drawable.logo_cp_xjp_30;
                break;
            case "北京PK10":
                location = R.drawable.logo_cp_sc_bj;
                break;
            case "英国30秒赛车":
                location = R.drawable.logo_cp_sc_30yg;
                break;
            case "体彩P3":
                location = R.drawable.logo_cp_pl3;
                break;
            case "新疆时时彩":
                location = R.drawable.logo_cp_ssc_xj;
                break;
            case "新德里1.5分彩":
                location = R.drawable.logo_cp_xdl_15;
                break;
            case "天津时时彩":
                location = R.drawable.logo_cp_ssc_tj;
                break;
            case "台湾45秒彩":
                location = R.drawable.logo_cp_tw_45;
                break;
            case "纽约30秒3D":
                location = R.drawable.logo_cp_3d_ny;
                break;
            case "英国60秒赛车":
                location = R.drawable.logo_cp_sc_60yg;
                break;
            case "英国120秒赛车":
                location = R.drawable.logo_cp_sc_120yg;
                break;
            case "韩国1.5分3D":
                location = R.drawable.logo_cp_3d_hg;
                break;
            case "首尔60秒":
                location = R.drawable.logo_cp_se_60;
                break;
            case "东京1.5分彩":
                location = R.drawable.logo_cp_dj_15;
                break;
            case "腾讯分分彩":
                location = R.drawable.logo_cp_tx_ff;
                break;
            case "新加坡2分彩":
                location = R.drawable.logo_cp_xjp_2;
                break;
            case "台湾5分彩":
                location = R.drawable.logo_cp_tw_5;
                break;
            case "韩国1.5分11选5":
                location = R.drawable.logo_cp_11x5_hg;
                break;
            case "时时乐":
                location = R.drawable.logo_cp_ssl;
                break;
            case "纽约30秒11选5":
                location = R.drawable.logo_cp_11x5_ny;
                break;
            default:
                location = R.drawable.default_head;
                break;
        }
        return location;
    }

    // 精确乘法
    public static double mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }

    /**
     * 提供精确的加法运算。
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */

    public static double add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.add(b2).doubleValue();
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */

    public static double sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

    // 默认除法运算精度
    private static final int DEF_DIV_SCALE = 10;

    /**
     * 提供（相对）精确的除法运算。当发生除不尽的情况时，由scale参数指
     * 定精度，以后的数字四舍五入。
     *
     * @param v1    被除数
     * @param v2    除数
     * @param scale 表示表示需要精确到小数点以后几位。
     * @return 两个参数的商
     */
    public static double div(double v1, double v2, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public static String getVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "v1.0.0";

        }
    }

    /**
     * 显示标签
     *
     * @param flexboxLayout FlexboxLayout
     * @param str           “聪明，强，无敌” --> 以 ，隔开的字符串
     */
    public static void showlabel(Context mContext, FlexboxLayout flexboxLayout, String str) {
        if (flexboxLayout != null) {
            flexboxLayout.removeAllViews();
            String[] allStr;
            if (!StringUtils.isEmpty(str)) {
                allStr = str.split(",");
                for (int i = 0; i < allStr.length; i++) {
                    TextView textView = new TextView(mContext);
                    textView.setText(allStr[i]);
                    textView.setGravity(Gravity.CENTER);
                    textView.setTextSize(12);
                    textView.setTextColor(mContext.getResources().getColor(R.color.white));
                    textView.setBackgroundResource(R.drawable.round_bule2);
                    flexboxLayout.addView(textView);
                    ViewGroup.LayoutParams params = textView.getLayoutParams();
                    if (params instanceof FlexboxLayout.LayoutParams) {
                        FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) params;
                        layoutParams.setMargins(5, 8, 0, 0);
                        textView.setLayoutParams(layoutParams);
                    }
                }
            }
        }
    }
}
