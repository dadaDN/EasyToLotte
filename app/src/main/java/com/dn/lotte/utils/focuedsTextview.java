package com.dn.lotte.utils;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ASUS on 2017/9/1.
 */

public class focuedsTextview extends TextView {
    public focuedsTextview(Context context) {
        super(context);
    }

    public focuedsTextview(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public focuedsTextview(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 重写父类的方法，textview直接获取焦点
     * @return
     */
    @Override
    public boolean isFocused() {
        return true;
    }
}
