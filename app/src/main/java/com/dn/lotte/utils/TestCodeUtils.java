package com.dn.lotte.utils;

import com.easy.common.commonutils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by DN on 2017/10/20.
 */

public class TestCodeUtils {
    /**
     * @param playCode title
     * @param balls    选中数 1,1,,,
     * @param pos      选中位置 选中为 1   11000 1,1,,,
     * @return
     */
    public static int BetNumerice(String playCode, String balls, String pos, boolean isSdz) {
//            #region 验证注数
        int NumSum = 0;
        int NumSumNext = 0;
        switch (playCode) {
            case "P_5ZX120":
                NumSum = RedZu120(balls);
                break;
            case "P_5ZX60":
                NumSum = RedZu60(balls);
                break;
            case "P_5ZX30":
                NumSum = RedZu30(balls);
                break;
            case "P_5ZX20":
                NumSum = RedZu20(balls);
                break;
            case "P_5ZX10":
                NumSum = RedZu10(balls);
                break;
            case "P_5ZX5":
                NumSum = RedZu5(balls);
                break;
            case "P_5TS1":
            case "P_5TS2":
            case "P_5TS3":
            case "P_5TS4":
                if (balls.contains(",") || (balls.length() > 1 && !balls.contains("_"))) {
                    NumSum = 0;
                } else
                    NumSum = RedTS(balls);
                break;
            case "P_4ZX24":
                NumSum = RedZu24(balls);
                break;
            case "P_4ZX12":
                NumSum = RedZu12(balls);
                break;
            case "P_4ZX6":
                NumSum = RedZu61(balls);
                break;
            case "P_4ZX4":
                NumSum = RedZu4(balls);
                break;
            case "P_5FS":
                if (balls.split(",").length != 5) {
                    NumSum = 0;
                } else
                    NumSum = RedFS(balls);
                break;
            case "P_4FS_L":
            case "P_4FS_R":
                if (balls.split(",").length != 4) {
                    NumSum = 0;
                } else
                    NumSum = RedFS(balls);
                break;
            case "P_3FS_L":
            case "P_3FS_C":
            case "P_3FS_R":
                if (balls.split(",").length != 3) {
                    NumSum = 0;
                } else
                    NumSum = RedFS(balls);
                break;
            case "P_2FS_L":
            case "P_2FS_R":

                if (balls.split(",").length != 2) {
                    NumSum = 0;
                } else {
                    if (isSdz) {
                        NumSum = RedFS2(balls);
                    } else
                        NumSum = RedFS(balls);
                }

                break;
            case "P_5DS":
                NumSum = RedDS(balls, 5);
                break;
            case "P_4DS_L":
            case "P_4DS_R":
                NumSum = RedDS(balls, 4);
                break;
            case "P_3DS_L":
            case "P_3DS_C":
            case "P_3DS_R":
                NumSum = RedDS(balls, 3);
                break;
            case "P_2DS_L":
            case "P_2DS_R":
            case "P_2ZDS_L":
            case "P_2ZDS_R":
                if (isSdz)
                    NumSum = RedDS3(balls, 2);
                else
                    NumSum = RedDS(balls, 2);

                break;
            case "P_3HX_L":
            case "P_3HX_C":
            case "P_3HX_R":
                NumSum = RedDS(balls, 3);
                break;
            case "P_3Z3_L":
            case "P_3Z3_C":
            case "P_3Z3_R":
                if (!balls.contains(",")) {
                    NumSum = RedZu3(balls);
                } else {
                    NumSum = 0;
                }
                break;
            case "P_3Z6_L":
            case "P_3Z6_C":
            case "P_3Z6_R":
                if (!balls.contains(",")) {
                    NumSum = RedZu6(balls);
                } else {
                    NumSum = 0;
                }
                break;
            case "P_2Z2_L":
            case "P_2Z2_R":
                if (!balls.contains(",")) {
                    NumSum = RedZu2(balls);
                } else {
                    NumSum = 0;
                }
                break;
            case "P_DD":
//                String[] split = balls.split(",");
//                if (balls.split(",").length != 5) {
//                    NumSum = 0;
//                } else {
//
//                }
                NumSum = RedDD(balls);
//                if(NumSum>8){
//                    NumSum = -1;
//                }
                boolean pddTrue = false;
                String[] PddArray = balls.split(",");
                for (int i = 0; i < PddArray.length; i++) {
                    if (PddArray[i].length() > 8) {
                        pddTrue = true;
                    }
                }
                if (pddTrue) {
//                    return JsonResult(0, "投注失败,定位胆单个位置最多允许投注8码！");
                }
                break;
            case "P_BDD_C":
            case "P_BDD_L":
            case "P_BDD_R":
                if (!balls.contains(",")) {
                    NumSum = RedFS(balls);
                } else {
                    NumSum = 0;
                }
                break;
            case "P_2DXDS_L":
            case "P_2DXDS_R":
            case "P_LHH_WQ":
            case "P_LHH_WB":
            case "P_LHH_WS":
            case "P_LHH_WG":
            case "P_LHH_QB":
            case "P_LHH_QS":
            case "P_LHH_QG":
            case "P_LHH_BS":
            case "P_LHH_BG":
            case "P_LHH_SG":
                NumSum = RedFS(balls);
                break;
            case "P_3HE_L":
            case "P_3HE_C":
            case "P_3HE_R":
                NumSum = RedHE3(balls);
                break;
            case "P_3ZHE_L":
            case "P_3ZHE_C":
            case "P_3ZHE_R":
                NumSum = RedZHE3(balls);
                break;
            case "P_3KD_L":
            case "P_3KD_C":
            case "P_3KD_R":
                NumSum = Red3KD(balls);
                break;
            case "P_3Z3DS_L":
            case "P_3Z6DS_L":
            case "P_3Z3DS_C":
            case "P_3Z6DS_C":
            case "P_3Z3DS_R":
            case "P_3Z6DS_R":
                NumSum = RedDS(balls, 3);
                break;
            case "P_3ZBD_L":
            case "P_3ZBD_C":
            case "P_3ZBD_R":
                NumSum = 54;
                break;
            case "P_3QTWS_L":
            case "P_3QTWS_C":
            case "P_3QTWS_R":
                NumSum = RedDD2(balls);
                break;
            case "P_3QTTS_L":
            case "P_3QTTS_C":
            case "P_3QTTS_R":
                NumSum = RedDD(balls) / 2;
                break;
            case "P_2HE_L":
            case "P_2HE_R":
                NumSum = RedHE2(balls);
                break;
            case "P_2ZHE_L":
            case "P_2ZHE_R":
                NumSum = RedZHE2(balls);
                break;
            case "P_2KD_L":
            case "P_2KD_R":
                NumSum = Red2KD(balls);
                break;
            case "P_2ZBD_L":
            case "P_2ZBD_R":
                NumSum = 9;
                break;
            case "P_5ZH":
                NumSum = Red5ZuHe(balls);
                break;
            case "P_4ZH_L":
            case "P_4ZH_R":
                NumSum = Red4ZuHe(balls);
                break;
            case "P_3ZH_L":
            case "P_3ZH_C":
            case "P_3ZH_R":
                NumSum = Red3ZuHe(balls);
                break;
            case "P_3BDD1_R":
            case "P_3BDD1_L":
            case "P_4BDD1":
                NumSum = RedDD(balls.replace(",", ""));
                break;
            case "P_3BDD2_R":
            case "P_3BDD2_L":
            case "P_4BDD2":
            case "P_5BDD2":
                NumSum = RedZu2(balls.replace(",", ""));
                break;
            case "P_5BDD3":
                NumSum = RedZu6(balls.replace(",", ""));
                break;
            case "P_5QJ3":
            case "P_4QJ3":
            case "P_3QJ2_L":
            case "P_3QJ2_R":
                NumSum = RedQwQj(balls);
                break;
            case "P_5QW3":
            case "P_4QW3":
            case "P_3QW2_L":
            case "P_3QW2_R":
                NumSum = RedFS(balls.replace(",", ""));
                break;
            case "R_4FS":
                NumSum = RedFS_R(balls, pos, 4);
                break;
            case "R_4DS":
                NumSum = RedDS_R(balls, pos, 4);
                break;
            case "R_4ZX24":
                NumSum = RedZu24(balls) * Combine(getInt(pos), 4);
                break;
            case "R_4ZX12":
                NumSum = RedZu12(balls) * Combine(getInt(pos), 4);
                break;
            case "R_4ZX6":
                NumSum = RedZu61(balls) * Combine(getInt(pos), 4);
                break;
            case "R_4ZX4":
                NumSum = RedZu4(balls) * Combine(getInt(pos), 4);
                break;
            case "R_3FS":
                NumSum = RedFS_R(balls, pos, 3);
                break;
            case "R_3DS":
                NumSum = RedDS_R(balls, pos, 3);
                break;
            case "R_3HX":
                NumSum = RedDS_R2(balls, pos, 3);
                break;
            case "R_3Z6":
                if (!balls.contains(",")) {
                    NumSum = RedZu6_R(balls, pos, 3);
                } else {
                    NumSum = 0;
                }
                break;
            case "R_3Z3":
                if (!balls.contains(",")) {
                    NumSum = RedZu3_R(balls, pos, 3);
                } else {
                    NumSum = 0;
                }
                break;
            case "R_3HE":
                NumSum = RedHE3(balls) * Combine(getInt(pos), 3);
                break;
            case "R_3ZHE":
                NumSum = RedZHE3(balls) * Combine(getInt(pos), 3);
                break;
            case "R_3KD":
                NumSum = Red3KD(balls) * Combine(getInt(pos), 3);
                break;
            case "R_3ZBD":
                NumSum = 54 * Combine(getInt(pos), 3);
                break;
            case "R_3QTWS":
                NumSum = RedDD2(balls) * Combine(getInt(pos), 3);
                break;
            case "R_3QTTS":
                NumSum = RedDD(balls) / 2 * Combine(getInt(pos), 3);
                break;
            case "R_3Z3DS":
            case "R_3Z6DS":
                NumSum = RedDS(balls, 3) * Combine(getInt(pos), 3);
                break;

            case "R_2FS":
                NumSum = RedFS_R(balls, pos, 2);
                break;
            case "R_2DS":
                if (isSdz)
                    NumSum = RedDS_R3(balls, pos, 2);
                else
                    NumSum = RedDS_R(balls, pos, 2);
                break;
            case "R_2Z2":
                if (!balls.contains(",")) {
                    NumSum = RedZu2_R(balls, pos, 2);
                } else {
                    NumSum = 0;
                }
                break;
            case "R_2HE":
                NumSum = RedHE2(balls) * Combine(getInt(pos), 2);
                break;
            case "R_2ZHE":
                NumSum = RedZHE2(balls) * Combine(getInt(pos), 2);
                break;
            case "R_2ZDS":
                NumSum = RedDS(balls, 2) * Combine(getInt(pos), 2);
                break;
            case "R_2KD":
                NumSum = Red2KD(balls) * Combine(getInt(pos), 2);
                break;
            case "R_2ZBD":
                NumSum = 9 * Combine(getInt(pos), 2);
                break;

            //11选5
            case "P11_RXDS_1":
                NumSum = RedDS2(balls, 1);
                break;
            case "P11_RXDS_2":
                NumSum = RedDS2(balls, 2);
                break;
            case "P11_RXDS_3":
                NumSum = RedDS2(balls, 3);
                break;
            case "P11_RXDS_4":
                NumSum = RedDS2(balls, 4);
                break;
            case "P11_RXDS_5":
                NumSum = RedDS2(balls, 5);
                break;
            case "P11_RXDS_6":
                NumSum = RedDS2(balls, 6);
                break;
            case "P11_RXDS_7":
                NumSum = RedDS2(balls, 7);
                break;
            case "P11_RXDS_8":
                NumSum = RedDS2(balls, 8);
                break;
            case "P11_RXFS_1":
                NumSum = RedRXFS_11(balls, 1);
                break;
            case "P11_RXFS_2":
                NumSum = RedRXFS_11(balls, 2);
                break;
            case "P11_RXFS_3":
                NumSum = RedRXFS_11(balls, 3);
                break;
            case "P11_RXFS_4":
                NumSum = RedRXFS_11(balls, 4);
                break;
            case "P11_RXFS_5":
                NumSum = RedRXFS_11(balls, 5);
                break;
            case "P11_RXFS_6":
                NumSum = RedRXFS_11(balls, 6);
                break;
            case "P11_RXFS_7":
                NumSum = RedRXFS_11(balls, 7);
                break;
            case "P11_RXFS_8":
                NumSum = RedRXFS_11(balls, 8);
                break;
            case "P11_3FS_L":
                NumSum = Red3FS_11(balls);
                break;
            case "P11_3ZFS_L":
                NumSum = Red3ZFS_11(balls);
                break;
            case "P11_2FS_L":
                NumSum = Red2FS_11(balls);
                break;
            case "P11_2ZFS_L":
                NumSum = Red2ZFS_11(balls);
                break;
            case "P11_3DS_L":
                NumSum = RedDS2(balls, 3);

                break;
            case "P11_3ZDS_L":
                NumSum = RedDS2(balls, 3);
                break;
            case "P11_2DS_L":
                NumSum = RedDS2(balls, 2);
                break;
            case "P11_2ZDS_L":
                NumSum = RedDS2(balls, 2);
                break;
            case "P11_DD":
//                if (balls.split(",").length != 3) {
//                    NumSum = 0;
//                } else
                NumSum = RedDD_12(balls);
//                if(NumSum>8){
//                    NumSum = -1;
//                }
//                boolean pddTrue2 = false;
//                String[] PddArray2 = balls.split(",");
//                for (int i = 0; i < PddArray2.length; i++) {
//                    if (PddArray2[i].split("_").length > 8) {
//                        pddTrue = true;
//                    }
//                }
//                if (pddTrue2) {
////                    return JsonResult(0, "投注失败,定位胆单个位置最多允许投注8码！");
//                }
                break;
            case "P11_BDD_L":
                if (balls.contains(",") || (balls.length() > 2 && !balls.contains("_"))) {
                    NumSum = 0;
                } else
                    NumSum = RedRXFS_11(balls, 1);
                break;
            case "P11_CDS":
                NumSum = RedRXFS_11(balls, 1);
                break;
            case "P11_CZW":
                NumSum = RedRXFS_11(balls, 1);
                break;
            case "P_DD_3":
                NumSum = RedDD(balls);
                break;

            //PK10
            case "PK10_One":
                NumSum = PK10FS_One(balls);
                break;
            case "PK10_TwoFS":
                NumSum = Red2FS_11(balls);
                break;
            case "PK10_TwoDS":
                NumSum = RedDS2(balls, 2);
                break;
            case "PK10_ThreeFS":
                NumSum = Red3FS_11(balls);
                break;
            case "PK10_ThreeDS":
                NumSum = RedDS2(balls, 3);
                break;
            case "PK10_DD1_5":
            case "PK10_DD6_10":

                NumSum = RedDD_12(balls);

//                if(NumSum>8){
//                    NumSum = -1;
//                }
//                boolean pddTrue3 = false;
//                String[] PddArray3 = balls.split(",");
//                for (int i = 0; i < PddArray3.length; i++) {
//                    if (PddArray3[i].split("_").length > 8) {
//                        pddTrue3 = true;
//                    }
//                }
//                if (pddTrue3) {
////                    return JsonResult(0, "投注失败,定位胆单个位置最多允许投注8码！");
//                }
                break;
            case "PK10_DXOne":
            case "PK10_DXTwo":
            case "PK10_DXThree":
            case "PK10_DSOne":
            case "PK10_DSTwo":
            case "PK10_DSThree":
                NumSum = PK10DXDS(balls);
                break;
            default:
                NumSum = 0;
                break;
        }
//            #endregion

        return NumSum;
    }

    //复试
    public static int RedFS(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 1;
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num *= num2;
            }
            return num;
        }
        return 0;
    }

    //复试
    public static int RedFS2(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 1;
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num *= num2;
            }
            String replace = "";
            int n = 0;
            int n2 = 0;
            for (String s0 : getStrArr(strArray2[0].split(""))) {
                for (String s1 : getStrArr(strArray2[1].split(""))) {
                    if (s0.equals(s1)) {
                        n = n + 1;
                    }
                }
            }
//            if (strArray2[0].length() > strArray2[1].length()) {
//                n2 = strArray2[0].length() - n;
//            } else {
//                replace = strArray2[1].replace(strArray2[0], "");
//                n2 = strArray2[1].length() - n;
//            }

            return num - n;
        }
        return 0;
    }

    //单式
    public static int RedDS(String balls, int num) {
        if (!StringUtils.isEmpty(balls)) {
//getStrArr()
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();
//            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                String[] str2 = strArray2[i].split(" ");
                if (str2[0].length() == num) {
                    list.add("");
                }
//                if (str2.length > num)
//                    return 0;
            }
            return list.size();
        }
        return 0;
    }
    //单式
    public static int RedDS4(String balls, int num) {
        if (!StringUtils.isEmpty(balls)) {
//getStrArr()
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();
//            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                String[] str2 = strArray2[i].split("_");
                if (str2[0].length() == num) {
                    list.add("");
                }
//                if (str2.length > num)
//                    return 0;
            }
            return list.size();
        }
        return 0;
    }
    //单式
    public static int RedDS3(String balls, int num) {
        if (!StringUtils.isEmpty(balls)) {
//getStrArr()
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();
//            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                String[] str2 = strArray2[i].split(" ");

                if (str2[0].length() == num) {
                    if (str2[0].equals("00") || str2[0].equals("11") || str2[0].equals("22") || str2[0].equals("33") || str2[0].equals("44") || str2[0].equals("55") || str2[0].equals("66") || str2[0].equals("77") || str2[0].equals("88") || str2[0].equals("99")) {

                    } else
                        list.add("");
                }
            }
            return list.size();
        }
        return 0;
    }

    public static boolean isEquals(String[] strArr) {
        String s = strArr[0];
        for (int i = 1; i < strArr.length; i++) {
            for (int j = 0; j < strArr.length; j++) {
                if (i != j) {
                    if (strArr[j].equals(strArr[i])) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public static boolean isTwo(String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i].length() != 2) {
                return false;
            }
        }
        return true;
    }

    //单式  01_02_03,02_03_04
    public static int RedDS2(String balls, int num) {
        if (!StringUtils.isEmpty(balls)) {
            int a = 0;
            String[] split = balls.split(",");

            for (int i = 0; i < split.length; i++) {
                String s = split[i];
                String[] split1 = s.split("_");
                if (split1.length == num) {
                    boolean b = isEquals(split1);
                    if (isEquals(split1)) {
                        if (isTwo(split1)) {
                            a++;
                        } else {
                            return a;
                        }
                    } else {
                        return a;
                    }
                } else {
                    if (split.length == 1) {
                        return 0;
                    } else {
                        return a;
                    }
                }
            }
            return a;
        }
        return 0;
    }

    public static int Pareto(int n, int r) {
        int num = 1;
        // n=1 r =2
        for (int i = n; i != (n - r); i--) {
            num *= i;
        }
        return num;
    }

    public static int Combine(int n, int r) {
        return (Pareto(n, r) / Pareto(r, r));
    }

    //组六
    public static int RedZu6(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            int n = balls.length();
            return (Pareto(n, 3) / Pareto(3, 3));
        }
        return 0;
    }

    //组三
    public static int RedZu3(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            int n = balls.length();
            return Pareto(n, 2);
        }
        return 0;
    }

    //组二
    public static int RedZu2(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            int num = balls.length();
            return num * (num - 1) / 2;
        }
        return 0;
    }

    //定位胆
    public static int RedDD(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 0;
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num += num2;
            }
            return num;
        }
        return 0;
    }

    //定位胆
    public static int RedDD2(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 0;
            for (int i = 0; i < strArray2.length; i++) {
                String s = strArray2[i].replace("_", "");
                int num2 = s.length();
                num += num2;
            }
            return num;
        }
        return 0;
    }

    //任选复试
    public static int RedFS_R(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 1;
            if (strArray2.length < PlayWzNum) {
                return 0;
            }
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num *= num2;
            }
            int n = 0;
            int n2 = 0;
            for (String s0 : getStrArr(strArray2[0].split(""))) {
                for (String s1 : getStrArr(strArray2[1].split(""))) {
                    if (s0.equals(s1)) {
                        n = n + 1;
                    }
                }
            }
            num = num - n;
//            int n = getInt(p);
            int combine = Combine(getInt(p), PlayWzNum);
            return num * Combine(getInt(p), PlayWzNum);
        }
        return 0;
    }

    //任选复试
    public static int RedFS_R2(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 1;
            if (strArray2.length < PlayWzNum) {
                return 0;
            }
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num *= num2;
            }

//            int n = getInt(p);
            int combine = Combine(getInt(p), PlayWzNum);
            return num * Combine(getInt(p), PlayWzNum);
        }
        return 0;
    }

    public static int getInt(String p) {
        int i = 0;

        String[] split = p.split(",");

        for (String s : split) {
            if (s.equals("1")) {
                i++;
            }
        }
        return i;
    }

    ;

    //任选单式
    public static int RedDS_R3(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
//            String[] strArray2 = balls.split(",");
//            int n = getInt(p);
//            int combine = Combine(n, PlayWzNum);
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();
//            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                String[] str2 = strArray2[i].split(" ");
                if (str2[0].length() == PlayWzNum) {
                    if (str2[0].equals("00") || str2[0].equals("11") || str2[0].equals("22") || str2[0].equals("33") || str2[0].equals("44") || str2[0].equals("55") || str2[0].equals("66") || str2[0].equals("77") || str2[0].equals("88") || str2[0].equals("99")) {

                    } else
                        list.add("");
                }
//                if (str2.length > num)
//                    return 0;
            }
            return list.size() * Combine(getInt(p), PlayWzNum);
//            return strArray2.length * 1 * Combine(n, PlayWzNum);
        }
        return 0;
    }

    //任选单式
    public static int RedDS_R(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
//            String[] strArray2 = balls.split(",");
//            int n = getInt(p);
//            int combine = Combine(n, PlayWzNum);
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();
//            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                String[] str2 = strArray2[i].split(" ");
                if (str2[0].length() == PlayWzNum) {
                    list.add("");
                }
//                if (str2.length > num)
//                    return 0;
            }
            return list.size() * Combine(getInt(p), PlayWzNum);
//            return strArray2.length * 1 * Combine(n, PlayWzNum);
        }
        return 0;
    }

    //任选单式123,234,111
    public static int RedDS_R2(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            List<String> listStr = Arrays.asList(balls.split(","));
            List newList = new ArrayList(new HashSet(listStr));
            String[] strArray2 = (String[]) newList.toArray(new String[newList.size()]);
            List<String> list = new ArrayList<>();

            for (int i = 0; i < strArray2.length; i++) {
                String[] str3 = strArray2[i].split(" ");
                String[] str2 = getStrArr(strArray2[i].split(""));
                String flag = str2[0];
                if (str2.length != PlayWzNum) {
                    return list.size() * Combine(getInt(p), PlayWzNum);
                }
                if (str2[0].equals(str2[1]) || str2[0].equals(str2[2]) || str2[1].equals(str2[2])) {
                    if (str2[0].equals(str2[1]) && str2[0].equals(str2[2])) {

                    } else {
                        if (str3[0].length() == PlayWzNum) {
                            list.add("");
                        }
                    }
                } else {
                    if (str3[0].length() == PlayWzNum) {
                        list.add("");
                    }
                }

            }
            return list.size() * Combine(getInt(p), PlayWzNum);
        }
        return 0;
    }

    //任选组六
    public static int RedZu6_R(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            int num = balls.length();
            int n = getInt(p);
            return (Pareto(num, 3) / Pareto(3, 3)) * Combine(n, PlayWzNum);
        }
        return 0;
    }

    //任选组三
    public static int RedZu3_R(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            int num = balls.length();
            int n = getInt(p);
            return Pareto(num, 2) * Combine(n, PlayWzNum);
        }
        return 0;
    }

    //任选组二
    public static int RedZu2_R(String balls, String p, int PlayWzNum) {
        if (!StringUtils.isEmpty(balls)) {
            int num = balls.length();
            int n = getInt(p);
            return num * (num - 1) / 2 * Combine(n, PlayWzNum);
        }
        return 0;
    }

    //三星直选和值
    public static int RedHE3(String balls) {
        int num = 0;
        if (!"".equals(balls)) {
//            String[] b = balls.split(",");
            String[] b = balls.split("_");
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 0) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 1) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 15;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 21;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 28;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 36;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 45;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 55;
                }
                if (Integer.parseInt(b[i]) == 10) {
                    num += 63;
                }
                if (Integer.parseInt(b[i]) == 11) {
                    num += 69;
                }
                if (Integer.parseInt(b[i]) == 12) {
                    num += 73;
                }
                if (Integer.parseInt(b[i]) == 13) {
                    num += 75;
                }
                if (Integer.parseInt(b[i]) == 14) {
                    num += 75;
                }
                if (Integer.parseInt(b[i]) == 15) {
                    num += 73;
                }
                if (Integer.parseInt(b[i]) == 16) {
                    num += 69;
                }
                if (Integer.parseInt(b[i]) == 17) {
                    num += 63;
                }
                if (Integer.parseInt(b[i]) == 18) {
                    num += 55;
                }
                if (Integer.parseInt(b[i]) == 19) {
                    num += 45;
                }
                if (Integer.parseInt(b[i]) == 20) {
                    num += 36;
                }
                if (Integer.parseInt(b[i]) == 21) {
                    num += 28;
                }
                if (Integer.parseInt(b[i]) == 22) {
                    num += 21;
                }
                if (Integer.parseInt(b[i]) == 23) {
                    num += 15;
                }
                if (Integer.parseInt(b[i]) == 24) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 25) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 26) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 27) {
                    num += 1;
                }
            }
        }
        return num;
    }

    //三星组选和值
    public static int RedZHE3(String balls) {
        int num = 0;
        if (!"".equals(balls)) {
            String[] b = balls.split("_");
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 1) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 5;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 8;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 11;
                }
                if (Integer.parseInt(b[i]) == 10) {
                    num += 13;
                }
                if (Integer.parseInt(b[i]) == 11) {
                    num += 14;
                }
                if (Integer.parseInt(b[i]) == 12) {
                    num += 14;
                }
                if (Integer.parseInt(b[i]) == 13) {
                    num += 15;
                }
                if (Integer.parseInt(b[i]) == 14) {
                    num += 15;
                }
                if (Integer.parseInt(b[i]) == 15) {
                    num += 14;
                }
                if (Integer.parseInt(b[i]) == 16) {
                    num += 14;
                }
                if (Integer.parseInt(b[i]) == 17) {
                    num += 13;
                }
                if (Integer.parseInt(b[i]) == 18) {
                    num += 11;
                }
                if (Integer.parseInt(b[i]) == 19) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 20) {
                    num += 8;
                }
                if (Integer.parseInt(b[i]) == 21) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 22) {
                    num += 5;
                }
                if (Integer.parseInt(b[i]) == 23) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 24) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 25) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 26) {
                    num += 1;
                }
            }
        }
        return num;
    }

    //三星直选跨度
    public static int Red3KD(String balls) {
        int num = 0;
        if (!StringUtils.isEmpty(balls)) {
            String[] b = getStrArr(balls.split(""));
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 0) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 1) {
                    num += 54;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 96;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 126;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 144;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 150;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 144;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 126;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 96;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 54;
                }
            }
        }
        return num;
    }


    //二星直选和值
    public static int RedHE2(String balls) {
        int num = 0;
        if (!"".equals(balls)) {
            String[] b = balls.split("_");
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 0) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 1) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 5;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 7;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 8;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 9;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 10) {
                    num += 9;
                }
                if (Integer.parseInt(b[i]) == 11) {
                    num += 8;
                }
                if (Integer.parseInt(b[i]) == 12) {
                    num += 7;
                }
                if (Integer.parseInt(b[i]) == 13) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 14) {
                    num += 5;
                }
                if (Integer.parseInt(b[i]) == 15) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 16) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 17) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 18) {
                    num += 1;
                }
            }
        }
        return num;
    }

    //二星组选和值
    public static int RedZHE2(String balls) {
        int num = 0;
        if (!"".equals(balls)) {
            String[] b = balls.split("_");
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 1) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 5;
                }
                if (Integer.parseInt(b[i]) == 10) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 11) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 12) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 13) {
                    num += 3;
                }
                if (Integer.parseInt(b[i]) == 14) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 15) {
                    num += 2;
                }
                if (Integer.parseInt(b[i]) == 16) {
                    num += 1;
                }
                if (Integer.parseInt(b[i]) == 17) {
                    num += 1;
                }
            }
        }
        return num;
    }

    //二星直选跨度
    public static int Red2KD(String balls) {
        int num = 0;
        if (!StringUtils.isEmpty(balls)) {
            String[] b = getStrArr(balls.split(""));
            for (int i = 0; i != b.length; i++) {
                if (Integer.parseInt(b[i]) == 0) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 1) {
                    num += 18;
                }
                if (Integer.parseInt(b[i]) == 2) {
                    num += 16;
                }
                if (Integer.parseInt(b[i]) == 3) {
                    num += 14;
                }
                if (Integer.parseInt(b[i]) == 4) {
                    num += 12;
                }
                if (Integer.parseInt(b[i]) == 5) {
                    num += 10;
                }
                if (Integer.parseInt(b[i]) == 6) {
                    num += 8;
                }
                if (Integer.parseInt(b[i]) == 7) {
                    num += 6;
                }
                if (Integer.parseInt(b[i]) == 8) {
                    num += 4;
                }
                if (Integer.parseInt(b[i]) == 9) {
                    num += 2;
                }
            }
        }
        return num;
    }

    //趣味区间
    public static int RedQwQj(String balls) {
        if (!StringUtils.isEmpty(balls)) {
//            balls = balls.replace(",", "");
            String[] strArray2 = balls.split(",");
            int num = 1;
            if (strArray2.length == 5) {
                num = 1;
                for (int i = 2; i < strArray2.length; i++) {
                    int num2 = strArray2[i].length();
                    num *= num2;
                }
                num = num * strArray2[0].length() / 2 * strArray2[1].length() / 2;
            }
            if (strArray2.length == 4) {
                num = 1;
                for (int i = 1; i < strArray2.length; i++) {
                    int num2 = strArray2[i].length();
                    num *= num2;
                }
                num = num * strArray2[0].length() / 2;
            }
            if (strArray2.length == 3) {
                num = 1;
                for (int i = 1; i < strArray2.length; i++) {
                    int num2 = strArray2[i].length();
                    num *= num2;
                }
                num = num * strArray2[0].length() / 2;
            }
            return num;
        }
        return 0;
    }

    //五星组合玩法
    public static int Red5ZuHe(String balls) {
        if (!StringUtils.isEmpty(balls)) {
//            balls = balls.replace(",", "");
            String[] strArray2 = balls.split(",");
            if (strArray2.length < 5)
                return 0;
            int num = 1;
            int num2 = 1;
            int num3 = 1;
            int num4 = 1;
            int num5 = 1;
            int i = 0;
            for (i = 0; i < strArray2.length; i++) {
                num *= strArray2[i].length();
            }
            for (i = 1; i < strArray2.length; i++) {
                num2 *= strArray2[i].length();
            }
            for (i = 2; i < strArray2.length; i++) {
                num3 *= strArray2[i].length();
            }
            for (i = 3; i < strArray2.length; i++) {
                num4 *= strArray2[i].length();
            }
            for (i = 4; i < strArray2.length; i++) {
                num5 *= strArray2[i].length();
            }
            return num + num2 + num3 + num4 + num5;
        }
        return 0;
    }

    //四星组合玩法
    public static int Red4ZuHe(String balls) {
        if (!StringUtils.isEmpty(balls)) {
//            balls = balls.replace(",", "");
            String[] strArray2 = balls.split(",");
            if (strArray2.length < 4)
                return 0;
            int num = 1;
            int num2 = 1;
            int num3 = 1;
            int num4 = 1;
            int i = 0;
            for (i = 0; i < strArray2.length; i++) {
                num *= strArray2[i].length();
            }
            for (i = 1; i < strArray2.length; i++) {
                num2 *= strArray2[i].length();
            }
            for (i = 2; i < strArray2.length; i++) {
                num3 *= strArray2[i].length();
            }
            for (i = 3; i < strArray2.length; i++) {
                num4 *= strArray2[i].length();
            }
            return num + num2 + num3 + num4;
        }
        return 0;
    }

    //三星组合玩法
    public static int Red3ZuHe(String balls) {
        if (!StringUtils.isEmpty(balls)) {
//            balls = balls.replace(",", "");
            String[] strArray2 = balls.split(",");
            if (strArray2.length < 3)
                return 0;
            int num = 1;
            int num2 = 1;
            int num3 = 1;
            int i = 0;
            for (i = 0; i < strArray2.length; i++) {
                num *= strArray2[i].length();
            }
            for (i = 1; i < strArray2.length; i++) {
                num2 *= strArray2[i].length();
            }
            for (i = 2; i < strArray2.length; i++) {
                num3 *= strArray2[i].length();
            }
            return num + num2 + num3;
        }
        return 0;
    }

    //11选5

    //任选复试
    public static int RedRXFS_11(String balls, int num) {
        if (!StringUtils.isEmpty(balls)) {
            String replace = balls.replace(",", "");
            if (replace.equals("")) {
                return 0;
            }
            String[] strArray2 = replace.split("_");

            return Combine(strArray2.length, num);
        }
        return 0;
    }

    //前三直选复式
    public static int Red3FS_11(String balls) {
        int num = 0;
        if (!StringUtils.isEmpty(balls)) {
            if (!balls.contains(","))
                return 0;
            String[] strArray2 = balls.split(",");
            if (strArray2.length == 3) {
                String s = strArray2[0];

                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                String[] strArray13 = strArray2[2].split("_");

                for (int i = 0; i < strArray11.length; i++) {
                    if (strArray11[i].length() > 2)
                        return 0;
                    for (int j = 0; j < strArray12.length; j++) {
                        if (strArray12[j].length() > 2)
                            return 0;
                        if (strArray11[i] != strArray12[j]) {
                            for (int k = 0; k < strArray13.length; k++) {
                                if (strArray13[k].length() > 2)
                                    return 0;
                                if (!strArray13[k].equals(strArray11[i]) && !strArray13[k].equals(strArray12[j]) && !strArray11[i].equals(strArray12[j])) {
                                    num++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }

    //前三组选复式
    public static int Red3ZFS_11(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray3 = balls.split(",");
            if (strArray3.length > 0) {
                String[] strArray2 = strArray3[0].split("_");
                for (int i = 0; i < strArray2.length; i++) {
                    if (strArray2[i].length() > 2)
                        return 0;
                }
                return Combine(strArray2.length, 3);
            } else {
                return 0;
            }


        }
        return 0;
    }

    //前二直选复式
    public static int Red2FS_11(String balls) {
        int num = 0;
        if (!StringUtils.isEmpty(balls)) {
            if (!balls.contains(","))
                return 0;
            String[] strArray2 = balls.split(",");
            if (strArray2.length == 2) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");

                for (int i = 0; i < strArray11.length; i++) {
                    if (strArray11[i].length() > 2)
                        return 0;
                    if (strArray11[i].length() < 2)
                        return 0;
                    for (int j = 0; j < strArray12.length; j++) {
                        if (strArray12[j].length() > 2)
                            return 0;
                        if (strArray12[j].length() < 2)
                            return 0;
                        if (!strArray11[i].equals(strArray12[j])) {
                            num++;
                        }
                    }
                }
            }
        }
        return num;
    }

    //前二组选复式
    public static int Red2ZFS_11(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray3 = balls.split(",");
            if (strArray3.length > 0) {
                String[] strArray2 = strArray3[0].split("_");
                for (int i = 0; i < strArray2.length; i++) {
                    if (strArray2[i].length() > 2)
                        return 0;
                }
                return strArray2.length * (strArray2.length - 1) / 2;
            } else {
                return 0;
            }


        }
        return 0;
    }

    //11定位胆
    public static int RedDD_11(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 0;
            for (int i = 0; i < strArray2.length; i++) {
                if (strArray2[i] != "") {
                    String s = "0110";
                    String[] strArr = getStrArr(strArray2[i].split("_"));
                    List<String> list = new ArrayList<>();
                    for (String ss : strArr) {
                        list.add(ss);
                    }
                    String[] strings = list.toArray(new String[list.size()]);
                    StringBuffer sb = new StringBuffer();
                    for (String ss : strings) {
                        sb.append(ss);
                    }
                    String[] strArr2 = getStrArr(sb.toString().split(""));
                    int num2 = strArr2.length;
                    num += num2;
                }
            }
            return num;
        }
        return 0;
    }

    //11定位胆
    public static int RedDD_12(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 0;
            for (int i = 0; i < strArray2.length; i++) {
                if (!strArray2[i].equals("")) {
                    String[] strArr = strArray2[i].split("_");
                    List<String> list = new ArrayList<>();
                    for (String ss : strArr) {
                        list.add(ss);
                    }
//                    String[] strings = list.toArray(new String[list.size()]);
//                    StringBuffer sb = new StringBuffer();
//                    for (String ss : strings) {
//                        sb.append(ss);
//                    }
//                    String[] strArr2 = getStrArr(sb.toString().split(""));
                    int num2 = list.size();
                    num += num2;
                }
            }
            return num;
        }
        return 0;
    }

    //组合
    public static int RedZH(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 5;
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num *= num2;
            }
            return num;
        }
        return 0;
    }

    //组120
    public static int RedZu120(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String replace = balls.replace("_", "");
            balls = replace;
            int n = balls.replace(",", "").length();
            return (Pareto(n, 5) / Pareto(5, 5));
        }
        return 0;
    }

    //组60
    public static int RedZu60(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray12.length; i++) {
                    for (int j = i; j < strArray12.length; j++) {
                        for (int k = j; k < strArray12.length; k++) {
                            if (strArray12[i] != strArray12[j] && strArray12[j] != strArray12[k] && strArray12[k] != strArray12[i])
                                danhao += strArray12[i] + "" + strArray12[j] + "" + strArray12[k] + ",";
                        }
                    }
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //组30
    public static int RedZu30(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
//                String[] strArray11 = getStrArr(strArray2[0].split(""));
//                String[] strArray12 = getStrArr(strArray2[1].split(""));
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray11.length; i++) {
                    for (int j = i; j < strArray11.length; j++) {
                        if (strArray11[i] != strArray11[j])
                            danhao += strArray11[i] + "" + strArray11[j] + ",";
                    }
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray12.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray12[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //组20
    public static int RedZu20(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            //345,456
            String[] strArray2 = balls.split(",");

            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
//                String[] strArray11 = getStrArr(strArray2[0].split(""));
//                String[] strArray12 = getStrArr(strArray2[1].split(""));
                int num = 0;
                String danhao = "";

                for (int i = 0; i < strArray12.length; i++) {
                    for (int j = i; j < strArray12.length; j++) {
                        if (strArray12[i] != strArray12[j])
                            danhao += strArray12[i] + "" + strArray12[j] + ",";
                    }
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    public static String[] getStrArr(String[] arr) {
        List<String> list22 = Arrays.asList(arr).subList(1, Arrays.asList(arr).size());
        return list22.toArray(new String[list22.size()]);
    }

    //组10
    public static int RedZu10(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray12.length; i++) {
                    danhao += strArray12[i] + ",";
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);

                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //组5
    public static int RedZu5(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray12.length; i++) {
                    danhao += strArray12[i] + ",";
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //特殊
    public static int RedTS(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            int num = 0;
            for (int i = 0; i < strArray2.length; i++) {
                int num2 = strArray2[i].length();
                num += num2;
            }
            return num;
        }
        return 0;
    }

    //组24
    public static int RedZu24(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String replace = balls.replace("_", "");
            balls = replace;
            int n = balls.replace(",", "").length();
            return (Pareto(n, 4) / Pareto(4, 4));
        }
        return 0;
    }

    //组12
    public static int RedZu12(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray12.length; i++) {
                    for (int j = i; j < strArray12.length; j++) {
                        if (strArray12[i] != strArray12[j])
                            danhao += strArray12[i] + "" + strArray12[j] + ",";
                    }
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //组6
    public static int RedZu61(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String replace = balls.replace("_", "");
            balls = replace;
            int n = balls.replace(",", "").length();
            return (Pareto(n, 2) / Pareto(2, 2));
        }
        return 0;
    }

    //组4
    public static int RedZu4(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            if (strArray2.length > 1) {
                String[] strArray11 = strArray2[0].split("_");
                String[] strArray12 = strArray2[1].split("_");
                int num = 0;
                String danhao = "";
                for (int i = 0; i < strArray12.length; i++) {
                    danhao += strArray12[i] + ",";
                }
                if (danhao.equals(""))
                    return 0;
                danhao = danhao.substring(0, danhao.length() - 1);
                String[] strArray = danhao.split(",");

                for (int i = 0; i < strArray.length; i++) {
                    for (int j = 0; j < strArray11.length; j++) {
                        String str1 = strArray[i];
                        if (str1.indexOf(strArray11[j]) == -1) {
                            num++;
                        }
                    }
                }
                return num;
            } else {
                return 0;
            }
        }
        return 0;
    }

    //PK10猜冠军
    public static int PK10FS_One(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArr = getStrArr(balls.split(""));
            String[] strArray2 = balls.split("_");
            return strArray2.length;
        }
        return 0;
    }

    //PK10大小单双
    public static int PK10DXDS(String balls) {
        if (!StringUtils.isEmpty(balls)) {
            String[] strArray2 = balls.split(",");
            for (int i = 0; i < strArray2.length; i++) {
                if (strArray2[i].trim().length() > 2)
                    return 0;
            }

            int num = 1;
            int num2 = strArray2.length;
            num *= num2;
            return num;
        }
        return 0;
    }

}
