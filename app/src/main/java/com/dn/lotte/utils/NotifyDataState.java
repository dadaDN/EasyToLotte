package com.dn.lotte.utils;

import com.dn.lotte.bean.PurchaseDetailGameBean;

import java.util.List;

/**
 * Created by Administrator on 2018/1/25 0025.
 */

public class NotifyDataState {

    /**
     * 改变游戏玩法里面选中的玩法 保证数据统一性
     *
     * @param moreGameList 游戏玩法所有数据
     * @param selectList   选中的玩法
     */
    public static void updataMoreGameChecked(List<PurchaseDetailGameBean.TableBean> moreGameList, List<PurchaseDetailGameBean.TableBean> selectList) {
        for (PurchaseDetailGameBean.TableBean tableBean : moreGameList) {
            for (PurchaseDetailGameBean.TableBean bean : selectList) {
                if (bean.getTitlename().equals(tableBean.getTitlename())) {
                    tableBean.setChecked(true);
                }
            }
        }
    }
//
//    /**
//     * 改变游戏玩法里面选中的玩法 保证数据统一性
//     *
//     * @param moreGame             游戏玩法选中或取消选中的数据
//     * @param selectList           选中的玩法
//     * @param isCheckedOrUnChecked 选中true 还是取消
//     */
//    public static void updataMoreGameChecked(PurchaseDetailGameBean.TableBean moreGame, List<PurchaseDetailGameBean.TableBean> selectList, boolean isCheckedOrUnChecked) {
//        List<PurchaseDetailGameBean.TableBean> mSelectList = CommonUtils.getSelectGameList();
//        ;
//
//        for (PurchaseDetailGameBean.TableBean bean : selectList) {
//            if (!isCheckedOrUnChecked) {
//                //取消选中 如果列表里有 移出
//                if (bean.getTitle().equals(moreGame.getTitle())) {
//                    mSelectList.remove(bean);
//                }
//            } else {
//                //选中 如果列表里没有
//                if (!bean.getTitle().equals(moreGame.getTitle())) {
//                    mSelectList.add(moreGame);
//                }
//            }
//        }
//        CommonUtils.setSelectGameList(mSelectList);
//    }
}
